@extends('landing.layout.default')
@section('content')
<section>
    <div class="about-us-bg-color">
        <div class="container">
            <div class="about-us-main">
                <h5 class="about-us-title pt-3 pb-2 text-center">{{ trans('landing.terms_conditions') }}</h5>
                @if (!empty($terms->terms_content))
                    <div class="cms-section my-3">

                        {!! $terms->terms_content !!}

                    </div>
                @else
                    @include('admin.layout.no_data')
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
