@extends('landing.layout.default')
@section('content')
    <section>
        <div class="contact-bg-color shadow py-4">
            <div class="container contact-container">
                <div class="contact-main">
                    <div class="main-text-title-2 col-md-7 w-100 text-center m-auto" >
                        <div class="contact-title pb-1">{{ trans('landing.contact_section_title') }}
                        </div>
                        <p class="contact-subtitle w-100 pt-4 text-muted">{{ trans('landing.contact_section_description') }}</p>
                    </div>
                    <div class="contact-form col-md-5 w-100 m-auto">
                        <div class="row mt-4 mb-5">

                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 my-1">
                
                                <div class="card border-0 shadow rounded p-3 h-100">
                
                                    <h5><span class="text-primary-color {{ session()->get('direction') == 2 ? 'ms-2' : 'me-2' }}"><i
                
                                                class="fa-solid fa-envelope fs text-primary-color"></i></span>{{ trans('landing.email') }}</h5>
                
                                    <p class="mb-0"><a href="mailto:"
                
                                            class="text-dark"> {{helper::appdata('')->email}}</a></p>
                
                                </div>
                
                            </div>
                
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 my-1">
                
                                <div class="card border-0 shadow rounded p-3 h-100 ">
                
                                    <h5><span class="text-primary-color  {{ session()->get('direction') == 2 ? 'ms-2' : 'me-2' }}"> <i
                
                                                class="fa-solid fa-phone text-primary-color"></i></span>{{ trans('landing.mobile') }}</h5>
                
                                    <p class="mb-0"><a href="tel:"
                
                                            class="text-dark">+{{helper::appdata('')->contact}}</a></p>
                
                                </div>
                
                            </div>
                
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 my-1">
                
                                <div class="card border-0 shadow rounded p-3 h-100">
                
                                    <h5><span class="text-primary-color {{ session()->get('direction') == 2 ? 'ms-2' : 'me-2' }}"> <i
                
                                                class="fa-solid fa-location-dot text-primary-color"></i></span>{{ trans('landing.address') }}</h5>
                
                                    <p class="mb-0 fs-7">
                                        {{helper::appdata('')->address}}
                                    </p>
                
                                </div>
                
                            </div>
                        </div>
                        <form class="row g-3 shadow-lg bg-white rounded-3 px-4 py-4" action="{{ URL::To('/inquiry') }}"  method="post">
                            @csrf
                            <h5 class="contact-form-title text-center">
                            {{ trans('landing.contact_us') }}
                            </h5>
                            <p class="contact-form-subtitle text-center text-muted">{{ trans('landing.contact_section_description') }}</p>
                            <div class="col-md-6">
                                <label for="name"
                                    class="form-label contact-form-label">{{ trans('landing.name') }}</label>
                                <input type="text" class="form-control contact-input" name="name"
                                    placeholder="{{ trans('landing.name') }}" required>
                            </div>
                            <div class="col-md-6">
                                <label for="email"
                                class="form-label contact-form-label">{{ trans('landing.email') }}</label>
                            <input type="email" class="form-control contact-input" name="email"
                                placeholder="{{ trans('landing.email') }}" required>
                            </div>
                            <div class="col-12">
                                <label for="inputAddress"
                                    class="form-label contact-form-label">{{ trans('landing.mobile') }}</label>
                                <input type="number" class="form-control contact-input" name="mobile"
                                    placeholder="{{ trans('landing.mobile') }}" required>
                            </div>
                            <div class="mb-3">
                                <label for="message"
                                    class="form-label contact-form-label">{{ trans('landing.message') }}</label>
                                <textarea class="form-control contact-input" rows="3" name="message" placeholder="{{ trans('landing.message') }}" required></textarea>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary btn-class py-2 rounded-2 text-center m-auto d-block">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
