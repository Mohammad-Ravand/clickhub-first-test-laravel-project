@extends('landing.layout.default')

@section('content')

<div class="container blog-container">
    <div class="blog-card my-3">
        <h5 class="latest-blog pb-3">{{ trans('landing.blog_section_title') }}</h5>
        <p class="blog-lorem-text col-md-12 pt-1 pb-5 text-muted">
            {{ trans('landing.blog_section_description') }}
        </p>
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-3">
            @include('landing.blogcommonview')
        </div>
        <div class="d-flex justify-content-center mt-3">

            {!! $blogs->links() !!}

        </div>

    </div>
</div>

@endsection
