@extends('landing.layout.default')

@section('content')
<!-- banner-section start -->
<section class="bg-color-banner" id="home">
    <div class="container header-container">
        <div class="banner">
            <div class="banner-page d-flex justify-content-between align-items-center vh-100">
                <div class="banner-text">
                    <h1>{{ trans('landing.hero_banner_title') }}</h1>
                    <p class="pt-4 pt-md-3">{{ trans('landing.hero_banner_description') }}</p>

                    <a href="{{ URL::to('/admin') }}" class="btn btn-primary py-2 mt-5 rounded-2" target="_blank">
                        {{ trans('landing.get_started') }}
                    </a>
                </div>
                <img src="{{ url(env('ASSETPATHURL') . 'landing/images/png/header_img.png') }}" alt="" class="banner-images img-fluid">
            </div>
        </div>
        <a class="{{ session()->get('direction') == 2 ? 'button-top-arrow-up-rtl' : 'button-top-arrow-up' }}">
            <i class="fa-solid fa-arrow-up"></i>
        </a>
    </div>
</section>
<!-- banner-section End -->
<section class="project-management py-5 overflow-hidden">
    <div class="container position-relative container-project-management">
        <h5 class="beautiful-ui-kit-title col-md-12">
            {{ trans('landing.how_it_work') }}
        </h5>
        <p class="subtitle col-md-12 sub-title-mein text-muted">
            {{ trans('landing.how_it_work_description') }}
        </p>
        <!-- Create Your Account start -->
        <div class="management-main row justify-content-between align-items-center">
            <div class="project-management-text col-md-6 order-2 order-lg-0">
                <div data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000" direction="false">
                    <div>
                        <h5 class="work-title position-relative col-md-8">
                            {{ trans('landing.how_it_work_step_one') }}
                        </h5>
                    </div>
                    <p class="mt-4 sub-title-mein">
                        {{ trans('landing.how_it_work_step_one_description') }}
                    </p>
                    <a href="{{ URL::to('/admin') }}" class="btn btn-primary btn-class py-2 mt-5 rounded-2" target="_blank">
                        {{ trans('landing.get_started') }}
                    </a>
                </div>
            </div>
            <div class="col-md-6 management-image-2 aos-init overflow-hidden">
                <div data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000">
                    <img src="{{ url(env('ASSETPATHURL') . 'landing/images/png/png2.png') }}" alt="" class="img-fluid project-management-image">
                </div>
            </div>
        </div>
        <!-- Create Your Account end -->
        <!-- Work-together start -->
        <div class="work-together row align-items-center justify-content-between">
            <div class="col-lg-5 together-img overflow-hidden">
                <div data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000">
                    <img src="{{ url(env('ASSETPATHURL') . 'landing/images/png/Work Together Image.png') }}" alt="" class="img-fluidn object-fit-cover w-100">
                </div>
            </div>
            <div class="col-lg-6 overflow-hidden">
                <div data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000">
                    <div class="work-together-content">
                        <div class="project-management-text">
                            <div>
                                <h5 class="Work-together-title position-relative {{ session()->get('direction') == 2 ? 'text-start' : 'text-end' }}">
                                    {{ trans('landing.how_it_work_step_two') }}
                                </h5>
                            </div>
                            <p class="with-notes mt-4 col-md-12 m-auto sub-title-mein {{ session()->get('direction') == 2 ? 'text-start' : 'text-end' }}">
                                {{ trans('landing.how_it_work_step_two_description') }}
                            </p>
                            <a href="{{ URL::to('/admin') }}" class="btn work-step-two btn-primary btn-class py-2 mt-5 rounded-2  {{ session()->get('direction') == 2 ? 'float-start' : 'float-end' }} " target="_blank">
                                {{ trans('landing.get_started') }}
                            </a>
                            <img src="{{ url(env('ASSETPATHURL') . 'landing/images/png/Work Together Image.png') }}" alt="" class="img-fluidn object-fit-cover d-none w-100">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Work-together End -->
        <!-- Create Your Account start -->
        <div class="management-main row justify-content-between align-items-center">
            <div class="project-management-text col-md-6 order-2 order-lg-0">
                <div data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000" direction="false">
                    <div>
                        <h5 class="work-title position-relative col-md-8">
                            {{ trans('landing.how_it_work_step_three') }}
                        </h5>
                    </div>
                    <p class="mt-4 sub-title-mein">
                        {{ trans('landing.how_it_work_step_three_description') }}
                    </p>
                    <a href="{{ URL::to('/admin') }}" class="btn btn-primary btn-class py-2 mt-5 rounded-2" target="_blank">
                        {{ trans('landing.get_started') }}
                    </a>
                </div>
            </div>
            <div class="col-md-6 management-image-2 aos-init overflow-hidden">
                <div data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000">
                    <img src="{{ url(env('ASSETPATHURL') . 'landing/images/png/account.png') }}" alt="" class="img-fluid project-management-image">
                </div>
            </div>
        </div>
        <!-- Create Your Account end -->
    </div>
</section>
<!-- features -->
<section id="features">
    <div class="beautiful-ui-kit-bg-color">
        <div class="container beautiful-ui-kit-container">
            <h5 class="beautiful-ui-kit-title col-md-12">
                {{ trans('landing.premium_features') }}
            </h5>
            <p class="subtitle col-md-12 sub-title-mein text-muted">
                {{ trans('landing.premium_features_description') }}
            </p>
            <div class="row  row-cols-1 row-cols-xxl-3 row-cols-xl-3 row-cols-lg-3 row-cols-md-2 mt-3 g-4">
                @php
                $strings = ['card-bg-color-1', 'card-bg-color-2', 'card-bg-color-3', 'card-bg-color-4', 'card-bg-color-5','card-bg-color-6'];
                $count = count($strings);
                @endphp
                @if ($features->count() > 0)
                @foreach ($features as $key => $feature)
                <div class="col " data-aos="zoom-in" data-aos-delay="100" data-aos-duration="1000">
                    <div class="card beautiful-card border-0 text-center m-auto h-100">
                        <div class="card-body beautiful-card-body {{ $strings[$key % $count] }} rounded-3">
                            <img src="{{ helper::image_path($feature->image) }}" class="rounded-3" alt="">
                            <h5 class="card-title my-3">{{ $feature->title }}</h5>
                            <p class="card-text mb-4 text-muted">
                                {{ Str::limit($feature->description) }}
                            </p>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
<!-- features -->
@if (App\Models\SystemAddons::where('unique_identifier', 'template')->first() != null &&
App\Models\SystemAddons::where('unique_identifier', 'template')->first()->activated == 1)
<!-- our-template -->
<section id="our-template">
    <div class="container clients-container">
        <div class="clients overflow-hidden">
            <div class="what-our-clients-says-main">
                <h5 class="what-our-clients-says-title">
                    {{ trans('landing.awesome_templates') }}
                </h5>
                <p class="how-works-subtitle text-center text-muted col-md-12 mx-auto sub-title-mein text-muted">
                    {{ trans('landing.awesome_templates_description') }}
                </p>
            </div>
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-3 row-cols-xxl-3 g-4 mt-3 over">
                <div class="col" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
                    <div class="card h-100 them-card-box overflow-hidden">
                        <img src="{{  url(env('ASSETPATHURL') . 'landing/images/theme/theme_1.png') }}" class="card-img-top them-name-images">
                        <div class="card-body">
                            <h5 class="card-title text-center">{{ trans('landing.theme_1') }}</h5>
                        </div>
                    </div>
                </div>
                <div class="col" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">
                    <div class="card h-100 them-card-box overflow-hidden">
                        <img src="{{  url(env('ASSETPATHURL') . 'landing/images/theme/theme_2.png') }}" class="card-img-top them-name-images">
                        <div class="card-body">
                            <h5 class="card-title text-center">{{ trans('landing.theme_2') }}</h5>
                        </div>
                    </div>
                </div>
                <div class="col" data-aos="fade-up" data-aos-delay="300" data-aos-duration="1000">
                    <div class="card h-100 them-card-box overflow-hidden">
                        <img src="{{  url(env('ASSETPATHURL') . 'landing/images/theme/theme_3.png') }}" class="card-img-top them-name-images">
                        <div class="card-body">
                            <h5 class="card-title text-center">{{ trans('landing.theme_3') }}</h5>
                        </div>
                    </div>
                </div>
                <div class="col" data-aos="fade-up" data-aos-delay="400" data-aos-duration="1000">
                    <div class="card h-100 them-card-box overflow-hidden">
                        <img src="{{  url(env('ASSETPATHURL') . 'landing/images/theme/theme_4.png') }}" class="card-img-top them-name-images">
                        <div class="card-body">
                            <h5 class="card-title text-center">{{ trans('landing.theme_4') }}</h5>
                        </div>
                    </div>
                </div>
                <div class="col" data-aos="fade-up" data-aos-delay="500" data-aos-duration="1000">
                    <div class="card h-100 them-card-box overflow-hidden">
                        <img src="{{  url(env('ASSETPATHURL') . 'landing/images/theme/theme_5.png') }}" class="card-img-top them-name-images">
                        <div class="card-body">
                            <h5 class="card-title text-center">{{ trans('landing.theme_5') }}</h5>
                        </div>
                    </div>
                </div>
                <div class="col" data-aos="fade-up" data-aos-delay="600" data-aos-duration="1000">
                    <div class="card h-100 them-card-box overflow-hidden">
                        <img src="{{  url(env('ASSETPATHURL') . 'landing/images/theme/comingsoon.png') }}" class="card-img-top them-name-images">
                        <div class="card-body">
                            <h5 class="card-title text-center">{{ trans('landing.coming_soon_more') }}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif

<!-- free_section -->
<section class="you-work-everywhere-you-are-bg-color">
    <div class="container">
        <div class="mb-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
            <h5 class="you-work-everywhere-you-are  position-relative text-center choose-plan-title">
                {{ trans('landing.free_section') }}
            </h5>
            <P class="choose-plan text-center pt-4 sub-title-mein">
                {{ trans('landing.free_section_description') }}
            </P>
            <div class="text-center">
                <a href="{{ URL::to('/admin') }}" class="btn btn-primary py-2 mt-5 rounded-2" target="_blank">
                    {{ trans('landing.get_started') }}
                </a>
            </div>
        </div>
    </div>
</section>
<!-- free_section -->

<!-- our-stores -->
<section id="our-stores">
    <div class="card-section-bg-color py-5">
        <div class="container card-section-container">
            <div>
                <h5 class="hotel-main-title">
                    {{ trans('landing.our_stores') }}
                </h5>
                <p class="hotel-main-subtitle com-md-12 sub-title-mein px-1 text-muted ">
                    {{ trans('landing.our_stores_description') }}
                </p>
            </div>
            <div class="row row-cols-1 mt-2 row-cols-sm-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-3 row-cols-xll-3 g-4">
                @include('landing.storelist')
            </div>
            <div class="d-flex justify-content-center mt-5 view-all-btn">
                <a href="{{ URL::to('/stores') }}" class="btn-secondary">{{ trans('landing.view_all') }}
                </a>
            </div>
        </div>
    </div>
</section>
<!-- our-stores -->

<!-- Mobile app -->
@if (App\Models\SystemAddons::where('unique_identifier', 'vendor_app')->first() != null &&
App\Models\SystemAddons::where('unique_identifier', 'vendor_app')->first()->activated == 1)
<section class="use-as-extension">
    <div class="container work-together-container">
        <div class="work-together row flex-row-reverse align-items-center justify-content-between">
            <div class="col-lg-5 overflow-hidden">
                <div data-aos="flip-left" data-aos-delay="100" data-aos-duration="1000">
                    <img src="{{ url(env('ASSETPATHURL') . 'landing/images/png/app.png') }}" alt="" class="img-fluidn object-fit-cover w-100">
                </div>
            </div>
            <div class="col-lg-6 overflow-hidden">
                <div data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000">
                    <div class="work-together-content">
                        <div class="extension-text">
                            <div>
                                <h5 class="Work-together-title position-relative use-extension">
                                    {{ trans('landing.mobile_app_section') }}
                                </h5>
                            </div>
                            <p class="with-notes mt-4 use-as-extension-text sub-title-mein text-muted">
                                {{ trans('landing.mobile_app_section_description') }}
                            </p>
                            <div class="d-flex mt-5 store-img-box">
                                <a href="https://drive.google.com/file/d/1PmphT3wVZj1mwY-y00PTPxaIj05VAUQe/view?usp=share_link" target="_blank">
                                    <img src="{{ url(env('ASSETPATHURL') . '/landing/images/playstore.png') }}" class="store-img" alt="">
                                </a>
                                <a href="https://drive.google.com/file/d/1PmphT3wVZj1mwY-y00PTPxaIj05VAUQe/view?usp=share_link" class="mx-3" target="_blank">
                                    <img src="{{ url(env('ASSETPATHURL') . '/landing/images/appstore.png') }}" class="store-img" alt="">
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endif
<!-- Mobile app -->

<!-- pricing-plans -->
<section id="pricing-plans">
    <div class="container choose-your-plan-container">
        <h5 class="Work-together-title position-relative text-center choose-plan-title">
            {{ trans('landing.pricing_plan_title') }}
        </h5>
        <P class="choose-plan text-center mt-3 sub-title-mein col-md-12 text-muted">
            {{ trans('landing.pricing_plan_description') }}
        </P>
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-3 g-4 choose-card">
            @foreach ($planlist as $plan)
            <div class="col d-flex plan-card">
                <div class="card plan-card-border h-100 w-100">
                    <div class="card-body  plan-card-body">
                        <p class="plan-card-frist-titel">{{ $plan->name }}</p>
                        <div class="d-flex align-items-center">
                            <h5 class="card-title plan-card-second-titel py-3">{{ helper::currency_formate($plan->price, '') }} /</h5>&nbsp;
                            <p class="plan-card-frist-titel">
                                @if ($plan->plan_type == 1)
                                @if ($plan->duration == 1)
                                {{ trans('labels.one_month') }}
                                @elseif($plan->duration == 2)
                                {{ trans('labels.three_month') }}
                                @elseif($plan->duration == 3)
                                {{ trans('labels.six_month') }}
                                @elseif($plan->duration == 4)
                                {{ trans('labels.one_year') }}
                                @elseif($plan->duration == 5)
                                {{ trans('labels.lifetime') }}
                                @endif
                                @endif
                                @if ($plan->plan_type == 2)
                                {{ $plan->days }}
                                {{ $plan->days > 1 ? trans('labels.days') : trans('labels.day') }}
                                @endif
                            </p>
                        </div>
                        <p class="card-text plan-card-text ">
                        <p class="capture pb-3">{{ $plan->description }}</p>
                        <div class="d-flex iconimg align-items-center py-2">
                            <i class="fa-regular fa-circle-check fs-5"></i>
                            <p class="px-3">
                                {{ $plan->order_limit == -1 ? trans('labels.unlimited') : $plan->order_limit }}
                                {{ $plan->order_limit > 1 || $plan->order_limit == -1 ? trans('labels.products') : trans('labels.products') }}
                            </p>
                        </div>
                        <div class="d-flex iconimg align-items-center py-2">
                            <i class="fa-regular fa-circle-check fs-5"></i>
                            <p class="px-3">
                                {{ $plan->appointment_limit == -1 ? trans('labels.unlimited') : $plan->appointment_limit }}
                                {{ $plan->appointment_limit > 1 || $plan->appointment_limit == -1 ? trans('labels.orders') : trans('labels.orders') }}
                            </p>
                        </div>
                        <div class="d-flex iconimg align-items-center py-2">
                            <i class="fa-regular fa-circle-check fs-5"></i>
                            @php
                            $themes = [];
                            if ($plan->themes_id != '' && $plan->themes_id != null) {
                            $themes = explode(',', $plan->themes_id);
                            } @endphp
                            <p class="px-3">{{ count($themes) }}
                                {{ count($themes) > 1 ? trans('labels.themes') : trans('labels.theme') }}
                            </p>
                        </div>
                        @if ($plan->custom_domain == 1)
                        <div class="d-flex iconimg align-items-center py-2">
                            <i class="fa-regular fa-circle-check fs-5"></i>
                            <p class="px-3">{{ trans('labels.custome_domain_available') }}</p>
                        </div>
                        @endif
                        @if ($plan->vendor_app == 1)
                        <div class="d-flex iconimg align-items-center py-2">
                            <i class="fa-regular fa-circle-check fs-5"></i>
                            <p class="px-3">{{ trans('labels.vendor_app_available') }}</p>
                        </div>
                        @endif
                        @if ($plan->google_analytics == 1)
                        <div class="d-flex iconimg align-items-center py-2">
                            <i class="fa-regular fa-circle-check fs-5"></i>
                            <p class="px-3">{{ trans('labels.google_analytics_available') }}</p>
                        </div>
                        @endif
                        @php $features = explode('|', $plan->features); @endphp
                        @foreach ($features as $feature)
                        <div class="d-flex iconimg align-items-center py-2">
                            <i class="fa-regular fa-circle-check fs-5"></i>
                            <p class="px-3 w-100">{{ $feature }}</p>
                        </div>
                        @endforeach
                    </div>
                    <div class="card-footer bg-white border-top-0">
                        <a href="{{ URL::to('/admin') }}" class="btn btn-primary py-2 rounded-2" target="_blank">
                            {{ trans('landing.get_started') }}
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!-- pricing-plans -->

<!-- Funfact start -->
<section class="fun-fact">
    <div class="container js-ranig-number">
        <div class="row js-nnum row-cols-md-2 row-cols-lg-4 row-cols-xl-4" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
            <div class="col-3 pt-5 pb-5 js-main-card">
                <div class="js-number text-center">
                    <i class="fa-solid fa-home fs-1"></i>
                    <div class="num" data-val="65"></div>
                    <div class="js-text-number">
                        <p>
                            {{ trans('landing.fun_fact_one') }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-3 pt-5 pb-5 js-main-card">
                <div class="js-number text-center">
                    <i class="fa-solid fa-calendar fs-1"></i>
                    <div class="num" data-val="10"></div>
                    <div class="js-text-number">
                        <p>
                            {{ trans('landing.fun_fact_two') }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-3 pt-5 pb-5 js-main-card">
                <div class="js-number text-center">
                    <i class="fa-solid fa-users fs-1"></i>
                    <div class="num" data-val="275"></div>
                    <div class="js-text-number">
                        <p>
                            {{ trans('landing.fun_fact_three') }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-3 pt-5 pb-5 js-main-card">
                <div class="js-number text-center">
                    <i class="fa-solid fa-th fs-1"></i>
                    <div class="num" data-val="600"></div>
                    <div class="js-text-number">
                        <p>
                            {{ trans('landing.fun_fact_four') }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Funfact End -->
<!-- What Our Clients Says -->
<div class="client-slaider-bg-color">
    <div class="container client-slaider-container">
        <div class="row align-items-center justify-content-between m-0">
            <div class="col-lg-5 overflow-hidden">
                <div data-aos="flip-left" data-aos-delay="100" data-aos-duration="1000">
                    <img src="{{ url(env('ASSETPATHURL') . 'landing/images/png/client.png') }}" alt="" class="img-fluidn object-fit-cover w-100">
                </div>
            </div>
            <div class="col-lg-6 overflow-hidden">
                <div data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000">
                    <div class="carousel-client-slaider owl-stage-outer">
                        <h5>{{ trans('landing.client_says') }}</h5>
                        <p class="text-muted">{{ trans('landing.client_says_description') }}</p>
                        <div class="owl-carousel client-slai owl-theme h-100 mt-4">
                            @foreach ($testimonials as $testimonial)
                            <div class="item client-items w-100">
                                <div class="card-body">
                                    <div class="card border-0 rounded-3 d-flex justify-content-center m-auto px-4 pt-4">
                                        <div class="d-flex align-items-center rattings-tar">
                                            @php
                                            $count = (int) $testimonial->star;
                                            @endphp
                                            @for ($i = 0; $i < $count; $i++) <img src="{{ url(env('ASSETPATHURL') . 'landing/images/png/star.png') }}" class="card-img-top star-img px-1 w-100 h-100" alt="...">
                                                @endfor
                                        </div>
                                        <p class="card-text client-sutext my-3">
                                            “{{ $testimonial->description }}”
                                        </p>
                                        <div class="d-flex align-items-center justify-content-between mt-3">
                                            <div class="d-flex align-items-center pb-4">
                                                <img src="{{ helper::image_path($testimonial->image) }}" alt="" class="invisible-img w-25 h-25 rounded-circle">
                                                <div class="invisible-img-text px-3">
                                                    <p class="annette-text m-0 p-0 text-start">{{ $testimonial->name }}</p>
                                                    <p class="google-ceo m-0 p-0 text-start">{{ $testimonial->position }}</p>
                                                </div>
                                            </div>
                                            <img src="{{ url(env('ASSETPATHURL') . 'landing/images/png/Quote.png') }}" alt="" class="quote-png">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- What Our Clients Says End -->

<!-- Blogs start -->
@if (App\Models\SystemAddons::where('unique_identifier', 'blog')->first() != null &&
App\Models\SystemAddons::where('unique_identifier', 'blog')->first()->activated == 1)
<section id="blogs">
    <div class="blogs-bg-color">
        <div class="container blog-container">
            <div class="blog-card my-5">
                <h5 class="latest-blog">{{ trans('landing.blog_section_title') }}
                </h5>
                <p class="blog-lorem-text col-md-12 pt-4 pb-5 sub-title-mein text-muted">
                    {{ trans('landing.blog_section_description') }}
                </p>
                <div class="owl-carousel blogs-slaider owl-theme pb-5">
                    @include('landing.blogcommonview')
                </div>
                <div class="d-flex justify-content-center view-all-btn">
                    <a href="{{ URL::to('/blogs') }}" class="btn-secondary">{{ trans('landing.view_all') }}</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<!-- Blogs End -->


<!-- subscription -->
<section>
    <div class="have-project-section-bg-color">
        <div class="container have-project-section-container">
            <div class="have-project-contain row align-items-center justify-content-between">
                <div class="col-lg-5 overflow-hidden">
                    <div data-aos="flip-right" data-aos-delay="100" data-aos-duration="1000">
                        <img src="{{ url(env('ASSETPATHURL') . 'landing/images/png/subscriptions.png') }}" alt="" class="img-fluidn object-fit-cover w-100">
                    </div>
                </div>
                <div class="col-lg-6 overflow-hidden">
                    <div>
                        <div class="" data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000">
                            <div>
                                <h6 class="have-project-title">
                                    {{ trans('landing.subscribe_section_title') }}
                                </h6>
                            </div>
                            <p class="have-project-subtitle mt-4 col-md-11 sub-title-mein">
                                {{ trans('landing.subscribe_section_description') }}
                            </p>
                        </div>
                        <form action="{{ URL::to('/emailsubscribe') }}" method="post">
                            @csrf
                            <div class="mt-4 form-control d-flex border-0 input-btn" data-aos="flip-left" data-aos-delay="100" data-aos-duration="1000">
                                <input type="email" class="form-control border-0" name="email" placeholder="{{ trans('labels.email') }}" required>
                                <button type="submit" class="btn btn-primary btn-class">{{ trans('landing.subscribe') }}</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- contact  start -->
<section id="contact-us">
    <div class="contact-bg-color">
        <div class="container contact-container">
            <div class="contact-main d-flex align-items-center">
                <div class="overflow-hidden">
                    <div class="main-text-title" data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000">
                        <div class="contact-title col-md-10 pb-4">
                            {{ trans('landing.contact_section_title') }}
                        </div>
                        <p class="contact-subtitle col-md-12 col-lg-10 pb-4 pb-4 sub-title-mein text-muted">{{ trans('landing.contact_section_description') }}</p>
                        <div class="pb-4 contact-email-box">
                            <div class="email-icon-text-box">
                                <p class="email-text p-0 m-0">{{ trans('landing.email_us') }}</p>
                                <p class="infogolio-email p-0 m-0"><a href="mailto:{{ helper::appdata('')->email }}">{{ helper::appdata('')->email }}</a>
                                </p>
                            </div>
                            <div class="email-icon-text-box mt-2">
                                <p class="email-text p-0 m-0">{{ trans('landing.call_us') }}</p>
                                <p class="infogolio-email p-0 m-0"><a href="tel:{{ helper::appdata('')->contact }}">{{ helper::appdata('')->contact }}</a>
                                </p>
                            </div>
                        </div>

                        <div class="email-icon-text-box-2">
                            <div class="email-md-center">
                                <div class="contact-line-with d-flex align-items-center pb-2">
                                    <div class="contact-line d-none d-lg-block d-xl-block">
                                    </div>
                                    <p class="connect-with-us px-3">
                                        {{ trans('landing.connect_with_us') }} :
                                    </p>
                                </div>
                                <div class="contact-icon d-flex">
                                    @if (helper::appdata('')->facebook_link != null)
                                    <a href="{{ helper::appdata('')->facebook_link }}" target="_blank" class="btn btn-primary btn-class facebook-icon"><i class="fa-brands fa-facebook-f"></i></a>
                                    @endif
                                    @if (helper::appdata('')->instagram_link != null)
                                    <a href="{{ helper::appdata('')->instagram_link }}" target="_blank" class="btn btn-primary btn-class"><i class="fa-brands fa-instagram"></i></a>
                                    @endif
                                    @if (helper::appdata('')->twitter_link != null)
                                    <a href="{{ helper::appdata('')->twitter_link }}" target="_blank" class="btn btn-primary btn-class"><i class="fa-brands fa-twitter"></i></a>
                                    @endif
                                    @if (helper::appdata('')->linkedin_link != null)
                                    <a href="{{ helper::appdata('')->linkedin_link }}" target="_blank" class="btn btn-primary btn-class"><i class="fa-brands fa-linkedin-in"></i></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contact-form col-md-5" data-aos="flip-left" data-aos-delay="100" data-aos-duration="1000">
                    <form class="row g-3 shadow-lg bg-white rounded-3 px-4  py-4" action="{{ URL::To('/inquiry') }}" method="post">
                        @csrf
                        <h5 class="contact-form-title text-center">
                            {{ trans('landing.contact_us') }}
                        </h5>
                        <p class="contact-form-subtitle text-center text-muted">{{ trans('landing.contact_section_description_two') }}</p>
                        <div class="col-md-6">
                            <label for="name" class="form-label contact-form-label">{{ trans('landing.name') }}</label>
                            <input type="text" class="form-control contact-input" name="name" placeholder="{{ trans('landing.name') }}" required>
                        </div>
                        <div class="col-md-6">
                            <label for="email" class="form-label contact-form-label">{{ trans('landing.email') }}</label>
                            <input type="email" class="form-control contact-input" name="email" placeholder="{{ trans('landing.email') }}" required>
                        </div>
                        <div class="col-12">
                            <label for="inputAddress" class="form-label contact-form-label">{{ trans('landing.mobile') }}</label>
                            <input type="number" class="form-control contact-input" name="mobile" placeholder="{{ trans('landing.mobile') }}" required>
                        </div>
                        <div class="mb-3">
                            <label for="message" class="form-label contact-form-label">{{ trans('landing.message') }}</label>
                            <textarea class="form-control contact-input" rows="3" name="message" placeholder="{{ trans('landing.message') }}" required></textarea>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary  btn-class">{{ trans('landing.submit') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- contact end -->
@endsection
@section('scripts')
<script>
    var layout = "{{session()->get('direction')}}";
</script>

@endsection