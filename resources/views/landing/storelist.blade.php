@php
$i = 1;
@endphp
@foreach ($userdata as $user)

<div class="col" data-aos="fade-up" data-aos-delay="{{$i++}}00" data-aos-duration="1000">
    <a href="{{URL::to($user->slug . '/')}}" target="_blank">
        <div class="card mx-1 rounded-0 view-all-hover">
            <img src="{{ helper::image_path($user->cover_image) }}"
                class="card-img-top rounded-0 object-fit-cover img-fluid object-fit-cover"
                height="185" alt="...">
            <div class="card-body">
                <h5 class="card-title hotel-title">{{ $user->website_title }}</h5>
                <p class="hotel-subtitle text-muted">
                    {{ $user->description }}
                </p>
            </div>
        </div>
    </a>
</div>
@endforeach