@extends('landing.layout.default')
@section('content')
    <!-- slaider-section start -->
    <section>
        <div class="owl-carousel hotels-slaider owl-theme">
            @foreach ($banners as $banner)
                <a href="{{ URL::to('/' . $banner['vendor_info']->slug) }}" target="_blank">
                    <div class="item item-1">
                        <img src="{{ helper::image_path($banner->image) }}" class="mg-fluid">
                    </div>
                </a>
            @endforeach


        </div>
    </section>
    <!-- slaider-section end -->
    <!--card-section start -->
    <section>
        <div class="container">


            <form action="{{ URL::to('/stores') }}" method="get">
                {{-- <input type="hidden" name="city_id" id="city_id" value="">
                 <input type="hidden" name="country_id" id="country_id" value=""> --}}
                <div class="row d-flex justify-content-center align-items-center my-4">
                    <div class="col-md-9">

                        <div class="card shadow w-100 p-3 border-0 d-flex">
                            <div class="row">
                                <div class="col-6">
                                    <label for="country" class="form-lables mb-1 hotel-label">{{ trans('landing.country') }}</label>

                                    <select name="country" class="form-select" id="country">
                                        <option value=""
                                            data-value="{{ URL::to('/stores?country=' . '&city=' . request()->get('city')) }}"
                                            data-id="0" selected>{{ trans('landing.select') }}</option>
                                        @foreach ($countries as $country)
                                            <option value="{{ $country->name }}"
                                                data-value="{{ URL::to('/stores?country=' . request()->get('country') . '&city=' . request()->get('city')) }}"
                                                data-id={{ $country->id }}
                                                {{ request()->get('country') == $country->name ? 'selected' : '' }}>
                                                {{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 {{ session()->get('direction') == 2 ? 'pe-0' : 'ps-0' }}">
                                    <div class="select-input-box">
                                        <label for="city" class="form-lables mb-1 hotel-label">{{ trans('landing.city') }}</label>
                                        <select name="city" class="form-select" id="city">
                                            <option value="">{{ trans('landing.select') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-center mt-4">
                                <label class="form-lables mb-1 hotel-label"></label>
                                <button type="submit" class="btn btn-primary  btn-class">{{ trans('landing.submit') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            @if ($stores->count() > 0)
                <div class="title-restaurant text-center mt-5 mb-5">
                    @if (!empty(request()->get('city')) && request()->get('city') != null)
                        <h5>{{ trans('landing.stores_in') }} {{ @$city_name }}</h5>
                    @endif
                </div>
                <div
                    class="row row-cols-1 row-cols-1 mb-3 row-cols-sm-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-3 row-cols-xll-3 g-4 mb-5">
                    @foreach ($stores as $store)
                        <div class="col">
                            <a href="{{ URL::to('/' . $store->slug) }}" target="_blank">
                                <div class="card mx-1 rounded-0 view-all-hover">
                                    <img src="{{ helper::image_path(helper::appdata($store->id)->cover_image) }}"
                                        class="card-img-top rounded-0 object-fit-cover img-fluid object-fit-cover"
                                        alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title hotel-title">{{ helper::appdata($store->id)->web_title }}
                                        </h5>
                                        <p class="hotel-subtitle text-muted">
                                            {{ helper::appdata($store->id)->footer_description }}
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="d-flex justify-content-center mt-3">

                    {!! $stores->links() !!}

                </div>
            @else
                @include('admin.layout.no_data')
            @endif
        </div>
    </section>
    <!--card-section end-->
@endsection
@section('scripts')
    <script>
        var cityurl = "{{ URL::to('admin/getcity') }}";
        var select = "{{ trans('landing.select') }}";
        var cityname = "{{ request()->get('city') }}";
    </script>
    <script src="{{ url(env('ASSETPATHURL') . '/landing/js/store.js') }}"></script>
@endsection
