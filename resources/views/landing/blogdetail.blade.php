@extends('landing.layout.default')

@section('content')
    <section>
        <div class="container">
            <div class="details-text">
                <h5 class="blog-details-title pb-4 pt-5" data-aos="fade-down" data-aos-darution="1000" data-aos-delay="100">
                {{ $getblog->title }}</h5>
                <img src="{{ helper::image_path($getblog->image) }}" class="img-fluid blog-details-img " alt="..."
                    data-aos="flip-up" data-aos-darution="1000" data-aos-delay="100">
                <div class="d-flex align-items-baseline pt-2" data-aos="flip-up" data-aos-darution="1000"
                    data-aos-delay="100">
                    <i class="fa-solid fa-calendar-days card-date"></i>
                    <p class="card-date px-2">{{ helper::date_format($getblog->created_at) }}</p>
                </div>
                <p class="details-footer m-0 pt-4" data-aos="fade-up" data-aos-darution="1000" data-aos-delay="100">
                    {!! $getblog->description !!}
                </p>

                <h5 class="recent-blogs-titel pt-5 pb-4">Related Blogs</h5>
            </div>
            <div class="owl-carousel blogs-slaider owl-theme pb-5">
                @include('landing.blogcommonview')

            </div>
            <div class="d-flex justify-content-center pb-5 view-all-btn">
                <a href="{{URL::to('/blogs')}}" class="btn-secondary">{{ trans('landing.view_all') }}
                </a>
            </div>
        </div>
    </section>
@endsection
