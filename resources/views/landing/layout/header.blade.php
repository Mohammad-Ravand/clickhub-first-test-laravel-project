<div class="header sticky-top">
    <div class="container">
        <div class="Navbar py-3">
            <div class="logo">
                <a href="{{ URL::to('/') }}">
                    <img src="{{ helper::image_path(helper::appdata('')->logo) }}" height="50" alt="">
                </a>
            </div>
            <div class="d-flex align-items-center ">
                @if (App\Models\SystemAddons::where('unique_identifier', 'language')->first() != null &&
                        App\Models\SystemAddons::where('unique_identifier', 'language')->first()->activated == 1)
                    <div class="d-none language-button-icon mx-2">
                        <a href="#" class="d-none">
                            <div class="dropdown show language-dropdown border rounded-2 mx-2">
                                     <a class=" dropdown-toggle mx-1 border-0 rounded-1 language-drop py-1 " href="#"
                                        role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="fa-solid fa-globe fs-4 pt-1"></i>
                                    </a>
                                <div class="dropdown-menu dropdown-menu-right mt-2" aria-labelledby="dropdownMenuLink">
                                        @foreach (Helper::listoflanguage() as $languagelist)
                                            <li>
                                                <a class="dropdown-item text-dark d-flex text-start"
                                                    href="{{ URL::to('/lang/change?lang=' . $languagelist->code) }}">
                                                    <img src="{{ helper::image_path($languagelist->image) }}" alt=""
                                                        class="img-fluid mx-1" width="25px"> {{ $languagelist->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="language-button d-none">
                        <div class="d-flex align-items-center mx-2 lng-n-button">
                            <a href="#">
                                <div class="dropdown border py-1 rounded-2 mx-3">
                                    <a class=" dropdown-toggle mx-1 border-0 rounded-1 language-drop" href="#"
                                        role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img src="{{ helper::image_path(session()->get('flag')) }}" alt=""
                                            class="img-fluid" width="25px">
                                        {{ session()->get('language') }}
                                    </a>

                                    <ul class="dropdown-menu drop-menu {{ session()->get('direction') == 2 ? 'drop-menu-rtl' : 'drop-menu'}}">
                                        @foreach (helper::listoflanguage() as $languagelist)
                                            <li>
                                                <a class="dropdown-item text-dark d-flex text-start"
                                                    href="{{ URL::to('/lang/change?lang=' . $languagelist->code) }}">
                                                    <img src="{{ helper::image_path($languagelist->image) }}"
                                                        alt="" class="img-fluid mx-1" width="25px">
                                                    {{ $languagelist->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </a>
                        </div>
                    </div>
                @endif
                <div class="togl-btn">
                    <i class="fa-solid fa-bars"></i>
                </div>
            </div>

            <nav class=" {{ session()->get('direction') == 2 ? 'menu-2' : 'menu' }}">
                <!--deletebtn-start-->
                <div class="{{ session()->get('direction') == 2 ? 'deletebtn-button-header-rtl' : 'deletebtn-button-header' }}">
                    <i class="fa-solid fa-xmark"></i>
                </div>
                <!--deletebtn-End-->
                <div class="menu-list-1152px-none mx-5">
                    <ul class="navbar-nav flex-row">
                        <li class="nav-item dropdown px-3">
                            <a class="nav-link text-white" href="{{ URL::to('/') }}" role="button">
                                {{ trans('landing.home') }}
                            </a>
                        </li>
                        <li class="nav-item dropdown px-3">
                            <a class="nav-link  text-white" href="{{ URL::to('/#features') }}" role="button">
                                {{ trans('landing.features') }}
                            </a>
                        </li>
                        <li class="nav-item dropdown px-3">
                            <a class="nav-link text-white" href="{{ URL::to('/#our-stores') }}" role="button">
                                {{ trans('landing.our_store_menu') }}
                            </a>
                        </li>
                        <li class="nav-item dropdown px-3">
                            <a class="nav-link text-white" href="{{ URL::to('/#pricing-plans') }}" role="button">
                                {{ trans('landing.pricing_plan') }}
                            </a>
                        </li>
                        @if (App\Models\SystemAddons::where('unique_identifier', 'blog')->first() != null &&
                                App\Models\SystemAddons::where('unique_identifier', 'blog')->first()->activated == 1)
                            <li class="nav-item dropdown px-3">
                                <a class="nav-link text-white" href="{{ URL::to('/#blogs') }}" role="button">
                                    {{ trans('landing.blogs') }}
                                </a>
                            </li>
                        @endif
                        <li class="nav-item dropdown px-3">
                            <a class="nav-link text-white" href="{{ URL::to('/#contact-us') }}" role="button">
                                {{ trans('landing.contact_us') }}
                            </a>
                        </li>
                    </ul>
                </div>
                
                <div class="header-btn d-flex align-items-center">
                @if (App\Models\SystemAddons::where('unique_identifier', 'language')->first() != null &&
                    App\Models\SystemAddons::where('unique_identifier', 'language')->first()->activated == 1)
                    <a href="#">
                        <div class="dropdown border py-1 rounded-2 mx-3">
                            <a class=" dropdown-toggle mx-1 border-0 rounded-1 language-drop" href="#"
                                role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="{{ helper::image_path(session()->get('flag')) }}" alt=""
                                    class="img-fluid" width="25px">
                                {{ session()->get('language') }}
                            </a>

                            <ul class="dropdown-menu drop-menu {{ session()->get('direction') == 2 ? 'drop-menu-rtl' : 'drop-menu'}}">
                                @foreach (helper::listoflanguage() as $languagelist)
                                    <li>
                                        <a class="dropdown-item text-dark d-flex text-start"
                                            href="{{ URL::to('/lang/change?lang=' . $languagelist->code) }}">
                                            <img src="{{ helper::image_path($languagelist->image) }}"
                                                alt="" class="img-fluid mx-1" width="25px">
                                            {{ $languagelist->name }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </a>
                    @endif
                    <a href="{{ URL::to('/admin') }}" target="_blank"
                        class="header-btn-login text-white px-3 py-2 border-0 rounded-2 py-2 px-2"> {{ trans('landing.get_started') }}</a>
                </div>
                
            </nav>
        </div>
    </div>
</div>
