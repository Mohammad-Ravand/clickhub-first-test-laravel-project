@extends('admin.layout.default')
@section('content')
    <div class="d-flex justify-content-between align-items-center mb-3">
        <h5 class="text-uppercase">{{ trans('labels.edit') }}</h5>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL::to('admin/banner') }}">{{ trans('labels.banner')
                        }}</a>
                </li>
                <li class="breadcrumb-item active {{session()->get('direction') == 2 ? 'breadcrumb-rtl' : ''}}" aria-current="page">{{ trans('labels.edit') }}</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card border-0 box-shadow">
                <div class="card-body">
                    <form action="{{ URL::to('admin/banner/update-' . $getbannerdata->id) }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="form-label">{{ trans('labels.image') }} (250 x 250) <span
                                        class="text-danger"> * </span></label>
                                <input type="file" class="form-control" name="image" required>
                                @error('image')
                                <span class="text-danger">{{ $message }}</span><br>
                                @enderror
                                <img src="{{ helper::image_path($getbannerdata->banner_image) }}"
                                    class="img-fluid rounded hw-50 mt-1" alt="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group text-end">
                                <a href="{{ URL::to('admin/banner') }}" class="btn btn-outline-danger">{{
                                    trans('labels.cancel') }}</a>
                                <button class="btn btn-secondary" @if (env('Environment') == 'sendbox') type="button"
                                    onclick="myFunction()" @else type="submit" @endif>{{ trans('labels.save')
                                    }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection