@extends('admin.layout.default')
@section('content')
<div class="d-flex justify-content-between align-items-center mb-3">
    <h5 class="text-uppercase">{{ trans('labels.addons_manager') }}</h5>
    <div class="d-inline-flex">
        <a href="{{ URL::to('admin/createsystem-addons') }}" class="btn btn-secondary px-2 d-flex">
            <i class="fa-regular fa-plus mx-1"></i>{{ trans('labels.install_update_addons') }}</a>
    </div>
</div>
<div class="search_row">
    <div class="card border-0 box-shadow h-100">
        <div class="card-body">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-link active" id="available-tab" data-bs-toggle="tab" href="#available" role="tab" aria-controls="available" aria-selected="false">{{ trans('labels.available_addons') }}</a>
                    <a class="nav-link" id="installed-tab" data-bs-toggle="tab" href="#installed" role="tab" aria-controls="installed" aria-selected="true">{{ trans('labels.installed_addons') }}</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="available" role="tabpanel" aria-labelledby="available-tab">
                    <?php
                    $payload = file_get_contents('https://paponapps.co.in/api/addonsapi.php?type=papon&item=store');
                    $obj = json_decode($payload);
                    ?>
                    <div class="row">
                        @foreach ($obj->data as $item)
                        <div class="col-md-6 col-lg-3 mt-3 d-flex">
                            <div class="card h-100 w-100">
                                <img class="img-fluid" src='{{ $item->image }}' alt="">
                                <div class="card-body">
                                    <h5 class="card-title mt-3">
                                        {{$item->name}}
                                    </h5>
                                    <p>{!! $item->short_description !!}</p>
                                </div>
                                <div class="card-footer">
                                    <a href="{{ $item->purchase }}" target="_blank" class="btn btn-sm btn-primary">{{trans('labels.buy_now')}}</a>
                                    <span class="btn btn-sm btn-success {{session()->get('direction') == 2 ? 'float-start' : 'float-end'}}">{{ $item->price }}</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- End Col -->
                </div>
                <div class="tab-pane fade" id="installed" role="tabpanel" aria-labelledby="installed-tab">
                    <div class="row">
                        @forelse(App\Models\SystemAddons::all() as $key => $addon)
                        <div class="col-md-6 col-lg-3 mt-3 d-flex">
                            <div class="card h-100 w-100">
                                <img class="img-fluid" src='{!! asset('storage/app/public/addons/' . $addon->image) !!}' alt="">
                                <div class="card-body">
                                    <h5 class="card-title mt-3">
                                        {{$addon->name}}
                                    </h5>
                                </div>
                                <div class="card-footer">
                                    <p class="card-text d-inline"><small class="text-muted">Version :
                                            {{ $addon->version }}</small></p>
                                    @if ($addon->activated == 1)

                                    <a @if (env('Environment') == 'sendbox') onclick="myFunction()" @else onclick="statusupdate('{{ URL::to('admin/systemaddons/status-' . $addon->id . '/2') }}')" @endif class="btn btn-sm btn-success {{session()->get('direction') == 2 ? 'float-start' : 'float-end'}}">{{ trans('labels.activated') }}</a>

                                    @else
                                    
                                    <a @if (env('Environment') == 'sendbox') onclick="myFunction()" @else onclick="statusupdate('{{ URL::to('admin/systemaddons/status-' . $addon->id . '/1') }}')" @endif class="btn btn-sm btn-danger {{session()->get('direction') == 2 ? 'float-start' : 'float-end'}}">{{ trans('labels.deactivated') }}</a>

                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Col -->
                        @empty
                        <div class="col-md-6 col-lg-3 mt-4">
                            <h4>{{ trans('labels.no_addon_installed') }}</h4>
                        </div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection