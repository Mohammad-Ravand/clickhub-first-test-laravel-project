@extends('admin.layout.default')
@section('content')
    <div class="d-flex justify-content-between align-items-center mb-3">
        <h5 class="text-uppercase">{{ trans('labels.users') }}</h5>
        <div class="d-inline-flex">
            <a href="{{ URL::to('admin/users/add') }}" class="btn btn-secondary px-2 d-flex">
                <i class="fa-regular fa-plus mx-1"></i>{{ trans('labels.add') }}</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card border-0">
                <div class="card-body">
                    <div class="table-responsive" id="table-display">
                        <table class="table table-striped table-bordered py-3 zero-configuration w-100">
                            <thead>
                                <tr class="text-uppercase fw-500">
                                    <td>{{ trans('labels.srno') }}</td>
                                    <td>{{ trans('labels.image') }}</td>
                                    <td>{{ trans('labels.name') }}</td>
                                    <td>{{ trans('labels.email') }}</td>
                                    <td>{{ trans('labels.mobile') }}</td>
                                    <td>{{ trans('labels.status') }}</td>
                                    <td>{{ trans('labels.action') }}</td>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach ($getuserslist as $user)
                                    <tr>
                                        <td>@php echo $i++; @endphp</td>
                                        <td> <img src="{{ helper::image_path($user->image) }}"
                                                class="img-fluid rounded hw-50" alt="" srcset=""> </td>
                                        <td> {{ $user->name }} </td>
                                        <td> {{ $user->email }} </td>
                                        <td> {{ $user->mobile }} </td>
                                        <td>
                                            @if ($user->is_available == 1)
                                                <a class="btn btn-sm btn-outline-success"
                                                    @if (env('Environment') == 'sendbox') onclick="myFunction()" @else onclick="statusupdate('{{ URL::to('admin/users/status-' . $user->slug . '/2') }}')" @endif><i
                                                        class="fa-sharp fa-solid fa-check"></i></a>
                                            @else
                                                <a class="btn btn-sm btn-outline-danger"
                                                    @if (env('Environment') == 'sendbox') onclick="myFunction()" @else onclick="statusupdate('{{ URL::to('admin/users/status-' . $user->slug . '/1') }}')" @endif><i
                                                        class="fa-sharp fa-solid fa-xmark"></i></a>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-outline-info"
                                                href="{{ URL::to('admin/users/edit-' . $user->slug) }}"> <i
                                                    class="fa fa-pen-to-square"></i></a>
                                            <a class="btn btn-sm btn-outline-dark"
                                                href="{{ URL::to('admin/users/login-' . $user->slug) }}"> <i
                                                    class="fa-regular fa-arrow-right-to-bracket"></i> </a>
                                            <a class="btn btn-sm btn-outline-secondary" href="{{ URL::to('/' . $user->slug) }}" target="_blank"><i class="fa-regular fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
