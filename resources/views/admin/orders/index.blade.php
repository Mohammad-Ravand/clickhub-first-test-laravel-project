@extends('admin.layout.default')
@section('content')
    <div class="d-flex justify-content-between mb-3">
        <h5 class="text-uppercase align-items-center">{{ request()->is('admin/report*') ? trans('labels.report') :
            trans('labels.orders') }}</h5>
             @if (request()->is('admin/report*'))
             <form action="{{ URL::to('/admin/report') }}" class="mb-">
                 <div class="input-group col-md-12 ps-0 justify-content-end">
                     <div class="input-group-append col-auto px-1">
                         <input type="date" class="form-control rounded" name="startdate" @isset($_GET['startdate'])
                             value="{{ $_GET['startdate'] }}" @endisset required>
                     </div>
                     <div class="input-group-append col-auto px-1">
                         <input type="date" class="form-control rounded" name="enddate" @isset($_GET['enddate'])
                             value="{{ $_GET['enddate'] }}" @endisset required>
                     </div>
                     <div class="input-group-append">
                         <button class="btn btn-primary rounded" type="submit">{{ trans('labels.fetch') }}</button>
                     </div>
                 </div>
             </form>
             @endif
    </div>
   
    @include('admin.orders.statistics')
    <div class="row">
        <div class="col-12">
            <div class="card border-0 my-3">
                <div class="card-body">
                    <div class="table-responsive">
                        @include('admin.orders.orderstable')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection