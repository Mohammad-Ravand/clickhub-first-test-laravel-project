<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ trans('labels.print') }}</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ helper::image_path(@helper::appdata($getorderdata->vendor_id)->favicon) }}">
    <style type="text/css">
        body {width: 88mm;height: 100%;background-color: #ffffff;margin: 0;padding: 0;--webkit-font-smoothing: antialiased;}
        #printDiv {font-weight: 600;margin: 0;padding: 0;background: #ffffff;}
        #printDiv div,#printDiv p,#printDiv a,#printDiv li,#printDiv td {-webkit-text-size-adjust: none;}
        .center {display: block;margin-left: auto;margin-right: auto;width: 50%;}
        @media print {
            @page {
                margin: 0;
            }
            body {
                margin: 1.6cm;
            }
        }
    </style>
</head>
<body>
    <div id="printDiv">
        <div class="">
            <table width="90%" border="0" cellpadding='2' cellspacing="2" align="center" bgcolor="#ffffff"
                style="padding-top:4px;">
                <tbody>
                    <tr>
                        <td
                            style="font-size: 15px;color: #fffffff; font-family: 'Open Sans', sans-serif; line-height:18px; vertical-align: bottom; text-align: center;">
                            <h3 style="margin-top: 2px;margin-bottom: 2px;">
                                {{ @helper::appdata($getorderdata->vendor_id)->web_title }}</h3>
                        </td>
                    </tr>
                    <tr>
                        <td
                            style="font-size: 13px;color: #fffffff; font-family: 'Open Sans', sans-serif; line-height:18px; vertical-align: bottom; text-align: center;">
                            #{{ $getorderdata->order_number }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
                <tbody>
                    <tr>
                        <td
                            style="font-size: 12px; color: #fffffff;  font-family: 'Open Sans', sans-serif; line-height:18px; vertical-align: bottom; text-align: center;">
                            {{ trans('labels.name') }} : {{ $getorderdata->customer_name }}
                            <br>{{ trans('labels.email') }} : {{ $getorderdata->customer_email }}
                            <br>{{ trans('labels.mobile') }} : {{ $getorderdata->mobile }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <table width="90%" border="0" cellpadding="2" cellspacing="2" align="center" class="fullPadding">
                <tbody>
                    @if ($getorderdata->order_notes != '')
                        <div style="padding: 5px 10px 5px 15px;">
                            <h5 style="margin-top: 2px;margin-bottom: 2px;">{{ trans('labels.order_note') }} : <small
                                    style="color: gray">{{ $getorderdata->order_notes }}</small></h5>
                        </div>
                    @endif
                    <table width="90%" border="0" cellpadding="0" cellspacing="0" align="center"
                        style="padding-top:2px">
                        <thead>
                            <tr>
                                <th style="font-size:15px;font-family:'Open Sans',sans-serif;color:#fffffff;font-weight:normal;line-height:1;vertical-align:top;padding-bottom:5px;text-align:left;"
                                    width="50%">{{ trans('labels.products') }}</th>
                                <th style="font-size:15px;font-family:'Open Sans',sans-serif;color:#fffffff;font-weight:normal;line-height:1;vertical-align:top;padding-bottom:5px;text-align:right;"
                                    width="20%">{{ trans('labels.unit_cost') }}</th>
                                <th style="font-size:15px;font-family:'Open Sans',sans-serif;color:#fffffff;font-weight:normal;line-height:1;vertical-align:top;padding-bottom:5px;text-align:right;"
                                    width="30%">{{ trans('labels.sub_total') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($ordersdetails as $orders) {  
                            ?>
                            @php
                                $itemprice = $orders->price;
                                if ($orders->variants_id != '') {
                                    $itemprice = $orders->variants_price;
                                }
                            @endphp
                            <tr>
                                <td
                                    style="font-size:12px;font-family:'Open Sans',sans-serif;color:#fffffff;line-height:18px;vertical-align:top;text-align:left;">
                                    <b>{{ $orders->item_name }}</b>
                                    @if ($orders->variants_id != '')
                                        - <small>{{ $orders->variants_name }}</small>
                                    @endif
                                    @if ($orders->extras_id != '')
                                        <?php
                                        $extras_id = explode(',', $orders->extras_id);
                                        $extras_name = explode(',', $orders->extras_name);
                                        $extras_price = explode(',', $orders->extras_price);
                                        $extras_total_price = 0;
                                        ?>
                                        <br>
                                        @foreach ($extras_id as $key => $addons)
                                            <small>
                                                <b class="text-muted">{{ $extras_name[$key] }}</b> :
                                                {{ helper::currency_formate($extras_price[$key], $getorderdata->vendor_id) }}<br>
                                            </small>
                                            @php
                                                $extras_total_price += $extras_price[$key];
                                            @endphp
                                        @endforeach
                                    @else
                                        @php
                                            $extras_total_price = 0;
                                        @endphp
                                    @endif
                                </td>
                                <td
                                    style="font-size:12px;font-family:'Open Sans',sans-serif;color:#fffffff;line-height:18px;vertical-align:top;text-align:right;">
                                    {{ helper::currency_formate($itemprice, $getorderdata->vendor_id) }}
                                    @if ($extras_total_price > 0)
                                        + {{ helper::currency_formate($extras_total_price, $getorderdata->vendor_id) }}
                                    @endif
                                    <b>x</b>{{ $orders->qty }}
                                </td>
                                <td
                                    style="font-size:12px;font-family:'Open Sans',sans-serif;color:#fffffff;line-height:18px;vertical-align:top;text-align:right;">
                                    {{ helper::currency_formate(($itemprice + $extras_total_price) * $orders->qty, $getorderdata->vendor_id) }}
                                </td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                    <table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
                        <tbody>
                            <tr>
                                <td style="font-size: 15px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right;"
                                    width="70%"><strong>{{ trans('labels.sub_total') }}</strong></td>
                                <td style="font-size: 15px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right;"
                                    width="30%">
                                    <strong>{{ helper::currency_formate($getorderdata->sub_total, $getorderdata->vendor_id) }}</strong>
                                </td>
                            </tr>
                            @if ($getorderdata->discount_amount > 0)
                                <tr>
                                    <td style="font-size: 15px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right;"
                                        width="70%"><strong>{{ trans('labels.discount') }}
                                            {{ $getorderdata->offer_code != '' ? '(' . $getorderdata->offer_code . ')' : '' }}</strong>
                                    </td>
                                    <td style="font-size: 15px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right;"
                                        width="30%">
                                        <strong>{{ helper::currency_formate($getorderdata->discount_amount, $getorderdata->vendor_id) }}</strong>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td style="font-size: 15px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right;"
                                    width="70%"><strong>{{ trans('labels.tax') }}</strong></td>
                                <td style="font-size: 15px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right;"
                                    width="30%">
                                    <strong>{{ helper::currency_formate($getorderdata->tax_amount, $getorderdata->vendor_id) }}</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 15px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right;"
                                    width="70%"><strong>{{ trans('labels.delivery_charge') }}</strong></td>
                                <td style="font-size: 15px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right;"
                                    width="30%">
                                    <strong>{{ helper::currency_formate($getorderdata->delivery_charge, $getorderdata->vendor_id) }}</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 15px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right;"
                                    width="70%"><strong>{{ trans('labels.grand_total') }}</strong></td>
                                <td style="font-size: 15px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 22px; vertical-align: top; text-align:right;"
                                    width="30%">
                                    <strong>{{ helper::currency_formate($getorderdata->grand_total, $getorderdata->vendor_id) }}</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </tbody>
            </table>
        </div>
    </div>
    <button id="btnPrint" class="hidden-print center">{{ trans('labels.print') }}</button>
    <script>
        const $btnPrint = document.querySelector("#btnPrint");
        $btnPrint.addEventListener("click", () => {
            window.print();
        });
    </script>
</body>