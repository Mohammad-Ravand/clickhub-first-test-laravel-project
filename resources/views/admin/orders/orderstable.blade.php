<table class="table table-striped table-bordered py-3 zero-configuration w-100">
    <thead>
        <tr class="text-uppercase fw-500">
            <td>{{ trans('labels.srno') }}</td>
            @if(request()->is('admin/customers*') && (Auth::user()->type == 1))
            <td>{{ trans('labels.vendor_title') }}</td>
            @endif
            <td>{{ trans('labels.order_number') }}</td>
            <td>{{ trans('labels.date_time') }}</td>
            <td>{{ trans('labels.grand_total') }}</td>
            <td>{{ trans('labels.payment_type') }}</td>
            <td>{{ trans('labels.status') }}</td>
            @if (Auth::user()->type == 2)
                <td>{{ trans('labels.action') }}</td>
            @endif
        </tr>
    </thead>
    <tbody>
        @php $i = 1; @endphp
        @foreach ($getorders as $orderdata)
            <tr id="dataid{{ $orderdata->id }}">
                <td>@php echo $i++; @endphp</td>
                @if(request()->is('admin/customers*') && (Auth::user()->type == 1))
                <td>{{ $orderdata['vendorinfo']->name }}</td>
                @endif
                <td>{{ $orderdata->order_number }}</td>
                <td>{{ helper::date_format($orderdata->delivery_date) }} <br>
                    {{ $orderdata->delivery_time }}
                </td>
                <td>{{ helper::currency_formate($orderdata->grand_total, $orderdata->vendor_id) }}</td>
                <td>
                    @if ($orderdata->payment_type == 1)
                        {{ trans('labels.cod') }}
                    @elseif ($orderdata->payment_type == 2)
                        {{ trans('labels.razorpay') }}
                    @elseif ($orderdata->payment_type == 3)
                        {{ trans('labels.stripe') }}
                    @elseif ($orderdata->payment_type == 4)
                        {{ trans('labels.flutterwave') }}
                    @elseif ($orderdata->payment_type == 5)
                        {{ trans('labels.paystack') }}
                    @elseif ($orderdata->payment_type == 7)
                        {{ trans('labels.mercadopago') }}
                    @elseif ($orderdata->payment_type == 8)
                        {{ trans('labels.paypal') }}
                    @elseif ($orderdata->payment_type == 9)
                        {{ trans('labels.myfatoorah') }}
                    @elseif ($orderdata->payment_type == 10)
                        {{ trans('labels.toyyibpay') }}
                    @endif
                    @if (in_array($orderdata->payment_type, [2, 3, 4, 5, 7, 8, 9, 10]))
                        : {{ $orderdata->payment_id }}
                    @endif
                </td>
                <td>
                    @if ($orderdata->status == 1)
                        @php
                            $status = trans('labels.pending');
                            $color = 'warning';
                        @endphp
                    @elseif ($orderdata->status == 2)
                        @php
                            $status = trans('labels.accepted');
                            $color = 'info';
                        @endphp
                    @elseif ($orderdata->status == 3)
                        @php
                            $status = trans('labels.rejected');
                            $color = 'danger';
                        @endphp
                    @elseif ($orderdata->status == 4)
                        @php
                            $status = trans('labels.cancelled');
                            $color = 'danger';
                        @endphp
                    @elseif ($orderdata->status == 5)
                        @php
                            $status = trans('labels.completed');
                            $color = 'success';
                        @endphp
                    @endif
                    <span class="badge bg-{{ $color }}">{{ $status }}</span>
                </td>
                @if (Auth::user()->type == 2)
                    <td>
                        <a href="{{ URL::to('admin/orders/print/' . $orderdata->order_number) }}"
                            class="btn btn-outline-primary mx-1">
                            <i class="fa fa-pdf" aria-hidden="true"></i> {{ trans('labels.print') }}
                        </a>             
                        <a class="btn btn-sm btn-light"
                            href="{{ URL::to('admin/orders/invoice/' . $orderdata->order_number) }}"> <i
                                class="fa-regular fa-eye"></i> </a>
                    </td>
                @endif
            </tr>
        @endforeach
    </tbody>
</table>
