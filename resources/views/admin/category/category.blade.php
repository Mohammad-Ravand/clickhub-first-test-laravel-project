@extends('admin.layout.default')
@section('content')
        <div class="d-flex justify-content-between align-items-center mb-3">
            <h5 class="text-uppercase">{{ trans('labels.category') }}</h5>
            <a href="{{ URL::to('admin/categories/add') }}" class="btn btn-secondary px-2 d-flex"><i
                    class="fa-regular fa-plus mx-1"></i>{{ trans('labels.add') }}</a>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card border-0 my-3">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered py-3 zero-configuration w-100">
                                <thead>
                                    <tr class="text-uppercase fw-500">
                                        <td>{{ trans('labels.srno') }}</td>
                                        <td>{{ trans('labels.category') }}</td>
                                        <td>{{ trans('labels.status') }}</td>
                                        <td>{{ trans('labels.action') }}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i=1; @endphp
                                    @foreach ($allcategories as $category)
                                        <tr class="fs-7">
                                            <td>@php echo $i++ @endphp</td>
                                            <td>{{ $category->name }}</td>
                                            <td>
                                                @if ($category->is_available == '1')
                                                    <a @if (env('Environment') == 'sendbox') onclick="myFunction()" @else onclick="statusupdate('{{ URL::to('admin/categories/change_status-' . $category->slug . '/2') }}')" @endif
                                                        class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i></a>
                                                @else
                                                    <a @if (env('Environment') == 'sendbox') onclick="myFunction()" @else onclick="statusupdate('{{ URL::to('admin/categories/change_status-' . $category->slug . '/1') }}')" @endif
                                                        class="btn btn-sm btn-outline-danger"><i class="fas fa-close"></i></a>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ URL::to('admin/categories/edit-' . $category->slug) }}"
                                                    class="btn btn-outline-info btn-sm "> <i
                                                        class="fa-regular fa-pen-to-square"></i></a>
                                                <a @if (env('Environment') == 'sendbox') onclick="myFunction()" @else onclick="statusupdate('{{ URL::to('admin/categories/delete-' . $category->slug) }}')" @endif
                                                    class="btn btn-outline-danger btn-sm "> <i
                                                        class="fa-regular fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
