@extends('admin.layout.default')
@section('content')
    <div class="d-flex justify-content-between align-items-center mb-3">
        <h5 class="text-uppercase">{{ trans('labels.about_us') }}</h5>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card border-0 box-shadow">
                <div class="card-body">
                    <div id="about-us-three" class="about-us">
                        <form action="{{ URL::to('admin/aboutus/update') }}" method="post">
                            @csrf
                            <textarea class="form-control" id="ckeditor" name="aboutus">{{ @$getaboutus->about_content }}</textarea>
                            @error('aboutus')
                                <span class="text-danger">{{ $message }}</span><br>
                            @enderror
                            <div class="form-group text-end">
                                <a href="{{ URL::to('admin/dashboard') }}"
                                    class="btn btn-outline-danger">{{ trans('labels.cancel') }}</a>
                                <button class="btn btn-primary my-2"
                                    @if (env('Environment') == 'sendbox') type="button" onclick="myFunction()" @else type="submit" @endif>{{ trans('labels.save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.12.1/ckeditor.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace('ckeditor');
    </script>
@endsection