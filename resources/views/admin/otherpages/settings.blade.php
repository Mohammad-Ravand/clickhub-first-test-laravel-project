@extends('admin.layout.default')
@section('content')
    <h5 class="text-uppercase">{{ trans('labels.settings') }}</h5>
    <div class="row settings mt-3">
        <div class="col-xl-3 mb-3">
            <div class="card card-sticky-top border-0">
                <ul class="list-group list-options">
                    <a href="#basicinfo" data-tab="basicinfo"
                        class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center active"
                        aria-current="true">{{ trans('labels.basic_info') }} <i class="fa-regular fa-angle-right"></i></a>
                    @if (Auth::user()->type == 2)
                        <a href="#themesettings" data-tab="themesettings"
                            class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                            aria-current="true">{{ trans('labels.theme_settings') }} <i
                                class="fa-regular fa-angle-right"></i></a>
                    @endif
                    <a href="#editprofile" data-tab="editprofile"
                        class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                        aria-current="true">{{ trans('labels.edit_profile') }} <i class="fa-regular fa-angle-right"></i></a>
                    <a href="#changepasssword" data-tab="changepasssword"
                        class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                        aria-current="true">{{ trans('labels.change_password') }} <i
                            class="fa-regular fa-angle-right"></i></a>
                    <a href="#seo" data-tab="seo"
                        class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                        aria-current="true">{{ trans('labels.seo') }} <i class="fa-regular fa-angle-right"></i></a>
                    @if (Auth::user()->type == 2)
                        @if (App\Models\SystemAddons::where('unique_identifier', 'whatsapp_message')->first() != null &&
                                App\Models\SystemAddons::where('unique_identifier', 'whatsapp_message')->first()->activated == 1)
                            <a href="#whatsappmessagesettings" data-tab="whatsappmessagesettings"
                                class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                                aria-current="true">{{ trans('labels.whatsapp_message') }} @if (env('Environment') == 'sendbox')
                                    <span class="badge badge bg-danger me-5">{{ trans('labels.addon') }}</span>
                                @endif
                                <i class="fa-regular fa-angle-right"></i></a>
                        @endif
                    @endif

                    @if (Auth::user()->type == 1)
                        @if (App\Models\SystemAddons::where('unique_identifier', 'custom_domain')->first() != null &&
                                App\Models\SystemAddons::where('unique_identifier', 'custom_domain')->first()->activated == 1)
                            <a href="#custom_domain" data-tab="custom_domain"
                                class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                                aria-current="true">{{ trans('labels.custom_domain') }}@if (env('Environment') == 'sendbox')
                                    <span class="badge badge bg-danger me-5">{{ trans('labels.addon') }}</span>
                                @endif <i class="fa-regular fa-angle-right"></i></a>
                        @endif
                        @if (App\Models\SystemAddons::where('unique_identifier', 'google_analytics')->first() != null &&
                                App\Models\SystemAddons::where('unique_identifier', 'google_analytics')->first()->activated == 1)
                            <a href="#google_analytics" data-tab="google_analytics"
                                class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                                aria-current="true">{{ trans('labels.google_analytics') }}@if (env('Environment') == 'sendbox')
                                    <span class="badge badge bg-danger me-5">{{ trans('labels.addon') }}</span>
                                @endif <i class="fa-regular fa-angle-right"></i></a>
                        @endif
                        <a href="#landing_page" data-tab="landing_page"
                            class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                            aria-current="true">{{ trans('labels.landing_page') }}
                            <i class="fa-regular fa-angle-right"></i>
                        </a>
                    @endif
                   
                </ul>
            </div>
        </div>
        <div class="col-xl-9">
            <div id="settingmenuContent">
                <div id="basicinfo">
                    <div class="row mb-5">
                        <div class="col-12">
                            <div class="card border-0 box-shadow">
                                <div class="card-body">
                                    <div class="d-flex align-items-center mb-3">
                                        <h5 class="text-uppercase">{{ trans('labels.basic_info') }}</h5>
                                    </div>
                                    <form action="{{ URL::to('admin/settings/update') }}" method="POST"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="form-label">{{ trans('labels.currency') }}<span
                                                                    class="text-danger"> * </span></label>
                                                            <input type="text" class="form-control" name="currency"
                                                                value="{{ @$settingdata->currency }}"
                                                                placeholder="{{ trans('labels.currency') }}" required>
                                                            @error('currency')
                                                                <small class="text-danger">{{ $message }}</small>
                                                            @enderror
                                                        </div>
                                                        <div class="col-md-3">
                                                            <p class="form-label">
                                                                {{ trans('labels.currency_position') }}
                                                            </p>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input form-check-input-secondary"
                                                                    type="radio" name="currency_position" id="radio"
                                                                    value="1"
                                                                    {{ @$settingdata->currency_position == 'left' ? 'checked' : '' }} />
                                                                <label for="radio"
                                                                    class="form-check-label">{{ trans('labels.left') }}</label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input form-check-input-secondary"
                                                                    type="radio" name="currency_position" id="radio1"
                                                                    value="2"
                                                                    {{ @$settingdata->currency_position == 'right' ? 'checked' : '' }} />
                                                                <label for="radio1"
                                                                    class="form-check-label">{{ trans('labels.right') }}</label>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="{{ env('Environment') == 'sendbox' ? 'col-md-2' : 'col-md-3' }}">
                                                            <label class="form-label"
                                                                for="">{{ trans('labels.maintenance_mode') }}
                                                            </label>
                                                            <input id="maintenance_mode-switch" type="checkbox"
                                                                class="checkbox-switch" name="maintenance_mode"
                                                                value="1"
                                                                {{ $settingdata->maintenance_mode == 1 ? 'checked' : '' }}>
                                                            <label for="maintenance_mode-switch" class="switch">
                                                                <span
                                                                    class="{{ session()->get('direction') == 2 ? 'switch__circle-rtl' : 'switch__circle' }}"><span
                                                                        class="switch__circle-inner"></span></span>
                                                                <span
                                                                    class="switch__left {{ session()->get('direction') == 2 ? 'pe-2' : 'ps-2' }}">{{ trans('labels.off') }}</span>
                                                                <span
                                                                    class="switch__right {{ session()->get('direction') == 2 ? 'ps-2' : 'pe-2' }}">{{ trans('labels.on') }}</span>
                                                            </label>
                                                        </div>
                                                        @if (Auth::user()->type == 2)
                                                            @if (App\Models\SystemAddons::where('unique_identifier', 'customer_login')->first() != null &&
                                                                    App\Models\SystemAddons::where('unique_identifier', 'customer_login')->first()->activated == 1)
                                                                <div
                                                                    class="{{ env('Environment') == 'sendbox' ? 'col-md-4' : 'col-md-3' }}">
                                                                    <label class="form-label"
                                                                        for="">{{ trans('labels.checkout_login_required') }}
                                                                    </label>
                                                                    @if (env('Environment') == 'sendbox')
                                                                        <span
                                                                            class="badge badge bg-danger ms-2 mb-0">{{ trans('labels.addon') }}</span>
                                                                    @endif
                                                                    <input id="checkout_login_required-switch"
                                                                        type="checkbox" class="checkbox-switch"
                                                                        name="checkout_login_required" value="1"
                                                                        {{ $settingdata->checkout_login_required == 1 ? 'checked' : '' }}>
                                                                    <label for="checkout_login_required-switch"
                                                                        class="switch">
                                                                        <span
                                                                            class="{{ session()->get('direction') == 2 ? 'switch__circle-rtl' : 'switch__circle' }}"><span
                                                                                class="switch__circle-inner"></span></span>
                                                                        <span
                                                                            class="switch__left {{ session()->get('direction') == 2 ? 'pe-2' : 'ps-2' }}">{{ trans('labels.off') }}</span>
                                                                        <span
                                                                            class="switch__right {{ session()->get('direction') == 2 ? 'ps-2' : 'pe-2' }}">{{ trans('labels.on') }}</span>
                                                                    </label>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-label">{{ trans('labels.time_zone') }}</label>
                                                    <select class="form-select" name="timezone">
                                                        <option
                                                            {{ @$settingdata->timezone == 'Pacific/Midway' ? 'selected' : '' }}
                                                            value="Pacific/Midway">(GMT-11:00) Midway Island, Samoa
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Adak' ? 'selected' : '' }}
                                                            value="America/Adak">(GMT-10:00) Hawaii-Aleutian
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Etc/GMT+10' ? 'selected' : '' }}
                                                            value="Etc/GMT+10">(GMT-10:00) Hawaii</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Pacific/Marquesas' ? 'selected' : '' }}
                                                            value="Pacific/Marquesas">(GMT-09:30) Marquesas Islands
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Pacific/Gambier' ? 'selected' : '' }}
                                                            value="Pacific/Gambier">(GMT-09:00) Gambier Islands
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Anchorage' ? 'selected' : '' }}
                                                            value="America/Anchorage">(GMT-09:00) Alaska</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Ensenada' ? 'selected' : '' }}
                                                            value="America/Ensenada">(GMT-08:00) Tijuana, Baja
                                                            California </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Etc/GMT+8' ? 'selected' : '' }}
                                                            value="Etc/GMT+8">(GMT-08:00) Pitcairn Islands</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Los_Angeles' ? 'selected' : '' }}
                                                            value="America/Los_Angeles">(GMT-08:00) Pacific Time
                                                            (US
                                                            &amp; Canada) </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Denver' ? 'selected' : '' }}
                                                            value="America/Denver">(GMT-07:00) Mountain Time (US
                                                            &amp;
                                                            Canada) </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Chihuahua' ? 'selected' : '' }}
                                                            value="America/Chihuahua">(GMT-07:00) Chihuahua, La
                                                            Paz,
                                                            Mazatlan </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Dawson_Creek' ? 'selected' : '' }}
                                                            value="America/Dawson_Creek">(GMT-07:00) Arizona
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Belize' ? 'selected' : '' }}
                                                            value="America/Belize">(GMT-06:00) Saskatchewan,
                                                            Central
                                                            America </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Cancun' ? 'selected' : '' }}
                                                            value="America/Cancun">(GMT-06:00) Guadalajara, Mexico
                                                            City,
                                                            Monterrey </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Chile/EasterIsland' ? 'selected' : '' }}
                                                            value="Chile/EasterIsland">(GMT-06:00) Easter Island
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Chicago' ? 'selected' : '' }}
                                                            value="America/Chicago">(GMT-06:00) Central Time (US
                                                            &amp;
                                                            Canada) </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/New_York' ? 'selected' : '' }}
                                                            value="America/New_York">(GMT-05:00) Eastern Time (US
                                                            &amp;
                                                            Canada) </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Havana' ? 'selected' : '' }}
                                                            value="America/Havana">(GMT-05:00) Cuba</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Bogota' ? 'selected' : '' }}
                                                            value="America/Bogota">(GMT-05:00) Bogota, Lima, Quito,
                                                            Rio
                                                            Branco </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Caracas' ? 'selected' : '' }}
                                                            value="America/Caracas">(GMT-04:30) Caracas</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Santiago' ? 'selected' : '' }}
                                                            value="America/Santiago">(GMT-04:00) Santiago</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/La_Paz' ? 'selected' : '' }}
                                                            value="America/La_Paz">(GMT-04:00) La Paz</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Atlantic/Stanley' ? 'selected' : '' }}
                                                            value="Atlantic/Stanley">(GMT-04:00) Faukland Islands
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Campo_Grande' ? 'selected' : '' }}
                                                            value="America/Campo_Grande">(GMT-04:00) Brazil
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Goose_Bay' ? 'selected' : '' }}
                                                            value="America/Goose_Bay">(GMT-04:00) Atlantic Time
                                                            (Goose
                                                            Bay) </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Glace_Bay' ? 'selected' : '' }}
                                                            value="America/Glace_Bay">(GMT-04:00) Atlantic Time
                                                            (Canada) </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/St_Johns' ? 'selected' : '' }}
                                                            value="America/St_Johns">(GMT-03:30) Newfoundland
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Araguaina' ? 'selected' : '' }}
                                                            value="America/Araguaina">(GMT-03:00) UTC-3</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Montevideo' ? 'selected' : '' }}
                                                            value="America/Montevideo">(GMT-03:00) Montevideo
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Miquelon' ? 'selected' : '' }}
                                                            value="America/Miquelon">(GMT-03:00) Miquelon, St.
                                                            Pierre
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Godthab' ? 'selected' : '' }}
                                                            value="America/Godthab">(GMT-03:00) Greenland</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Argentina' ? 'selected' : '' }}
                                                            value="America/Argentina/Buenos_Aires">(GMT-03:00)
                                                            Buenos
                                                            Aires </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Sao_Paulo' ? 'selected' : '' }}
                                                            value="America/Sao_Paulo">(GMT-03:00) Brasilia</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'America/Noronha' ? 'selected' : '' }}
                                                            value="America/Noronha">(GMT-02:00) Mid-Atlantic
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Atlantic/Cape_Verde' ? 'selected' : '' }}
                                                            value="Atlantic/Cape_Verde">(GMT-01:00) Cape Verde Is.
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Atlantic/Azores' ? 'selected' : '' }}
                                                            value="Atlantic/Azores">(GMT-01:00) Azores</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Europe/Belfast' ? 'selected' : '' }}
                                                            value="Europe/Belfast">(GMT) Greenwich Mean Time :
                                                            Belfast
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Europe/Dublin' ? 'selected' : '' }}
                                                            value="Europe/Dublin">(GMT) Greenwich Mean Time :
                                                            Dublin
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Europe/Lisbon' ? 'selected' : '' }}
                                                            value="Europe/Lisbon">(GMT) Greenwich Mean Time :
                                                            Lisbon
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Europe/London' ? 'selected' : '' }}
                                                            value="Europe/London">(GMT) Greenwich Mean Time :
                                                            London
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Africa/Abidjan' ? 'selected' : '' }}
                                                            value="Africa/Abidjan">(GMT) Monrovia, Reykjavik
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Europe/Amsterdam' ? 'selected' : '' }}
                                                            value="Europe/Amsterdam">(GMT+01:00) Amsterdam, Berlin,
                                                            Bern, Rome, Stockholm, Vienna</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Europe/Belgrade' ? 'selected' : '' }}
                                                            value="Europe/Belgrade">(GMT+01:00) Belgrade,
                                                            Bratislava,
                                                            Budapest, Ljubljana, Prague</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Europe/Brussels' ? 'selected' : '' }}
                                                            value="Europe/Brussels">(GMT+01:00) Brussels,
                                                            Copenhagen,
                                                            Madrid, Paris </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Africa/Algiers' ? 'selected' : '' }}
                                                            value="Africa/Algiers">(GMT+01:00) West Central Africa
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Africa/Windhoek' ? 'selected' : '' }}
                                                            value="Africa/Windhoek">(GMT+01:00) Windhoek</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Beirut' ? 'selected' : '' }}
                                                            value="Asia/Beirut">(GMT+02:00) Beirut</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Africa/Cairo' ? 'selected' : '' }}
                                                            value="Africa/Cairo">(GMT+02:00) Cairo</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Gaza' ? 'selected' : '' }}
                                                            value="Asia/Gaza">(GMT+02:00) Gaza</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Africa/Blantyre' ? 'selected' : '' }}
                                                            value="Africa/Blantyre">(GMT+02:00) Harare, Pretoria
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Jerusalem' ? 'selected' : '' }}
                                                            value="Asia/Jerusalem">(GMT+02:00) Jerusalem</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Europe/Minsk' ? 'selected' : '' }}
                                                            value="Europe/Minsk">(GMT+02:00) Minsk</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Damascus' ? 'selected' : '' }}
                                                            value="Asia/Damascus">(GMT+02:00) Syria</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Europe/Moscow' ? 'selected' : '' }}
                                                            value="Europe/Moscow">(GMT+03:00) Moscow, St.
                                                            Petersburg,
                                                            Volgograd </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Africa/Addis_Ababa' ? 'selected' : '' }}
                                                            value="Africa/Addis_Ababa">(GMT+03:00) Nairobi</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Tehran' ? 'selected' : '' }}
                                                            value="Asia/Tehran">(GMT+03:30) Tehran</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Dubai' ? 'selected' : '' }}
                                                            value="Asia/Dubai">(GMT+04:00) Abu Dhabi, Muscat
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Yerevan' ? 'selected' : '' }}
                                                            value="Asia/Yerevan">(GMT+04:00) Yerevan</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Kabul' ? 'selected' : '' }}
                                                            value="Asia/Kabul">(GMT+04:30) Kabul</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Yekaterinburg' ? 'selected' : '' }}
                                                            value="Asia/Yekaterinburg">(GMT+05:00) Ekaterinburg
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Tashkent' ? 'selected' : '' }}
                                                            value="Asia/Tashkent"> (GMT+05:00) Tashkent</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Kolkata' ? 'selected' : '' }}
                                                            value="Asia/Kolkata"> (GMT+05:30) Chennai, Kolkata,
                                                            Mumbai,
                                                            New Delhi</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Katmandu' ? 'selected' : '' }}
                                                            value="Asia/Katmandu">(GMT+05:45) Kathmandu</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Dhaka' ? 'selected' : '' }}
                                                            value="Asia/Dhaka">(GMT+06:00) Astana, Dhaka</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Novosibirsk' ? 'selected' : '' }}
                                                            value="Asia/Novosibirsk">(GMT+06:00) Novosibirsk
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Rangoon' ? 'selected' : '' }}
                                                            value="Asia/Rangoon">(GMT+06:30) Yangon (Rangoon)
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Bangkok' ? 'selected' : '' }}
                                                            value="Asia/Bangkok">(GMT+07:00) Bangkok, Hanoi,
                                                            Jakarta
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Kuala_Lumpur' ? 'selected' : '' }}
                                                            value="Asia/Kuala_Lumpur">(GMT+08:00) Kuala Lumpur
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Krasnoyarsk' ? 'selected' : '' }}
                                                            value="Asia/Krasnoyarsk">(GMT+07:00) Krasnoyarsk
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Hong_Kong' ? 'selected' : '' }}
                                                            value="Asia/Hong_Kong">(GMT+08:00) Beijing, Chongqing,
                                                            Hong
                                                            Kong, Urumqi</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Irkutsk' ? 'selected' : '' }}
                                                            value="Asia/Irkutsk">(GMT+08:00) Irkutsk, Ulaan Bataar
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Australia/Perth' ? 'selected' : '' }}
                                                            value="Australia/Perth">(GMT+08:00) Perth</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Australia/Eucla' ? 'selected' : '' }}
                                                            value="Australia/Eucla">(GMT+08:45) Eucla</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Tokyo' ? 'selected' : '' }}
                                                            value="Asia/Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Seoul' ? 'selected' : '' }}
                                                            value="Asia/Seoul">(GMT+09:00) Seoul</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Yakutsk' ? 'selected' : '' }}
                                                            value="Asia/Yakutsk">(GMT+09:00) Yakutsk</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Australia/Adelaide' ? 'selected' : '' }}
                                                            value="Australia/Adelaide">(GMT+09:30) Adelaide
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Australia/Darwin' ? 'selected' : '' }}
                                                            value="Australia/Darwin">(GMT+09:30) Darwin</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Australia/Brisbane' ? 'selected' : '' }}
                                                            value="Australia/Brisbane">(GMT+10:00) Brisbane
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Australia/Hobart' ? 'selected' : '' }}
                                                            value="Australia/Hobart">(GMT+10:00) Hobart</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Vladivostok' ? 'selected' : '' }}
                                                            value="Asia/Vladivostok">(GMT+10:00) Vladivostok
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Australia/Lord_Howe' ? 'selected' : '' }}
                                                            value="Australia/Lord_Howe">(GMT+10:30) Lord Howe
                                                            Island
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Etc/GMT-11' ? 'selected' : '' }}
                                                            value="Etc/GMT-11">(GMT+11:00) Solomon Is., New
                                                            Caledonia
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Magadan' ? 'selected' : '' }}
                                                            value="Asia/Magadan">(GMT+11:00) Magadan</option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Pacific/Norfolk' ? 'selected' : '' }}
                                                            value="Pacific/Norfolk">(GMT+11:30) Norfolk Island
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Asia/Anadyr' ? 'selected' : '' }}
                                                            value="Asia/Anadyr">(GMT+12:00) Anadyr, Kamchatka
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Pacific/Auckland' ? 'selected' : '' }}
                                                            value="Pacific/Auckland">(GMT+12:00) Auckland,
                                                            Wellington
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Etc/GMT-12' ? 'selected' : '' }}
                                                            value="Etc/GMT-12">(GMT+12:00) Fiji, Kamchatka,
                                                            Marshall
                                                            Is. </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Pacific/Chatham' ? 'selected' : '' }}
                                                            value="Pacific/Chatham">(GMT+12:45) Chatham Islands
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Pacific/Tongatapu' ? 'selected' : '' }}
                                                            value="Pacific/Tongatapu">(GMT+13:00) Nuku'alofa
                                                        </option>
                                                        <option
                                                            {{ @$settingdata->timezone == 'Pacific/Kiritimati' ? 'selected' : '' }}
                                                            value="Pacific/Kiritimati">(GMT+14:00) Kiritimati
                                                        </option>
                                                    </select>
                                                    @error('timezone')
                                                        <small class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                                @if (Auth::user()->type == 1)
                                                    @if (App\Models\SystemAddons::where('unique_identifier', 'vendor_app')->first() != null &&
                                                            App\Models\SystemAddons::where('unique_identifier', 'vendor_app')->first()->activated == 1)
                                                        <div class="form-group">
                                                            <label
                                                                class="form-label">{{ trans('labels.firebase_server_key') }}</label>
                                                            @if (env('Environment') == 'sendbox')
                                                                <span
                                                                    class="badge badge bg-danger ms-2 mb-0">{{ trans('labels.addon') }}</span>
                                                            @endif
                                                            <input type="text" class="form-control"
                                                                name="firebase_server_key"
                                                                value="{{ @$settingdata->firebase }}"
                                                                placeholder="{{ trans('labels.firebase_server_key') }}"
                                                                required>
                                                            @error('firebase_server_key')
                                                                <small class="text-danger">{{ $message }}</small> <br>
                                                            @enderror
                                                        </div>
                                                    @endif
                                                @endif
                                                <div class="form-group">
                                                    <label class="form-label">{{ trans('labels.website_title') }}<span
                                                            class="text-danger"> * </span></label>
                                                    <input type="text" class="form-control" name="website_title"
                                                        value="{{ @$settingdata->website_title }}"
                                                        placeholder="{{ trans('labels.website_title') }}">
                                                    @error('website_title')
                                                        <small class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">{{ trans('labels.copyright') }}<span
                                                            class="text-danger"> * </span></label>
                                                    <input type="text" class="form-control" name="copyright"
                                                        value="{{ @$settingdata->copyright }}"
                                                        placeholder="{{ trans('labels.copyright') }}">
                                                    @error('copyright')
                                                        <small class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            @if (Auth::user()->type == 2)
                                                @if (App\Models\SystemAddons::where('unique_identifier', 'unique_slug')->first() != null &&
                                                        App\Models\SystemAddons::where('unique_identifier', 'unique_slug')->first()->activated == 1)
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label">{{ trans('labels.personlized_link') }}<span
                                                                class="text-danger"> * </span></label>
                                                        @if (env('Environment') == 'sendbox')
                                                            <span
                                                                class="badge badge bg-danger ms-2 mb-0">{{ trans('labels.addon') }}</span>
                                                        @endif
                                                        <div class="input-group">
                                                            <span class="input-group-text">{{ URL::to('/') }}</span>
                                                            <input type="text" class="form-control" id="slug"
                                                                name="slug" value="{{ Auth::user()->slug }}"
                                                                required>
                                                        </div>
                                                        @error('slug')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                @endif
                                            @endif
                                            @if (Auth::user()->type == 2)
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label">{{ trans('labels.contact_email') }}<span
                                                                class="text-danger"> * </span></label>
                                                        <input type="email" class="form-control" name="email"
                                                            value="{{ @$settingdata->email }}"
                                                            placeholder="{{ trans('labels.contact_email') }}" required>
                                                        @error('email')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label">{{ trans('labels.contact_mobile') }}<span
                                                                class="text-danger"> * </span></label>
                                                        <input type="number" class="form-control" name="contact_mobile"
                                                            value="{{ @$settingdata->contact }}"
                                                            placeholder="{{ trans('labels.contact_mobile') }}" required>
                                                        @error('contact_mobile')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label class="form-label">{{ trans('labels.address') }}<span
                                                                class="text-danger"> * </span></label>
                                                        <textarea class="form-control" name="address" rows="3" placeholder="{{ trans('labels.address') }}">{{ @$settingdata->address }}</textarea>
                                                        @error('address')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label">{{ trans('labels.delivery_type') }}<span
                                                                class="text-danger"> * </span></label>
                                                        <select class="form-select" name="delivery_type">
                                                            <option value="" selected disabled>
                                                                {{ trans('labels.select') }}
                                                            </option>
                                                            <option value="delivery"
                                                                {{ @$settingdata->delivery_type == 'delivery' ? 'selected' : '' }}>
                                                                {{ trans('labels.delivery') }}
                                                            </option>
                                                            <option value="pickup"
                                                                {{ @$settingdata->delivery_type == 'pickup' ? 'selected' : '' }}>
                                                                {{ trans('labels.pickup') }}
                                                            </option>
                                                            <option value="both"
                                                                {{ @$settingdata->delivery_type == 'both' ? 'selected' : '' }}>
                                                                {{ trans('labels.both') }}
                                                            </option>
                                                        </select>
                                                        @error('delivery_type')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label">{{ trans('labels.facebook_link') }}</label>
                                                        <input type="text" class="form-control" name="facebook_link"
                                                            value="{{ @$settingdata->facebook_link }}"
                                                            placeholder="{{ trans('labels.facebook_link') }}">
                                                        @error('facebook_link')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label">{{ trans('labels.twitter_link') }}</label>
                                                        <input type="text" class="form-control" name="twitter_link"
                                                            value="{{ @$settingdata->twitter_link }}"
                                                            placeholder="{{ trans('labels.twitter_link') }}">
                                                        @error('twitter_link')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label">{{ trans('labels.instagram_link') }}</label>
                                                        <input type="text" class="form-control" name="instagram_link"
                                                            value="{{ @$settingdata->instagram_link }}"
                                                            placeholder="{{ trans('labels.instagram_link') }}">
                                                        @error('instagram_link')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label">{{ trans('labels.linkedin_link') }}</label>
                                                        <input type="text" class="form-control" name="linkedin_link"
                                                            value="{{ @$settingdata->linkedin_link }}"
                                                            placeholder="{{ trans('labels.linkedin_link') }}">
                                                        @error('linkedin_link')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-label">{{ trans('labels.description') }}<span
                                                                class="text-danger"> * </span></label>
                                                        <textarea class="form-control" name="description" rows="3" placeholder="{{ trans('labels.description') }}">{{ @$settingdata->description }}</textarea>
                                                        @error('description')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row justify-content-between">
                                                            <label class="col-auto col-form-label"
                                                                for="">{{ trans('labels.footer_features') }}
                                                                <span class="" data-bs-toggle="tooltip"
                                                                    data-bs-placement="top"
                                                                    title="Ex. <i class='fa-solid fa-truck-fast'></i> Visit https://fontawesome.com/ for more info">
                                                                    <i class="fa-solid fa-circle-info"></i> </span></label>
                                                            @if (count($getfooterfeatures) > 0)
                                                                <span class="col-auto"><button
                                                                        class="btn btn-sm btn-outline-info" type="button"
                                                                        onclick="add_features('{{ trans('labels.icon') }}','{{ trans('labels.title') }}','{{ trans('labels.description') }}')">
                                                                        {{ trans('labels.add_new') }}
                                                                        {{ trans('labels.footer_features') }} <i
                                                                            class="fa-sharp fa-solid fa-plus"></i></button></span>
                                                            @endif
                                                        </div>
                                                        @forelse ($getfooterfeatures as $key => $features)
                                                            <div class="row">
                                                                <input type="hidden" name="edit_icon_key[]"
                                                                    value="{{ $features->id }}">
                                                                <div class="col-md-3 form-group">
                                                                    <div class="input-group">
                                                                        <input type="text"
                                                                            class="form-control feature_required  {{ session()->get('direction') == 2 ? 'input-group-rtl' : '' }}"
                                                                            onkeyup="show_feature_icon(this)"
                                                                            name="edi_feature_icon[{{ $features->id }}]"
                                                                            placeholder="{{ trans('labels.icon') }}"
                                                                            value="{{ $features->icon }}" required>
                                                                        <p
                                                                            class="input-group-text {{ session()->get('direction') == 2 ? 'input-group-icon-rtl' : '' }}">
                                                                            {!! $features->icon !!}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 form-group">
                                                                    <input type="text" class="form-control"
                                                                        name="edi_feature_title[{{ $features->id }}]"
                                                                        placeholder="{{ trans('labels.title') }}"
                                                                        value="{{ $features->title }}" required>
                                                                </div>
                                                                <div class="col-md-5 form-group">
                                                                    <input type="text" class="form-control"
                                                                        name="edi_feature_description[{{ $features->id }}]"
                                                                        placeholder="{{ trans('labels.description') }}"
                                                                        value="{{ $features->description }}" required>
                                                                </div>
                                                                <div class="col-md-1 form-group">
                                                                    <button class="btn btn-danger" type="button"
                                                                        onclick="statusupdate('{{ URL::to('admin/settings/delete-feature-' . $features->id) }}')">
                                                                        <i class="fa fa-trash"></i> </button>
                                                                </div>
                                                            </div>
                                                        @empty
                                                            <div class="row">
                                                                <div class="col-md-3 form-group">
                                                                    <div class="input-group">
                                                                        <input type="text"
                                                                            class="form-control feature_required"
                                                                            onkeyup="show_feature_icon(this)"
                                                                            name="feature_icon[]"
                                                                            placeholder="{{ trans('labels.icon') }}">
                                                                        <p class="input-group-text"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 form-group">
                                                                    <input type="text"
                                                                        class="form-control feature_required"
                                                                        name="feature_title[]"
                                                                        placeholder="{{ trans('labels.title') }}"
                                                                        required>
                                                                </div>
                                                                <div class="col-md-5 form-group">
                                                                    <input type="text"
                                                                        class="form-control feature_required"
                                                                        name="feature_description[]"
                                                                        placeholder="{{ trans('labels.description') }}"
                                                                        required>
                                                                </div>
                                                                <div class="col-md-1 form-group">
                                                                    <button class="btn btn-info" type="button"
                                                                        onclick="add_features('{{ trans('labels.icon') }}','{{ trans('labels.title') }}','{{ trans('labels.description') }}')">
                                                                        <i class="fa-sharp fa-solid fa-plus"></i> </button>
                                                                </div>
                                                            </div>
                                                        @endforelse
                                                        <span class="extra_footer_features"></span>
                                                    </div>
                                                </div>
                                            @endif

                                            @if (Auth::user()->type == 2)
                                                <div class="form-group col-sm-6">
                                                    <label class="form-label">{{ trans('labels.banner') }}<small>(1920
                                                            x 400)</small></label>
                                                    <input type="file" class="form-control" name="banner">
                                                    @error('banner')
                                                        <small class="text-danger">{{ $message }}</small> <br>
                                                    @enderror
                                                    <img class="img-fluid rounded hw-70 mt-1 object-fit-cover"
                                                        src="{{ helper::image_path(@$settingdata->banner) }}"
                                                        alt="">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label
                                                        class="form-label">{{ trans('labels.landing_page_cover_image') }}
                                                        (650 x 300)
                                                    </label>
                                                    <input type="file" class="form-control"
                                                        name="landin_page_cover_image">
                                                    @error('landin_page_cover_image')
                                                        <small class="text-danger">{{ $message }}</small> <br>
                                                    @enderror
                                                    <img class="img-fluid rounded hw-70 mt-1 object-fit-cover"
                                                        src="{{ helper::image_path($settingdata->cover_image) }}"
                                                        alt="">
                                                </div>
                                                @if (App\Models\SystemAddons::where('unique_identifier', 'notification')->first() != null &&
                                                        App\Models\SystemAddons::where('unique_identifier', 'notification')->first()->activated == 1)
                                                    <div class="form-group col-md-6">
                                                        <label
                                                            class="form-label">{{ trans('labels.notification_sound') }}</label>
                                                        @if (env('Environment') == 'sendbox')
                                                            <span
                                                                class="badge badge bg-danger ms-2 mb-0">{{ trans('labels.addon') }}</span>
                                                        @endif
                                                        <input type="file" class="form-control"
                                                            name="notification_sound">
                                                        @error('notification_sound')
                                                            <small class="text-danger">{{ $message }}</small><br>
                                                        @enderror
                                                        @if (!empty($settingdata->notification_sound) && $settingdata->notification_sound != null)
                                                            <audio controls class="mt-1">
                                                                <source
                                                                    src="{{ url(env('ASSETPATHURL') . 'admin-assets/notification/' . $settingdata->notification_sound) }}"
                                                                    type="audio/mpeg">
                                                            </audio>
                                                        @endif
                                                    </div>
                                                @endif
                                            @endif
                                            <div class="form-group text-end">
                                                <button class="btn btn-secondary"
                                                    @if (env('Environment') == 'sendbox') type="button" onclick="myFunction()" @else type="submit" name="updatebasicinfo" value="1" @endif>{{ trans('labels.save') }}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if (Auth::user()->type == 2)
                    <div id="themesettings">
                        <div class="row mb-5">
                            <div class="col-12">
                                <div class="card border-0 box-shadow">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center mb-3">
                                            <h5 class="text-uppercase">{{ trans('labels.theme_settings') }}</h5>
                                        </div>
                                        <form method="POST" action="{{ URL::to('admin/settings/updatetheme') }}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="form-label">{{ trans('labels.logo') }}
                                                                    <small>(250 x
                                                                        250)</small></label>
                                                                <input type="file" class="form-control"
                                                                    name="logo">
                                                                @error('logo')
                                                                    <small class="text-danger">{{ $message }}</small>
                                                                    <br>
                                                                @enderror
                                                                <img class="img-fluid rounded hw-70 mt-1 object-fit-contain"
                                                                    src="{{ helper::image_path(@$settingdata->logo) }}"
                                                                    alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="form-label">{{ trans('labels.favicon') }}
                                                                    (16 x
                                                                    16)</label>
                                                                <input type="file" class="form-control"
                                                                    name="favicon">
                                                                @error('favicon')
                                                                    <small class="text-danger">{{ $message }}</small>
                                                                    <br>
                                                                @enderror
                                                                <img class="img-fluid rounded hw-70 mt-1 object-fit-contain"
                                                                    src="{{ helper::image_path(@$settingdata->favicon) }}"
                                                                    alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label
                                                                    class="form-label">{{ trans('labels.primary_color') }}
                                                                    <span class="text-danger"> * </span> </label>
                                                                <input type="color"
                                                                    class="form-control form-control-color w-100 border-0"
                                                                    name="primary_color"
                                                                    value="{{ @$settingdata->primary_color }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label
                                                                    class="form-label">{{ trans('labels.secondary_color') }}
                                                                    <span class="text-danger"> * </span> </label>
                                                                <input type="color"
                                                                    class="form-control form-control-color w-100 border-0"
                                                                    name="secondary_color"
                                                                    value="{{ @$settingdata->secondary_color }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                    if (Auth::user()->allow_without_subscription == 1) {
                                                        $themes = explode(',', '1,2,3,4,5');
                                                    } else {
                                                        $themes = explode(',', @$settingdata->template);
                                                    }
                                                @endphp
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-label">{{ trans('labels.theme') }}
                                                            <span class="text-danger"> * </span> </label>
                                                        @if (env('Environment') == 'sendbox')
                                                            <span
                                                                class="badge badge bg-danger ms-2">{{ trans('labels.addon') }}</span>
                                                        @endif
                                                        <ul class="theme-selection">
                                                            @foreach ($themes as $item)
                                                                <li>
                                                                    <input type="radio" name="template"
                                                                        id="template{{ $item }}"
                                                                        value="{{ $item }}"
                                                                        {{ @$settingdata->template == $item ? 'checked' : '' }}>
                                                                    <label for="template{{ $item }}">
                                                                        <img
                                                                            src="{{ helper::image_path('theme-' . $item . '.png') }}">
                                                                    </label>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="form-group text-end">
                                                    <button class="btn btn-secondary"
                                                        @if (env('Environment') == 'sendbox') type="button" onclick="myFunction()" @else type="submit" @endif>{{ trans('labels.save') }}</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div id="editprofile">
                    <div class="row mb-5">
                        <div class="col-12">
                            <div class="card border-0 box-shadow">
                                <div class="card-body">
                                    <div class="d-flex align-items-center mb-3">
                                        <h5 class="text-uppercase">{{ trans('labels.edit_profile') }}</h5>
                                    </div>
                                    <form method="POST"
                                        action="{{ URL::to('admin/settings/update-profile-' . Auth::user()->slug) }}"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label class="form-label">{{ trans('labels.name') }}<span
                                                        class="text-danger"> * </span></label>
                                                <input type="text" class="form-control" name="name"
                                                    value="{{ Auth::user()->name }}"
                                                    placeholder="{{ trans('labels.name') }}" required>
                                                @error('name')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="form-label">{{ trans('labels.email') }}<span
                                                        class="text-danger"> * </span></label>
                                                <input type="email" class="form-control" name="email"
                                                    value="{{ Auth::user()->email }}"
                                                    placeholder="{{ trans('labels.email') }}" required>
                                                @error('email')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="form-label"
                                                    for="mobile">{{ trans('labels.mobile') }}<span
                                                        class="text-danger"> * </span></label>
                                                <input type="number" class="form-control" name="mobile" id="mobile"
                                                    value="{{ Auth::user()->mobile }}"
                                                    placeholder="{{ trans('labels.mobile') }}" required>
                                                @error('mobile')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="form-label">{{ trans('labels.image') }}<small>(250
                                                        x 250)</small></label>
                                                <input type="file" class="form-control" name="profile">
                                                @error('profile')
                                                    <span class="text-danger">{{ $message }}</span> <br>
                                                @enderror
                                                <img class="img-fluid rounded hw-70 mt-1"
                                                    src="{{ helper::image_Path(Auth::user()->image) }}" alt="">
                                            </div>
                                            @if (Auth::user()->type == 2)
                                                <div class="form-group col-md-6">
                                                    <label for="country"
                                                        class="form-label">{{ trans('labels.country') }}<span
                                                            class="text-danger"> * </span></label>
                                                    <select name="country" class="form-select" id="country" required>
                                                        <option value="">{{ trans('labels.select') }}</option>
                                                        @foreach ($countries as $country)
                                                            <option value="{{ $country->id }}"
                                                                {{ $country->id == Auth::user()->country_id ? 'selected' : '' }}>
                                                                {{ $country->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="city"
                                                        class="form-label">{{ trans('labels.city') }}<span
                                                            class="text-danger">
                                                            * </span></label>
                                                    <select name="city" class="form-select" id="city" required>
                                                        <option value="">{{ trans('labels.select') }}</option>
                                                    </select>
                                                </div>
                                            @endif
                                            <div class="form-group text-end">
                                                <button class="btn btn-secondary"
                                                    @if (env('Environment') == 'sendbox') type="button" onclick="myFunction()" @else type="submit" name="updateprofile" value="1" @endif>{{ trans('labels.save') }}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="changepasssword">
                    <div class="row mb-5">
                        <div class="col-12">
                            <div class="card border-0 box-shadow">
                                <div class="card-body">
                                    <div class="d-flex align-items-center mb-3">
                                        <h5 class="text-uppercase">{{ trans('labels.change_password') }}</h5>
                                    </div>
                                    <form action="{{ URL::to('admin/settings/change-password') }}" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="form-group col-sm-12">
                                                <label class="form-label">{{ trans('labels.current_password') }}<span
                                                        class="text-danger"> * </span></label>
                                                <input type="password" class="form-control" name="current_password"
                                                    value="{{ old('current_password') }}"
                                                    placeholder="{{ trans('labels.current_password') }}" required>
                                                @error('current_password')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="form-label">{{ trans('labels.new_password') }}<span
                                                        class="text-danger"> * </span></label>
                                                <input type="password" class="form-control" name="new_password"
                                                    value="{{ old('new_password') }}"
                                                    placeholder="{{ trans('labels.new_password') }}" required>
                                                @error('new_password')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="form-label">{{ trans('labels.confirm_password') }}<span
                                                        class="text-danger"> * </span></label>
                                                <input type="password" class="form-control" name="confirm_password"
                                                    value="{{ old('confirm_password') }}"
                                                    placeholder="{{ trans('labels.confirm_password') }}" required>
                                                @error('confirm_password')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group text-end">
                                                <button class="btn btn-secondary"
                                                    @if (env('Environment') == 'sendbox') type="button" onclick="myFunction()" @else type="submit" @endif>{{ trans('labels.save') }}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="seo">
                    <div class="row mb-5">
                        <div class="col-12">
                            <div class="card border-0 box-shadow">
                                <div class="card-body">
                                    <div class="d-flex align-items-center mb-3">
                                        <h5 class="text-uppercase">{{ trans('labels.seo') }}</h5>
                                    </div>
                                    <form action="{{ URL::to('admin/settings/updateseo') }}" method="POST"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="form-label">{{ trans('labels.meta_title') }}<span
                                                        class="text-danger"> * </span></label>
                                                <input type="text" class="form-control" name="meta_title"
                                                    value="{{ @$settingdata->meta_title }}"
                                                    placeholder="{{ trans('labels.meta_title') }}" required>
                                                @error('meta_title')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">{{ trans('labels.meta_description') }}<span
                                                        class="text-danger"> * </span></label>
                                                <textarea class="form-control" name="meta_description" rows="3"
                                                    placeholder="{{ trans('labels.meta_description') }}" required>{{ @$settingdata->meta_description }}</textarea>
                                                @error('meta_description')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">{{ trans('labels.og_image') }} (1200 x
                                                    650) <span class="text-danger"> * </span></label>
                                                <input type="file" class="form-control" name="og_image">
                                                @error('og_image')
                                                    <span class="text-danger">{{ $message }}</span> <br>
                                                @enderror
                                                <img class="img-fluid rounded hw-70 mt-1 object-fit-cover"
                                                    src="{{ helper::image_Path(@$settingdata->og_image) }}"
                                                    alt="">
                                            </div>
                                            <div class="form-group text-end">
                                                <button class="btn btn-secondary"
                                                    @if (env('Environment') == 'sendbox') type="button" onclick="myFunction()" @else type="submit" @endif>{{ trans('labels.save') }}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if (Auth::user()->type == 2)
                    @if (App\Models\SystemAddons::where('unique_identifier', 'whatsapp_message')->first() != null &&
                            App\Models\SystemAddons::where('unique_identifier', 'whatsapp_message')->first()->activated == 1)
                        <div id="whatsappmessagesettings">
                            <div class="row mb-5">
                                <div class="col-12">
                                    <div class="card border-0 box-shadow">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center mb-3">
                                                <h5 class="text-uppercase">
                                                    {{ trans('labels.whatsapp_message_settings') }}
                                                </h5>
                                            </div>
                                            <form method="POST"
                                                action="{{ URL::to('admin/settings/whatsapp_update') }}"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label
                                                                class="form-label fw-bold">{{ trans('labels.order_variable') }}
                                                            </label>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <ul class="list-group list-group-flush">
                                                                        <li class="list-group-item px-0">Order No :
                                                                            <code>{order_no}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Payment type :
                                                                            <code>{payment_type}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Subtotal :
                                                                            <code>{sub_total}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Total Tax :
                                                                            <code>{total_tax}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Delivery
                                                                            charge : <code>{delivery_charge}</code></li>
                                                                        <li class="list-group-item px-0">Discount
                                                                            amount : <code>{discount_amount}</code></li>
                                                                        <li class="list-group-item px-0">Grand total :
                                                                            <code>{grand_total}</code>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <ul class="list-group list-group-flush">
                                                                        <li class="list-group-item px-0">Customer name
                                                                            : <code>{customer_name}</code></li>
                                                                        <li class="list-group-item px-0">Customer
                                                                            mobile : <code>{customer_mobile}</code></li>
                                                                        <li class="list-group-item px-0">Address :
                                                                            <code>{address}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Building :
                                                                            <code>{building}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Landmark :
                                                                            <code>{landmark}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Postal code :
                                                                            <code>{postal_code}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Delivery type
                                                                            : <code>{delivery_type}</code></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <ul class="list-group list-group-flush">
                                                                        <li class="list-group-item px-0">Notes :
                                                                            <code>{notes}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Item Variable
                                                                            : <code>{item_variable}</code></li>
                                                                        <li class="list-group-item px-0">Time :
                                                                            <code>{time}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Date :
                                                                            <code>{date}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Store name :
                                                                            <code>{store_name}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Store URL :
                                                                            <code>{store_url}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Track order
                                                                            URL : <code>{track_order_url}</code></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label
                                                                class="form-label fw-bold">{{ trans('labels.item_variable') }}
                                                            </label>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <ul class="list-group list-group-flush">
                                                                        <li class="list-group-item px-0">Item name :
                                                                            <code>{item_name}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">QTY :
                                                                            <code>{qty}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Variants :
                                                                            <code>{variantsdata}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Item price :
                                                                            <code>{item_price}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">
                                                                            <input type="text" name="item_message"
                                                                                class="form-control"
                                                                                placeholder="{{ trans('labels.item_message') }}"
                                                                                value="{{ @$settingdata->item_message }}">
                                                                            @error('item_message')
                                                                                <span class="text-danger"
                                                                                    id="timezone_error">{{ $message }}</span>
                                                                            @enderror
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label
                                                                class="form-label fw-bold">{{ trans('labels.whatsapp_message') }}
                                                                <span class="text-danger"> * </span> </label>
                                                            <textarea class="form-control" required="required" name="whatsapp_message" cols="50" rows="10">{{ @$settingdata->whatsapp_message }}</textarea>
                                                            @error('whatsapp_message')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-label">{{ trans('labels.contact') }}<span
                                                                    class="text-danger"> * </span></label>
                                                            <input type="text" class="form-control numbers_only"
                                                                name="contact" value="{{ @$settingdata->contact }}"
                                                                placeholder="{{ trans('labels.contact') }}" required>
                                                            @error('contact')
                                                                <small class="text-danger">{{ $message }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group text-end">
                                                        <button class="btn btn-secondary"
                                                            @if (env('Environment') == 'sendbox') type="button" onclick="myFunction()" @else type="submit" @endif>{{ trans('labels.save') }}</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
                @if (Auth::user()->type == 1)
                    @if (App\Models\SystemAddons::where('unique_identifier', 'custom_domain')->first() != null &&
                            App\Models\SystemAddons::where('unique_identifier', 'custom_domain')->first()->activated)
                        <div id="custom_domain">
                            <div class="row mb-5">
                                <div class="col-12">
                                    <div class="card border-0 box-shadow">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center mb-3">
                                                <h5 class="text-uppercase">{{ trans('labels.custom_domain') }}</h5>
                                            </div>
                                            <form method="POST" action="{{ URL::to('admin/settings/updatecustomedomain') }}"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label
                                                                class="form-label">{{ trans('labels.cname_section_title') }}
                                                                <span class="text-danger"> * </span> </label>
                                                            <input type="text" class="form-control" name="cname_title"
                                                                required value="{{ @$settingdata->cname_title }}">
                                                            @error('cname_title')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label
                                                                class="form-label">{{ trans('labels.cname_section_text') }}
                                                                <span class="text-danger"> * </span> </label>
                                                            <textarea class="form-control" rows="3" id="cname_text" required name="cname_text">{{ @$settingdata->cname_text }}</textarea>
                                                            @error('cname_text')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group text-end">
                                                        <button class="btn btn-secondary"
                                                            @if (env('Environment') == 'sendbox') type="button" onclick="myFunction()" @else type="submit"  @endif>{{ trans('labels.save') }}</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if (App\Models\SystemAddons::where('unique_identifier', 'google_analytics')->first() != null &&
                            App\Models\SystemAddons::where('unique_identifier', 'google_analytics')->first()->activated)
                        <div id="google_analytics">
                            <div class="row mb-5">
                                <div class="col-12">
                                    <div class="card border-0 box-shadow">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center mb-3">
                                                <h5 class="text-uppercase">{{ trans('labels.google_analytics') }}</h5>
                                            </div>
                                            <form method="POST" action="{{ URL::to('admin/settings/updateanalytics') }}"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="form-label">{{ trans('labels.tracking_id') }}
                                                                <span class="text-danger"> * </span> </label>
                                                            <input type="text" class="form-control" name="tracking_id"
                                                                required value="{{ @$settingdata->tracking_id }}">
                                                            @error('tracking_id')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="form-label">{{ trans('labels.view_id') }} <span
                                                                    class="text-danger"> * </span> </label>
                                                            <input type="text" class="form-control" name="view_id"
                                                                required value="{{ @$settingdata->view_id }}">
                                                            @error('view_id')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group text-end">
                                                        <button class="btn btn-secondary"
                                                            @if (env('Environment') == 'sendbox') type="button" onclick="myFunction()" @else type="submit" @endif>{{ trans('labels.save') }}</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div id="landing_page">
                        <div class="row mb-5">
                            <div class="col-12">
                                <div class="card border-0 box-shadow">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center mb-3">
                                            <h5 class="text-uppercase">{{ trans('labels.landing_page') }}</h5>
                                        </div>
                                        <form action="{{ URL::to('/admin/landingsettings') }}" method="POST"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label class="form-label">{{ trans('labels.logo') }} <small>(250 x
                                                            250)</small></label>
                                                    <input type="file" class="form-control" name="logo">
                                                    @error('logo')
                                                        <small class="text-danger">{{ $message }}</small> <br>
                                                    @enderror
                                                    <img class="img-fluid rounded hw-70 mt-1 object-fit-contain"
                                                        src="{{ helper::image_path(@$settingdata->logo) }}"
                                                        alt="">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label class="form-label">{{ trans('labels.favicon') }} (16 x
                                                        16)</label>
                                                    <input type="file" class="form-control" name="favicon">
                                                    @error('favicon')
                                                        <small class="text-danger">{{ $message }}</small> <br>
                                                    @enderror
                                                    <img class="img-fluid rounded hw-70 mt-1 object-fit-contain"
                                                        src="{{ helper::image_path(@$settingdata->favicon) }}"
                                                        alt="">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label class="form-label">{{ trans('labels.primary_color') }}</label>
                                                    <input type="color"
                                                        class="form-control form-control-color w-100 border-0"
                                                        name="landing_primary_color"
                                                        value="{{ $settingdata->primary_color }}">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label
                                                        class="form-label">{{ trans('labels.secondary_color') }}</label>
                                                    <input type="color"
                                                        class="form-control form-control-color w-100 border-0"
                                                        name="landing_secondary_color"
                                                        value="{{ $settingdata->secondary_color }}">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="form-label">{{ trans('labels.contact_email') }}<span
                                                            class="text-danger"> * </span></label>
                                                    <input type="email" class="form-control" name="landing_email"
                                                        value="{{ @$settingdata->email }}"
                                                        placeholder="{{ trans('labels.contact_email') }}" required>
                                                    @error('landing_email')
                                                        <small class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="form-label">{{ trans('labels.contact_mobile') }}<span
                                                            class="text-danger"> * </span></label>
                                                    <input type="number" class="form-control" name="landing_mobile"
                                                        value="{{ @$settingdata->contact }}"
                                                        placeholder="{{ trans('labels.contact_mobile') }}" required>
                                                    @error('contact_mobile')
                                                        <small class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">{{ trans('labels.address') }}<span
                                                            class="text-danger"> * </span></label>
                                                    <textarea class="form-control" name="landing_address" rows="3" placeholder="{{ trans('labels.address') }}">{{ @$settingdata->address }}</textarea>
                                                    @error('address')
                                                        <small class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label">{{ trans('labels.facebook_link') }}</label>
                                                        <input type="text" class="form-control"
                                                            name="landing_facebook_link"
                                                            value="{{ @$settingdata->facebook_link }}"
                                                            placeholder="{{ trans('labels.facebook_link') }}">
                                                        @error('facebook_link')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label">{{ trans('labels.twitter_link') }}</label>
                                                        <input type="text" class="form-control"
                                                            name="landing_twitter_link"
                                                            value="{{ @$settingdata->twitter_link }}"
                                                            placeholder="{{ trans('labels.twitter_link') }}">
                                                        @error('twitter_link')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label">{{ trans('labels.instagram_link') }}</label>
                                                        <input type="text" class="form-control"
                                                            name="landing_instagram_link"
                                                            value="{{ @$settingdata->instagram_link }}"
                                                            placeholder="{{ trans('labels.instagram_link') }}">
                                                        @error('instagram_link')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label">{{ trans('labels.linkedin_link') }}</label>
                                                        <input type="text" class="form-control"
                                                            name="landing_linkedin_link"
                                                            value="{{ @$settingdata->linkedin_link }}"
                                                            placeholder="{{ trans('labels.linkedin_link') }}">
                                                        @error('linkedin_link')
                                                            <small class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-end">
                                                <button
                                                    @if (env('Environment') == 'sendbox') type="button" onclick="myFunction()" @else type="submit" @endif
                                                    class="btn btn-secondary">{{ trans('labels.save') }}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var cityurl = "{{ URL::to('admin/getcity') }}";
        var select = "{{ trans('labels.select') }}";
        var cityid = "{{ Auth::user()->city_id != null ? Auth::user()->city_id : '0' }}";
    </script>
    <script src="{{ url('storage/app/public/admin-assets/js/user.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.12.1/ckeditor.js"></script>
    <script src="{{ url(env('ASSETPATHURL') . 'admin-assets/js/settings.js') }}"></script>
@endsection
