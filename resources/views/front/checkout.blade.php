@include('front.theme.header')
<div class="cart-sec">
    <h2 class="cart-title pb-5 text-center m-auto {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
        {{ trans('labels.checkout') }}
    </h2>
    <div class="container">
        <div class="mb-5">
            @if (count($cartdata) == 0)
                @include('admin.layout.no_data')
            @else
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="yourcart-sec">
                    {{-- customer-title --}}
                    @foreach ($cartdata as $cart)
                        <?php
                        $data[] = [
                            'total_price' => $cart->qty * $cart->price,
                            'tax' => ($cart->qty * $cart->price * $cart->tax) / 100,
                        ];
                        ?>
                    @endforeach
                    <center>
                        <div class="mb-4 delivery-option @if (helper::appdata($storeinfo->id)->delivery_type != 'both') d-none @endif ">
                            <div class="delivery-title ">
                                <h5 class="text-center ">{{ trans('labels.delivery_option') }}</h5>
                            </div>
                            <div class="d-flex justify-content-center cart-delivery-type open">
                                @if (helper::appdata($storeinfo->id)->delivery_type == 'delivery')
                                    <label for="cart-delivery">
                                        <input type="radio" name="cart-delivery" id="cart-delivery" checked
                                            value="1">
                                    </label>
                                @endif
                                @if (helper::appdata($storeinfo->id)->delivery_type == 'pickup')
                                    <label for="cart-pickup">
                                        <input type="radio" name="cart-delivery" id="cart-pickup" checked
                                            value="2">
                                    </label>
                                @endif
                                @if (helper::appdata($storeinfo->id)->delivery_type == 'both')
                                    <label for="cart-delivery" class="col-md-4">
                                        <input type="radio" name="cart-delivery" id="cart-delivery" checked
                                            value="1">
                                        <div class="pro-order">
                                            <img src="{{ asset('storage/app/public/images/delivery.png') }}"
                                                alt="">
                                            <h5 class="mt-2">{{ trans('labels.delivery') }}</h5>
                                        </div>
                                    </label>
                                    <label for="cart-pickup" class="col-md-4">
                                        <input type="radio" name="cart-delivery" id="cart-pickup" value="2">
                                        <div class="pro-order">
                                            <img src="{{ asset('storage/app/public/images/takeaway.png') }}"
                                                alt="">
                                            <h5 class="mt-2">{{ trans('labels.pickup') }}</h5>
                                        </div>
                                    </label>
                                @endif
                            </div>
                        </div>
                    </center>
                    <div class="card mb-4 card-shadow">
                        <div class="card-body">
                            <h5 class="customer-title {{ session()->get('direction') == 2 ? 'text-right' : '' }}">
                                {{ trans('labels.date_time') }}</h5>
                            <div class="row">
                                <div
                                    class="delivery-date col-md-6 {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                                    <label id="delivery_date"
                                        class="justify-content-start">{{ trans('labels.delivery_date') }}</label>
                                    <label id="pickup_date" style="display: none;">
                                        {{ trans('labels.pickup_date') }}</label>
                                    <input type="date" class="form-control" name="delivery_date"
                                        value="{{ old('delivery_date') }}" id="delivery_dt">
                                </div>
                                <div
                                    class="delivery-time col-md-6 {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                                    <label id="delivery"
                                        class="justify-content-start">{{ trans('labels.delivery_time') }}</label>
                                    <label id="pickup" class="justify-content-start"
                                        style="display: none;">{{ trans('labels.pickup_time') }}</label>
                                    <label id="store_close"
                                        class="d-none text-danger">{{ trans('labels.today_store_closed') }}</label>
                                    <select name="delivery_time" id="delivery_time" class="form-select">
                                        <option value="{{ old('delivery_time') }}">{{ trans('labels.select') }}
                                        </option>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-4 card-shadow" id="open">
                        <div class="card-body">
                            <h5 class="customer-title {{ session()->get('direction') == 2 ? 'text-right' : '' }}">
                                {{ trans('labels.delivery_info') }}</h5>
                            <div class="row delivery-sec {{ session()->get('direction') == 2 ? 'text-right' : '' }}">
                                <div class="col-12">
                                    <label for="delivery_area"
                                        class="form-label d-flex justify-content-start">{{ trans('labels.delivery_area') }}</label>
                                    <select name="delivery_area" id="delivery_area" class="form-control">
                                        <option value=""price="{{ 0 }}">
                                            {{ trans('labels.select') }}</option>
                                        @foreach ($deliveryarea as $area)
                                            <option value="{{ $area->name }}" price="{{ $area->price }}">
                                                {{ $area->name }} -
                                                {{ helper::currency_formate($area->price, $storeinfo->id) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mt-3 col-md-6">
                                    <label for="address"
                                        class="form-label d-flex justify-content-start">{{ trans('labels.address') }}</label>
                                    <input type="text" class="form-control"
                                        placeholder="{{ trans('labels.address') }}"
                                        @if (env('Environment') == 'sendbox') value="1112 4th Ave" @else value="{{ old('address') }}" @endif
                                        name="address" id="address" required="">
                                    <label for="building"
                                        class="form-label d-flex justify-content-start">{{ trans('labels.building') }}</label>
                                    <input type="text" class="form-control"
                                        placeholder="{{ trans('labels.building') }}" name="building"
                                        @if (env('Environment') == 'sendbox') value="Seattle" @else value="{{ old('building') }}" @endif
                                        id="building" required="">
                                </div>
                                <div class="mt-3 col-md-6">
                                    <label for="landmark"
                                        class="form-label d-flex justify-content-start">{{ trans('labels.landmark') }}</label>
                                    <input type="text" class="form-control"
                                        placeholder="{{ trans('labels.landmark') }}" name="landmark"
                                        @if (env('Environment') == 'sendbox') value="Washington" @else value="{{ old('landmark') }}" @endif
                                        id="landmark" required="">
                                    <label for="postal_code"
                                        class="form-label d-flex justify-content-start">{{ trans('labels.pincode') }}</label>
                                    <input type="text" class="form-control"
                                        placeholder="{{ trans('labels.pincode') }}" id="postal_code"
                                        @if (env('Environment') == 'sendbox') value="98101" @else 
                                    value="{{ old('pincode') }}" @endif
                                        name="postal_code" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-4 card-shadow">
                        <div class="card-body">
                            <h5 class="customer-title {{ session()->get('direction') == 2 ? 'text-right' : '' }}">
                                {{ trans('labels.customer') }}</h5>
                            <form>
                                <div class="row {{ session()->get('direction') == 2 ? 'text-right' : '' }}">
                                    <div class="mb-3 col-md-6">
                                        <label for="customer_name"
                                            class="form-label d-flex justify-content-start">{{ trans('labels.name') }}</label>
                                        <input type="text" placeholder="{{ trans('labels.name') }}"
                                            class="form-control" name="customer_name"
                                            @if (env('Environment') == 'sendbox') value="Etha Jaskolski"
                                     @else 
                                     value ="{{ @Auth::user() && @Auth::user()->type == 3 ? @Auth::user()->name : '' }}" @endif
                                            id="customer_name">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label for="customer_mobile"
                                            class="form-label d-flex justify-content-start">{{ trans('labels.mobile') }}</label>
                                        <input type="text" class="form-control"
                                            placeholder="{{ trans('labels.mobile') }}" name="customer_mobile"
                                            @if (env('Environment') == 'sendbox') value="937-267-4384" @else
                                        value="{{ @Auth::user() && @Auth::user()->type == 3 ? @Auth::user()->mobile : '' }}" @endif
                                            id="customer_mobile" onkeypress="return isNumber(event)">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label for="customer_email"
                                            class="form-label d-flex justify-content-start">{{ trans('labels.email') }}</label>
                                        <input type="email" class="form-control"
                                            placeholder="{{ trans('labels.email') }}" name="customer_email"
                                            @if (env('Environment') == 'sendbox') value="etha@gmail.com" @else 
                                        value="{{ @Auth::user() && @Auth::user()->type == 3 ? @Auth::user()->email : '' }}" @endif
                                            id="customer_email">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label for="notes"
                                            class="form-label d-flex justify-content-start">{{ trans('labels.note') }}</label>
                                        <textarea id="notes" name="notes" placeholder="{{ trans('labels.enter_order_note') }}" class="form-control"></textarea>
                                    </div>
                                </div>
                                <input type="hidden" id="vendor" name="vendor"
                                    value="{{ helper::storeinfo($storeinfo->slug)->id }}" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 payment-side">
                <div class="payment-side-position">
                    <?php
                    $sub_total = array_sum(array_column(@$data, 'total_price'));
                    $tax = array_sum(array_column(@$data, 'tax'));
                    $total = array_sum(array_column(@$data, 'total_price'));
                    ?>
                    @if (count($cartdata) == 0)
                        @php
                            $data = [];
                        @endphp
                    @else
                        @if (App\Models\SystemAddons::where('unique_identifier', 'coupon')->first() != null &&
                                App\Models\SystemAddons::where('unique_identifier', 'coupon')->first()->activated == 1)
                            @if (Session::has('offer_amount'))
                                <div class="promo-code my-3">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="d-flex align-items-center">
                                            <img src="{{ asset('storage/app/public/front/images/coupon.png') }}"
                                                alt="" class="cuppon-image mt-0">
                                            <div class="cuppon-text">
                                                <h5 class="m-0">{{ Session::get('offer_code') }} applied</h5>
                                            </div>
                                        </span>
                                        <a class="text-danger" onclick="RemoveCopon()">{{ trans('labels.remove') }}
                                        </a>
                                    </div>
                                </div>
                            @else
                                @if ($coupons->count() > 0)
                                    <div class="promo-code my-3">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal"
                                            class="d-flex justify-content-between align-items-center">
                                            <div class="d-flex ">
                                                <img src="{{ asset('storage/app/public/front/images/coupon.png') }}"
                                                    alt="" class="cuppon-image">
                                                <div class="cuppon-text">
                                                    <h5
                                                        class="m-0 {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                                                        {{ trans('labels.apply_coupon') }}</h5>
                                                    <p
                                                        class="text-muted m-0 {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                                                        {{ trans('labels.save_offer') }} </p>
                                                </div>
                                            </div>
                                            <div class="d-flex">
                                                <h5 class="cuppon-offer">{{ $coupons->count() }}
                                                    {{ trans('labels.offer') }}</h5>
                                            </div>
                                        </a>
                                    </div>
                                @endif
                            @endif
                        @endif
                    @endif
                    <div class="payment-sec my-3">
                        <h3 class="payment-title {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                            {{ trans('labels.payment_summary') }}</h3>
                        <div class="form form-a active">
                            <div class="total-sec">
                                <div class="sub-total justify-content-between " id="subtotal">
                                    <h6>{{ trans('labels.sub_total') }}</h6>
                                    <span>{{ helper::currency_formate($sub_total, $storeinfo->id) }}</span>
                                </div>
                                <div class="sub-total justify-content-between">
                                    @if ($tax == 0)
                                        <p class="d-none"></p>
                                    @else
                                        <h6>{{ trans('labels.tax') }}</h6>
                                        <span>{{ helper::currency_formate($tax, $storeinfo->id) }}</span>
                                    @endif
                                </div>
                                <div class="sub-total justify-content-between" id="delivery_charge_hide">
                                    <h6>{{ trans('labels.delivery_charge') }}</h6>
                                    <span
                                        id="shipping_charge">{{ helper::currency_formate('0.0', $storeinfo->id) }}</span>
                                </div>
                                @if (Session::has('offer_amount'))
                                    <p class="pro-total offer_amount">{{ trans('labels.discount') }}
                                        ({{ Session::get('offer_code') }})</span>
                                        <span id="offer_amount"> -
                                            {{ helper::currency_formate(Session::get('offer_amount'), $storeinfo->id) }}
                                        </span>
                                    </p>
                                @endif
                                @if (Session::has('offer_amount'))
                                    <p class="cart-total">{{ trans('labels.total_amount') }} <span id="total_amount">
                                            {{ helper::currency_formate($total + $tax - Session::get('offer_amount'), $storeinfo->id) }}
                                        </span></p>
                                @else
                                    <p class="cart-total">{{ trans('labels.total_amount') }} <span
                                            id="total_amount">{{ helper::currency_formate($total + $tax, $storeinfo->id) }}</span>
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="form form-b">
                            <div class="total-sec">
                                <div class="sub-total">
                                    <h6>{{ trans('labels.sub_total') }}</h6>
                                    <span>{{ helper::currency_formate($sub_total, $storeinfo->id) }}</span>
                                </div>
                            </div>
                            @if (Session::has('offer_amount'))
                                <p class="pro-total offer_amount">{{ trans('labels.discount') }}
                                    ({{ Session::get('offer_code') }})</span>
                                    <span id="offer_amount">
                                        -
                                        {{ helper::currency_formate(Session::get('offer_amount'), $storeinfo->id) }}
                                    </span>
                                </p>
                            @endif
                            @if (Session::has('offer_amount'))
                                <p class="cart-total">{{ trans('labels.total_amount') }} <span id="total_amount">
                                        {{ helper::currency_formate($total + $tax - Session::get('offer_amount'), $storeinfo->id) }}
                                    </span></p>
                            @else
                                <p class="cart-total">{{ trans('labels.total_amount') }} <span
                                        id="total_amount">{{ helper::currency_formate($total + $tax, $storeinfo->id) }}</span>
                                </p>
                            @endif
                        </div>
                        <input type="hidden" name="sub_total" id="sub_total" value="{{ $sub_total }}">
                        <input type="hidden" name="tax" id="tax" value="{{ $tax }}">
                        <input type="hidden" name="delivery_charge" id="delivery_charge" value="0">
                        @if (Session::has('offer_amount'))
                            <input type="hidden" name="grand_total" id="grand_total"
                                value="{{ number_format($total - Session::get('offer_amount'), 2) }}">
                            @php
                                $hidepayment = $total - Session::get('offer_amount');
                            @endphp
                        @else
                            <input type="hidden" name="grand_total" id="grand_total"
                                value="{{ number_format($total, 2) }}">
                            @php
                                $hidepayment = $total;
                            @endphp
                        @endif
                    </div>

                    <div class="customer-info mt-3 @if ($hidepayment <= 0) d-none @endif">
                        <h3 class="select-payment {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                            {{ trans('labels.payment_option') }}</h3>
                        <div class="row justify-content-center m-0">
                            @foreach ($paymentlist as $key => $payment)
                                <label class="form-check-label col-md-6 p-0" for="{{ $payment->payment_name }}">
                                    <input class="form-check-input payment-input" type="radio" name="payment"
                                        id="{{ $payment->payment_name }}"
                                        data-payment_type="{{strtolower($payment->payment_name)}}" name="payment"
                                        type="radio"
                                        @if (!$key) {!! 'checked' !!} @endif>
                                    <div class="payment-check">
                                        <img src="{{ helper::image_path($payment->image) }}" class="img-fluid"
                                            alt="" width="30px" />
                                        @if (strtolower($payment->payment_name) == 'razorpay')
                                            <input type="hidden" name="razorpay" id="razorpay"
                                                value="{{ $payment->public_key }}">
                                        @endif
                                        @if (strtolower($payment->payment_name) == 'stripe')
                                            <input type="hidden" name="stripe" id="stripe"
                                                value="{{ $payment->public_key }}">
                                        @endif
                                        @if (strtolower($payment->payment_name) == 'flutterwave')
                                            <input type="hidden" name="flutterwavekey" id="flutterwavekey"
                                                value="{{ $payment->public_key }}">
                                        @endif
                                        @if (strtolower($payment->payment_name) == 'paystack')
                                            <input type="hidden" name="paystackkey" id="paystackkey"
                                                value="{{ $payment->public_key }}">
                                        @endif
                                        <p>{{ $payment->payment_name }}</p>
                                    </div>
                                </label>
                            @endforeach
                        </div>
                    </div>
                    <div class="mt-3 whatsapp-order">
                        <button target="_blank" type="button" class="btn whatsapp-order-btn w-100"
                            onclick="Order()">{{ trans('labels.place_order') }}</button>
                    </div>

                </div>
            </div>
            @endif
        </div>
    </div>
</div>
<input type="hidden" id="delivery_time_required" value="{{ trans('messages.delivery_time_required') }}">
<input type="hidden" id="delivery_date_required" value="{{ trans('messages.delivery_date_required') }}">
<input type="hidden" id="address_required" value="{{ trans('messages.address_required') }}">
<input type="hidden" id="no_required" value="{{ trans('messages.no_required') }}">
<input type="hidden" id="landmark_required" value="{{ trans('messages.landmark_required') }}">
<input type="hidden" id="pincode_required" value="{{ trans('messages.pincode_required') }}">
<input type="hidden" id="delivery_area_required" value="{{ trans('messages.delivery_area') }}">
<input type="hidden" id="pickup_date_required" value="{{ trans('messages.pickup_date_required') }}">
<input type="hidden" id="pickup_time_required" value="{{ trans('messages.pickup_time_required') }}">
<input type="hidden" id="customer_mobile_required" value="{{ trans('messages.customer_mobile_required') }}">
<input type="hidden" id="customer_email_required" value="{{ trans('messages.customer_email_required') }}">
<input type="hidden" id="customer_name_required" value="{{ trans('messages.customer_name_required') }}">
<input type="hidden" id="currency" value="{{ helper::appdata($storeinfo->id)->currency }}">

<form action="{{ url('/orders/paypalrequest') }}" method="post" class="d-none">
    {{ csrf_field() }}
    <input type="hidden" name="return" value="2">
    <input type="submit" class="callpaypal" name="submit">
</form>
<input type="hidden" name="sloturl" id="sloturl" value="{{ URL::to($storeinfo->slug . '/timeslot') }}">
<input type="hidden" name="store_id" id="store_id" value="{{ $storeinfo->id }}">
@if (Session::has('offer_amount'))
    <input type="hidden" name="discount_amount" id="discount_amount" value="{{ Session::get('offer_amount') }}">
    <input type="hidden" name="couponcode" id="couponcode" value="{{ Session::get('offer_code') }}">
@else
    <input type="hidden" name="discount_amount" id="discount_amount" value="">
    <input type="hidden" name="couponcode" id="couponcode" value="">
@endif
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title" id="exampleModalLabel">{{ trans('labels.coupons_offers') }}</h5>
                <button type="button" class="close m-0 p-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="promo-wrap coupon-group">
                    <input class="form-control m-0" type="text"
                        placeholder="{{ trans('messages.enter_promocode') }}" name="promocode" id="promocode"
                        autocomplete="off" readonly>
                    <button class="btn apply-btn" onclick="ApplyCopon()">{{ trans('labels.apply') }}</button>
                </div>
                <div class="available-cuppon {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                    <h5 class="available-title" id="exampleModalLabel">{{ trans('labels.available_coupons') }}
                        {{-- <div class="sepreter-line mt-2"></div> --}}
                    </h5>
                </div>
                @foreach ($coupons as $coupon)
                    <div class="d-flex justify-content-between align-items-center coupon-btn mb-2">
                        <div class="coupon_codewrapper">
                            <div class="coupon_circle1"></div>
                            <div class="coupon_circle2"></div>
                            <div class="coupon_couponcode">
                                <span class="coupon_text">{{ $coupon->code }}</span>
                            </div>
                        </div>
                        <span class="coupon_text">{{ $coupon->name }}</span>
                        <button class="btn apply-btn"
                            onclick="$('#promocode').val('{{ $coupon->code }}')">{{ trans('labels.copy') }}</button>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@include('front.theme.footer')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="https://checkout.stripe.com/v2/checkout.js"></script>
<script src="https://checkout.flutterwave.com/v3.js"></script>
<script src="https://js.paystack.co/v1/inline.js"></script>
<script>
    $(document).ready(function() {
        $("input[name$='cart-delivery']").click(function() {
            var test = $(this).val();
            if (test == 1) {
                $("#open").show();
                $("#delivery_charge_hide").show();
                $("#delivery").show();
                $("#pickup").hide();
                $("#delivery_date").show();
                $("#pickup_date").hide();
                var sub_total = parseFloat($('#sub_total').val());
                var delivery_charge = parseFloat($('#delivery_charge').val());
                var tax = parseFloat($('#tax').val());
                var discount_amount = parseFloat($('#discount_amount').val());
                if (isNaN(discount_amount)) {
                    $('#total_amount').text(currency_formate(parseFloat(sub_total + tax +
                        delivery_charge)));
                    $('#grand_total').val((sub_total + tax + delivery_charge).toFixed(2));
                } else {
                    $('#total_amount').text(currency_formate(parseFloat(sub_total + tax +
                        delivery_charge - discount_amount)));
                    $('#grand_total').val((sub_total + tax + delivery_charge - discount_amount).toFixed(
                        2));
                }
            } else {
                $("#open").hide();
                $("#delivery_charge_hide").hide();
                $("#delivery").hide();
                $("#pickup").show();
                $("#delivery_date").hide();
                $("#pickup_date").show();
                var sub_total = parseFloat($('#sub_total').val());
                var delivery_charge = parseFloat($('#delivery_charge').val());
                var tax = parseFloat($('#tax').val());
                var discount_amount = parseFloat($('#discount_amount').val());
                if (isNaN(discount_amount)) {
                    $('#total_amount').text(currency_formate(parseFloat(sub_total + tax)));
                    $('#grand_total').val((sub_total + tax).toFixed(2));
                } else {
                    $('#total_amount').text(currency_formate(parseFloat(sub_total + tax -
                        discount_amount)));
                    $('#grand_total').val((sub_total + tax - discount_amount).toFixed(2));
                }
            }
        });
        if ("{{ helper::appdata($storeinfo->id)->delivery_type }}" != "both") {
            $(function() {
                $("input[name$='cart-delivery']").click();
            });
        }
    });
    $("#delivery_area").change(function() {
        var currency = parseFloat($('#currency').val());
        var deliverycharge = parseFloat($('option:selected', this).attr('price'));
        $('#shipping_charge').text(currency_formate(deliverycharge));
        $('#delivery_charge').val(deliverycharge);
        var sub_total = parseFloat($('#sub_total').val());
        var delivery_charge = parseFloat($('#delivery_charge').val());
        var tax = parseFloat($('#tax').val());
        var discount_amount = parseFloat($('#discount_amount').val());
        if (isNaN(discount_amount)) {
            $('#total_amount').text(currency_formate(parseFloat(sub_total + delivery_charge + tax)));
            $('#grand_total').val(((sub_total + delivery_charge + tax)).toFixed(2));
        } else {
            $('#total_amount').text(currency_formate(parseFloat(sub_total + delivery_charge + tax -
                discount_amount)));
            $('#grand_total').val(((sub_total + delivery_charge + tax - discount_amount)).toFixed(2));
        }
    });

    function Order() {
        var sub_total = parseFloat($('#sub_total').val());
        var tax = parseFloat($('#tax').val());
        var grand_total = parseFloat($('#grand_total').val());
        var delivery_time = $('#delivery_time').val();
        var delivery_date = $('#delivery_dt').val();
        var delivery_area = $('#delivery_area').val();
        var delivery_charge = parseFloat($('#delivery_charge').val());
        var discount_amount = parseFloat($('#discount_amount').val());
        var couponcode = $('#couponcode').val();
        var order_type = $("input:radio[name=cart-delivery]:checked").val();
        var address = $('#address').val();
        var postal_code = $('#postal_code').val();
        var building = $('#building').val();
        var landmark = $('#landmark').val();
        var notes = $('#notes').val();
        var customer_name = $('#customer_name').val();
        var customer_email = $('#customer_email').val();
        var customer_mobile = $('#customer_mobile').val();
        var vendor = $('#vendor').val();
        var payment_type = $('input[name="payment"]:checked').attr("data-payment_type");
        var flutterwavekey = $('#flutterwavekey').val();
        var paystackkey = $('#paystackkey').val();
        var mailformat = /^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/;
        if (order_type == "1") {
            if (delivery_date == "") {
                $('#ermsg').text($('#delivery_date_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (delivery_time == "") {
                $('#ermsg').text($('#delivery_time_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (delivery_area == "") {
                $('#ermsg').text($('#delivery_area_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (address == "") {
                $('#ermsg').text($('#address_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (landmark == "") {
                $('#ermsg').text($('#landmark_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (building == "") {
                $('#ermsg').text($('#no_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (postal_code == "") {
                $('#ermsg').text($('#pincode_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (customer_name == "") {
                $('#ermsg').text($('#customer_name_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (customer_mobile == "") {
                $('#ermsg').text($('#customer_mobile_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (customer_email == "") {
                $('#ermsg').text($('#customer_email_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (!validateEmail(customer_email)) {
                $('#ermsg').text($('#customer_email_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            }
        } else if (order_type == "2") {
            if (delivery_date == "") {
                $('#ermsg').text($('#pickup_date_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (delivery_time == "") {
                $('#ermsg').text($('#pickup_time_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (customer_name == "") {
                $('#ermsg').text($('#customer_name_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (customer_mobile == "") {
                $('#ermsg').text($('#customer_mobile_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            } else if (customer_email == "") {
                $('#ermsg').text($('#customer_email_required').val());
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
                return false;
            }
        }
        $('#preloader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ URL::to('/orders/checkplan') }}",
            data: {
                vendor: vendor,
            },
            method: 'POST',
            success: function(response) {
                if (response.status == 1) {
                    //COD
                    if (payment_type == "cod") {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "{{ URL::to('/orders/paymentmethod') }}",
                            data: {
                                sub_total: sub_total,
                                tax: tax,
                                grand_total: grand_total,
                                delivery_time: delivery_time,
                                delivery_date: delivery_date,
                                delivery_area: delivery_area,
                                delivery_charge: delivery_charge,
                                discount_amount: discount_amount,
                                couponcode: couponcode,
                                order_type: order_type,
                                address: address,
                                postal_code: postal_code,
                                building: building,
                                landmark: landmark,
                                notes: notes,
                                customer_name: customer_name,
                                customer_email: customer_email,
                                customer_mobile: customer_mobile,
                                vendor_id: vendor,
                                payment_type: payment_type,
                            },
                            method: 'POST',
                            success: function(response) {
                                $('#preloader').hide();
                                if (response.status == 1) {
                                    window.location.href =
                                        "{{ URL::to($storeinfo->slug) }}/success/" +
                                        response.order_number;
                                } else {
                                    $('#ermsg').text(response.message);
                                    $('#error-msg').addClass('alert-danger');
                                    $('#error-msg').css("display", "block");
                                    setTimeout(function() {
                                        $("#error-msg").hide();
                                    }, 5000);
                                }
                            },
                            error: function(error) {
                                $('#preloader').hide();
                            }
                        });
                    }
                    //Razorpay
                    if (payment_type == "razorpay") {
                        $('#preloader').hide();
                        var options = {
                            "key": $('#razorpay').val(),
                            "amount": (parseInt(grand_total * 100)), // 2000 paise = INR 20
                            "name": "{{ helper::appdata($storeinfo->id)->website_title }}",
                            "description": "Order payment",
                            "image": "{{ helper::appdata(@$storeinfo->id)->image }}",
                            "handler": function(response) {
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                            'content')
                                    },
                                    url: "{{ URL::to('/orders/paymentmethod') }}",
                                    type: 'post',
                                    dataType: 'json',
                                    data: {
                                        payment_id: response.razorpay_payment_id,
                                        sub_total: sub_total,
                                        tax: tax,
                                        grand_total: grand_total,
                                        delivery_time: delivery_time,
                                        delivery_date: delivery_date,
                                        delivery_area: delivery_area,
                                        delivery_charge: delivery_charge,
                                        discount_amount: discount_amount,
                                        couponcode: couponcode,
                                        order_type: order_type,
                                        address: address,
                                        postal_code: postal_code,
                                        building: building,
                                        landmark: landmark,
                                        notes: notes,
                                        customer_name: customer_name,
                                        customer_email: customer_email,
                                        customer_mobile: customer_mobile,
                                        vendor_id: vendor,
                                        payment_type: payment_type,
                                    },
                                    success: function(response) {
                                        $('#preloader').hide();
                                        if (response.status == 1) {
                                            window.location.href =
                                                "{{ URL::to($storeinfo->slug) }}/success/" +
                                                response.order_number;
                                        } else {
                                            $('#ermsg').text(response.message);
                                            $('#error-msg').addClass('alert-danger');
                                            $('#error-msg').css("display", "block");
                                            setTimeout(function() {
                                                $("#error-msg").hide();
                                            }, 5000);
                                        }
                                    },
                                    error: function(error) {
                                        $('#preloader').hide();
                                    }
                                });
                            },
                            "prefill": {
                                "contact": customer_mobile,
                                "email": customer_email,
                                "name": customer_name,
                            },
                            "theme": {
                                "color": "#366ed4"
                            }
                        };
                        var rzp1 = new Razorpay(options);
                        rzp1.open();
                        e.preventDefault();
                    }
                    //Stripe
                    if (payment_type == "stripe") {
                        var handler = StripeCheckout.configure({
                            key: $('#stripe').val(),
                            image: "{{ helper::appdata(@$storeinfo->id)->image }}",
                            locale: 'auto',
                            token: function(token) {
                                // You can access the token ID with `token.id`.
                                // Get the token ID to your server-side code for use.
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]')
                                            .attr('content')
                                    },
                                    url: "{{ URL::to('/orders/paymentmethod') }}",
                                    data: {
                                        stripeToken: token.id,
                                        sub_total: sub_total,
                                        tax: tax,
                                        grand_total: grand_total,
                                        delivery_time: delivery_time,
                                        delivery_date: delivery_date,
                                        delivery_area: delivery_area,
                                        delivery_charge: delivery_charge,
                                        discount_amount: discount_amount,
                                        couponcode: couponcode,
                                        order_type: order_type,
                                        address: address,
                                        postal_code: postal_code,
                                        building: building,
                                        landmark: landmark,
                                        notes: notes,
                                        customer_name: customer_name,
                                        customer_email: customer_email,
                                        customer_mobile: customer_mobile,
                                        vendor_id: vendor,
                                        payment_type: payment_type,
                                    },
                                    method: 'POST',
                                    success: function(response) {
                                        $('#preloader').hide();
                                        if (response.status == 1) {
                                            window.location.href =
                                                "{{ URL::to($storeinfo->slug) }}/success/" +
                                                response.order_number;
                                        } else {
                                            $('#ermsg').text(response.message);
                                            $('#error-msg').addClass(
                                                'alert-danger');
                                            $('#error-msg').css("display", "block");
                                            setTimeout(function() {
                                                $("#error-msg").hide();
                                            }, 5000);
                                        }
                                    },
                                    error: function(error) {
                                        $('#preloader').hide();
                                    }
                                });
                            },
                            opened: function() {
                                $('#preloader').hide();
                            },
                            closed: function() {
                                $('#preloader').hide();
                            }
                        });
                        //Stripe Popup
                        handler.open({
                            name: "{{ helper::appdata($storeinfo->id)->website_title }}",
                            description: 'Order payment',
                            amount: grand_total * 100,
                            currency: "USD",
                            email: customer_email
                        });
                        e.preventDefault();
                        // Close Checkout on page navigation:
                        $(window).on('popstate', function() {
                            handler.close();
                        });
                    }
                    //Flutterwave
                    if (payment_type == "flutterwave") {
                        FlutterwaveCheckout({
                            public_key: flutterwavekey,
                            tx_ref: customer_name,
                            amount: grand_total,
                            currency: "NGN",
                            payment_options: " ",
                            customer: {
                                email: customer_email,
                                phone_number: customer_mobile,
                                name: customer_name,
                            },
                            callback: function(data) {
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]')
                                            .attr('content')
                                    },
                                    url: "{{ URL::to('/orders/paymentmethod') }}",
                                    method: 'POST',
                                    dataType: 'json',
                                    data: {
                                        payment_id: data.flw_ref,
                                        sub_total: sub_total,
                                        tax: tax,
                                        grand_total: grand_total,
                                        delivery_time: delivery_time,
                                        delivery_date: delivery_date,
                                        delivery_area: delivery_area,
                                        delivery_charge: delivery_charge,
                                        discount_amount: discount_amount,
                                        couponcode: couponcode,
                                        order_type: order_type,
                                        address: address,
                                        postal_code: postal_code,
                                        building: building,
                                        landmark: landmark,
                                        notes: notes,
                                        customer_name: customer_name,
                                        customer_email: customer_email,
                                        customer_mobile: customer_mobile,
                                        vendor_id: vendor,
                                        payment_type: payment_type,
                                    },
                                    success: function(response) {
                                        $('#preloader').hide();
                                        if (response.status == 1) {
                                            window.location.href =
                                                "{{ URL::to($storeinfo->slug) }}/success/" +
                                                response.order_number;
                                        } else {
                                            $('#ermsg').text(response.message);
                                            $('#error-msg').addClass(
                                                'alert-danger');
                                            $('#error-msg').css("display", "block");
                                            setTimeout(function() {
                                                $("#error-msg").hide();
                                            }, 5000);
                                        }
                                    },
                                    error: function(error) {
                                        $('#preloader').hide();
                                    }
                                });
                            },
                            onclose: function() {
                                $('#preloader').hide();
                            },
                            customizations: {
                                title: "{{ helper::appdata($storeinfo->id)->website_title }}",
                                description: "Order payment",
                                logo: "{{ helper::appdata(@$storeinfo->id)->image }}",
                            },
                        });
                    }
                    //Paystack
                    if (payment_type == "paystack") {
                        let handler = PaystackPop.setup({
                            key: paystackkey,
                            email: customer_email,
                            amount: grand_total * 100,
                            currency: 'GHS', // Use GHS for Ghana Cedis or USD for US Dollars
                            ref: 'trx_' + Math.floor((Math.random() * 1000000000) + 1),
                            label: "Order payment",
                            onClose: function() {
                                $('#preloader').hide();
                            },
                            callback: function(response) {
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]')
                                            .attr('content')
                                    },
                                    url: "{{ URL::to('/orders/paymentmethod') }}",
                                    data: {
                                        payment_id: response.trxref,
                                        sub_total: sub_total,
                                        tax: tax,
                                        grand_total: grand_total,
                                        delivery_time: delivery_time,
                                        delivery_date: delivery_date,
                                        delivery_area: delivery_area,
                                        delivery_charge: delivery_charge,
                                        discount_amount: discount_amount,
                                        couponcode: couponcode,
                                        order_type: order_type,
                                        address: address,
                                        postal_code: postal_code,
                                        building: building,
                                        landmark: landmark,
                                        notes: notes,
                                        customer_name: customer_name,
                                        customer_email: customer_email,
                                        customer_mobile: customer_mobile,
                                        vendor_id: vendor,
                                        payment_type: payment_type,
                                    },
                                    method: 'POST',
                                    success: function(response) {
                                        $('#preloader').hide();
                                        if (response.status == 1) {
                                            window.location.href =
                                                "{{ URL::to($storeinfo->slug) }}/success/" +
                                                response.order_number;
                                        } else {
                                            $('#ermsg').text(response.message);
                                            $('#error-msg').addClass(
                                                'alert-danger');
                                            $('#error-msg').css("display", "block");
                                            setTimeout(function() {
                                                $("#error-msg").hide();
                                            }, 5000);
                                        }
                                    },
                                    error: function(error) {
                                        $('#preloader').hide();
                                    }
                                });
                            }
                        });
                        handler.openIframe();
                    }
                    //mercado pago
                    if (payment_type == "mercadopago") {
                        $('#preloader').show();
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "{{ URL::to('/orders/mercadoorderrequest') }}",
                            data: {
                                sub_total: sub_total,
                                tax: tax,
                                grand_total: grand_total,
                                delivery_time: delivery_time,
                                delivery_date: delivery_date,
                                delivery_area: delivery_area,
                                delivery_charge: delivery_charge,
                                discount_amount: discount_amount,
                                couponcode: couponcode,
                                order_type: order_type,
                                address: address,
                                postal_code: postal_code,
                                building: building,
                                landmark: landmark,
                                notes: notes,
                                customer_name: customer_name,
                                customer_email: customer_email,
                                customer_mobile: customer_mobile,
                                vendor_id: vendor,
                                payment_type: payment_type,
                                slug: "{{ $storeinfo->slug }}",
                                url: "{{ URL::to($storeinfo->slug) }}/payment/",
                                failure: "{{ url()->current() }}",
                            },
                            method: 'POST',
                            success: function(response) {
                                $('#preloader').hide();
                                if (response.status == 1) {
                                    window.location.href = response.url;
                                }
                            },
                            error: function(error) {
                                $('#preloader').hide();
                            }
                        });
                    }

                    //PayPal
                    if (payment_type == "paypal") {
                        $('#preloader').show();
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "{{ URL::to('/orders/paypalrequest') }}",
                            data: {
                                sub_total: sub_total,
                                tax: tax,
                                grand_total: grand_total,
                                delivery_time: delivery_time,
                                delivery_date: delivery_date,
                                delivery_area: delivery_area,
                                delivery_charge: delivery_charge,
                                discount_amount: discount_amount,
                                couponcode: couponcode,
                                order_type: order_type,
                                address: address,
                                postal_code: postal_code,
                                building: building,
                                landmark: landmark,
                                notes: notes,
                                customer_name: customer_name,
                                customer_email: customer_email,
                                customer_mobile: customer_mobile,
                                vendor_id: vendor,
                                payment_type: payment_type,
                                return: '1',
                                slug: "{{ $storeinfo->slug }}",
                                url: "{{ URL::to($storeinfo->slug) }}/payment/",
                                failure: "{{ url()->current() }}",
                            },
                            method: 'POST',
                            success: function(response) {
                                $('#preloader').hide();
                                if (response.status == 1) {
                                    $(".callpaypal").trigger("click")
                                }
                            },
                            error: function(error) {
                                $('#preloader').hide();
                            }
                        });
                    }

                    // myfatoorah
                    if (payment_type == 'myfatoorah') {
                        $('#preloader').show();
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "{{ URL::to('/orders/myfatoorahrequest') }}",
                            data: {
                                sub_total: sub_total,
                                tax: tax,
                                grand_total: grand_total,
                                delivery_time: delivery_time,
                                delivery_date: delivery_date,
                                delivery_area: delivery_area,
                                delivery_charge: delivery_charge,
                                discount_amount: discount_amount,
                                couponcode: couponcode,
                                order_type: order_type,
                                address: address,
                                postal_code: postal_code,
                                building: building,
                                landmark: landmark,
                                notes: notes,
                                customer_name: customer_name,
                                customer_email: customer_email,
                                customer_mobile: customer_mobile,
                                vendor_id: vendor,
                                payment_type: payment_type,
                                return: '1',
                                slug: "{{ $storeinfo->slug }}",
                                url: "{{ URL::to($storeinfo->slug) }}/payment/",
                                failure: "{{ url()->current() }}",
                            },
                            method: 'POST',
                            success: function(response) {
                                $('#preloader').hide();
                                if (response.status == 1) {
                                    window.location.href = response.url;
                                } else {
                                    $('#preloader').hide();
                                    toastr.error(response.message);
                                    return false;
                                }
                            },
                            error: function(error) {
                                $('#preloader').hide();
                                toastr.error(wrong);
                                return false;
                            }
                        });
                    }

                    //toyyibpay
                    if (payment_type == 'toyyibpay') {
                        $('#preloader').show();
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "{{ URL::to('/orders/toyyibpayrequest') }}",
                            data: {
                                sub_total: sub_total,
                                tax: tax,
                                grand_total: grand_total,
                                delivery_time: delivery_time,
                                delivery_date: delivery_date,
                                delivery_area: delivery_area,
                                delivery_charge: delivery_charge,
                                discount_amount: discount_amount,
                                couponcode: couponcode,
                                order_type: order_type,
                                address: address,
                                postal_code: postal_code,
                                building: building,
                                landmark: landmark,
                                notes: notes,
                                customer_name: customer_name,
                                customer_email: customer_email,
                                customer_mobile: customer_mobile,
                                vendor_id: vendor,
                                payment_type: payment_type,
                                return: '1',
                                slug: "{{ $storeinfo->slug }}",
                                url: "{{ URL::to($storeinfo->slug) }}/payment/",
                                failure: "{{ url()->current() }}",
                            },
                            method: 'POST',
                            success: function(response) {
                                $('#preloader').hide();
                                if (response.status == 1) {
                                    window.location.href = response.url;
                                } else {
                                    $('#preloader').hide();
                                    toastr.error(wrong);
                                    return false;
                                }
                            },
                            error: function(error) {
                                $('#preloader').hide();
                                toastr.error(wrong);
                                return false;
                            }
                        });
                    }
                } else {
                    $('#preloader').hide();
                    $('#ermsg').text(response.message);
                    $('#error-msg').addClass('alert-danger');
                    $('#error-msg').css("display", "block");
                    setTimeout(function() {
                        $("#error-msg").hide();
                    }, 5000);
                }
            },
            error: function(error) {
                $('#preloader').hide();
            }
        });
    }

    function ApplyCopon() {
        $('#preloader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ URL::to('/cart/applypromocode') }}",
            method: 'post',
            data: {
                promocode: jQuery('#promocode').val(),
                sub_total: parseFloat($('#sub_total').val())
            },
            success: function(response) {
                $('#preloader').hide();
                if (response.status == 1) {
                    location.reload();
                } else {
                    $('#ermsg').text(response.message);
                    $('#error-msg').addClass('alert-danger');
                    $('#error-msg').css("display", "block");
                    setTimeout(function() {
                        $("#success-msg").hide();
                    }, 5000);
                    $('#exampleModal').modal('hide');
                }
            }
        });
    }

    function RemoveCopon() {
        $('#preloader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ URL::to('/cart/removepromocode') }}",
            method: 'post',
            data: {
                promocode: jQuery('#promocode').val()
            },
            success: function(response) {
                $('#preloader').hide();
                if (response.status == 1) {
                    location.reload();
                } else {
                    $('#ermsg').text(response.message);
                    $('#error-msg').addClass('alert-danger');
                    $('#error-msg').css("display", "block");
                    setTimeout(function() {
                        $("#success-msg").hide();
                    }, 5000);
                }
            }
        });
    }
    // Disable previous date
    $(function() {
        var dtToday = new Date();
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if (month < 10)
            month = '0' + month.toString();
        if (day < 10)
            day = '0' + day.toString();
        var maxDate = year + '-' + month + '-' + day;
        $('#delivery_dt').attr('min', maxDate);
    });

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    }
</script>
<script>
    var select = "{{ trans('labels.select') }}";
</script>
<script src="{{ url(env('ASSETPATHURL') . 'front/js/checkout.js') }}"></script>
