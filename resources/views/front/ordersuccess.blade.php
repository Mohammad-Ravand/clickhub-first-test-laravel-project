<div style="display: none">
    @include('front.theme.header')
</div>
<section class="order-success-sec">
    <div class="container">
        <div class="justify-items-center align-items-center text-center order-success-img">
            <img src="{{ helper::image_path('order.gif') }}" class="success-img" alt="ordersuccess" srcset="">
            <h4 class="mb-5 order-title {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                {{ trans('labels.order_successfull') }}</h4>
            <div class="input-group mb-5 col-md-8">
                <input type="text"
                    value="{{ URL::to($storeinfo->slug . '/track-order/' . $orderdata->order_number) }}" id="data"
                    class="form-control" readonly>
                <button onclick="copytext('{{ trans('labels.copied') }}')" class="btn copy-btn">
                    <span class="tooltiptext" id="tool">{{ trans('labels.copy') }}</span>
                </button>
            </div>
            <div class="d-md-flex">
                <a href="{{ URL::to($storeinfo->slug) }}" class="btn mx-2 shop-btn">
                    <i class="far fa-home-lg {{ session()->get('direction') == 2 ? 'ml-1' : 'mr-1' }}"></i>
                    {{ trans('labels.continue_shop') }}
                </a>
                @if (App\Models\SystemAddons::where('unique_identifier', 'whatsapp_message')->first() != null &&
                        App\Models\SystemAddons::where('unique_identifier', 'whatsapp_message')->first()->activated == 1)
                    <a href="https://api.whatsapp.com/send?phone={{ helper::appdata($storeinfo->id)->contact }}&text={{ $whmessage }}"
                        target="_blank" class="btn mx-2 order-btn">
                        <i class="fab fa-whatsapp {{ session()->get('direction') == 2 ? 'ml-1' : 'mr-1' }}"></i>
                        {{ trans('labels.send_order_whatsapp') }}
                    </a>
                @endif
            </div>
        </div>
    </div>
</section>
<div style="display: none">
    @include('front.theme.footer')
    <script>
        function copytext(copied) {
            "use strict";
            var copyText = document.getElementById("data");
            copyText.select();
            document.execCommand("copy");
            document.getElementById("tool").innerHTML = copied;
        }
    </script>
</div>
