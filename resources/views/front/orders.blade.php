@include('front.theme.header')
<section class="product-prev-sec product-list-sec">
    <div class="container">
        <div class="user-bg-color mb-5">
            <div class="container">
                <div class="row">
                    @include('front.theme.sidebar')
                    <div class="col-lg-9 col-xxl-9">
                        <div class="border px-3 rounded table-box">
                            <div class='d-flex align-items-center py-4 table-top-box'>
                                <a href="{{ URL::to($storeinfo->slug.'/orders?type=processing') }}" class="w-100">
                                    <button type='button' class="btn-outline-warning border border-warning rounded warning-icon-box px-3 py-3 w-100 {{ app('request')->input('type') == 'processing' ? 'preparing-box-active' : '' }} processing-box">
                                        <span class='warning-icon d-flex d-flex align-items-center'>
                                            <i class="fa-regular fa-hourglass  d-flex justify-content-center align-items-center"></i>
                                            <div class='px-3'>
                                                <p class='m-0 p-0'>{{ trans('labels.preparing') }}</p>
                                                <p class='text-start-pro m-0 p-0 text-left'>{{ $totalprocessing }}</p>
                                            </div>
                                        </span>
                                    </button>
                                </a>
                                <a href="{{ URL::to($storeinfo->slug.'/orders?type=completed') }}" class="w-100 mx-3 delivered-box">
                                    <button type='button' class="btn-outline-success border border-success rounded warning-icon-box px-3 py-3 w-100 {{ app('request')->input('type') == 'completed' ? 'completed-box-active' : '' }} processing-box">
                                        <span class='success-icon d-flex d-flex align-items-center'>
                                            <i class='fa-regular fa-circle-check d-flex justify-content-center align-items-center'>
                                            </i>
                                            <div class='px-3'>
                                                <p class='m-0 p-0'>{{ trans('labels.delivered') }}</p>
                                                <p class='text-start-pro m-0 p-0 text-left'>{{ $totalcompleted }}</p>
                                            </div>
                                        </span>
                                    </button>
                                </a>
                                <a href="{{ URL::to($storeinfo->slug.'/orders?type=rejected') }}" class="w-100 rejected-box">
                                    <button type='button' class="btn-outline-danger border border-danger rounded px-3 py-3 w-100 {{ app('request')->input('type') == 'rejected' ? 'rejected-box-active' : '' }}">
                                        <span class='danger-icon d-flex d-flex align-items-center'>
                                            <i class='fa-solid fa-xmark d-flex justify-content-center align-items-center'></i>
                                            <div class='px-3'>
                                                <p class='m-0 p-0'>{{ trans('labels.rejected') }}</p>
                                                <p class='text-start-pro m-0 p-0 text-left'>{{ $totalrejected }}</p>
                                            </div>
                                        </span>
                                    </button>
                                </a>
                            </div>
                            <div class="table-responsive py-4">
                                <table class="table table-striped table-bordered py-3 zero-configuration w-100" id="orders">
                                    <thead>
                                        <tr class="text-uppercase">
                                            <td>{{ trans('labels.srno') }}</td>
                                            <td>{{ trans('labels.order_number') }}</td>
                                            <td>{{ trans('labels.date') }}</td>
                                            <td>{{ trans('labels.grand_total') }}</td>
                                            <td>{{ trans('labels.payment_type') }}</td>
                                            <td>{{ trans('labels.status') }}</td>
                                            <td>{{ trans('labels.action') }}</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1; @endphp
                                        @foreach ($getorders as $orderdata)
                                        <tr id="dataid{{ $orderdata->id }}">
                                            <td>@php echo $i++; @endphp</td>
                                            <td>{{ $orderdata->order_number }}</td>
                                            <td>{{ helper::date_format($orderdata->created_at) }}</td>
                                            <td>{{ helper::currency_formate($orderdata->grand_total, $orderdata->vendor_id) }}</td>
                                            <td>
                                                @if ($orderdata->payment_type == 1)
                                                {{ trans('labels.cod') }}
                                                @elseif ($orderdata->payment_type == 2)
                                                {{ trans('labels.razorpay') }}
                                                @elseif ($orderdata->payment_type == 3)
                                                {{ trans('labels.stripe') }}
                                                @elseif ($orderdata->payment_type == 4)
                                                {{ trans('labels.flutterwave') }}
                                                @elseif ($orderdata->payment_type == 5)
                                                {{ trans('labels.paystack') }}
                                                @elseif ($orderdata->payment_type == 7)
                                                {{ trans('labels.mercadopago') }}
                                                @elseif ($orderdata->payment_type == 8)
                                                {{ trans('labels.paypal') }}
                                                @elseif ($orderdata->payment_type == 9)
                                                    {{ trans('labels.myfatoorah') }}
                                                @elseif ($orderdata->payment_type == 10)
                                                    {{ trans('labels.toyyibpay') }}
                                                @endif
                                                @if (in_array($orderdata->payment_type, [2, 3, 4, 5, 7, 8, 9, 10]))
                                                : {{ $orderdata->payment_id }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($orderdata->status == '1')
                                                <span class="text-warning"> <i class="fa-regular fa-bell"></i>
                                                    {{ trans('labels.placed') }}</span>
                                                @elseif($orderdata->status == '2')
                                                <span class="text-info"> <i class="fa-regular fa-tasks"></i>
                                                    {{ trans('labels.preparing') }}</span>
                                                @elseif($orderdata->status == '3')
                                                <span class="text-danger"> <i class="fa-regular fa-close"></i>
                                                    {{ trans('labels.cancelled_by_you') }}</span>
                                                @elseif($orderdata->status == '4')
                                                <span class="text-danger"> <i class="fa-regular fa-close"></i>
                                                    {{ trans('labels.cancelled_by_user') }}</span>
                                                @elseif($orderdata->status == '5')
                                                <span class="text-success"> <i class="fa-regular fa-check"></i>
                                                    {{ trans('labels.delivered') }}</span>
                                                @else
                                                --
                                                @endif
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-light eye-icon-box p-0 " href="{{ URL::to($storeinfo->slug . '/track-order/' . $orderdata->order_number) }}">
                                                    <i class="fa-regular fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('front.theme.footer')