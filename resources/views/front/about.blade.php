@include('front.theme.header')

<section class="product-prev-sec product-list-sec">

    <div class="container">

    <h2 class="sec-head text-center">{{trans('labels.about_us')}}</h2>

        {!!@$about->about_content!!}

    </div>

</section>

@include('front.theme.footer')