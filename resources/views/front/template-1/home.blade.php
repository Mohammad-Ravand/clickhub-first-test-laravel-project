@include('front.theme.header')
<section class="feature-sec">
    <div class="container">
        <div class="feature-carousel owl-carousel owl-theme m-0">
            @foreach ($bannerimage as $image)
                <div class="item">
                    <div class="feature-box">
                        <img src='{{ helper::image_path($image->banner_image) }}'>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<section class="product-prev-sec product-list-sec pt-3">
    <div class="container">
        <div class="product-rev-wrap row">
            <div class="cat-aside cat-aside-theme1 col-xl-3 col-lg-3">
                <div class="card-header cat-dispaly bg-transparent px-0">
                    <h4 class="{{ session()->get('direction') == 2 ? 'text-right' : '' }} m-0">{{ trans('labels.category') }}</h4>
                </div>
                <div class="cat-aside-wrap responsiv-cat-aside mt-2 {{ session()->get('direction') == 2 ? 'text-right' : '' }}">
                    @foreach ($getcategory as $key => $category)
                    @php
                    $check_cat_count = 0;
                    @endphp
                    @foreach ($getitem as $item)
                        @if ($category->id == $item->cat_id)
                            @php
                                $check_cat_count++;
                            @endphp
                        @endif
                    @endforeach
                         @if ($check_cat_count > 0)
                        <a href="#{{ $category->slug }}"
                            class="border-top-no cat-check {{ session()->get('direction') == 2 ? 'cat-right-margin' : 'cat-left-margin' }} {{ $key == 0 ? 'active' : '' }}">
                            <p>{{ $category->name }}</p>
                        </a>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="cat-product w-100 col-xl-9 col-lg-9  {{ session()->get('direction') == 2 ? 'pr-3' : 'pl-3' }} custom-categories-main-sec">
                @foreach ($getcategory as $key => $category)
                    @php
                        $check_cat_count = 0;
                    @endphp
                    @foreach ($getitem as $item)
                        @if ($category->id == $item->cat_id)
                            @php
                                $check_cat_count++;
                            @endphp
                        @endif
                    @endforeach
                    @if ($key != 0 && $check_cat_count > 0)
                        <hr class="mb-2">
                    @endif
                    @if ($check_cat_count > 0)
                        <div class="card-header responsive-padding bg-transparent px-0  custom-cat-name-sec" id="{{ $category->slug }}">
                            <h4 class="mb-2 {{ session()->get('direction') == 2 ? 'text-right mt-2' : '' }}">{{ $category->name }}
                            </h4>
                        </div>
                    @endif
                    <div class="row recipe-card custom-product-card">
                        @if (!$getcategory->isEmpty())
                            @php $i = 0; @endphp
                            @foreach ($getitem as $item)
                                @if ($category->id == $item->cat_id)
                                    <div class="col-xl-3 col-md-4 mb-3 responsive-col custom-product-column" id="pro-box">
                                        <div class="pro-box">
                                            <div class="pro-img">
                                                <img src="{{ asset('storage/app/public/item/' . $item->image) }}"
                                                    alt="">
                                            </div>
                                            <div class="product-details-wrap">
                                                <div class="product-details-inner">
                                                    <h4 id="itemname"
                                                        onclick="GetProductOverview('{{ $item->id }}')"
                                                        class="{{ session()->get('direction') == 2 ? 'text-right' : '' }}">
                                                        {{ $item->item_name }}</h4>
                                                </div>
                                                <div class="d-flex align-items-baseline">
                                                    <p class="pro-pricing">
                                                        @if (count($item['variation']) > 0)
                                                            @foreach ($item->variation as $key => $value)
                                                                {{ helper::currency_formate($value->price, $storeinfo->id) }}
                                                            @break
                                                            @endforeach
                                                        @else
                                                            {{ helper::currency_formate($item->item_price, $storeinfo->id) }}
                                                        @endif
                                                    </p>
                                                    @if (count($item['variation']) < 1)
                                                        @if ($item->item_original_price > 0)
                                                            <p class="pro-pricing pro-org-value text-muted">
                                                                {{ helper::currency_formate($item->item_original_price, $storeinfo->id) }}
                                                            </p>
                                                        @endif
                                                    @endif
                                                </div>
                                                <button class="btn btn-sm m-0 py-1 btn-content"
                                                onclick="GetProductOverview('{{ $item->id }}')">{{ trans('labels.add_to_cart') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                    @php $i += 1; @endphp
                                @endif
                            @endforeach
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@include('front.theme.footer')
