@include('front.theme.header')

<div class="container">
    <div class="row mb-4">

        <div class="col-xl-3 col-lg-6 col-sm-6 my-1">

            <div class="card border-0 box-shadow rounded p-3 h-100">
                
                <h5>
                    <i class="fa-solid fa-envelope fs icon-color mr-2"></i> 

                    <span class="mr-2">{{ trans('labels.email') }}</span></h5>

                <p class="mb-0">
                    
                    <a href="mailto:{{ $contactinfo->email }}"
                        class="text-dark">{{ $contactinfo->email }}</a>
                </p>

            </div>

        </div>

        <div class="col-xl-3 col-lg-6 col-sm-6 my-1">

            <div class="card border-0 box-shadow rounded p-3 h-100 ">

                <h5>
                    <i class="fa-solid fa-phone icon-color mr-2"></i>

                    <span class="mr-2">{{ trans('labels.mobile') }}</span>
                </h5>

                <p class="mb-0">
                    <a href="tel:{{ $contactinfo->mobile }}"
                        class="text-dark">{{ $contactinfo->mobile }}</a>
                </p>

            </div>

        </div>

        <div class="col-xl-3 col-lg-6 col-sm-6 my-1">

            <div class="card border-0 box-shadow rounded p-3 h-100">

                <h5>
                    <i class="fa-solid fa-location-dot icon-color mr-2"></i>
                    <span class="mr-2"> {{ trans('labels.address') }}</span>
                </h5>

                <p class="mb-0 fs-7">

                    {{ empty(helper::appdata($contactinfo->id)->address) ? '-' : helper::appdata($contactinfo->id)->address }}

                </p>

            </div>

        </div>

        <div class="col-xl-3 col-lg-6 col-sm-6 my-1">

            <div class="card border-0 box-shadow rounded p-3 h-100">

                <h5>
                    <i class="far fa-question-circle fs-5 icon-color mr-2"></i>
                    <span class="mr-2 cursor-pointer" data-toggle="modal" data-target="#infomodal">
                        {{ trans('labels.working_hours') }}
                    </span>
                </h5>

                <p class="mb-0" dir="ltr">{{ $todayworktime->open_time }} <b>{{ trans('labels.to') }}</b>

                    {{ $todayworktime->close_time }} </p>

            </div>

        </div>

    </div>

    <div class="row d-flex justify-content-center align-items-center">

        <div class="col-lg-6 col-md-12 col-sm-12">

            <div class="card border-0 border-0 box-shadow p-5 mb-3">

                <form method="POST" action="{{ URL::to($storeinfo->slug . '/submit') }}">

                    @csrf

                    <div class="mb-3 text-center">
                        <h2>Contact Us</h2>
                        <p class="text-muted font-weight-bold form-subtitle">{{ trans('labels.contact_further_question') }}</p>
                    </div>

                    <div class="mb-3 ">

                        <div class="row">
                            <div>
   
                               <input type="hidden" name="vendor_id" value="{{ $storeinfo->id }}">

                            </div>

                            <div class="col-md-6 form-group mb-0">

                            <label class="form-label d-flex justify-content-start align-items-center">
                                First Name
                                <span class="text-danger mx-1"> * </span>
                            </label>

                                <input type="text" class="form-control" name="fname"
                                    placeholder="{{ trans('labels.first_name') }}">

                                @error('fname')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror

                            </div>

                            <div class="col-md-6 form-group mb-0">

                                <label class="form-label d-flex justify-content-start align-items-center">
                                     Last Name
                                     <span class="text-danger mx-1"> * </span>
                                 </label>

                                <input type="text" class="form-control" name="lname"
                                    placeholder="{{ trans('labels.last_name') }}">

                                @error('lname')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror

                            </div>

                            <div class="col-md-6 form-group mb-0">
    
                                <label class="form-label d-flex justify-content-start align-items-center">
                                    Email
                                     <span class="text-danger mx-1"> * </span>
                                 </label>

                                <input type="text" class="form-control" name="email"
                                    placeholder="{{ trans('labels.email') }}">

                                @error('email')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror

                            </div>

                            <div class="col-md-6 form-group mb-0">

                                <label class="form-label d-flex justify-content-start align-items-center">
                                    Mobile
                                     <span class="text-danger mx-1"> * </span>
                                 </label>

                                <input type="text" class="form-control" name="mobile"
                                    placeholder="{{ trans('labels.mobile') }}">

                                @error('mobile')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror

                            </div>

                            <div class="form-group col-md-12 mb-0">

                                <label class="form-label d-flex justify-content-start align-items-center">
                                    Message
                                     <span class="text-danger mx-1"> * </span>
                                 </label>

                                <textarea class="form-control" rows="2" name="message" placeholder="{{ trans('labels.message') }}"></textarea>

                                @error('message')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror

                            </div>

                        </div>

                    </div>

                    <div class="d-flex">

                        <button type="submit" name="submit"
                            class="btn w-100">{{ trans('labels.submit') }}</button>

                    </div>

                </form>

            </div>

        </div>

    </div>
</div>



@include('front.theme.footer')
