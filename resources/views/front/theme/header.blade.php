<!DOCTYPE html>
<html lang="en" dir="{{ session()->get('direction') == 2 ? 'rtl' : 'ltr' }}">

<head>
    <title>{{ helper::appdata($storeinfo->id)->website_title }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:title" content="{{ helper::appdata($storeinfo->id)->meta_title }}" />
    <meta property="og:description" content="{{ helper::appdata($storeinfo->id)->meta_description }}" />
    <meta property="og:image" content='{{ helper::image_path(helper::appdata($storeinfo->id)->og_image) }}' />
    <link rel="icon" href='{{ helper::image_path(helper::appdata($storeinfo->id)->favicon) }}'
        type="image/x-icon"><!-- favicon-icon  -->
    <link rel="stylesheet" type="text/css" href="{{ url(env('ASSETPATHURL') . 'front/css/all.min.css') }}"><!-- font-awsome css  -->
    <link rel="stylesheet" type="text/css" href="{{ url(env('ASSETPATHURL') . 'front/css/bootstrap.min.css') }}"><!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ url(env('ASSETPATHURL') . 'front/css/owl.carousel.min.css') }}"><!-- owl.carousel css -->
    <link rel="stylesheet" type="text/css" href="{{ url(env('ASSETPATHURL') . 'front/css/style.css') }}"><!-- style css  -->
    <link rel="stylesheet" type="text/css" href="{{ url(env('ASSETPATHURL') . 'front/css/fonts.css') }}"><!-- Fonts css  -->
    <link rel="stylesheet" type="text/css" href="{{ url(env('ASSETPATHURL') . 'front/css/responsive.css') }}"><!-- responsive css  -->
    <link rel="stylesheet" type="text/css" href="{{ url(env('ASSETPATHURL') . 'admin-assets/css/sweetalert/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ url(env('ASSETPATHURL') . 'admin-assets/css/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.3/css/dataTables.bootstrap4.min.css">
</head>

<body>
    <!--*******************
 Preloader start
 ********************-->
    <div id="preloader">
        <input type="hidden" name="hdnsession" id="hdnsession" value="{{ session()->get('direction') }}">
        <div class="loader">
        </div>
    </div>
    <!--*******************
 Preloader end
 ********************-->
    <!-- navbar -->
    <nav class="main-header">
        <div class="container">
            <div class="d-md-flex align-items-center mobile-header">
                <div class="col-md-6 p-0 ">
                    <div class="header-contact  ">
                        <a href="mailto:{{ helper::appdata($storeinfo->id)->email  }}" target="_blank" class="me-3"><i
                                class="fa-solid fa-envelope mx-2"></i>{{ helper::appdata($storeinfo->id)->email  }}</a>
                        <a href="tel:{{ helper::appdata($storeinfo->id)->contact }}" target="_blank" class="me-3"><i
                                class="fa-solid fa-phone mx-2"></i>{{ helper::appdata($storeinfo->id)->contact }}</a>
                    </div>
                </div>
                <div class="col-md-6 p-0 ">
                    <div class="header-social">      
                    <!-- dekstop-tablet-mobile-language-dropdown-button-start-->
                        @if (App\Models\SystemAddons::where('unique_identifier', 'language')->first() != null &&
                        App\Models\SystemAddons::where('unique_identifier', 'language')->first()->activated == 1)
                        <div class="dropdown show language-dropdown">
                            <a class="btn dropdown-toggle open-btn d-flex justify-content-between align-items-center language-dropdown-text px-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{ helper::image_path(session()->get('flag')) }}" alt=""   class="img-fluid" width="25px">
                                <span class="dropdown-link py-1 px-2">
                                    {{ session()->get('language') }}
                                </span>
                                <i class="fa-solid fa-caret-down text-dark"></i>
                            </a>
                            <div class="dropdown-menu dekstop-tablet-mobile-language-dropdown {{ session()->get('direction') == 2 ? 'dropdown-menu-right' : 'dropdown-menu' }}">
                                @foreach (helper::listoflanguage() as $languagelist)
                                    <li>
                                        <a class="dropdown-item text-dark d-flex text-left px-3 py-1"
                                            href="{{ URL::to('/lang/change?lang=' . $languagelist->code) }}">
                                            <img src="{{ helper::image_path($languagelist->image) }}"
                                                alt="" class="img-fluid mx-1" width="25px">
                                            {{ $languagelist->name }}
                                        </a>
                                    </li>
                                @endforeach
                                
                            </div>
                        </div>
                        @endif
                    <!-- dekstop-tablet-mobile-language-dropdown-button-end-->
                        <button type="button" class="btn text-white open-btn mx-3 info-icon" data-toggle="modal"
                            data-target="#infomodal">
                            <i class="fa-regular fa-circle-info mx-1"></i>{{ trans('labels.info') }}
                        </button>
                        @if (App\Models\SystemAddons::where('unique_identifier', 'customer_login')->first() != null &&
                            App\Models\SystemAddons::where('unique_identifier', 'customer_login')->first()->activated == 1)
                            @if (Auth::user() && Auth::user()->type == 3)
                            <div class="dropdown header-drop-button px-2">
                                <button class="btn dropdown-toggle p-0 drop-icon border-0 bg-white text-dark" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa-solid fa-user px-1"></i>
                                    {{ Auth::user()->name }}
                                    <i class="fa-solid fa-caret-down px-1"></i>
                                </button>
                                <div class="dropdown-menu header-dropdown-menu rounded-0 border-0 p-0 " aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item border-bottom" href="{{ URL::to($storeinfo->slug . '/profile/') }}">{{ trans('labels.profile') }} </a>
                                    <a class="dropdown-item border-bottom" href="{{ URL::to($storeinfo->slug . '/logout/') }}">{{ trans('labels.logout') }}</a>
                                </div>
                            </div>
                            @else
                            @if(helper::appdata($storeinfo->id)->checkout_login_required == 1)
                            <div class="login-button-and-user d-flex">
                                <a href="{{ URL::to($storeinfo->slug . '/login/') }}" class="btn text-white open-btn">{{ trans('labels.login') }}</a>
                            </div>
                            @endif
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- navbar -->
    <!-- Banner -->
    <div class="card border-0">
        <img class="banner-bg" src=" {{ helper::image_path(helper::appdata($storeinfo->id)->banner) }} "
            alt="">
        <div class="card-img-overlay banner-content">
            <div class="container">
                <h3 class="banner-text">{{ helper::appdata($storeinfo->id)->description }}</h3>
            </div>
        </div>
    </div>
    <div class="logo-sec">
        <div class="container">
            <div class="d-md-flex align-items-end">
                <a href="{{ URL::to($storeinfo->slug) }}" class="logo col-md-6 p-0">
                    <img src="{{ helper::image_path(helper::appdata(@$storeinfo->id)->logo) }}" alt=""
                        class="logo-image">
                    <h3 class="mx-3">{{ Str::limit(helper::appdata($storeinfo->id)->website_title, 20) }}</h3>
                </a>
                @if (request()->route()->getName() == 'front.home')
                    <div class="search-bar col-md-6 p-0">
                        <form action="">
                            <div class="input-group h-100">
                                <input type="text" id="searchText" class="form-control"
                                    placeholder="{{ trans('labels.search_here') }}" name="search">
                                <button type="submit" class="input-group-text search-btn"><i
                                        class="far fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <!-- Banner -->
    <div id="success-msg" class="alert alert-dismissible mt-3" style="display: none;">
        <span id="msg"></span>
    </div>
    <div id="error-msg" class="alert alert-dismissible mt-3" style="display: none;">
        <span id="ermsg"></span>
    </div>
    <style>
        :root {
            --primary-font: 'Lexend', sans-serif;
            --font-family: var(--font-family);
            /* Color */
            --primary-color: #000;
            --primary-bg-color: #f4f4f8;
            /* --body-color: #f7f7f7; */
            --active-tab: #3ba2a484;
            /* Hover Color */
            --btn-color: {{ helper::appdata($storeinfo->id)->primary_color }};
            --primary-bg-color-hover: #000;
            --primary-color-hover: {{ helper::appdata($storeinfo->id)->secondary_color }};
            --active-menu: {{ helper::appdata($storeinfo->id)->primary_color }}30;
        }
    </style>
