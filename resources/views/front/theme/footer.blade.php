{!! helper::appdata($storeinfo->id)->whatsapp_widget !!}

<section class="product-service">

    <div class="bg-light">
        <div class="container-fluid py-2">

            <div class="row align-items-center justify-content-center">

                @foreach (helper::footer_features(@$storeinfo->id) as $feature)
                <div class="col-xl-3 col-lg-3 col-md-6 text-center wow animate__animated animate__zoomIn py-3">

                    <h1 class="free-icon icon-color">

                        {!! $feature->icon !!}

                    </h1>

                    <div class="free-content">

                        <h4 class="fw-bold">{{ $feature->title }}</h4>

                        <p class="fs-7">{{ $feature->description }}</p>

                    </div>

                </div>
                @endforeach

            </div>

        </div>

    </div>


</section>
<!-- footer -->
<footer class="footer-sec2">
    <div class="container">
        <div class="row footer-conten">
            <div class="col-md-4">
                <h4 class="footer-header text-white footer-text-start">
                    {{ trans('labels.store_details') }}
                </h4>
                <div class="contact {{ session()->get('direction') == 2 ? 'text-right' : 'text-left' }}">
                    <ul class="list-group border-0 p-0">
                        <li class="list-group-item  text-white border-0 ">
                            <a href="https://www.google.com/maps/place/323/{{ helper::appdata($storeinfo->id)->address }}" target="_blank">
                                <i class="fa-sharp fa-solid fa-location-dot"></i>
                                <p class="mx-2">{{ helper::appdata($storeinfo->id)->address }}</p>
                            </a>
                        </li>
                        <li class="list-group-item  text-white border-0 ">
                            <a href="tel:{{ helper::appdata($storeinfo->id)->contact }}" target="_blank">
                                <i class="fa-solid fa-phone"></i>
                                <p class="mx-2">{{ helper::appdata($storeinfo->id)->contact }}</p>
                            </a>
                        </li>
                        <li class="list-group-item  text-white border-0">
                            <a href="mailto:{{ helper::appdata($storeinfo->id)->email }}" target="_blank">
                                <i class="fa-solid fa-envelope"></i>
                                <p class="mx-2">{{ helper::appdata($storeinfo->id)->email }}</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 footer-content footer-text-start">
                <h4 class="footer-header text-white">{{ trans('labels.pages') }}</h4>
                <div class="row footer-circle-icon">
                    <ul class="list-group border-0 p-0 footer-social col-md-9 col-lg-6 col-xl-6 col-xxl-6 mb-0">
                        <li class="list-group-item border-0">
                            <a href="{{ URL::to($storeinfo->slug . '/privacypolicy') }}">
                                <i class="fa-solid fa-circle-dot"></i>
                                <p class="text-white mx-2">{{ trans('labels.privacy_policy') }}</p>
                            </a>
                        </li>
                        <li class="list-group-item border-0">
                            <a href="{{ URL::to($storeinfo->slug . '/terms_condition') }}">
                                <i class="fa-solid fa-circle-dot"></i>
                                <p class="text-white mx-2">{{ trans('labels.terms_condition') }}</p>
                            </a>
                        </li>
                        <li class="list-group-item border-0">
                            <a href="{{ URL::to($storeinfo->slug . '/aboutus') }}">
                                <i class="fa-solid fa-circle-dot"></i>
                                <p class="text-white mx-2">{{ trans('labels.about_us') }}</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="list-group border-0 p-0 footer-social col-md-7 col-lg-6 col-xl-6 col-xxl-6">
                        @if (App\Models\SystemAddons::where('unique_identifier', 'blog')->first() != null &&
                        App\Models\SystemAddons::where('unique_identifier', 'blog')->first()->activated == 1)
                        <li class="list-group-item border-0">
                            <a href="{{ URL::to($storeinfo->slug . '/blogs') }}">
                                <i class="fa-solid fa-circle-dot"></i>
                                <p class="text-white mx-2">{{ trans('labels.blogs') }}</p>
                            </a>
                        </li>
                        @endif
                        <li class="list-group-item border-0">
                            <a href="{{ URL::to($storeinfo->slug . '/contact') }}">
                                <i class="fa-solid fa-circle-dot"></i>
                                <p class="text-white mx-2">{{ trans('labels.contact_us') }}</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <h4 class="footer-header text-white footer-text-start">
                    {{ trans('labels.follow_us') }}
                </h4>
                <div class="d-flex">
                    <ul class="list-group border-0 p-0 footer-social col-md-6">
                        <li class="list-group-item border-0">
                            @if (helper::appdata($storeinfo->id)->facebook_link != '')
                            <a href="{{ helper::appdata($storeinfo->id)->facebook_link }}" target="_blank"><i class="fa-brands fa-facebook"></i>
                                <p class="text-white mx-2">{{ trans('labels.facebook') }}</p>
                            </a>
                            @endif
                        </li>
                        <li class="list-group-item border-0">
                            @if (helper::appdata($storeinfo->id)->twitter_link != '')
                            <a href="{{ helper::appdata($storeinfo->id)->twitter_link }}" target="_blank"><i class="fab fa-twitter"></i>
                                <p class="text-white mx-2">{{ trans('labels.twitter') }}</p>
                            </a>
                            @endif
                        </li>
                    </ul>
                    <ul class="list-group border-0 p-0 footer-social col-md-6">
                        <li class="list-group-item border-0">
                            @if (helper::appdata($storeinfo->id)->instagram_link != '')
                            <a href="{{ helper::appdata($storeinfo->id)->instagram_link }}" target="_blank"><i class="fab fa-instagram"></i>
                                <p class="text-white mx-2">{{ trans('labels.instagram') }}</p>
                            </a>
                            @endif
                        </li>
                        <li class="list-group-item border-0">
                            @if (helper::appdata($storeinfo->id)->linkedin_link != '')
                            <a href="{{ helper::appdata($storeinfo->id)->linkedin_link }}" target="_blank"><i class="fab fa-linkedin"></i>
                                <p class="text-white mx-2">{{ trans('labels.linkedin') }}</p>
                            </a>
                            @endif
                        </li>
                    </ul>
                </div>
                <form action="{{ URL::to($storeinfo->slug . '/subscribe') }}" method="post">
                    @csrf
                    <div class="d-flex align-items-center justify-content-between m-0 mt-2 form-control py-1 input-box-main px-1 border-0">
                        <input type="hidden" value="{{ $storeinfo->id }}" name="id">
                        <input type="email" class="form-control mb-0 footer-input w-75 border-0 py-0" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        <button type="submit" class="subscribe-button py-1 px-3 border-0">{{ trans('labels.subscribe') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="copy-right-sec">
        <p class="mb-0">{{ helper::appdata($storeinfo->id)->copyright }}</p>
    </div>
    </div>
</footer>
<!-- footer -->

<!-- Product View -->
<div class="modal fade" id="viewproduct-over" tabindex="-1" aria-labelledby="add-payment" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="m-0 close cart-close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa-regular fa-xmark"></i></span>
                </button>
            </div>
            <div class="modal-body px-4 pro-modal">
                <div class="card border-0">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <div class="sp-wrap gallerys modal-image"></div>
                        </div>
                        <div class="col-md-8 {{ session()->get('direction') == 2 ? '' : 'pro-side' }}">
                            <div class="card-body repsonsive-cart-modal p-0 {{ session()->get('direction') == 2 ? 'text-right' : 'text-left' }}">
                                <p class="pro-title" id="item_name"></p>
                                <div class="d-flex align-items-center modal-price">
                                    <p class="pro-text pricing" id="item_price"></p>
                                    <p class="card-text pro-org-value text-muted pricing" id="item_original_price">
                                    </p>
                                </div>
                            </div>
                            <p id="tax" class="responcive-tax {{ session()->get('direction') == 2 ? 'text-right' : 'text-left' }} mb-2">
                            </p>
                            <div class="input-group qty-input2 align-items-center p-0 col-4 responsive-margin">
                                <a class="btn btn-sm py-0 change-qty" data-type="minus" value="minus value"><i class="fa fa-minus"></i>
                                </a>
                                <input type="number" class="border text-center" name="number" value="1" id="item_qty" readonly>
                                <a class="btn btn-sm py-0 change-qty icon-position" data-type="plus" value="plus value"><i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="price" id="price" value="">
                    <div class="modal-dec {{ session()->get('direction') == 2 ? 'text-right' : 'text-left' }}">
                        <p class="card-text" id="item_description"></p>
                    </div>
                    <div class="woo_pr_color flex_inline_center mb-2 {{ session()->get('direction') == 2 ? 'text-right' : 'text-left' }}">
                        <div class="woo_colors_list">
                            <span id="variation"></span>
                        </div>
                    </div>
                    <div class="woo_pr_color flex_inline_center mb-2">
                        <div class="woo_colors_list {{ session()->get('direction') == 2 ? 'text-right' : 'text-left' }}">
                            <span id="extras"></span>
                            {{ $errors->login->first('extras') }}
                        </div>
                    </div>
                    <div class="woo_btn_action">
                        <input type="hidden" name="vendor" id="overview_vendor">
                        <input type="hidden" name="item_id" id="overview_item_id">
                        <input type="hidden" name="item_name" id="overview_item_name">
                        <input type="hidden" name="item_image" id="overview_item_image">
                        <input type="hidden" name="item_price" id="overview_item_price">
                        <input type="hidden" name="item_original_price" id="overview_item_original_price">
                        <input type="hidden" name="tax" id="tax_val">
                        <button class="btn btn-block mb-2 modal-btn" onclick="AddtoCart()">
                            {{ trans('labels.addcart') }}
                            <i class="ti-shopping-cart-full ml-2"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->
<!-- MODAL-INFORMATION -->
<div class="modal fade" id="infomodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <div class="modal-logo d-flex align-items-end">
                        <img src="{{ helper::appdata(@$storeinfo->id)->image }}" alt="" class="modal-logo-image">
                        <h3 class="mx-3">{{ Str::limit(helper::appdata($storeinfo->id)->website_title, 20) }}</h3>
                    </div>
                </h5>
                <button type="button" class="m-0 close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa-regular fa-xmark"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="business-sec">
                    <h5 class="business-title card-header">
                        {{ trans('labels.working_hours') }}
                    </h5>
                    <div class="working-hours mb-4">
                        <ul class="list-group border-0 bg-none p-0">
                            @if (is_array(@helper::timings($storeinfo->id)) || is_object(@helper::timings($storeinfo->id)))
                            @foreach (@helper::timings($storeinfo->id) as $time)
                            <li class="list-group-item d-md-flex border-0 default-color">
                                <p class="font-weight-bold col-md-6 text-center">
                                    {{ trans('labels.' . strtolower($time->day)) }}
                                </p>
                                <div class="col-md-6 d-flex justify-content-center">
                                    <p class=" text-center">
                                        {{ $time->is_always_close == 1 ? trans('labels.closed') : $time->open_time . ' ' . trans('labels.to') . ' ' . $time->close_time }}
                                    </p>
                                </div>
                            </li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                    <div class="store-timings mb-4 text-center">
                        <h5 class="card-header text-center"> {{ trans('labels.about') }} </h5>
                        <i class="fal fa-light fa-mobile"></i> {{ trans('labels.contact') }} :
                        {{ helper::appdata($storeinfo->id)->contact }} <br>
                        <i class="fal fa-map-marker-alt"></i> {{ trans('labels.address') }} :
                        {{ helper::appdata($storeinfo->id)->address }}
                    </div>
                    <div class="services mb-4 text-center">
                        <h5 class="card-header text-center"> {{ trans('labels.types_services') }}</h5>
                        @if (helper::appdata($storeinfo->id)->delivery_type == 'both')
                        <i class="fa fa-check"></i> {{ trans('labels.delivery') }} <i class="fa fa-check"></i>
                        {{ trans('labels.pickup') }}
                        @elseif (helper::appdata($storeinfo->id)->delivery_type == 'delivery')
                        <i class="fa fa-check"></i> {{ trans('labels.delivery') }}
                        @elseif (helper::appdata($storeinfo->id)->delivery_type == 'pickup')
                        <i class="fa fa-check"></i> {{ trans('labels.pickup') }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->
<!-- MODAL_SELECTED_ADDONS--START -->
<div class="modal fade" id="modal_selected_addons" tabindex="-1" aria-labelledby="selected_addons_Label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="selected_addons_Label">{{ trans('labels.selected_addons') }}</h5>
                <button type="button" class="m-0 p-0 close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa-regular fa-xmark"></i></span>
                </button>
            </div>
            <div class="modal-body p-0 extra-variation-modal">
                <ul class="list-group list-group-flush p-0 {{ session()->get('direction') == 2 ? 'text-right' : 'text-left' }}">
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- MODAL_SELECTED_ADDONS--END -->
<input type="hidden" name="currency" id="currency" value="{{ helper::appdata($storeinfo->id)->currency }}">
<!-- Age Verification Modal -->
<div class="modal fade" id="vendorplan" role="dialog" data-toggle="modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content close-modal">
            <div class="modal-body">
                <div class="col-lg-12">
                    <img src="{{ helper::image_path(helper::appdata(@$storeinfo->id)->logo) }}" class="img-responsive center-block d-block mx-auto" alt="Sample Image" width="100px">
                </div>
                <h3 class="hidden-xs mt-5" style="text-align: center;">
                    <strong>{{ trans('labels.vendor_is_unavailable') }}</strong>
                </h3>
            </div>
        </div>
    </div>
</div>
<!-- View order btn -->
@if (session()->get('cart') &&
$storeinfo->id == session()->get('vendor_id') &&
request()->route()->getName() != 'front.cart')
<a href="{{ URL::to($storeinfo->slug . '/cart/') }}" class="cart-btn {{ session()->get('direction') == 2 ? 'cart-button-rtl' : 'cart-button' }}">
    <i class="fa-solid fa-basket-shopping"></i><span id="cartcnt">{{ session()->get('cart') }}</span>
</a>
@endif
<!-- View order btn -->
<!-- jquery -->
<script src="{{ url(env('ASSETPATHURL') . 'admin-assets/js/jquery/jquery.min.js') }}"></script>
<!-- bootstrap js -->
<script src="{{ url(env('ASSETPATHURL') . 'front/js/bootstrap.bundle.js') }}"></script>
<!-- owl.carousel js -->
<script src="{{ url(env('ASSETPATHURL') . 'front/js/owl.carousel.min.js') }}"></script>
<!-- lazyload js -->
<script src="{{ url(env('ASSETPATHURL') . 'front/js/lazyload.js') }}"></script>
<!-- fontawesome js-->
<script src="{{ url(env('ASSETPATHURL') . 'admin-assets/js/toastr/toastr.min.js') }}"></script><!-- Toastr JS -->
<!-- custom js -->
<script>
    let rtl = {{ session()->get('direction') == 2 ? 'true' : 'false'}};
</script>
<script src="{!! asset('storage/app/public/front/js/custom.js') !!}"></script>
<script src="{!! asset('storage/app/public/admin-assets/js/sweetalert/sweetalert2.min.js') !!}"></script>
<script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.3/js/dataTables.bootstrap4.min.js"></script>
@yield('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#orders').DataTable();
    });
    $(window).on('load', function() {
        $('#preloader').hide();
    });
    toastr.options = {
        "closeButton": true,
        "positionClass": "toast-top-right",
    }
    @if(Session::has('success'))
    toastr.success("{{ session('success') }}");
    @endif
    @if(Session::has('error'))
    toastr.error("{{ session('error') }}");
    @endif

    function GetProductOverview(id) {
        $('#preloader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ URL::to('product-details/details') }}",
            data: {
                id: id
            },
            method: 'POST', //Post method,
            dataType: 'json',
            success: function(response) {
                $('#preloader').hide();
                jQuery("#viewproduct-over").modal('show');
                $('#overview_vendor').val(response.ResponseData.vendor_id);
                $('#overview_item_id').val(response.ResponseData.id);
                $('#overview_item_name').val(response.ResponseData.item_name);
                $('#overview_item_image').val(response.ResponseData.image_name);
                $('#overview_item_price').val(response.ResponseData.item_price);
                $('#item_name').text(response.ResponseData.item_name);
                $('#item_description').text(response.ResponseData.description);
                $('#category_name').text(response.ResponseData.name);
                $('#tax_val').val(response.ResponseData.tax);
                $('.gallerys').html("<img src=" + response.ResponseData.image +
                    " class='img-fluid  width='100%''>");
                if (response.ResponseData.tax > 0) {
                    $('#tax').html("<span style='color: red'>" + response.ResponseData.tax +
                        "% {{ trans('labels.additional_tax') }}</span>");
                } else {
                    $('#tax').html(
                        "<span style='color: green'>{{ trans('labels.inclusive_taxes') }}</span>");
                }
                if (response.ResponseData.item_original_price > 0) {
                    $('#overview_item_original_price').val(response.ResponseData.item_original_price);
                    $('#item_original_price').html(response.ResponseData.item_original_p);
                } else {
                    $('#overview_item_original_price').val('');
                    $('#item_original_price').html('');
                }
                var e;
                var i;
                var sessionValue = $("#hdnsession").val();
                var classforview = "";
                var classforul = "extra-food";
                if (sessionValue == "2") {
                    var classforview = "d-flex";
                    var classforul = "mr-0 pr-2 extra-food-rtl";
                }
                let html = '';
                let text_align = '';
                if (response.ResponseData.extras.length != 0) {
                    html += '<h5 class="extra-title">{{ trans('labels.extras')}}</h5>';
                }
                html += '<ul class="list-unstyled ' + classforul + '"><div id="pricelist">';
                for (e in response.ResponseData.extras) {
                    if (response.ResponseData.extras[e].price < 0) {
                        html += '<li class="mb-2"><input type="checkbox" name="addons[]" extras_name="' +
                            response
                            .ResponseData.extras[e].name + '" class="Checkbox" value="' + response
                            .ResponseData.extras[e].id + '" price="' + response.ResponseData.extras[e]
                            .price + '"><p>' + response.ResponseData.extras[e].name + '</p></li>'
                    } else {
                        html += '<li class="mb-2"><input type="checkbox" name="addons[]" extras_name="' +
                            response
                            .ResponseData.extras[e].name + '" class="Checkbox" value="' + response
                            .ResponseData.extras[e].id + '" price="' + response.ResponseData.extras[e]
                            .price + '"><p>' + response.ResponseData.extras[e].name + ' : ' +
                            currency_formate(parseFloat(response.ResponseData.extras[e].price)) +
                            '</p></li>'
                    }
                }
                html += '</div></ul>';
                $('#extras').html(html);
                let varhtml = '';
                if (response.ResponseData.variation.length != 0) {
                    $('#overview_item_original_price').val('');
                    $('#item_original_price').html('');
                    varhtml += '<h5 class="extra-title">{{ trans('labels.variants ')}}</h5>';
                }
                for (i in response.ResponseData.variation) {
                    if (i == 0) {
                        var checked = "checked";
                        $('#price').val(response.ResponseData.variation[i].price);
                        $('#item_price').text(currency_formate(parseFloat(response.ResponseData.variation[i]
                            .price)));
                        $('#overview_item_price').val(response.ResponseData.variation[i].price);
                    } else {
                        var checked = "";
                    }
                    var original_price_text = '';
                    if (response.ResponseData.variation[i].original_price > 0) {
                        original_price_text = '<small class="text-danger"><strike> ' + currency_formate(
                                parseFloat(response.ResponseData.variation[i].original_price)) +
                            ' </strike></small>';
                    }
                    varhtml += '<div class="custom-varient custom-size"><input type="radio" ' + checked +
                        ' variation-id="' + response.ResponseData.variation[i].id +
                        '" class="custom-control-input Radio" name="variation" id="variation-' + i + '-' +
                        response.ResponseData.variation[i].item_id + '" variants_name="' + response
                        .ResponseData.variation[i].name + '" value="' + response.ResponseData.variation[i]
                        .name + ' - ' + response.ResponseData.variation[i].price + '" price="' + response
                        .ResponseData.variation[i].price +
                        '"><label class="custom-control-label" for="variation-' + i + '-' + response
                        .ResponseData.variation[i].item_id + '">' + response.ResponseData.variation[i]
                        .name + ' - ' + currency_formate(parseFloat(response.ResponseData.variation[i]
                            .price)) + ' ' + original_price_text + ' </label></div>'
                }
                $('#variation').html(varhtml);
                if (response.ResponseData.variation.length === 0) {
                    console.log('test');
                    $('#price').val(response.ResponseData.item_price);
                    $('#item_price').text(response.ResponseData.item_p);
                    $('#overview_item_price').val(response.ResponseData.item_price);
                }
            },
            error: function(error) {
                $('#preloader').hide();
            }
        })
    }

    function AddtoCart() {
        var vendor = $('#overview_vendor').val();
        var item_id = $('#overview_item_id').val();
        var item_name = $('#overview_item_name').val();
        var item_image = $('#overview_item_image').val();
        var item_price = $('#overview_item_price').val();
        var item_qty = $('#viewproduct-over #item_qty').val();
        var item_original_price = $('#overview_item_original_price').val();
        var tax = $('#tax_val').val();
        var price = $('#price').val();
        var variants_id = $('input[name="variation"]:checked').attr("variation-id");
        var variants_name = $('input[name="variation"]:checked').attr("variants_name");
        var variants_price = $('input[name="variation"]:checked').attr("price");
        var extras_id = ($('.Checkbox:checked').map(function() {
            return this.value;
        }).get().join(', '));
        var extras_name = ($('.Checkbox:checked').map(function() {
            return $(this).attr('extras_name');
        }).get().join(', '));
        var extras_price = ($('.Checkbox:checked').map(function() {
            return $(this).attr('price');
        }).get().join(', '));
        $('#preloader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ URL::to('/add-to-cart') }}",
            data: {
                vendor_id: vendor,
                item_id: item_id,
                item_name: item_name,
                item_image: item_image,
                item_price: item_price,
                item_original_price: item_original_price,
                tax: tax,
                variants_id: variants_id,
                variants_name: variants_name,
                variants_price: variants_price,
                extras_id: extras_id,
                extras_name: extras_name,
                extras_price: extras_price,
                qty: item_qty,
                price: price
            },
            method: 'POST', //Post method,
            dataType: 'json',
            success: function(response) {
                if (response.status == 1) {
                    $('#cartcnt').text(response.cartcnt);
                    location.reload();
                } else {
                    $("#preloader").hide();
                    $("#viewproduct-over").modal('hide');
                    $('#ermsg').text(response.message);
                    $('#error-msg').addClass('alert-danger');
                    $('#error-msg').css("display", "block");
                    setTimeout(function() {
                        $("#success-msg").hide();
                    }, 5000);
                }
            },
            error: function(error) {}
        })
    };
    $('body').on('change', 'input[type="checkbox"]', function(e) {
        var total = parseFloat($("#price").val());
        if ($(this).is(':checked')) {
            total += parseFloat($(this).attr('price')) || 0;
        } else {
            total -= parseFloat($(this).attr('price')) || 0;
        }
        $('h3.pricing').text(currency_formate(parseFloat(total)));
        $('#price').val(total);
    })
    $('body').on('change', 'input[type="radio"]', function(e) {
        $('h3.pricing').text(currency_formate(parseFloat($(this).attr('price'))));
        $('#price').val(parseFloat($(this).attr('price')));
        $('input[type=checkbox]').prop('checked', false);
    })
    $(window).on('load', function() {
        var vendor_id = "{{ $storeinfo->id }}";
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ URL::to('/orders/checkplan') }}",
            data: {
                vendor: vendor_id,
            },
            method: 'POST',
            success: function(response) {
                if (response.status == 2) {
                    $('#vendorplan').modal('show');
                } else {
                    $('#vendorplan').modal('hide');
                }
            },
            error: function(error) {}
        });
    });

    function currency_formate(price) {
        if ("{{ @helper::appdata($storeinfo->id)->currency_position }}" == "left") {
            return "{{ @helper::appdata($storeinfo->id)->currency }}" + parseFloat(price).toFixed(2);
        } else {
            return parseFloat(price).toFixed(2) + "{{ @helper::appdata($storeinfo->id)->currency }}";
        }
    }
    $('.cat-check').on('click', function() {
        if ($(this).attr('data-cat-type') == 'first') {
            $('html, body').animate({
                scrollTop: 0
            }, '1000');
        }
        $('.cat-aside-wrap').find('.active').removeClass('active');
        $(this).addClass('active');
    });

    function RemoveCart(cart_id) {

        "use strict";
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success mx-1',
                cancelButton: 'btn btn-danger bg-danger mx-1'
            },
            buttonsStyling: false
        })
        swalWithBootstrapButtons.fire({
            icon: 'error',
            title: "{{ trans('messages.are_you_sure') }}",
            showCancelButton: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            confirmButtonText: "{{ trans('messages.yes') }}",
            cancelButtonText: "{{ trans('messages.no') }}",
            reverseButtons: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve, reject) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{ URL::to('/cart/deletecartitem') }}",
                        data: {
                            cart_id: cart_id
                        },
                        method: 'POST',
                        success: function(response) {
                            if (response.status == 1) {
                                location.reload();
                            } else {
                                swal("Cancelled", "{{ trans('messages.wrong') }} :(",
                                    "error");
                            }
                        },
                        error: function(e) {
                            swal("Cancelled", "{{ trans('messages.wrong') }} :(",
                                "error");
                        }
                    });
                });
            },
        }).then((result) => {
            if (!result.isConfirmed) {
                result.dismiss === Swal.DismissReason.cancel
            }
        })
    }

    function qtyupdate(cart_id, item_id, type) {
        var qtys = parseInt($("#number_" + cart_id).val());
        var item_id = item_id;
        var cart_id = cart_id;
        if (type == "decreaseValue") {
            qty = qtys - 1;
        } else {
            qty = qtys + 1;
        }
        if (qty >= "1") {
            $('#preloader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ URL::to('/cart/qtyupdate') }}",
                data: {
                    cart_id: cart_id,
                    qty: qty,
                    item_id,
                    item_id,
                    type,
                    type
                },
                method: 'POST',
                success: function(response) {
                    if (response.status == 1) {
                        location.reload();
                    } else {
                        $('#preloader').hide();
                        $('#ermsg').text(response.message);
                        $('#error-msg').addClass('alert-danger');
                        $('#error-msg').css("display", "block");
                        setTimeout(function() {
                            $("#success-msg").hide();
                        }, 5000);
                    }
                },
                error: function(error) {}
            });
        } else {
            // $('#preloader').show();
            if (qty < "1") {
                $('#ermsg').text("You've reached the minimum units allowed for the purchase of this item");
                $('#error-msg').addClass('alert-danger');
                $('#error-msg').css("display", "block");
                setTimeout(function() {
                    $("#error-msg").hide();
                }, 5000);
            }
        }
    }
</script>
<script>
    function showaddons(name, price) {
        $('#modal_selected_addons').find('.list-group-flush').html(
            '<div class="text-center"><div class="spinner-border" role="status"><span class="visually-hidden">Loading...</span></div></div>'
        );
        var response = '';
        $.each(name.split(','), function(key, value) {
            response += '<li class="list-group-item"> <b> ' + value + ' </b> <p class="mb-0">' +
                currency_formate(price.split(',')[key]) + '</p> </li>';
        });
        $('#modal_selected_addons').find('.list-group-flush').html(response);
    }
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id={{ helper::appdata(1)->tracking_id }}"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', '{{ helper::appdata(1)->tracking_id }}');
</script>
</body>

</html>