<div class="col-lg-3 col-xxl-3 ">
    <div class="card-v border h-100 py-3 rounded">
        <div class="title-and-image">
            <img src="{{ helper::image_path(@Auth::user()->image) }}" alt="" class="object-fit-cover img-fluid w-25 h-25 rounded-circle d-flex justify-content-center m-auto">
            <h5 class="title mt-2 mx-3 text-center">{{ @Auth::user()->name }}</h5>
        </div>
        <div class="user-list-saide-bar mt-4">
            <ul class="px-3 m-0">
                <a href="{{ URL::to($storeinfo->slug . '/profile/') }}" class="settings-link">
                    <li class="list-unstyled border my-2 py-1 rounded {{ request()->is($storeinfo->slug.'/profile*') ? 'active-menu' : '' }}">
                        <i class="fa-solid fa-user px-3"></i>{{ trans('labels.profile') }}
                    </li>
                </a>
                <a href="{{ URL::to($storeinfo->slug . '/orders/') }}" class="settings-link ">
                    <li class="list-unstyled  border my-2 py-1 rounded {{ request()->is($storeinfo->slug.'/orders*') ? 'active-menu' : '' }}">
                        <i class="fa-solid fa-list-check px-3"></i>{{ trans('labels.orders') }}
                    </li>
                </a>
                @if(@Auth::user()->google_id == "" && @Auth::user()->facebook_id == "")
                <a href="{{ URL::to($storeinfo->slug . '/change-password/') }}" class="settings-link">
                    <li class="list-unstyled  border my-2 py-1 rounded {{ request()->is($storeinfo->slug.'/change-password*') ? 'active-menu' : '' }}">
                        <i class="fa-solid fa-key px-3"></i>{{ trans('labels.change_password') }}
                    </li>
                </a>
                @endif
                <a href="{{ URL::to($storeinfo->slug . '/logout/') }}" class="settings-link ">
                    <li class="list-unstyled  border my-2 py-1 rounded">
                        <i class="fa-solid fa-arrow-right-from-bracket px-3"></i>{{ trans('labels.logout') }}
                    </li>
                </a>
            </ul>
        </div>
    </div>
</div>