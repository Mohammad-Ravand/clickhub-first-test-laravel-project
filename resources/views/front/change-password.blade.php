@include('front.theme.header')
<section class="product-prev-sec product-list-sec">
    <div class="container">
        <div class="user-bg-color mb-5">
            <div class="container">
                <div class="row">
                    @include('front.theme.sidebar')
                    <div class="col-lg-9 col-xxl-9">
                        <div class="card-v p-0 border rounded user-form">
                            <div class="settings-box">
                                <div class="settings-box-header border-bottom px-4 py-3">
                                    <h5 class="mb-0">{{ trans('labels.change_password') }}</h5>
                                </div>
                                <div class="settings-box-body p-4">
                                    <form id="deatilsForm" action="{{ URL::to($storeinfo->slug . '/change_password/') }}" method="POST">
                                        @csrf
                                        <div class="row row-cols-1 row-cols-sm-2 g-3 mb-3">
                                            <div class="col-12">
                                                <label class="form-label">{{ trans('labels.current_password') }} : <span class="required">*</span></label>
                                                <input type="password" name="current_password" class="form-control form-control-md" placeholder="{{ trans('labels.current_password') }}" required="">
                                                @error('current_password')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="col-12">
                                                <label class="form-label">{{ trans('labels.new_password') }} : <span class="required">*</span></label>
                                                <input type="password" name="new_password" class="form-control form-control-md mb-0" placeholder="{{ trans('labels.new_password') }}" required="">
                                                @error('new_password')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">{{ trans('labels.confirm_password') }} : <span class="required">*</span></label>
                                            <input type="password" name="confirm_password" class="form-control form-control-md mb-0" placeholder="{{ trans('labels.confirm_password') }}" required="">
                                            @error('confirm_password')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button class="btn btn-primary btn-md p-1 px-3 m-0">{{ trans('labels.save') }}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('front.theme.footer')

