@include('front.theme.header')
<section class="order-details">
    <div class="container">
        <h2 class="sec-head {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
            {{ trans('labels.order_details') }}</h2>
        <p>({{ $summery['order_number'] }} - {{ $summery['created_at'] }})</p>
        @if ($summery['status'] == 1)
            <ul
                class="{{ session()->get('direction') == '2' ? 'progressbar-rtl' : 'progressbar' }}  d-flex px-0 text-center justify-content-center mb-4">
                <li class="active">{{ trans('labels.order_placed') }}</li>
                <li>{{ trans('labels.preparing') }}</li>
                <li>{{ trans('labels.order_delivered') }}</li>
            </ul>
        @elseif($summery['status'] == 2)
            <ul
                class="{{ session()->get('direction') == '2' ? 'progressbar-rtl' : 'progressbar' }} d-flex px-0 text-center justify-content-center mb-4">
                <li class="active">{{ trans('labels.order_placed') }}</li>
                <li class="active">{{ trans('labels.preparing') }}</li>
                <li>{{ trans('labels.order_delivered') }}</li>
            </ul>
        @elseif($summery['status'] == 3)
            <ul
                class="{{ session()->get('direction') == '2' ? 'progressbar-rtl' : 'progressbar' }} d-flex px-0 text-danger text-center justify-content-center mb-4">
                <li class="active text-danger">{{ trans('labels.rejected') }}</li>
            </ul>
        @elseif($summery['status'] == 4)
            <ul
                class="{{ session()->get('direction') == '2' ? 'progressbar-rtl' : 'progressbar' }} d-flex px-0 text-danger text-center justify-content-center mb-4">
                <li class="active text-danger">{{ trans('labels.cancelled_by_user') }}</li>
            </ul>
        @elseif($summery['status'] == 5)
            <ul
                class="{{ session()->get('direction') == '2' ? 'progressbar-rtl' : 'progressbar' }} d-flex px-0 text-center justify-content-center mb-4">
                <li class="active">{{ trans('labels.order_placed') }}</li>
                <li class="active">{{ trans('labels.preparing') }}</li>
                <li class="active">{{ trans('labels.delivered') }}</li>
            </ul>
        @endif
        <div class="row">
            <div class="col-lg-8 mb-4">
                @foreach ($orderdetails as $odata)
                    <div class="order-details-box">
                        <div class="order-details-name">
                            <h3 class="{{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                                {{ $odata->item_name }}
                                @if ($odata->variants_id != '')
                                    {{ $odata->variants_name }}
                                @endif
                            </h3>
                        </div>
                        <ul class="p-0 list-font mb-1">
                            @if ($odata->variants_id != '')
                                <li>
                                    <P> {{ $odata->variants_name }}:{{ helper::currency_formate($odata->variants_price, $storeinfo->id) }}
                                    </P>
                                </li>
                            @endif
                            @if ($odata->variants_id != '' && $odata->extras_id != '')
                                <span class="variant-gap">&nbsp;|&nbsp;</span>
                            @endif
                            @if ($odata->extras_id != '')
                                <li>
                                    <div class="d-flex">
                                        <a href="javascript:void(0)" class="adone-hover m-0"
                                            onclick="showaddons('{{ $odata->extras_name }}','{{ $odata->extras_price }}')"
                                            data-toggle="modal" data-target="#modal_selected_addons">Extra
                                        </a>
                                    </div>
                                </li>
                            @endif
                        </ul>
                        <div class="d-flex justify-content-between align-items-baseline">
                            <p class="pro-qty">{{ trans('labels.qty') }} : {{ $odata->qty }}</p>
                            <span
                                class="pro-total-price {{ session()->get('direction') == '2' ? 'price-right-margin' : 'price-left-margin' }}">
                                @if ($odata->variants_id != '')
                                    {{ helper::currency_formate($odata->variants_price, $storeinfo->id) }}
                                @else
                                    {{ helper::currency_formate($odata->price, $storeinfo->id) }}
                                @endif
                            </span>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-lg-4 mb-4">
                <div class="order-payment-summary {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                    <h3>{{ trans('labels.payment_summary') }}</h3>
                    <p>{{ trans('labels.sub_total') }}
                        <span>{{ helper::currency_formate(@$summery['sub_total'], $storeinfo->id) }}</span>
                    </p>
                    @if ($summery['tax'] == 0)
                        <p class="d-none"></p>
                    @else
                        <p>{{ trans('labels.tax') }}
                            <span>{{ helper::currency_formate(@$summery['tax'], $storeinfo->id) }} </span>
                        </p>
                    @endif
                    @if ($summery['delivery_charge'] == 0)
                        <p class="d-none"></p>
                    @else
                        <p>{{ trans('labels.delivery_charge') }}
                            <span>{{ helper::currency_formate(@$summery['delivery_charge'], $storeinfo->id) }}
                            </span>
                        </p>
                    @endif
                    @if ($summery['discount_amount'] == 0)
                        <p class="d-none"></p>
                    @else
                        <p>{{ trans('labels.discount') }} ({{ $summery['couponcode'] }}) <span>-
                                {{ helper::currency_formate(@$summery['discount_amount'], $storeinfo->id) }}</span>
                        </p>
                    @endif
                    <p class="order-details-total">{{ trans('labels.total_amount') }}
                        <span>{{ helper::currency_formate($summery['grand_total'], $storeinfo->id) }}</span>
                    </p>
                </div>
                @if ($summery['order_type'] == 1)
                    <div class="order-add {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                        <h6>{{ trans('labels.delivery_info') }}</h6>
                        <span>{{ trans('labels.address') }}</span>
                        <p>{{ $summery['address'] }}</p>
                        <span>{{ trans('labels.building') }}</span>
                        <p>{{ $summery['building'] }}</p>
                        <span>{{ trans('labels.landmark') }}</span>
                        <p>{{ $summery['landmark'] }}</p>
                        <span>{{ trans('labels.pincode') }}</span>
                        <p>{{ $summery['pincode'] }}</p>
                    </div>
                    @endif
                    <div class="order-add {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                        <h6>{{ trans('labels.customer') }}</h6>
                        <span>{{ trans('labels.name') }}</span>
                        <p>{{ $summery['customer_name'] }}</p>
                        <span>{{ trans('labels.email') }}</span>
                        <p>{{ $summery['customer_email'] }}</p>
                        <span>{{ trans('labels.mobile') }}</span>
                        <p>{{ $summery['mobile'] }}</p>
                    </div>
                    @if($summery['status'] == 1)
                    <a href="{{ URL::to($storeinfo->slug . '/cancel-order/' . $summery['order_number']) }}" class="btn-cancel text-white">{{ trans('labels.cancel') }}</a>
                    @endif
            </div>
        </div>
    </div>
</section>
@include('front.theme.footer')
