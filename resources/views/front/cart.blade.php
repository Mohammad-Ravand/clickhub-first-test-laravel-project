@include('front.theme.header')
<div class="cart-sec">
    <div class="container">
        <div class="mb-5">
            @if (count($cartdata) == 0)
                <h2 class="cart-title border-0 mb-3 {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                    {{ trans('labels.my_cart') }}
                </h2>
                @php
                    $data = [];
                @endphp
                <p class="{{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                    {{ trans('labels.data_not_found') }}</p>
            @else
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="yourcart-sec">
                    <h2 class="cart-title text-center {{ session()->get('direction') == '2' ? 'text-right' : '' }}">
                        {{ trans('labels.my_cart') }}
                    </h2>
                    @if (count($cartdata) == 0)
                        @php
                            $data = [];
                        @endphp
                        <p class="text-center">{{ trans('labels.data_not_found') }}</p>
                    @else
                        {{-- customer-title --}}
                        @foreach ($cartdata as $cart)
                            <?php
                            $data[] = [
                                'total_price' => $cart->qty * $cart->price,
                                'tax' => ($cart->qty * $cart->price * $cart->tax) / 100,
                            ];
                            $sub_total = array_sum(array_column(@$data, 'total_price'))
                            ?>
                            <div class="item-box mb-3">
                                <div class="item-info">
                                    <div class="col-sm-1 col-md-2 col-lg-2 col-xl-1 item-image p-0 d-flex align-items-center">
                                        <img src="{{ asset('storage/app/public/item/' . $cart->item_image) }}"
                                            alt="" class="img-fluid card-pro-image">
                                    </div>
                                    <div
                                        class="col-sm-11  col-md-10 col-lg-10 col-xl-11 d-flex justify-content-center flex-column {{ session()->get('direction') == 2 ? 'pr-3' : 'pl-3' }}">
                                        <div
                                            class="d-flex justify-content-between px-0 {{ session()->get('direction') == 2 ? 'text-right' : '' }}">
                                            <h5 class="cart-card-title card-font p-0 mb-2 col-8">
                                                {{ $cart->item_name }}</h5>
                                            <div
                                                class="{{ session()->get('direction') == 2 ? 'rtl-item-delete' : 'item-delete' }}">
                                                <i class="fas fa-trash-alt text-danger"
                                                    onclick="RemoveCart('{{ $cart->id }}')"></i>
                                            </div>
                                        </div>
                                        <ul class="p-0 list-font mb-2 d-flex">
                                            @if ($cart->variants_id != '')
                                                <li>
                                                    <P> {{ $cart->variants_name }}:{{ helper::currency_formate($cart->variants_price, $storeinfo->id) }}
                                                    </P>
                                                </li>
                                            @endif
                                            @if ($cart->variants_id != '' && $cart->extras_id != '')
                                                <span class="variant-gap">&nbsp;|&nbsp;</span>
                                            @endif
                                            @if ($cart->extras_id != '')
                                                <li>
                                                    <div class="d-flex">
                                                        <a href="javascript:void(0)" class="adone-hover m-0"
                                                            onclick="showaddons('{{ $cart->extras_name }}','{{ $cart->extras_price }}')"
                                                            data-toggle="modal"
                                                            data-target="#modal_selected_addons">{{ trans('labels.extras') }}
                                                        </a>
                                                    </div>
                                                </li>
                                            @endif
                                        </ul>
                                        <div
                                            class="d-flex justify-content-between align-items-center responsive-cart-total">
                                            <p
                                                class="cart-total-price m-0 {{ session()->get('direction') == 2 ? 'text-right' : 'text-left' }}">
                                                {{ helper::currency_formate($cart->qty * $cart->price, $storeinfo->id) }}
                                            </p>
                                            <div
                                                class="col-md-3 col-lg-2 col-xl-1 input-group qty-input2  input-width d-flex justify-content-between p-0  {{ session()->get('direction') == 2 ? 'rtl-input-postion' : 'input-postion' }}">
                                                <a class="btn btn-sm py-0 change-qty cart-padding" data-type="minus"
                                                    value="minus value"
                                                    onclick="qtyupdate('{{ $cart->id }}','{{ $cart->item_id }}','decreaseValue')">
                                                    <i class="fa fa-minus"></i>
                                                </a>
                                                <input type="number" class="border text-center"
                                                    id="number_{{ $cart->id }}" name="number"
                                                    value="{{ $cart->qty }}" min="1" max="10" readonly>
                                                <a class="btn btn-sm py-0 change-qty icon-position cart-padding"
                                                    data-type="plus"
                                                    onclick="qtyupdate('{{ $cart->id }}','{{ $cart->item_id }}','increase')"
                                                    value="plus value"><i class="fa fa-plus"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="promo-code d-flex justify-content-between align-items-center my-3">
                            <div class="d-flex ">
                                <div class="cuppon-text">
                                    <h4 class="m-0 card-sub-total-text">{{ trans('labels.sub_total') }} : {{ helper::currency_formate($sub_total, $storeinfo->id) }}</h4>
                                </div>
                            </div>
                            <div class="d-flex">
                                @if (App\Models\SystemAddons::where('unique_identifier', 'customer_login')->first() != null &&
                                    App\Models\SystemAddons::where('unique_identifier', 'customer_login')->first()->activated == 1)
                                    @if(Auth::user() && Auth::user()->type == 3)
                                        <a class="btn text-white open-btn mx-3" href="{{ URL::to(@$storeinfo->slug . '/checkout') }}"><span>{{ trans('labels.checkout') }}</span></a>
                                    @else
                                        @if(helper::appdata($storeinfo->id)->checkout_login_required == 1)
                                            <button type="button" class="btn text-white open-btn mx-3 cart-button-1" data-toggle="modal" data-target="#loginmodel">
                                                {{ trans('labels.checkout') }}
                                            </button>
                                        @else
                                            <a class="btn text-white open-btn mx-3" href="{{ URL::to(@$storeinfo->slug . '/checkout') }}"><span>{{ trans('labels.checkout') }}</span></a>
                                        @endif
                                    @endif
                                @else
                                    <a class="btn text-white open-btn mx-3" href="{{ URL::to(@$storeinfo->slug . '/checkout') }}"><span>{{ trans('labels.checkout') }}</span></a>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
<!-- Login Model Start -->
<div class="modal fade" id="loginmodel" tabindex="-1" aria-labelledby="loginmodelLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content p-2">
            <div class="modal-body text-center">
                <h3 class="promocodemodellable-titel m-0 text-start" id="promocodemodellable">{{trans('labels.proceed_as_guest_or_login')}}</h3>
                <p class="mb-3 promocodemodellable-subtitel">{{trans('labels.dont_have_account_guest')}}</p>
                <div class="d-flex justify-content-start align-items-center proceed-as-guest-o-login">
                            
                    <a href="{{ URL::to(@$storeinfo->slug . '/login')}}" class="btn  rounded-4 login-account border-0 m-0 w-75 cart-promocodemodellable-1 ">{{ trans('labels.login_with_your_account') }}</a>

                    <a href="{{ URL::to(@$storeinfo->slug . '/checkout') }}" class="{{ session()->get('direction') == 2 ? 'mr-3' : 'ml-3' }} btn text-white rounded-4 cart-promocodemodellable w-75">{{ trans('labels.continue_as_guest') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Login Model End -->

@include('front.theme.footer')
