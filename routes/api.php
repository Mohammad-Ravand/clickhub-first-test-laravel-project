<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\api\UserController as VendorController;
use App\Http\Controllers\api\HomeController as VendorHomeController;
use App\Http\Controllers\api\OtherController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace'=>'Api'],function (){
    Route::post('register',[VendorController::class,'register_vendor']);
    Route::post('sociallogin',[VendorController::class,'social_login']);
    Route::post('login',[VendorController::class,'check_admin_login']);
    Route::post('forgotpassword',[VendorController::class,'forgotpassword']);
    Route::post('changepassword', [VendorController::class, 'change_password']);
    Route::post('editprofile', [VendorController::class, 'edit_profile']);
    Route::get('getcountry', [VendorController::class, 'getcountry']);
    Route::post('getcity', [VendorController::class, 'getcity']);

    Route::post('home',[VendorHomeController::class,'index']);
    Route::post('orderdetail',[VendorHomeController::class,'order_detail']);
    Route::post('orderhistory',[VendorHomeController::class,'order_history']);
    Route::post('statuschange',[VendorHomeController::class,'status_change']);
    
    Route::get('cmspages',[OtherController::class,'getcontent']);
    Route::post('inquiries',[OtherController::class,'inquiries']);
});