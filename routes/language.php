<?php


use App\Http\Controllers\addons\LanguageController;
use App\Http\Controllers\addons\LangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang/change', [LangController::class, 'change'])->name('changeLang');
Route::group(['namespace' => 'admin', 'prefix' => 'admin'], function () {
    Route::group(['middleware' => 'AuthMiddleware'], function () {
            Route::group(['prefix' => 'language-settings'], function () {
            Route::get('/', [LanguageController::class, 'index']);
            Route::get('/add', [LanguageController::class,'add']);
		    Route::post('/store', [LanguageController::class,'store']);
            Route::get('/{code}', [LanguageController::class,'index']);
            Route::post('/update', [LanguageController::class,'storeLanguageData']);
            Route::get('/language/edit-{id}', [LanguageController::class,'edit']);
            Route::post('/update-{id}', [LanguageController::class, 'update']);
            Route::get('/layout/update-{id}/{status}', [LanguageController::class,'layout']);
            Route::get('/layout/status-{id}/{status}', [LanguageController::class,'status']);
        });
    });
});

