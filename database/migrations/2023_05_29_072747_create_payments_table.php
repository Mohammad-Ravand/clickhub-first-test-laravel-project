<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('vendor_id')->nullable();
            $table->string('payment_name', 255);
            $table->string('currency', 255)->default('');
            $table->string('image', 255);
            $table->text('public_key')->nullable();
            $table->text('secret_key')->nullable();
            $table->text('encryption_key')->nullable();
            $table->integer('environment');
            $table->text('bank_name')->nullable();
            $table->string('account_number', 255)->nullable();
            $table->string('account_holder_name', 255)->nullable();
            $table->string('bank_ifsc_code', 255)->nullable();
            $table->integer('is_available');
            $table->integer('is_activate')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
