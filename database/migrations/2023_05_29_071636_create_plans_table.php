<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->longText('description');
            $table->string('features', 255)->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
            $table->float('price');
            $table->string('themes_id', 255)->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
            $table->integer('plan_type')->comment('1 = duration, 2 = days');
            $table->string('duration', 255)->comment("1=1 month\r\n2=3 month\r\n3=6 month\r\n4=1\r\n year\r\n\r\n\r\n");
            $table->integer('days');
            $table->integer('order_limit');
            $table->integer('appointment_limit');
            $table->integer('custom_domain')->comment('1=yes,2=no');
            $table->integer('google_analytics')->comment('1=yes,2=no');
            $table->integer('vendor_app');
            $table->integer('is_available')->default(1)->comment('1=Yes, 2=No');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
