<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('vendor_id');
            $table->string('currency');
            $table->string('currency_position');
            $table->integer('maintenance_mode')->default(2)->comment('1 = yes, 2 = no');
            $table->integer('checkout_login_required')->default(2)->comment('1 = Yes, 2 = No');
            $table->string('logo')->default('default-logo.png');
            $table->string('favicon')->default('favicon-.png');
            $table->string('delivery_type', 10);
            $table->string('timezone');
            $table->string('address');
            $table->string('email');
            $table->string('mobile');
            $table->text('description')->nullable();
            $table->string('contact');
            $table->string('copyright');
            $table->string('website_title');
            $table->string('meta_title');
            $table->text('meta_description');
            $table->string('og_image')->default('og_image.png');
            $table->string('facebook_link');
            $table->string('twitter_link');
            $table->string('instagram_link');
            $table->string('linkedin_link');
            $table->longText('whatsapp_widget')->nullable();
            $table->longText('whatsapp_message');
            $table->text('item_message');
            $table->integer('language')->default(1);
            $table->integer('template')->default(1);
            $table->string('primary_color')->default('#171a29');
            $table->string('secondary_color')->default('#171a29');
            $table->text('custom_domain')->nullable();
            $table->text('cname_title')->nullable();
            $table->text('cname_text')->nullable();
            $table->string('interval_time');
            $table->integer('interval_type');
            $table->integer('time_format')->default(1)->comment('1=Yes,2=No');
            $table->string('banner')->default('default-banner.png');
            $table->string('tracking_id')->nullable();
            $table->string('view_id')->nullable();
            $table->longText('firebase')->nullable();
            $table->string('cover_image')->default('default-cover.png');
            $table->string('notification_sound')->default('notification.mp3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            Schema::dropIfExists('settings');
        });
    }
}
