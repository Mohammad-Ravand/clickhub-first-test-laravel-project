<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('vendor_id');
            $table->integer('plan_id');
            $table->string('plan_name', 255)->nullable();
            $table->string('payment_type', 255);
            $table->string('payment_id')->nullable();
            $table->float('amount')->default(0);
            $table->string('duration', 255);
            $table->integer('days')->nullable();
            $table->string('purchase_date', 255);
            $table->string('service_limit', 255);
            $table->string('appoinment_limit', 255);
            $table->integer('custom_domain');
            $table->integer('google_analytics');
            $table->integer('vendor_app');
            $table->string('expire_date', 255);
            $table->string('themes_id')->nullable();
            $table->string('screenshot')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
