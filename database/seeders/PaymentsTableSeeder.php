<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->insert([
            [
                'id' => 1,
                'vendor_id' => 1,
                'payment_name' => 'COD',
                'currency' => '',
                'image' => 'cod.png',
                'public_key' => NULL,
                'secret_key' => NULL,
                'encryption_key' => '',
                'environment' => 1,
                'bank_name' => NULL,
                'account_number' => NULL,
                'account_holder_name' => NULL,
                'bank_ifsc_code' => NULL,
                'is_available' => 2,
                'is_activate' => 1,
                'created_at' => '2021-09-02 14:06:58',
                'updated_at' => '2023-01-19 02:59:26'
            ],
            [
                'id' => 2,
                'vendor_id' => 1,
                'payment_name' => 'RazorPay',
                'currency' => 'INR',
                'image' => 'razorpay.png',
                'public_key' => 'rzp_test_4r8y0wDMkrUDFn',
                'secret_key' => 'nEDuJlpL3x2BqHxYlQBYtrto',
                'encryption_key' => '',
                'environment' => 1,
                'bank_name' => NULL,
                'account_number' => NULL,
                'account_holder_name' => NULL,
                'bank_ifsc_code' => NULL,
                'is_available' => 1,
                'is_activate' => 1,
                'created_at' => '2021-09-02 14:06:58',
                'updated_at' => '2023-01-26 09:13:41'
            ],
            [
                'id' => 3,
                'vendor_id' => 1,
                'payment_name' => 'Stripe',
                'currency' => 'USD',
                'image' => 'stripe.png',
                'public_key' => 'pk_test_51IjNgIJwZppK21ZQa6e7ZVOImwJ2auI54TD6xHici94u7DD5mhGf1oaBiDyL9mX7PbN5nt6Weap4tmGWLRIrslCu00d8QgQ3nI',
                'secret_key' => 'sk_test_51IjNgIJwZppK21ZQK85uLARMdhtuuhA81PB24VDfiqSW8SXQZKrZzvbpIkigEb27zZPBMF4UEG7PK9587Xresuc000x8CdE22A',
                'encryption_key' => '',
                'environment' => 1,
                'bank_name' => NULL,
                'account_number' => NULL,
                'account_holder_name' => NULL,
                'bank_ifsc_code' => NULL,
                'is_available' => 1,
                'is_activate' => 1,
                'created_at' => '2021-09-02 14:06:58',
                'updated_at' => '2023-01-26 09:13:41'
            ],
            [
                'id' => 4,
                'vendor_id' => 1,
                'payment_name' => 'Flutterwave',
                'currency' => 'NGN',
                'image' => 'flutterwave.png',
                'public_key' => 'FLWPUBK_TEST-61c94068c4a44548a771cc7cf9548d05-X',
                'secret_key' => 'FLWSECK_TEST-1140781769b7bd5cfd6b3fb6d5704017-X',
                'encryption_key' => 'FLWSECK_TEST863a39eb1475',
                'environment' => 1,
                'bank_name' => NULL,
                'account_number' => NULL,
                'account_holder_name' => NULL,
                'bank_ifsc_code' => NULL,
                'is_available' => 1,
                'is_activate' => 1,
                'created_at' => '2021-10-21 07:28:05',
                'updated_at' => '2023-03-03 13:41:24'
            ],
            [
                'id' => 5,
                'vendor_id' => 1,
                'payment_name' => 'Paystack',
                'currency' => 'GHS',
                'image' => 'paystack.png',
                'public_key' => 'pk_test_8a6a139a3bae6e41cbbbc41f4d7b65d4da9f7967',
                'secret_key' => 'sk_test_6ab143b6f0c2a209373adeef55a64411c1a91ae9',
                'encryption_key' => '',
                'environment' => 1,
                'bank_name' => NULL,
                'account_number' => NULL,
                'account_holder_name' => NULL,
                'bank_ifsc_code' => NULL,
                'is_available' => 1,
                'is_activate' => 1,
                'created_at' => '2021-10-21 07:28:12',
                'updated_at' => '2023-01-26 09:13:41'
            ],
            [
                'id' => 6,
                'vendor_id' => 1,
                'payment_name' => 'Banktransfer',
                'currency' => '',
                'image' => 'banktransfer.png',
                'public_key' => NULL,
                'secret_key' => NULL,
                'encryption_key' => '',
                'environment' => 0,
                'bank_name' => 'Axis Bank',
                'account_number' => '1122334455667788',
                'account_holder_name' => 'John Doe',
                'bank_ifsc_code' => '123456',
                'is_available' => 1,
                'is_activate' => 1,
                'created_at' => '2021-10-21 07:28:12',
                'updated_at' => '2023-03-03 13:41:12'
            ],
            [
                'id' => 7,
                'vendor_id' => 1,
                'payment_name' => 'Mercadopago',
                'currency' => 'R$',
                'image' => 'mercadopago.png',
                'public_key' => '-',
                'secret_key' => 'APP_USR-3693146734015792-042811-c6deca56df8ac66e83efb5334c46110c-126508225',
                'encryption_key' => '',
                'environment' => 1,
                'bank_name' => NULL,
                'account_number' => NULL,
                'account_holder_name' => NULL,
                'bank_ifsc_code' => NULL,
                'is_available' => 1,
                'is_activate' => 1,
                'created_at' => '2021-10-21 07:28:12',
                'updated_at' => '2023-03-03 13:41:24'
            ],
            [
                'id' => 8,
                'vendor_id' => 1,
                'payment_name' => 'PayPal',
                'currency' => 'USD',
                'image' => 'paypal.png',
                'public_key' => 'AcRx7vvy79nbNxBemacGKmnnRe_CtxkItyspBS_eeMIPREwfCEIfPg1uX-bdqPrS_ZFGocxEH_SJRrIJ',
                'secret_key' => 'EGtgNkjt3I5lkhEEzicdot8gVH_PcFiKxx6ZBiXpVrp4QLDYcVQQMLX6MMG_fkS9_H0bwmZzBovb4jLP',
                'encryption_key' => '',
                'environment' => 1,
                'bank_name' => NULL,
                'account_number' => NULL,
                'account_holder_name' => NULL,
                'bank_ifsc_code' => NULL,
                'is_available' => 1,
                'is_activate' => 1,
                'created_at' => '2021-10-21 07:28:12',
                'updated_at' => '2023-02-03 15:51:22'
            ],
            [
                'id' => 9,
                'vendor_id' => 1,
                'payment_name' => 'MyFatoorah',
                'currency' => 'KWT',
                'image' => 'myfatoorah.png',
                'public_key' => '',
                'secret_key' => 'rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL',
                'encryption_key' => '',
                'environment' => 1,
                'bank_name' => NULL,
                'account_number' => NULL,
                'account_holder_name' => NULL,
                'bank_ifsc_code' => NULL,
                'is_available' => 1,
                'is_activate' => 1,
                'created_at' => '2021-10-21 07:28:12',
                'updated_at' => '2023-03-06 23:16:55'
            ],
            [
                'id' => 10,
                'vendor_id' => 1,
                'payment_name' => 'toyyibpay',
                'currency' => 'RM',
                'image' => 'toyyibpay.png',
                'public_key' => 'ts75iszg',
                'secret_key' => 'luieh2jt-8hpa-m2xv-wrkv-ejrfvhjppnsj',
                'encryption_key' => '',
                'environment' => 1,
                'bank_name' => NULL,
                'account_number' => NULL,
                'account_holder_name' => NULL,
                'bank_ifsc_code' => NULL,
                'is_available' => 1,
                'is_activate' => 1,
                'created_at' => '2021-10-21 07:28:12',
                'updated_at' => '2023-03-06 23:17:57'
            ],
        ]);
    }
}
