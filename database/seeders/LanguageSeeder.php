<?php

namespace Database\Seeders;

use App\Models\Languages;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //insert english language
        Languages::create([
            'code' => 'en',
            'name' => 'English',
            'image' => 'flag-64005c4be9359.png',
            'layout' => 1,
            'is_default' => 1,
            'is_available' => 1,
        ]);
    }
}
