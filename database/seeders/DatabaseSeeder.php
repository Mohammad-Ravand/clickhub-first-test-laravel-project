<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(LanguageSeeder::class);
        $this->call(SettingsSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(AboutTableSeeder::class);
        $this->call(TermsTableSeeder::class);
        $this->call(PrivacypolicySeeder::class);
    }
}
