<?php

namespace Database\Seeders;

use App\Models\Settings;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::create([
            'id' => 1,
            'vendor_id' => 1,
            'currency' => '$',
            'currency_position' => 'left',
            'maintenance_mode' => 2,
            'checkout_login_required' => 2,
            'logo' => 'default-logo.png',
            'favicon' => 'default-favicon.png',
            'delivery_type' => '',
            'timezone' => 'Asia/Kolkata',
            'address' => '248 Cedar Swamp Rd, Jackson, New Mexico - 08527',
            'email' => 'paponapp2244@gmail.com',
            'mobile' => '',
            'description' => null,
            'contact' => '+919016996697',
            'copyright' => 'Copyright © 2023 Papon IT Solutions. All Rights Reserved',
            'website_title' => 'StoreMart | Admin',
            'meta_title' => 'StoreMart SaaS - Online Product Selling Business Builder SaaS',
            'meta_description' => 'StoreMart is a software as a service (SaaS) platform that allows users to build and manage an online store for selling products. It provides users with a range of features and tools to help them create and customize their store, add and manage products, process orders, and handle payments. StoreMart also includes marketing and analytics tools to help users promote their store and track their performance. It is designed to be user-friendly and easy to use, making it a good option for people who want to start an online store without a lot of technical expertise.',
            'og_image' => 'og_image.png',
            'facebook_link' => 'https://www.facebook.com/',
            'twitter_link' => 'https://twitter.com/',
            'instagram_link' => 'https://www.instagram.com/',
            'linkedin_link' => 'https://www.linkedin.com/',
            'whatsapp_widget' => '',
            'whatsapp_message' => '',
            'item_message' => '',
            'language' => 1,
            'template' => 1,
            'primary_color' => '#01112b',
            'secondary_color' => '#f05a1e',
            'custom_domain' => '-',
            'cname_title' => 'Read All Instructions Carefully Before Sending Custom Domain Request',
            'cname_text' => '<p>If you\'re using cPanel or Plesk then you need to manually add custom domain in your server with the same root directory as the script\'s installation&nbsp;and user need to point their custom domain A record with your server IP. Example : 68.178.145.4</p>',
            'interval_time' => '',
            'interval_type' => 0,
            'time_format' => 1,
            'banner' => '',
            'tracking_id' => 'UA-168896572-2',
            'view_id' => '284502084',
            'firebase' => 'AAAAlio1OzI:APA91bG85HXcf1TKLW_T8CqOh2HwYPTb58yxLyv93v9e1tRvEojTNFi9Um-sFQHzTZ_O6w6gjy1KNwhKF72hW0wvaHElwJGTrsVKELGAGc_Ff0r1arQBMZwwX9gNXz-mKMMZVigUUl86',
            'cover_image' => '',
            'notification_sound' => '',
            'created_at' => '2023-03-30 00:51:14',
            'updated_at' => null,
        ]);
    }
}
