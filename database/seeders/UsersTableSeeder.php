<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id' => 1,
            'name' => 'Admin',
            'slug' => 'admin',
            'email' => 'admin@gmail.com',
            'mobile' => '1234567890',
            'image' => 'default-logo.png',
            'password' => Hash::make('admin'), // Replace YOUR_PASSWORD_HERE with the desired password for the admin user
            'google_id' => '',
            'facebook_id' => '',
            'login_type' => 'normal',
            'type' => 1,
            'description' => '',
            'plan_id' => '',
            'token' => 'cNjSsm-TREC9n58ZQeIDBL:APA91bHSLQ2S9VFhM2yGoQJG7d-noXkcsVXRQi8Y-XSUJIFZQgzF75Kbu5beKH8dGUZ9SqIND3yauVdcG6-SwcVjU4oIjpJUx5JC9cORZp-NvPtNkJT1IMLb0KgnN68UWAtzwvri8KqT',
            'country_id' => '',
            'city_id' => '',
            'purchase_amount' => '',
            'purchase_date' => '',
            'available_on_landing' => '',
            'payment_id' => '',
            'payment_type' => '',
            'free_plan' => 0,
            'is_delivery' => '',
            'allow_without_subscription' => 2,
            'is_verified' => 2,
            'is_available' => 2,
            'remember_token' => '',
            'license_type' => 'Extended License',
            'created_at' => '2022-08-15 23:01:17',
            'updated_at' => '2023-03-31 13:27:51',
        ]);
    }
}
