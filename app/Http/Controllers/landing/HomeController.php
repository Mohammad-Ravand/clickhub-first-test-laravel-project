<?php
namespace App\Http\Controllers\landing;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Features;
use App\Models\PricingPlan;
use App\Models\User;
use App\Models\Testimonials;
use App\Models\Blog;
use App\Models\Subscriber;
use App\Models\Settings;
use App\Models\Country;
use App\Models\City;
use App\Models\Terms;
use App\Models\About;
use App\Models\Privacypolicy;
use App\Models\Promotionalbanner;
use App\Models\Faq;
use App\Models\Contact;
class HomeController extends Controller
{
    public function lending()
    {
        // if the current host contains the website domain
        $host = $_SERVER['HTTP_HOST'];
        if ($host  ==  env('WEBSITE_HOST')) {
            $settingdata = helper::appdata('')->first();
            $planlist = PricingPlan::where('is_available',1)->orderBy('price')->get();
            return view('landing.index', compact('planlist'));
        }
        // if the current host doesn't contain the website domain (meaning, custom domain)
        else {
            // if the current package doesn't have 'custom domain' feature || the custom domain is not connected
            $settingdata = Settings::where('custom_domain', $host)->first();
            $vendor = User::where('id', @$settingdata->vendor_id)->first();
            $vendordata = helper::vendor_data(@$vendor->slug);
            if(empty($vendordata))
            {
                abort(404);
            }
            $getbannersection1 = Banner::where('section', '1')->where('is_available',1)->where('vendor_id', $vendordata->id)->orderByDesc('id')->get();
            $getbannersection2 = Banner::where('section', '2')->where('is_available',1)->where('vendor_id', $vendordata->id)->orderByDesc('id')->get();
            $getbannersection3 = Banner::where('section', '3')->where('is_available',1)->where('vendor_id', $vendordata->id)->orderByDesc('id')->get();
            $getblog = Blog::where('vendor_id', $vendordata->id)->orderByDesc('id')->take(3)->get();
            $getcategories = Category::where('is_available', "1")->where('is_deleted', "2")->where('vendor_id', $vendordata->id)->get();
            $getservices = Service::with('service_image', 'category_info')->where('is_available', "1")->where('is_deleted', "2")->where('vendor_id', $vendordata->id)->orderByDesc('id')->take(12)->get();
            return view('landing.index', compact('getbannersection1', 'getbannersection2', 'getbannersection3', 'getblog', 'getcategories', 'getservices', 'vendordata'));
        }
    }
    public function index(Request $request)
    {
     
        $admindata =User::where('type',1)->first();
        $features = Features::where('vendor_id',$admindata->id)->get();
        $planlist = PricingPlan::where('is_available',1)->orderBy('price')->get();
        $testimonials = Testimonials::where('vendor_id',$admindata->id)->orderByDesc('id')->get();
        $blogs = Blog::where('vendor_id',$admindata->id)->orderByDesc('id')->take(6)->get();
        $userdata = User::select('users.id','name','slug','settings.description','website_title','cover_image')->where('available_on_landing',1)->join('settings','users.id', '=', 'settings.vendor_id')->get();
        return view('landing.index',compact('features','planlist','testimonials','blogs','userdata'));
    }
    public function emailsubscribe(Request $request)
    {
        $newsubscriber = new Subscriber();
        $newsubscriber->vendor_id = 1;
        $newsubscriber->email = $request->email;
        $newsubscriber->save();
        return redirect()->back()->with('success',trans('messages.success'));
    }
    public function inquiry(Request $request)
    {
        $newinquiry = new Contact();
        $newinquiry->vendor_id = 1;
        $newinquiry->name = $request->name;
        $newinquiry->email = $request->email;
        $newinquiry->mobile = $request->mobile;
        $newinquiry->message = $request->message;
        $newinquiry->save();
        return redirect()->back()->with('success',trans('messages.success'));
    }
    public function termscondition()
    {
       
        $terms= Terms::select('terms_content')->where('vendor_id',1)->first();
        return view('landing.terms_condition',compact('terms'));
    }
    public function aboutus()
    {
       
        $aboutus= About::select('about_content')->where('vendor_id',1)->first();
        return view('landing.aboutus',compact('aboutus'));
    }
    public function privacypolicy()
    {
        
        $privacypolicy= Privacypolicy::select('privacypolicy_content')->where('vendor_id',1)->first();
        return view('landing.privacypolicy',compact('privacypolicy'));
    }
  
    public function faqs(Request $request)
    {
       
        $allfaqs = Faq::where('vendor_id',1)->get();
        return view('landing.faq',compact('allfaqs'));
    }
    public function contact()
    {
        return view('landing.contact');
    }
    public function allstores(Request $request)
    {
        $countries =Country::where('is_deleted',2)->where('is_available',1)->get();
        $banners = Promotionalbanner::with('vendor_info')->get();
        if($request->country =="" && $request->city =="")
        {
            $stores = User::where('available_on_landing',1);
        }
        $city_name = "";
        if($request->has('country') && $request->country !="")
        {
            $country = Country::select('id')->where('name',$request->country)->first();
            $stores = User::where('available_on_landing',1)->where('country_id',$country->id);
        }
        if($request->has('city') && $request->city !="")
        {
            $city = City::where('city',$request->city)->first();
            $stores = User::where('available_on_landing',1)->where('city_id',$city->id);
            $city_name = $city->city;
        }
        
        if( $stores != null)
        {
            $stores = $stores->paginate(12);
        }
        
        return view('landing.stores',compact('countries','stores','city_name','banners'));
    }
}
