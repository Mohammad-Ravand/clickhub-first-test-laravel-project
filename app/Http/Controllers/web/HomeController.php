<?php
namespace App\Http\Controllers\web;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Category;
use App\Models\Item;
use App\Models\Cart;
use App\Models\DeliveryArea;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\Settings;
use App\Models\Subscriber;
use App\Models\User;
use App\Models\Timing;
use App\Models\Payment;
use App\Models\Contact;
use App\Models\Coupons;
use App\Models\Terms;
use App\Models\About;
use App\Models\Privacypolicy;
use App\Models\Banner;
use App\Helpers\helper;
use URL;
use Session;
use Omnipay\Omnipay;
use Illuminate\Support\Facades\Auth;
use MyFatoorah\Library\PaymentMyfatoorahApiV2;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
class HomeController extends Controller
{
    /**3
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $storeinfo = helper::storeinfo($request->vendor);
        $getcategory = Category::where('vendor_id', @$storeinfo->id)->where('is_available', '=', '1')->where('is_deleted', '2')->orderBy('id', 'ASC')->get();
        $getitem = Item::with(['variation', 'extras'])->where('vendor_id', @$storeinfo->id)->where('is_available', '1')->orderBy('id', 'ASC')->get();
        $settingdata = Settings::where('vendor_id', @$storeinfo->id)->select('template')->first();
        
        $bannerimage = Banner::where('vendor_id', @$storeinfo->id)->orderBy('id', 'ASC')->get();
        $cartitems = Cart::select('id', 'item_id', 'item_name', 'item_image', 'item_price', 'extras_id', 'extras_name', 'extras_price', 'qty', 'price', 'tax', 'variants_id', 'variants_name', 'variants_price')
            ->where('vendor_id', @$storeinfo->id);
        if(Auth::user() && Auth::user()->type == 3) {
            $cartitems->where('user_id', @Auth::user()->id);
        } else {
            $cartitems->where('session_id', Session::getId());
        }
        $cartdata = $cartitems->get();
        if (empty($storeinfo)) {
            abort(404);
        }
        if(Auth::user() && Auth::user()->type == 3) {
            $count = Cart::where('user_id', Auth::user()->id)->where('vendor_id', @$storeinfo->id)->count();
        } else {
            $count = Cart::where('session_id', Session::getId())->where('vendor_id', @$storeinfo->id)->count();
        }
        session()->put('cart', $count);
        return view('front.template-' . $settingdata->template . '.home', compact('getcategory', 'getitem', 'storeinfo', 'bannerimage', 'cartdata'));
    }
    public function privacyshow(Request $request)
    {
        $storeinfo = helper::storeinfo($request->vendor);
        $privacy = Privacypolicy::where('vendor_id', @$storeinfo->id)->orderBy('id', 'ASC')->first();
        return view('front.privacy', compact('storeinfo', 'privacy'));
    }
    public function terms_condition(Request $request)
    {
        $storeinfo = helper::storeinfo($request->vendor);
        $terms = terms::where('vendor_id', @$storeinfo->id)->orderBy('id', 'ASC')->first();
        return view('front.terms', compact('storeinfo', 'terms'));
    }
    public function aboutus(Request $request)
    {
        $storeinfo = helper::storeinfo($request->vendor);
        $about = About::where('vendor_id', @$storeinfo->id)->orderBy('id', 'ASC')->first();
        return view('front.about', compact('storeinfo', 'about'));
    }
    public function show(Request $request)
    {
        $storeinfo = helper::storeinfo($request->vendor);
        $getcategory = Category::where('is_available', '=', '1')->where('is_deleted', '2')->where('vendor_id', @$storeinfo->id)->orderBy('id', 'ASC')->get();
        $getitem = Item::where('cat_id', '=', $request->id)->where('is_available', '1')->where('vendor_id', @$storeinfo->id)->orderBy('id', 'ASC')->paginate(9);
        $settingdata = Settings::where('vendor_id', $storeinfo->id)->select('template')->first();
        $cartitems = Cart::select('id', 'item_id', 'item_name', 'item_image', 'item_price', 'extras_id', 'extras_name', 'extras_price', 'qty', 'price', 'tax', 'variants_id', 'variants_name', 'variants_price')
            ->where('vendor_id', @$storeinfo->id);
        if(Auth::user() && Auth::user()->type == 3) {
            $cartitems->where('user_id', @Auth::user()->id);
        } else {
            $cartitems->where('session_id', Session::getId());
        }
        $cartdata = $cartitems->get();
        return view('front.template-' . $settingdata->template . '.home', compact('getcategory', 'getitem', 'storeinfo', 'bannerimage', 'cartdata'));
    }
    public function details(Request $request)
    {
        $getitem = Item::with(['variation', 'extras'])->select('items.vendor_id', 'items.id', \DB::raw("CONCAT('" . asset('/storage/app/public/item/') . "/', items.image) AS image"), 'items.image as image_name', 'items.item_name', 'items.item_price', 'items.item_original_price', 'items.tax', 'items.description', 'categories.name')
            ->join('categories', 'items.cat_id', '=', 'categories.id')
            ->where('items.id', $request->id)->first();
        if (count($getitem['variation']) <= 0) {
            $getitem->item_p = helper::currency_formate($getitem->item_price, $getitem->vendor_id);
            $getitem->item_original_p = helper::currency_formate($getitem->item_original_price, $getitem->vendor_id);
            $getitem->item_original_price = $getitem->item_original_price;
        }
        return response()->json(['ResponseCode' => 1, 'ResponseText' => 'Success', 'ResponseData' => $getitem], 200);
    }
    public function addtocart(Request $request)
    {
        try {
            $cart = new Cart;
            if(Auth::user() && Auth::user()->type == 3)
            {
                $cart->user_id = Auth::user()->id;
            } else {
                $cart->session_id = Session::getId();
            }
            $cart->vendor_id = $request->vendor_id;
            $cart->item_id = $request->item_id;
            $cart->item_name = $request->item_name;
            $cart->item_image = $request->item_image;
            $cart->item_price = $request->item_price;
            $cart->tax = $request->tax;
            $cart->extras_id = $request->extras_id;
            $cart->extras_name = $request->extras_name;
            $cart->extras_price = $request->extras_price;
            $cart->qty = $request->qty;
            $cart->price = $request->price;
            $cart->variants_id = $request->variants_id;
            $cart->variants_name = $request->variants_name;
            $cart->variants_price = $request->variants_price;
            $cart->save();
            if(Auth::user() && Auth::user()->type == 3)
            {
                $count = Cart::where('user_id', Auth::user()->id)->where('vendor_id', @$storeinfo->id)->count();
            } else {
                $count = Cart::where('session_id', Session::getId())->where('vendor_id', @$storeinfo->id)->count();
            }
            session()->put('cart', $count);
            session()->put('vendor_id', $request->vendor_id);
            return response()->json(['status' => 1, 'message' => 'Item has been added to your cart'], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'message' => $e], 400);
        }
    }
    public function cart(Request $request)
    {
        $storeinfo = helper::storeinfo($request->vendor);
        $cartitems = Cart::select('id', 'item_id', 'item_name', 'item_image', 'item_price', 'extras_id', 'extras_name', 'extras_price', 'qty', 'price', 'tax', 'variants_id', 'variants_name', 'variants_price')
            ->where('vendor_id', @$storeinfo->id);
        if(Auth::user() && Auth::user()->type == 3) {
            $cartitems->where('user_id', @Auth::user()->id);
        } else {
            $cartitems->where('session_id', Session::getId());
        }
        $cartdata = $cartitems->get();
    
        return view('front.cart', compact('cartdata', 'storeinfo'));
    }
    public function checkout(Request $request)
    {
        $storeinfo = helper::storeinfo($request->vendor);
        $cartitems = Cart::select('id', 'item_id', 'item_name', 'item_image', 'item_price', 'extras_id', 'extras_name', 'extras_price', 'qty', 'price', 'tax', 'variants_id', 'variants_name', 'variants_price')
            ->where('vendor_id', @$storeinfo->id);
        if(Auth::user() && Auth::user()->type == 3) {
            $cartitems->where('user_id', @Auth::user()->id);
        } else {
            $cartitems->where('session_id', Session::getId());
        }
        $cartdata = $cartitems->get();
        $deliveryarea = DeliveryArea::where('vendor_id', @$storeinfo->id)->get();
        $paymentlist = Payment::where('is_available', '1')->where('vendor_id', @$storeinfo->id)->where('is_activate',1)->get();
        $coupons = Coupons::where('vendor_id', @$storeinfo->id)->orderBy('id', 'ASC')->get();
        
        return view('front.checkout', compact('cartdata', 'deliveryarea', 'storeinfo', 'paymentlist', 'coupons'));
    }
    public function qtyupdate(Request $request)
    {
        if ($request->cart_id == "") {
            return response()->json(["status" => 0, "message" => "Cart ID is required"], 200);
        }
        if ($request->qty == "") {
            return response()->json(["status" => 0, "message" => "Qty is required"], 200);
        }
        $cartdata = Cart::where('id', $request->cart_id)
            ->get()
            ->first();
        if ($request->type == "decreaseValue") {
            $qty = $cartdata->qty - 1;
        } else {
            $qty = $cartdata->qty + 1;
        }
        $update = Cart::where('id', $request['cart_id'])->update(['qty' => $qty]);
        return response()->json(['status' => 1, 'message' => 'Qty has been update'], 200);
    }
    public function deletecartitem(Request $request)
    {
        if ($request->cart_id == "") {
            return response()->json(["status" => 0, "message" => "Cart Id is required"], 200);
        }
        $cart = Cart::where('id', $request->cart_id)->delete();
        if(Auth::user() && Auth::user()->type == 3) {
            $count = Cart::where('user_id', Auth::user()->id)->where('vendor_id', @$storeinfo->id)->count();
        } else {
            $count = Cart::where('session_id', Session::getId())->where('vendor_id', @$storeinfo->id)->count();
        }
        session()->forget(['offer_amount', 'offer_code', 'offer_type']);
        if ($cart) {
            return response()->json(['status' => 1, 'message' => 'Success', 'cartcnt' => $count], 200);
        } else {
            return response()->json(['status' => 0], 200);
        }
    }
    public function applypromocode(Request $request)
    {
        if ($request->promocode == "") {
            return response()->json(["status" => 0, "message" => trans('messages.enter_promocode')], 200);
        }
        $promocode = Coupons::select('price', 'type', 'code')->where('code', $request->promocode)->first();
        if ($request->sub_total < @$promocode->price) {
            return response()->json(["status" => 0, "message" => trans('messages.not_eligible')], 200);
        }
        session([
            'offer_amount' => @$promocode->price,
            'offer_code' => @$promocode->code,
            'offer_type' => @$promocode->type,
        ]);
        if (@$promocode->code == $request->promocode) {
            return response()->json(['status' => 1, 'message' => trans('messages.promocode_applied'), 'data' => $promocode], 200);
        } else {
            return response()->json(['status' => 0, 'message' => trans('messages.wrong_promocode')], 200);
        }
    }
    public function removepromocode(Request $request)
    {
        $remove = session()->forget(['offer_amount', 'offer_code', 'offer_type']);
        if (!$remove) {
            return response()->json(['status' => 1, 'message' => trans('messages.promocode_removed')], 200);
        } else {
            return response()->json(['status' => 0, 'message' => trans('messages.wrong')], 200);
        }
    }
    public function checkplan(Request $request)
    {
        $checkplan = helper::checkplan($request->vendor, '');
        return $checkplan;
    }
    public function paymentmethod(Request $request)
    {
        $orderresponse = helper::createorder($request->vendor_id, $request->payment_type, $request->payment_id, $request->customer_email, $request->customer_name, $request->customer_mobile, $request->stripeToken, $request->grand_total, $request->delivery_charge, $request->address, $request->building, $request->landmark, $request->postal_code, $request->discount_amount, $request->sub_total, $request->tax, $request->delivery_time, $request->delivery_date, $request->delivery_area, $request->couponcode, $request->order_type, $request->notes);
        return response()->json(['status' => 1, 'message' => trans('messages.order_placed'), "order_number" => $orderresponse], 200);
    }
   
    public function terms(Request $request)
    {
        $storeinfo = helper::storeinfo($request->vendor);
        $terms = Terms::select('terms_content')
            ->where('vendor_id', @$storeinfo->id)
            ->first();
       
        return view('front.terms', compact('storeinfo', 'terms'));
    }
    public function privacy(Request $request)
    {
        $storeinfo = helper::storeinfo($request->vendor);
        $privacypolicy = Privacypolicy::select('privacypolicy_content')
            ->where('vendor_id', @$storeinfo->id)
            ->first();
        return view('front.privacy', compact('storeinfo', 'privacypolicy'));
    }
    public function book(Request $request)
    {
        $storeinfo = helper::storeinfo($request->vendor);
        
        return view('front.book', compact('storeinfo'));
    }
    public function trackorder(Request $request)
    {
        $storeinfo = helper::storeinfo($request->vendor);
        $status = Order::select('order_number', DB::raw('DATE_FORMAT(created_at, "%d %M %Y") as date'), 'address', 'building', 'landmark', 'pincode', 'order_type', 'id', 'discount_amount', 'order_number', 'status', 'order_notes', 'tax', 'delivery_charge', 'couponcode', 'sub_total', 'grand_total', 'customer_name', 'customer_email', 'mobile')->where('order_number', $request->ordernumber)->first();
        $orderdata = Order::where('order_number', $request->ordernumber)->get();
        $orderdetails = OrderDetails::where('order_details.order_id', $status->id)->get();
        $summery = array(
            'id' => "$status->id",
            'tax' => "$status->tax",
            'discount_amount' => $status->discount_amount,
            'order_number' => $status->order_number,
            'created_at' => $status->date,
            'delivery_charge' => "$status->delivery_charge",
            'address' => $status->address,
            'building' => $status->building,
            'landmark' => $status->landmark,
            'pincode' => $status->pincode,
            'order_notes' => $status->order_notes,
            'status' => $status->status,
            'order_type' => $status->order_type,
            'couponcode' => $status->couponcode,
            'sub_total' => $status->sub_total,
            'grand_total' => $status->grand_total,
            'customer_name' => "$status->customer_name",
            'customer_email' => "$status->customer_email",
            'mobile' => "$status->mobile",
        );
        return view('front.track-order', compact('storeinfo', 'orderdata', 'summery', 'orderdetails'));
    }
    public function ordersuccess(Request $request)
    {

        $storeinfo = helper::storeinfo($request->vendor);
        $orderdata = Order::where('order_number', $request->order_number)->first();
        $data = OrderDetails::where('order_id', $orderdata->id)->get();
            foreach ($data as $value) {
                if ($value['variants_id'] != "") {
                    $item_p = $value['qty'] * $value['variants_price'];
                    $variantsdata = '(' . $value['variants_name'] . ')';
                } else {
                    $variantsdata = "";
                    $item_p = $value['qty'] * $value['price'];
                }
                $extras_id = explode(",", $value['extras_id']);
                $extras_name = explode(",", $value['extras_name']);
                $extras_price = explode(",", $value['extras_price']);
                $item_message = helper::appdata($orderdata->vendor_id)->item_message;
                $itemvar = ["{qty}", "{item_name}", "{variantsdata}", "{item_price}"];
                $newitemvar = [$value['qty'], $value['item_name'], $variantsdata, helper::currency_formate($item_p, $orderdata->vendor_id)];
                $pagee[] = str_replace($itemvar, $newitemvar, $item_message);
                if ($value['extras_id'] != "") {
                    foreach ($extras_id as $key => $addons) {
                        @$pagee[] .= "👉" . $extras_name[$key] . ':' . helper::currency_formate($extras_price[$key], $orderdata->vendor_id) . '%0a';
                    }
                }
            }
            $items = implode(",", $pagee);
        
      
        $itemlist = str_replace(',', '%0a', $items);
        if ($orderdata->order_type == 1) {
            $order_type = trans('labels.delivery');
        } else {
            $order_type = trans('labels.pickup');
        }
        //payment_type = COD : 1,RazorPay : 2, Stripe : 3, Flutterwave : 4, Paystack : 5, Mercado Pago : 7, PayPal : 8, MyFatoorah : 9, toyyibpay : 10
        if ($orderdata->payment_type == 1) {
            $payment_type = trans('labels.cod');
        }
        if ($orderdata->payment_type == 2) {
            $payment_type = trans('labels.razorpay');
        }
        if ($orderdata->payment_type == 3) {
            $payment_type = trans('labels.stripe');
        }
        if ($orderdata->payment_type == 4) {
            $payment_type = trans('labels.flutterwave');
        }
        if ($orderdata->payment_type == 5) {
            $payment_type = trans('labels.paystack');
        }
        if ($orderdata->payment_type == 7) {
            $payment_type = trans('labels.mercadopago');
        }
        if ($orderdata->payment_type == 8) {
            $payment_type = trans('labels.paypal');
        }
        if ($orderdata->payment_type == 9) {
            $payment_type = trans('labels.myfatoorah');
        }
        if ($orderdata->payment_type == 10) {
            $payment_type = trans('labels.toyyibpay');
        }
        if (!empty($request->order_number) && !empty($orderdata)) {
            $vendorinfo = User::where('id', $orderdata->vendor_id)->first();
            $storeinfo = helper::storeinfo($vendorinfo->slug);
            $whatsapp_message = helper::appdata($orderdata->vendor_id)->whatsapp_message;
            $vendorinfo = User::where('id', $orderdata->vendor_id)->first();
            $var = ["{delivery_type}", "{order_no}", "{item_variable}", "{sub_total}", "{total_tax}", "{delivery_charge}", "{discount_amount}", "{grand_total}", "{notes}", "{customer_name}", "{customer_mobile}", "{address}", "{building}", "{landmark}", "{postal_code}", "{date}", "{time}", "{payment_type}", "{store_name}", "{track_order_url}", "{store_url}"];
            $newvar = [$order_type, $orderdata->order_number, $itemlist, helper::currency_formate($orderdata->sub_total, $orderdata->vendor_id), helper::currency_formate($orderdata->tax, $orderdata->vendor_id), helper::currency_formate($orderdata->delivery_charge, $orderdata->vendor_id), helper::currency_formate($orderdata->discount_amount, $orderdata->vendor_id), helper::currency_formate($orderdata->grand_total, $orderdata->vendor_id), $orderdata->order_notes, $orderdata->customer_name, $orderdata->mobile, $orderdata->address, $orderdata->building, $orderdata->landmark, $orderdata->postal_code, $orderdata->delivery_date, $orderdata->delivery_time, $payment_type, $vendorinfo->name, URL::to($vendorinfo->slug . "/track-order/" . $orderdata->order_number), URL::to($vendorinfo->slug)];
            $whmessage = str_replace($var, $newvar, str_replace("\n", "%0a", $whatsapp_message));
            return view('front.ordersuccess', compact('storeinfo', 'orderdata', 'whmessage'));
        } else {
            abort(404);
        }
    }

    public function ordercreate(Request $request)
    {
        
        if (@$request->paymentId != "") {
            $paymentid = $request->paymentId;
        }
        if (@$request->payment_id != "") {
            $paymentid = $request->payment_id;
        }
        if (@$request->transaction_id != "") {
            $paymentid = $request->transaction_id;
        }
        $orderresponse = helper::createorder(Session::get('vendor_id'), Session::get('payment_type'), @$paymentid, Session::get('customer_email'), Session::get('customer_name'), Session::get('customer_mobile'), Session::get('stripeToken'), Session::get('grand_total'), Session::get('delivery_charge'), Session::get('address'), Session::get('building'), Session::get('landmark'), Session::get('postal_code'), Session::get('discount_amount'), Session::get('sub_total'), Session::get('tax'), Session::get('delivery_time'), Session::get('delivery_date'), Session::get('delivery_area'), Session::get('couponcode'), Session::get('order_type'), Session::get('notes'));
        $slug = Session::get('slug');
        $order_number = $orderresponse;
        return view('front.mercadoorder', compact('slug', 'order_number'));
    }
    
 
   
 
    public function timeslot(Request $request)
    {
        try {
            $timezone = helper::appdata($request->vendor_id);
            date_default_timezone_set($timezone->timezone);
            $day = date('l', strtotime($request->inputDate));
            $minute = "";
            $time = Timing::where('vendor_id', $request->vendor_id)->where('day', $day)->first();
            if ($time->is_always_close == 1) {
                $slots = "1";
            } else {
                if (helper::appdata($request->vendor_id)->interval_type == 2) {
                    $minute = (float)helper::appdata($request->vendor_id)->interval_time * 60;
                }
                if (helper::appdata($request->vendor_id)->interval_type == 1) {
                    $minute = helper::appdata($request->vendor_id)->interval_time;
                }
                if (helper::appdata($request->vendor_id)->time_format == 1) {
                    $format = "h:i a";
                }
                if (helper::appdata($request->vendor_id)->time_format == 2) {
                    $format = "H:i";
                }
                $period = new CarbonPeriod(".$time->open_time.", $minute . 'minutes', ".$time->close_time.");
                $currenttime = Carbon::now()->format($format);
                $current_date = Carbon::now()->format('Y-m-d');
                $slots = [];
                foreach ($period as $item) {
                    $slottime = $item->format('h:i a');
                    if (strtotime($slottime) !=  strtotime($time->close_time)) {
                        if (strtotime($slottime) != strtotime($time->break_start)) {
                            if (strtotime($slottime) != strtotime($time->break_end)) {
                                if ($request->inputDate == $current_date) {
                                    if (strtotime($slottime) <= strtotime($currenttime)) {
                                        $status = "";
                                    } else {
                                        $status = "active";
                                    }
                                } else {
                                    $status = "active";
                                }
                                if (date('h:i a', strtotime($slottime)) ==  date('h:i a', strtotime($time->open_time))) {
                                    $slottime = $time->open_time;
                                }
                                $endtime = $item->addMinutes($minute)->format('h:i a');
                                if ($endtime  == "00:00 AM") {
                                    $endtime = $time->close_time;
                                }
                                $slots[] = array(
                                    'slot' =>  $slottime . ' - ' . $endtime,
                                    'status' => $status,
                                );
                            }
                        }
                    }
                }
            }
            return $slots;
        } catch (\Throwable $th) {
            return response()->json(['status' => 0, 'message' => trans('messages.wrong')], 200);
        }
    }
    public function user_subscribe(Request $request)
    {
        try {
            $request->validate([
                'email' => 'required',
            ], [
    
                'email.required' => trans('messages.email_required'),
            ]);
           
            $subscribe= new Subscriber();
            $subscribe->vendor_id = $request->id;
            $subscribe->email = $request->email;
            $subscribe->save();
            return redirect()->back()->with('success',trans('messages.success'));
        } catch (\Throwable $th) {
          
            return redirect()->back()->with('error',trans('messages.wrong'));
        }
        
    }

    public function contact(Request $request)
    {
        $storeinfo = helper::storeinfo($request->vendor);
        $getworkinghours = Timing::where('vendor_id',$storeinfo->id)->get();
        $day = date('l', time());
        $todayworktime = Timing::where('vendor_id',$storeinfo->id)->where('day',$day)->first();
        $contactinfo = Settings::where('vendor_id',$storeinfo->id)->first();
        return view('front.contact',compact('storeinfo','getworkinghours','todayworktime','contactinfo'));
    }
    public function save_contact(Request $request)
    {
        $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'mobile' => 'required',
            'message' => 'required',
        ], [
            'fname.required' =>trans('message.first_name_required'),
            'lname.required' =>trans('message.last_name_required'),
            'email.required' =>trans('message.email_required'),
            'email.email' =>trans('message.invalid_email'),
            'mobile.required' =>trans('message.mobile_required'),
            'message.required' => trans('messages.message_required'),
        ]);
        $newcontact = new Contact();
        $newcontact->vendor_id = $request->vendor_id;
        $newcontact->name = $request->fname . $request->lname;
        $newcontact->email = $request->email;
        $newcontact->mobile = $request->mobile;
        $newcontact->message = $request->message;
        $newcontact->save();
        $vendordata = User::select('name','email')->where('id', $request->vendor_id)->first();
        helper::vendor_contact_data($vendordata->name,$vendordata->email,$request->fname . $request->lname,$request->email,$request->mobile,$request->message);

        return redirect()->back()->with('success', trans('messages.success'));
    }

    public function cancelorder($vendor,$order_number)
    {
        $update = Order::where('order_number', $order_number)->update(['status' => "4"]);

        $orderdata = Order::where('order_number', $order_number)->first();

        $emaildata = User::select('id', 'name', 'slug', 'email', 'mobile','token')->where('id', $orderdata->vendor_id)->first();
        
        $title = trans('labels.order_update');
        $body = "#".$order_number." has been cancelled";

        helper::push_notification($emaildata->token,$title,$body,"order",$orderdata->id);

        return redirect()->back()->with('success', trans('messages.success'));
    }
    
    public function invoice(Request $request)
    {
        $getorderdata = Order::where('order_number', $request->order_number)->first();
        if (empty($getorderdata)) {
            abort(404);
        }
        $ordersdetails = OrderDetails::where('order_id', $getorderdata->id)->get();
        return view('front.invoice', compact('getorderdata', 'ordersdetails'));
    }
}
