<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\helper;
use App\Models\Category;
use App\Models\Item;
use App\Models\Variants;
use App\Models\Cart;
use App\Models\Extra;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
class ProductController extends Controller
{
    public function index()
    {
        $getproductslist = Item::with('variation', 'category_info')->where('vendor_id', Auth::user()->id)->orderByDesc('id')->get();
        return view('admin.product.product', compact('getproductslist'));
    }
    public function add(Request $request)
    {
        $checkplan = helper::checkplan(Auth::user()->id, '');
        $v = json_decode(json_encode($checkplan));
        if (@$v->original->status == 2) {
            return redirect('admin/products')->with('error', @$v->original->message);
        }
        $getcategorylist = Category::where('is_available', 1)->where('is_deleted', 2)->where('vendor_id', Auth::user()->id)->get();
        return view('admin.product.add_product', compact("getcategorylist"));
    }
    public function save(Request $request)
    {
        $checkplan = helper::checkplan(Auth::user()->id, '');
        $v = json_decode(json_encode($checkplan));
        if (@$v->original->status == 2) {
            return redirect('admin/products')->with('error', @$v->original->message);
        }
        $request->validate([
            'product_name' => 'required',
            'price' => 'required_if:has_variants,2',
            'description' => 'required',
            'tax' => 'required',
            'category' => 'required',
            'product_image' => 'required|image|mimes:jpg,jpeg,png',
        ], [
                'product_name.required' => trans('messages.name_required'),
                'price.required_if' => trans('messages.price_required'),
                'description.required' => trans('messages.description_required'),
                'tax.required' => trans('messages.tax_required'),
                'category.required' => trans('messages.category_required'),
                'product_image.required' => trans('messages.image_required'),
                'product_image.image' => trans('messages.valid_image'),
                'product_image.mimes' => trans('messages.valid_image_format'),
            ]);
        $check_slug = Item::where('slug', Str::slug($request->product_name, '-'))->first();
        if (!empty($check_slug)) {
            $last_id = Item::select('id')->orderByDesc('id')->first()->id;
            $slug = Str::slug($request->product_name . ' ' . $last_id, '-');
        } else {
            $slug = Str::slug($request->product_name, '-');
        }
        $price = $request->price;
        $original_price = $request->original_price;
        if ($request->has_variants == 1) {
            foreach ($request->variation as $key => $no) {
                if (@$no != "" && @$request->variation_price[$key] != "" && @$request->variation_original_price[$key] != "") {
                    $price = $request->variation_price[$key];
                    $original_price = $request->variation_original_price[$key];
                    break;
                }
            }
        }
        $product = new Item();
        $product->vendor_id = Auth::user()->id;
        $product->cat_id = $request->category;
        $product->item_name = $request->product_name;
        $product->slug = $slug;
        $product->item_price = $price;
        $product->item_original_price = $original_price;
        $product->has_variants = $request->has_variants;
        $product->tax = $request->tax;
        $product->description = $request->description;
        if ($request->has('product_image')) {
            $image = 'item-' . uniqid() . "." . $request->file('product_image')->getClientOriginalExtension();
            $request->file('product_image')->move(storage_path('app/public/item/'), $image);
            $product->image = $image;
        }
        $product->save();
        if ($request->has_variants == 1) {
            foreach ($request->variation as $key => $no) {
                if (@$no != "" && @$request->variation_price[$key] != "") {
                    $variation = new Variants();
                    $variation->item_id = $product->id;
                    $variation->name = $no;
                    $variation->price = $request->variation_price[$key];
                    $variation->original_price = $request->variation_original_price[$key];
                    $variation->save();
                }
            }
        }
        foreach ($request->extras_name as $key => $no) {
            if (@$no != "" && @$request->extras_price[$key] != "") {
                $extras = new Extra();
                $extras->item_id = $product->id;
                $extras->name = $no;
                $extras->price = $request->extras_price[$key];
                $extras->save();
            }
        }
        return redirect('admin/products/')->with('success', trans('messages.success'));
    }
    public function edit($slug)
    {
        $getproductdata = Item::where('slug', $slug)->first();
        if (!empty($getproductdata)) {
            $getcategorylist = Category::where('is_available', 1)->where('is_deleted', 2)->where('vendor_id', Auth::user()->id)->get();
            return view('admin.product.edit_product', compact('getproductdata', 'getcategorylist'));
        }
        return redirect('admin/products')->with('error', trans('messages.wrong'));
    }
    public function update_product(Request $request, $slug)
    {
        $request->validate([
            'product_name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'tax' => 'required',
            'category' => 'required',
        ], [
                'product_name.required' => trans('messages.name_required'),
                'price.required' => trans('messages.price_required'),
                'description.required' => trans('messages.description_required'),
                'tax.required' => trans('messages.tax_required'),
                'category.required' => trans('messages.category_required')
            ]);
        try {
            $check_slug = Item::where('slug', Str::slug($request->product_name, '-'))->first();
            if (!empty($check_slug)) {
                $last_id = Item::select('id')->orderByDesc('id')->first()->id;
                $slug = Str::slug($request->product_name . ' ' . $last_id, '-');
            } else {
                $slug = Str::slug($request->product_name, '-');
            }
            $price = $request->price;
            $original_price = $request->original_price;
            if ($request->has_variants == 1) {
                $variation_id = $request->variation_id;
                foreach ($request->variation as $key => $no) {
                    if (@$no != "" && @$request->variation_price[$key] != "" && @$request->variation_original_price[$key] != "") {
                        if (@$variation_id[$key] == "") {
                            $price = $request->variation_price[$key];
                            $original_price = $request->variation_original_price[$key];
                            break;
                        } else if (@$variation_id[$key] != "") {
                            $price = $request->variation_price[$key];
                            $original_price = $request->variation_original_price[$key];
                            break;
                        }
                    }
                }
            }
            $product = Item::where('slug', $request->slug)->first();
            $product->cat_id = $request->category;
            $product->item_name = $request->product_name;
            $product->item_price = $price;
            $product->item_original_price = $price;
            $product->item_original_price = $original_price;
            $product->slug = $slug;
            $product->has_variants = $request->has_variants;
            $product->tax = $request->tax;
            $product->description = $request->description;
            $product->update();
            if ($request->has_variants == 2) {
                Variants::where('item_id', $request->id)->delete();
            }
            if ($request->has_variants == 1) {
                $variation_id = $request->variation_id;
                foreach ($request->variation as $key => $no) {
                    if (@$no != "" && @$request->variation_price[$key] != "" && @$request->variation_original_price[$key] != "") {
                        if (@$variation_id[$key] == "") {
                            $variation = new Variants();
                            $variation->item_id = $product->id;
                            $variation->name = $no;
                            $variation->price = $request->variation_price[$key];
                            $variation->original_price = $request->variation_original_price[$key];
                            $variation->save();
                        } else if (@$variation_id[$key] != "") {
                            Variants::where('id', @$variation_id[$key])->update(['price' => $request->variation_price[$key], 'name' => $request->variation[$key], 'original_price' => $request->variation_original_price[$key]]);
                        }
                    }
                }
            }
            $extras_id = $request->extras_id;
            foreach ($request->extras_name as $key => $no) {
                if (@$no != "" && @$request->extras_price[$key] != "") {
                    if (@$extras_id[$key] == "") {
                        $extras = new Extra();
                        $extras->item_id = $product->id;
                        $extras->name = $no;
                        $extras->price = $request->extras_price[$key];
                        $extras->save();
                    } else if (@$extras_id[$key] != "") {
                        Extra::where('id', @$extras_id[$key])->update(['name' => $request->extras_name[$key], 'price' => $request->extras_price[$key]]);
                    }
                }
            }
            return redirect('admin/products')->with('success', trans('messages.success'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', trans('messages.wrong'));
        }
    }
    public function update_image(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_image' => 'required|image|mimes:jpg,jpeg,png',
        ], [
                'product_image.required' => trans('messages.image_required'),
                'product_image.image' => trans('messages.valid_image'),
                'product_image.mimes' => trans('messages.valid_image_format'),
            ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput()->with('error', trans('messages.wrong'));
        } else {
            if ($request->has('product_image')) {
                if (file_exists(storage_path('app/public/item/' . $request->image))) {
                    unlink(storage_path('app/public/item/' . $request->image));
                }
                $productimage = 'item-' . uniqid() . "." . $request->file('product_image')->getClientOriginalExtension();
                $request->file('product_image')->move(storage_path('app/public/item/'), $productimage);
                Item::where('id', $request->id)->update(['image' => $productimage]);
                return redirect()->back()->with('success', trans('messages.success'));
            } else {
                return redirect()->back()->with('error', trans('messages.wrong'));
            }
        }
    }
    public function delete_variation(Request $request)
    {
        $checkvariationcount = Variants::where('item_id', $request->product_id)->count();
        if ($checkvariationcount > 1) {
            $UpdateDetails = Variants::where('id', $request->id)->delete();
            if ($UpdateDetails) {
                Cart::where('variants_id', $request->id)->delete();
                return redirect()->back()->with('success', trans('messages.success'));
            } else {
                return redirect()->back()->with('error', trans('messages.wrong'));
            }
        } else {
            return redirect()->back()->with('error', trans('messages.last_variation'));
        }
    }
    public function delete_extras(Request $request)
    {
        $deletedata = Extra::where('id', $request->id)->delete();
        if ($deletedata) {
            return redirect()->back()->with('success', trans('messages.success'));
        } else {
            return redirect()->back()->with('error', trans('messages.wrong'));
        }
    }
    public function status($slug, $status)
    {
        try {
            $checkproduct = Item::where('slug', $slug)->first();
            $checkproduct->is_available = $status;
            $checkproduct->save();
            if ($status == 2) {
                Cart::where('item_id', $checkproduct->id)->delete();
            }
            return redirect('admin/products')->with('success', trans('messages.success'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', trans('messages.wrong'));
        }
    }
    public function delete_product($slug)
    {
        try {
            $checkproduct = Item::where('slug', $slug)->first();
            $deletevariations = Variants::where('item_id', $checkproduct->id)->delete();
            $deleteextras = Extra::where('item_id', $checkproduct->id)->delete();
            $deletecarts = Cart::where('item_id', $checkproduct->id)->delete();
            if (file_exists(storage_path('app/public/item/' . $checkproduct->image))) {
                unlink(storage_path('app/public/item/' . $checkproduct->image));
            }
            $checkproduct->delete();
            return redirect()->back()->with('success', trans('messages.success'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', trans('messages.wrong'));
        }
    }
}