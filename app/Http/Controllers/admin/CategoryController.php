<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $allcategories = Category::where('vendor_id', Auth::user()->id)->where('is_deleted', 2)->orderByDesc('id')->get();
        return view('admin.category.category', compact("allcategories"));
    }
    public function add_category(Request $request)
    {
        return view('admin.category.add');
    }
    public function save_category(Request $request)
    {
        $request->validate([
            'category_name' => 'required',
        ], [
                'category_name.required' => trans('messages.category_name_required'),
            ]);
        $check_slug = Category::where('slug', Str::slug($request->category_name, '-'))->first();
        if (!empty($check_slug)) {
            $last_id = Category::select('id')->orderByDesc('id')->first()->id;
            $slug = Str::slug($request->category_name . ' ' . $last_id, '-');
        } else {
            $slug = Str::slug($request->category_name, '-');
        }
        $savecategory = new Category();
        $savecategory->vendor_id = Auth::user()->id;
        $savecategory->name = $request->category_name;
        $savecategory->slug = $slug;
        $savecategory->save();
        return redirect('admin/categories/')->with('success', trans('messages.success'));
    }
    public function edit_category(Request $request)
    {
        $editcategory = category::where('slug', $request->slug)->first();
        return view('admin.category.edit', compact("editcategory"));
    }
    public function update_category(Request $request)
    {
        $request->validate([
            'category_name' => 'required',
        ], [
                'category_name.required' => trans('messages.category_name_required'),
            ]);
        $check_slug = Category::where('slug', Str::slug($request->category_name, '-'))->first();
        if (!empty($check_slug)) {
            $last_id = Category::select('id')->orderByDesc('id')->first()->id;
            $slug = Str::slug($request->category_name . ' ' . $last_id, '-');
        } else {
            $slug = Str::slug($request->category_name, '-');
        }
        $editcategory = Category::where('slug', $request->slug)->first();
        $editcategory->name = $request->category_name;
        $editcategory->slug = $slug;
        $editcategory->update();
        return redirect('admin/categories')->with('success', trans('messages.success'));
    }
    public function change_status(Request $request)
    {
        Category::where('slug', $request->slug)->update(['is_available' => $request->status]);
        return redirect('admin/categories')->with('success', trans('messages.success'));
    }
    public function delete_category(Request $request)
    {
        $checkcategory = Category::where('slug', $request->slug)->first();
        if (!empty($checkcategory)) {
            Item::where('category_id', $checkcategory->id)->update(['is_deleted' => 1]);
            $checkcategory->is_deleted = 1;
            $checkcategory->save();
            return redirect('admin/categories')->with('success', trans('messages.success'));
        } else {
            return redirect()->back()->with('error', trans('messages.wrong'));
        }
    }
}