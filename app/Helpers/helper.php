<?php

namespace App\Helpers;

use App\Models\Item;
use App\Models\Settings;
use App\Models\User;
use App\Models\Timing;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\Transaction;
use App\Models\Payment;
use App\Models\PricingPlan;
use App\Models\Cart;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Stripe\Stripe;
use Stripe\Customer;
use App\Models\Languages;
use Stripe\Charge;
use Illuminate\Support\Str;
use App\Models\Footerfeatures;
use Session;
use App;

class helper
{
    public static function appdata($vendor_id)
    {
        if (!empty($vendor_id)) {
            $data = Settings::where('vendor_id', $vendor_id)->first();
        } else {
            $data = Settings::where('vendor_id', 1)->first();
        }
        return $data;
    }
    public static function image_path($image)
    {
        $path = asset('storage/app/public/images/not-found');
        if (Str::contains($image, 'nodata')) {
            $path = asset('storage/app/public/admin-assets/images/' . $image);
        }
        if (Str::contains($image, 'authformbgimage')) {
            $path = asset('storage/app/public/admin-assets/images/about/' . $image);
        }
        if (Str::contains($image, 'theme-')) {
            $path = asset('storage/app/public/admin-assets/images/theme/' . $image);
        }
        if (Str::contains($image, 'feature-')) {
            $path = asset('storage/app/public/admin-assets/images/feature/' . $image);
        }
        if (Str::contains($image, 'testimonial-')) {
            $path = asset('storage/app/public/admin-assets/images/testimonials/' . $image);
        }
        if (Str::contains($image, 'banktransfer') || Str::contains($image, 'cod') || Str::contains($image, 'razorpay') || Str::contains($image, 'stripe') || Str::contains($image, 'wallet') || Str::contains($image, 'flutterwave') || Str::contains($image, 'paystack') || Str::contains($image, 'mercadopago') || Str::contains($image, 'paypal') || Str::contains($image, 'myfatoorah') || Str::contains($image, 'toyyibpay')) {
            $path = asset('storage/app/public/admin-assets/images/about/payment/' . $image);
        }
        if (Str::contains($image, 'res')) {
            $path = asset('storage/app/public/admin-assets/images/about/' . $image);
        }
        if (Str::contains($image, 'logo')) {
            $path = asset('storage/app/public/admin-assets/images/about/logo/' . $image);
        }
        if (Str::contains($image, 'favicon')) {
            $path = asset('storage/app/public/admin-assets/images/about/favicon/' . $image);
        }
        if (Str::contains($image, 'og_image')) {
            $path = asset('storage/app/public/admin-assets/images/about/og_image/' . $image);
        }
        if (Str::contains($image, 'item-')) {
            $path = asset('storage/app/public/item/' . $image);
        }
        if (Str::contains($image, 'banner') || Str::contains($image, 'promotion-')) {
            $path = asset('storage/app/public/admin-assets/images/banners/' . $image);
        }
        if (Str::contains($image, 'order')) {
            $path = asset('storage/app/public/front/images/' . $image);
        }
        if (Str::contains($image, 'profile')) {
            $path = asset('storage/app/public/admin-assets/images/profile/' . $image);
        }
        if (Str::contains($image, 'blog')) {
            $path = asset('storage/app/public/admin-assets/images/blog/' . $image);
        }
        if (Str::contains($image, 'flag')) {
            $path = asset('storage/app/public/admin-assets/images/language/' . $image);
        }
        if (Str::contains($image, 'cover')) {
            $path = asset('storage/app/public/admin-assets/images/coverimage/' . $image);
        }
        return $path;
    }
    public static function currency_formate($price, $vendor_id)
    {
        if (helper::appdata($vendor_id)->currency_position == "left") {
            return helper::appdata($vendor_id)->currency . number_format($price, 2);
        }
        if (helper::appdata($vendor_id)->currency_position == "right") {
            return number_format($price, 2) . helper::appdata($vendor_id)->currency;
        }
        return $price;
    }
    public static function vendortime($vendor)
    {
        date_default_timezone_set(helper::appdata($vendor)->timezone);
        $t = date('d-m-Y');
        $time = Timing::select('close_time')->where('vendor_id', $vendor)->where('day', date("l", strtotime($t)))->first();
        $txt = "Opened until " . date("D", strtotime($t)) . " " . $time->close_time . "";
        return $txt;
    }
    public static function date_format($date)
    {
        return date("j M Y", strtotime($date));
        // return date("j F Y",strtotime($date));
    }

    public static function get_plan_exp_date($duration, $days)
    {
        date_default_timezone_set(helper::appdata('')->timezone);
        $purchasedate = date("Y-m-d h:i:sa");
        $exdate = "";
        if (!empty($duration) && $duration != "") {
            if ($duration == "1") {
                $exdate = date('Y-m-d', strtotime($purchasedate . ' + 30 days'));
            }
            if ($duration == "2") {
                $exdate = date('Y-m-d', strtotime($purchasedate . ' + 90 days'));
            }
            if ($duration == "3") {
                $exdate = date('Y-m-d', strtotime($purchasedate . ' + 180 days'));
            }
            if ($duration == "4") {
                $exdate = date('Y-m-d', strtotime($purchasedate . ' + 365 days'));
            }
            if ($duration == "4") {
                $exdate = "";
            }
        }
        if (!empty($days) && $days != "") {
            $exdate = date('Y-m-d', strtotime($purchasedate . ' + ' . $days . 'days'));
        }
        return $exdate;
    }
    public static function timings($vendor)
    {
        $timings = Timing::where('vendor_id', @$vendor)->get();
        return $timings;
    }
    public static function storeinfo($vendor)
    {
        $vendorinfo = User::where('slug', $vendor)->first();
        return $vendorinfo;
    }
    public static function checkplan($id, $type)
    {
        date_default_timezone_set(helper::appdata($id)->timezone);
        $vendorinfo = User::where('id', $id)->first();
        $checkplan = Transaction::where('plan_id', $vendorinfo->plan_id)->where('vendor_id', $vendorinfo->id)->orderByDesc('id')->first();

        $totalservice = Item::where('vendor_id', $vendorinfo->id)->count();
        if ($vendorinfo->allow_without_subscription != 1) {
            if (!empty($checkplan)) {
                if ($vendorinfo->is_available == 2) {
                    return response()->json(['status' => 2, 'message' => trans('messages.account_blocked_by_admin'), 'showclick' => "0", 'plan_message' => '', 'plan_date' => '', 'checklimit' => ''], 200);
                }
                if ($checkplan->payment_type == 'banktransfer') {
                    if ($checkplan->status == 1) {
                        return response()->json(['status' => 2, 'message' => trans('messages.bank_request_pending'), 'showclick' => "0", 'plan_message' => trans('messages.bank_request_pending'), 'plan_date' => '', 'checklimit' => ''], 200);
                    } elseif ($checkplan->status == 3) {
                        return response()->json(['status' => 2, 'message' => trans('messages.bank_request_rejected'), 'showclick' => "1", 'plan_message' => trans('messages.bank_request_rejected'), 'plan_date' => '', 'checklimit' => ''], 200);
                    }
                }
                if ($checkplan->expire_date != "") {
                    if (date('Y-m-d') > $checkplan->expire_date) {

                        return response()->json(['status' => 2, 'message' => trans('messages.plan_expired'), 'expdate' => $checkplan->expire_date, 'showclick' => "1", 'plan_message' => trans('messages.plan_expired'), 'plan_date' => $checkplan->expire_date, 'checklimit' => ''], 200);
                    }
                }
                if (Str::contains(request()->url(), 'admin')) {
                    if ($checkplan->service_limit != -1) {
                        if ($totalservice >= $checkplan->service_limit) {
                            if (Auth::user()->type == 1) {
                                return response()->json(['status' => 2, 'message' => trans('messages.products_limit_exceeded'), 'expdate' => '', 'showclick' => "1", 'plan_message' => trans('messages.plan_expires'), 'plan_date' => '', 'checklimit' => ''], 200);
                            }
                            if (Auth::user()->type == 2) {
                                if ($checkplan->expire_date != "") {
                                    return response()->json(['status' => 2, 'message' => trans('messages.vendor_products_limit_message'), 'expdate' => '', 'showclick' => "1", 'plan_message' => trans('messages.plan_expires'), 'plan_date' => $checkplan->expire_date, 'checklimit' => 'service'], 200);
                                } else {
                                    return response()->json(['status' => 2, 'message' => trans('messages.vendor_products_limit_message'), 'expdate' => '', 'showclick' => "1", 'plan_message' => trans('messages.lifetime_subscription'), 'plan_date' => $checkplan->expire_date, 'checklimit' => 'service'], 200);
                                }
                            }
                        }
                    }
                    if ($checkplan->appoinment_limit != -1) {
                        if ($checkplan->appoinment_limit <= 0) {
                            if (Auth::user()->type == 1) {
                                return response()->json(['status' => 2, 'message' => trans('messages.order_limit_exceeded'), 'expdate' => '', 'showclick' => "1", 'plan_message' => trans('messages.plan_expires'), 'plan_date' => '', 'checklimit' => ''], 200);
                            }
                            if (Auth::user()->type == 2) {
                                if ($checkplan->expire_date != "") {
                                    return response()->json(['status' => 2, 'message' => trans('messages.vendor_order_limit_message'), 'expdate' => '', 'showclick' => "1", 'plan_message' => trans('messages.plan_expires'), 'plan_date' => $checkplan->expire_date, 'checklimit' => 'booking'], 200);
                                } else {
                                    return response()->json(['status' => 2, 'message' => trans('messages.vendor_order_limit_message'), 'expdate' => '', 'showclick' => "1", 'plan_message' => trans('messages.lifetime_subscription'), 'plan_date' => $checkplan->expire_date, 'checklimit' => 'service'], 200);
                                }
                            }
                        }
                    }
                }
                if ($type == 3) {
                    if ($checkplan->appoinment_limit != -1) {
                        if ($checkplan->appoinment_limit <= 0) {
                            return response()->json(['status' => 2, 'message' => trans('messages.front_store_unavailable'), 'expdate' => '', 'showclick' => "1", 'plan_message' => trans('messages.plan_expires'), 'plan_date' => '', 'checklimit' => 'booking'], 200);
                        }
                    }
                }
                if ($checkplan->expire_date != "") {

                    return response()->json(['status' => 1, 'message' => trans('messages.plan_expires'), 'expdate' => $checkplan->expire_date, 'showclick' => "0", 'plan_message' => trans('messages.plan_expires'), 'plan_date' => $checkplan->expire_date, 'checklimit' => ''], 200);
                } else {

                    return response()->json(['status' => 1, 'message' => trans('messages.lifetime_subscription'), 'expdate' => $checkplan->expire_date, 'showclick' => "0", 'plan_message' => trans('messages.lifetime_subscription'), 'plan_date' => $checkplan->expire_date, 'checklimit' => ''], 200);
                }
            } else {
                if (Auth::user()->type == 1) {
                    return response()->json(['status' => 2, 'message' => trans('messages.doesnot_select_any_plan'), 'expdate' => '', 'showclick' => "0", 'plan_message' => '', 'plan_date' => '', 'checklimit' => ''], 200);
                }
                if (Auth::user()->type == 2) {
                    return response()->json(['status' => 2, 'message' => trans('messages.vendor_plan_purchase_message'), 'expdate' => '', 'showclick' => "1", 'plan_message' => '', 'plan_date' => '', 'checklimit' => ''], 200);
                }
            }
        } else {
            return response()->json(['status' => 1, 'message' => trans('messages.success')], 200);
        }
    }

    public static function createorder($vendor, $payment_type_data, $payment_id, $customer_email, $customer_name, $customer_mobile, $stripeToken, $grand_total, $delivery_charge, $address, $building, $landmark, $postal_code, $discount_amount, $sub_total, $tax, $delivery_time, $delivery_date, $delivery_area, $couponcode, $order_type, $notes)
    {
        try {
            date_default_timezone_set(helper::appdata($vendor)->timezone);
            //payment_type = COD : 1,RazorPay : 2, Stripe : 3, Flutterwave : 4, Paystack : 5, Mercado Pago : 7, PayPal : 8, MyFatoorah : 9, toyyibpay : 10
            if ($payment_type_data == "cod") {
                $payment_type = 1;
            }
            if ($payment_type_data == "razorpay") {
                $payment_type = 2;
            }
            if ($payment_type_data == "stripe") {
                $payment_type = 3;
            }
            if ($payment_type_data == "flutterwave") {
                $payment_type = 4;
            }
            if ($payment_type_data == "paystack") {
                $payment_type = 5;
            }
            if ($payment_type_data == "mercadopago") {
                $payment_type = 7;
            }
            if ($payment_type_data == "paypal") {
                $payment_type = 8;
            }
            if ($payment_type_data == "myfatoorah") {
                $payment_type = 9;
            }
            if ($payment_type_data == "toyyibpay") {
                $payment_type = 10;
            }
            if ($payment_type_data == "stripe") {
                $getstripe = Payment::select('environment', 'secret_key')->where('payment_name', 'Stripe')->where('vendor_id', $vendor)->first();
                $skey = $getstripe->secret_key;
                Stripe::setApiKey($skey);
                $customer = Customer::create(
                    array(
                        'email' => $customer_email,
                        'source' => $stripeToken,
                        'name' => $customer_name,
                    )
                );
                $charge = Charge::create(
                    array(
                        'customer' => $customer->id,
                        'amount' => $grand_total * 100,
                        'currency' => 'usd',
                        'description' => 'eCommerce',
                    )
                );
                $payment_id = $charge['id'];
            }
            if ($order_type == "2") {
                $delivery_charge = "0.00";
                $address = "";
                $building = "";
                $landmark = "";
                $postal_code = "";
            } else {
                $delivery_charge = $delivery_charge;
                $address = $address;
                $building = $building;
                $landmark = $landmark;
                $postal_code = $postal_code;
            }
            if ($discount_amount == "NaN") {
                $discount_amount = 0;
            } else {
                $discount_amount = $discount_amount;
            }
            $order_number = substr(str_shuffle(str_repeat("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 10)), 0, 10);
            $order = new Order;
            $order->vendor_id = $vendor;
            $order->user_id = @Auth::user() && @Auth::user()->type == 3 ? @Auth::user()->id : null;
            $order->order_number = $order_number;
            $order->payment_type = $payment_type;
            $order->payment_id = @$payment_id;
            $order->sub_total = $sub_total;
            $order->tax = $tax;
            $order->grand_total = $grand_total;
            $order->status = '1';
            $order->address = $address;
            $order->delivery_time = $delivery_time;
            $order->delivery_date = $delivery_date;
            $order->delivery_area = $delivery_area;
            $order->delivery_charge = $delivery_charge;
            $order->discount_amount = $discount_amount;
            $order->couponcode = $couponcode;
            $order->order_type = $order_type;
            $order->building = $building;
            $order->landmark = $landmark;
            $order->pincode = $postal_code;
            $order->customer_name = $customer_name;
            $order->customer_email = $customer_email;
            $order->mobile = $customer_mobile;
            $order->order_notes = $notes;
            $order->save();
            $order_id = DB::getPdo()->lastInsertId();

            if (@Auth::user() && @Auth::user()->type == 3) {
                $data = Cart::where('user_id', Auth::user()->id)->get();
            } else {
                $data = Cart::where('session_id', Session::getId())->get();
            }
            foreach ($data as $value) {
                
                $OrderPro = new OrderDetails;
                $OrderPro->order_id = $order_id;
                $OrderPro->item_id = $value['item_id'];
                $OrderPro->item_name = $value['item_name'];
                $OrderPro->item_image = $value['item_image'];
                $OrderPro->extras_id = $value['extras_id'];
                $OrderPro->extras_name = $value['extras_name'];
                $OrderPro->extras_price = $value['extras_price'];
                if ($value['variants_id'] == "") {
                    $OrderPro->price = $value['item_price'];
                } else {
                    $OrderPro->price = $value['price'];
                }
                $OrderPro->variants_id = $value['variants_id'];
                $OrderPro->variants_name = $value['variants_name'];
                $OrderPro->variants_price = $value['variants_price'];
                $OrderPro->qty = $value['qty'];
                $OrderPro->save();
            }

            if (@Auth::user() && @Auth::user()->type == 3) {
                $data = Cart::where('user_id', Auth::user()->id)->delete();
            } else {
                $data = Cart::where('session_id', Session::getId())->delete();
            }

            session()->forget(['offer_amount', 'offer_code', 'offer_type']);

            if (@Auth::user() && @Auth::user()->type == 3) {
                $count = Cart::where('user_id', Auth::user()->id)->count();
            } else {
                $count = Cart::where('session_id', Session::getId())->count();
            }

            session()->put('cart', $count);
            $vendorinfo = User::select('id', 'name', 'slug', 'email', 'mobile', 'token')->where('id', $vendor)->first();
            $trackurl = URL::to(@$vendorinfo->slug . '/track-order/' . $order_number);
            helper::create_order_invoice($customer_email, $customer_name, $vendorinfo->email, $vendorinfo->name, $order_number, $order_type, helper::date_format($delivery_date), $delivery_time, helper::currency_formate($grand_total, $vendorinfo->vendor_id), $trackurl);

            $title = trans('labels.order_update');
            $body = "Congratulations! Your store just received a new order " . $order_number;

            helper::push_notification($vendorinfo->token, $title, $body, "order", $order->id);

            return $order_number;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public static function push_notification($token, $title, $body, $type, $order_id)
    {
        $customdata = array(
            "type" => $type,
            "order_id" => $order_id,
        );

        $msg = array(
            'body' => $body,
            'title' => $title,
            'sound' => 1/*Default sound*/
        );
        $fields = array(
            'to'           => $token,
            'notification' => $msg,
            'data' => $customdata
        );
        $headers = array(
            'Authorization: key=' . @helper::appdata('')->firebase,
            'Content-Type: application/json'
        );
        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $firebaseresult = curl_exec($ch);
        curl_close($ch);

        return $firebaseresult;
    }

    public static function vendor_register($vendor_name, $vendor_email, $vendor_mobile, $vendor_password, $firebasetoken, $slug, $google_id, $facebook_id, $country_id, $city_id)
    {
        try {
           
            if (!empty($slug) || $slug != null) {
                $slug;
            } else {
                $check = User::where('slug', Str::slug($vendor_name, '-'))->first();
                if ($check != "") {
                    $last = User::select('id')->orderByDesc('id')->first();
                    $slug =   Str::slug($vendor_name . " " . ($last->id + 1), '-');
                } else {
                    $slug = Str::slug($vendor_name, '-');
                }
            }
            $rec = Settings::where('vendor_id', '1')->first();

            date_default_timezone_set($rec->timezone);
            $logintype = "normal";
            if ($google_id != "") {
                $logintype = "google";
            }

            if ($facebook_id != "") {
                $logintype = "facebook";
            }

            $user = new User;
            $user->name = $vendor_name;
            $user->email = $vendor_email;
            $user->password = $vendor_password;
            $user->google_id = $google_id;
            $user->facebook_id = $facebook_id;
            $user->mobile = $vendor_mobile;
            $user->image = "default-logo.png";
            $user->slug = $slug;
            $user->login_type = $logintype;
            $user->type = 2;
            $user->token = $firebasetoken;
            $user->country_id = $country_id;
            $user->city_id = $city_id;
            $user->is_verified = 2;
            $user->is_available = 1;
            $user->save();

            $vendor_id = \DB::getPdo()->lastInsertId();

            $days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

            foreach ($days as $day) {

                $timedata = new Timing;
                $timedata->vendor_id = $vendor_id;
                $timedata->day = $day;
                $timedata->open_time = '12:00am';
                $timedata->close_time = '11:59pm';
                $timedata->is_always_close = '2';
                $timedata->save();
            }
            $paymentlist = Payment::select('payment_name', 'currency', 'image')->where('vendor_id', '1')->where('id', "!=", "6")->get();
            foreach ($paymentlist as $payment) {
                $gateway = new Payment;
                $gateway->vendor_id = $vendor_id;
                $gateway->payment_name = $payment->payment_name;
                $gateway->currency = $payment->currency;
                $gateway->image = $payment->image;
                $gateway->public_key = NULL;
                $gateway->secret_key = NULL;
                $gateway->encryption_key = NULL;
                $gateway->environment = '1';
                $gateway->is_available = '1';
                $gateway->save();
            }

            $data = new Settings;
            $data->vendor_id = $vendor_id;
            $data->currency = $rec->currency;
            $data->logo = "default-logo.png";
            $data->favicon = "default-favicon.png";
            $data->og_image = "default-og.png";
            $data->banner = "default-banner.png";
            $data->currency_position = $rec->currency_position;
            $data->timezone = $rec->timezone;
            $data->address = "Your address";
            $data->contact = "Your contact";
            $data->email = "youremail@gmail.com";
            $data->description = "Your description";
            $data->copyright = $rec->copyright;
            $data->website_title = "Your store name";
            $data->meta_title = "Your store name";
            $data->meta_description = "Description";
            $data->facebook_link = "Your facebook page link";
            $data->linkedin_link = "Your linkedin page link";
            $data->instagram_link = "Your instagram page link";
            $data->twitter_link = "Your twitter page link";
            $data->delivery_type = "both";
            $data->item_message = "🔵 {qty} X {item_name} {variantsdata} - {item_price}";
            $data->whatsapp_message = "Hi, 
I would like to place an order 👇
*{delivery_type}* Order No: {order_no}
---------------------------
{item_variable}
---------------------------
👉Subtotal : {sub_total}
👉Tax : {total_tax}
👉Delivery charge : {delivery_charge}
👉Discount : - {discount_amount}
---------------------------
📃 Total : {grand_total}
---------------------------
📄 Comment : {notes}

✅ Customer Info

Customer name : {customer_name}
Customer phone : {customer_mobile}

📍 Delivery Details

Address : {address}, {building}, {landmark}, {postal_code}

---------------------------
Date : {date}
Time : {time}
---------------------------
💳 Payment type :
{payment_type}

{store_name} will confirm your order upon receiving the message.

Track your order 👇
{track_order_url}

Click here for next order 👇
{store_url}";

            $data->save();

            return $vendor_id;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public static function plandetail($plan_id)
    {
        $planinfo = PricingPlan::where('id', $plan_id)->first();
        return $planinfo;
    }
    public static function footer_features($vendor_id)
    {
        return FooterFeatures::select('id', 'icon', 'title', 'description')->where('vendor_id', $vendor_id)->get();
    }

    //Send email 

    public static function send_subscription_email($vendor_email, $vendor_name, $plan_name, $duration, $price, $payment_method, $transaction_id)
    {
        $admininfo = User::where('id', '1')->first();

        $data = ['title' => "Subscription Purchase Confirmation", 'vendor_email' => $vendor_email, 'vendor_name' => $vendor_name, 'admin_email' => $admininfo->email, 'admin_name' => $admininfo->name, 'plan_name' => $plan_name, 'duration' => $duration, 'price' => $price, 'payment_method' => $payment_method, 'transaction_id' => $transaction_id];

        $adminemail = ['title' => "New Subscription Purchase Notification", 'vendor_email' => $vendor_email, 'vendor_name' => $vendor_name, 'admin_email' => $admininfo->email, 'admin_name' => $admininfo->name, 'plan_name' => $plan_name, 'duration' => $duration, 'price' => $price, 'payment_method' => $payment_method, 'transaction_id' => $transaction_id];

        try {
            Mail::send('email.subscription', $data, function ($message) use ($data) {
                $message->from(env('MAIL_USERNAME'))->subject($data['title']);
                $message->to($data['vendor_email']);
            });

            Mail::send('email.adminsubscription', $adminemail, function ($message) use ($adminemail) {
                $message->from(env('MAIL_USERNAME'))->subject($adminemail['title']);
                $message->to($adminemail['admin_email']);
            });
            return 1;
        } catch (\Throwable $th) {
            return 1;
        }
    }

    public static function bank_transfer_request($vendor_email, $vendor_name, $admin_email, $admin_name)
    {
        $adminemail = ['title' => "Bank transfer", 'vendor_email' => $vendor_email, 'vendor_name' => $vendor_name, 'admin_email' => $admin_email, 'admin_name' => $admin_name];
        $data = ['title' => "Bank transfer", 'vendor_email' => $vendor_email, 'vendor_name' => $vendor_name, 'admin_email' => $admin_email, 'admin_name' => $admin_name];
        try {
            Mail::send('email.banktransfervendor', $data, function ($message) use ($data) {
                $message->from(env('MAIL_USERNAME'))->subject($data['title']);
                $message->to($data['vendor_email']);
            });

            Mail::send('email.banktransferadmin', $adminemail, function ($message) use ($adminemail) {
                $message->from(env('MAIL_USERNAME'))->subject($adminemail['title']);
                $message->to($adminemail['admin_email']);
            });
            return 1;
        } catch (\Throwable $th) {
            return 1;
        }
    }

    public static function subscription_rejected($vendor_email, $vendor_name, $plan_name, $payment_method)
    {
        $admindata = User::select('name', 'email')->where('id', '1')->first();
        $data = ['title' => "Bank transfer rejected", 'vendor_email' => $vendor_email, 'vendor_name' => $vendor_name, 'admin_email' => $admindata->email, 'admin_name' => $admindata->name, 'plan_name' => $plan_name, 'payment_method' => $payment_method];
        try {
            Mail::send('email.banktransferreject', $data, function ($message) use ($data) {
                $message->from(env('MAIL_USERNAME'))->subject($data['title']);
                $message->to($data['vendor_email']);
            });
            return 1;
        } catch (\Throwable $th) {
            return 1;
        }
    }

    public static function vendor_contact_data($vendor_name, $vendor_email, $full_name, $useremail, $usermobile, $usermessage)
    {
        $data = ['title' => "Inquiry", 'vendor_name' => $vendor_name, 'vendor_email' => $vendor_email, 'full_name' => $full_name, 'useremail' => $useremail, 'usermobile' => $usermobile, 'usermessage' => $usermessage];
        try {
            Mail::send('email.vendorcontcatform', $data, function ($message) use ($data) {
                $message->from(env('MAIL_USERNAME'))->subject($data['title']);
                $message->to($data['vendor_email']);
            });
            return 1;
        } catch (\Throwable $th) {
            return 1;
        }
    }

    public static function create_order_invoice($customer_email, $customer_name, $companyemail, $companyname, $order_number, $order_type, $delivery_date, $delivery_time, $grand_total, $trackurl)
    {
        $data = ['title' => "Order Invoice", 'order_type' => $order_type, 'customer_email' => $customer_email, 'customer_name' => $customer_name, 'company_email' => $companyemail, 'company_name' => $companyname, 'order_number' => $order_number, 'delivery_date' => $delivery_date, 'delivery_time' => $delivery_time, 'grand_total' => $grand_total, 'trackurl' => $trackurl];

        $vendordata = ['title' => "Order Invoice", 'order_type' => $order_type, 'customer_email' => $customer_email, 'customer_name' => $customer_name, 'company_email' => $companyemail, 'company_name' => $companyname, 'order_number' => $order_number, 'delivery_date' => $delivery_date, 'delivery_time' => $delivery_time, 'grand_total' => $grand_total, 'trackurl' => $trackurl];
        try {
            Mail::send('email.customerorderemail', $data, function ($message) use ($data) {
                $message->from(env('MAIL_USERNAME'))->subject($data['title']);
                $message->to($data['customer_email']);
            });

            Mail::send('email.vendororderemail', $vendordata, function ($companymessage) use ($vendordata) {
                $companymessage->from(env('MAIL_USERNAME'))->subject($vendordata['title']);
                $companymessage->to($vendordata['company_email']);
            });
            return 1;
        } catch (\Throwable $th) {
            return 1;
        }
    }

    public static function order_status_email($email, $name, $title, $message_text, $vendor_id)
    {
        $data = ['email' => $email, 'name' => $name, 'title' => $title, 'message_text' => $message_text, 'logo' => helper::image_path(@helper::appdata($vendor_id)->logo)];
        try {
            Mail::send('email.orderemail', $data, function ($message) use ($data) {
                $message->from(env('MAIL_USERNAME'))->subject($data['title']);
                $message->to($data['email']);
            });
            return 1;
        } catch (\Throwable $th) {
            return 1;
        }
    }

    public static function send_pass($email, $name, $password, $id)
    {
        $data = ['title' => "New Password", 'email' => $email, 'name' => $name, 'password' => $password, 'logo' => helper::appdata($id)->image];
        try {
            Mail::send('email.sendpassword', $data, function ($message) use ($data) {
                $message->from(env('MAIL_USERNAME'))->subject($data['title']);
                $message->to($data['email']);
            });
            return 1;
        } catch (\Throwable $th) {
            return 1;
        }
    }
    // Email end

    public static function language()
    {
        if (session()->get('locale') == null) {
            $layout = Languages::select('name', 'layout', 'image', 'is_default', 'code')->where('is_default', 1)->first();

            App::setLocale($layout->code);
            session()->put('locale', $layout->code);
            session()->put('language', $layout->name);
            session()->put('flag', $layout->image);
            session()->put('direction', $layout->layout);
        } else {
            $layout = Languages::select('name', 'layout', 'image', 'is_default', 'code')->where('code', session()->get('locale'))->first();
            App::setLocale(session()->get('locale'));
            session()->put('locale', @$layout->code);
            session()->put('language', @$layout->name);
            session()->put('flag', @$layout->image);
            session()->put('direction', @$layout->layout);
        }
    }

    public static function listoflanguage()
    {
        $listoflanguage = Languages::where('is_available', '1')->get();
        return $listoflanguage;
    }
}
