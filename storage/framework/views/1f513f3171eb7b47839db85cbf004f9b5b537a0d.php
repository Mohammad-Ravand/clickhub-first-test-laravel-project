<?php $__env->startSection('content'); ?>
    <div class="wrapper">
        <section>
            <div class="row justify-content-center align-items-center g-0 w-100 h-100vh">
                <div class="col-lg-6 col-sm-8 col-auto px-5">
                    <div class="card box-shadow overflow-hidden border-0">
                        <div class="bg-primary-light">
                            <div class="row">
                                <div class="col-7 d-flex align-items-center">
                                    <div class="text-primary p-4">
                                        <h4 class="mb-1"><?php echo e(trans('labels.register')); ?></h4>
                                        <p class="fs-7"><?php echo e(trans('labels.get_free_account')); ?></p>
                                    </div>
                                </div>
                                <div class="col-5 align-self-end">
                                    <img src="<?php echo e(helper::image_path('authformbgimage.png')); ?>" class="img-fluid"
                                        alt="">
                                </div>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <form class="my-3" method="POST" action="<?php echo e(URL::to('admin/register_vendor')); ?>">
                                <?php echo csrf_field(); ?>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="name" class="form-label"><?php echo e(trans('labels.name')); ?><span
                                                class="text-danger"> * </span></label>
                                        <input type="text" class="form-control" name="name"
                                            value="<?php echo e(old('name')); ?>" id="name"
                                            placeholder="<?php echo e(trans('labels.name')); ?>" required>
                                        <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                            <span class="text-danger"><?php echo e($message); ?></span>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="form-label"><?php echo e(trans('labels.email')); ?><span
                                                class="text-danger"> * </span></label>
                                        <input type="email" class="form-control" name="email"
                                            value="<?php echo e(old('email')); ?>" id="email"
                                            placeholder="<?php echo e(trans('labels.email')); ?>" required>
                                        <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                            <span class="text-danger"><?php echo e($message); ?></span>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="mobile" class="form-label"><?php echo e(trans('labels.mobile')); ?><span
                                                class="text-danger"> * </span></label>
                                        <input type="number" class="form-control" name="mobile"
                                            value="<?php echo e(old('mobile')); ?>" id="mobile"
                                            placeholder="<?php echo e(trans('labels.mobile')); ?>" required>
                                        <?php $__errorArgs = ['mobile'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                            <span class="text-danger"><?php echo e($message); ?></span>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="password" class="form-label"><?php echo e(trans('labels.password')); ?><span
                                                class="text-danger"> * </span></label>
                                        <input type="password" class="form-control" name="password"
                                            value="<?php echo e(old('password')); ?>" id="password"
                                            placeholder="<?php echo e(trans('labels.password')); ?>" required>
                                        <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                            <span class="text-danger"><?php echo e($message); ?></span>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="country" class="form-label"><?php echo e(trans('labels.country')); ?><span
                                                class="text-danger"> * </span></label>
                                        <select name="country" class="form-select" id="country" required>
                                            <option value=""><?php echo e(trans('labels.select')); ?></option>
                                            <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($country->id); ?>"><?php echo e($country->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>

                                    </div>
                                    <div class="form-group col-6">
                                        <label for="city" class="form-label"><?php echo e(trans('labels.city')); ?><span
                                                class="text-danger"> * </span></label>
                                        <select name="city" class="form-select" id="city" required>
                                            <option value=""><?php echo e(trans('labels.select')); ?></option>
                                        </select>

                                    </div>
                                    <?php if(App\Models\SystemAddons::where('unique_identifier', 'unique_slug')->first() != null &&
                                            App\Models\SystemAddons::where('unique_identifier', 'unique_slug')->first()->activated == 1): ?>
                                        <div class="form-group">
                                            <label for="basic-url"
                                                class="form-label"><?php echo e(trans('labels.personlized_link')); ?><span
                                                    class="text-danger"> * </span></label><?php if(env('Environment') == 'sendbox'): ?><span
                                                    class="badge badge bg-danger ms-2 mb-0"><?php echo e(trans('labels.addon')); ?></span> <?php endif; ?>
                                            <div class="input-group">
                                                <span class="input-group-text"><?php echo e(URL::to('/')); ?>/</span>
                                                <input type="text" class="form-control" id="slug" name="slug"
                                                    value="<?php echo e(old('slug')); ?>" required>
                                            </div>
                                            <?php $__errorArgs = ['slug'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <span class="text-danger"><?php echo e($message); ?></span>
                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                        </div>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <input class="form-check-input" type="checkbox" value="" name="check_terms"
                                            id="check_terms" checked required>
                                        <label class="form-check-label" for="check_terms">
                                            <?php echo e(trans('labels.i_accept_the')); ?> <span class="fw-600"><a
                                                    href="<?php echo e(URL::to('/termscondition')); ?>" target="_blank"><?php echo e(trans('labels.terms')); ?></a> </span>
                                        </label>
                                    </div>
                                </div>

                                <button class="btn btn-primary w-100 mb-3" <?php if(env('Environment') == 'sendbox'): ?> type="button" onclick="myFunction()" <?php else: ?> type="submit" <?php endif; ?>><?php echo e(trans('labels.register')); ?></button>
                                <p class="fs-7 text-center mb-3"><?php echo e(trans('labels.already_have_an_account')); ?>

                                    <a href="<?php echo e(URL::to('/admin')); ?>"
                                        class="text-primary fw-semibold"><?php echo e(trans('labels.login')); ?></a>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script>
        var cityurl = "<?php echo e(URL::to('admin/getcity')); ?>";
        var select = "<?php echo e(trans('labels.select')); ?>";
        var cityid = '0';
    </script>
    <script src="<?php echo e(url(env('ASSETPATHURL') . '/admin-assets/js/user.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.auth_default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Pc\Desktop\project\resources\views/admin/auth/register.blade.php ENDPATH**/ ?>