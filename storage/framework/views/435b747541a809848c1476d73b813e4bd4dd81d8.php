<?php $__env->startSection('content'); ?>
    <div class="d-flex justify-content-between align-items-center">
        <h5 class="text-uppercase"><?php echo e(trans('labels.testimonials')); ?></h5>
        <a href="<?php echo e(URL::to('admin/testimonials/add')); ?>" class="btn btn-secondary px-2 d-flex">
            <i class="fa-regular fa-plus mx-1"></i><?php echo e(trans('labels.add')); ?>

        </a>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <div class="card border-0 mb-3">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered py-3 zero-configuration w-100 dataTable no-footer">
                            <thead>
                                <tr class="text-uppercase fw-500">
                                    <td><?php echo e(trans('labels.srno')); ?></td>
                                    <td><?php echo e(trans('labels.image')); ?></td>
                                    <td><?php echo e(trans('labels.name')); ?></td>
                                    <td><?php echo e(trans('labels.position')); ?></td>
                                    <td><?php echo e(trans('labels.description')); ?></td>
                                    <td><?php echo e(trans('labels.ratting')); ?></td>
                                    <td><?php echo e(trans('labels.action')); ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i = 1;
                                ?>
                            <?php $__currentLoopData = $testimonials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $testimonial): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="fs-7">
                                <td><?php
                                    echo $i++;
                                ?></td>
                                <td><img src="<?php echo e(helper::image_path($testimonial->image)); ?>"  class="img-fluid rounded hw-50 object-fit-cover" alt=""></td>
                                <td><?php echo e($testimonial->name); ?></td>
                                <td><?php echo e($testimonial->position); ?></td>
                                <td><?php echo e($testimonial->description); ?></td>
                                <td><?php echo e($testimonial->star); ?></td>
                                <td>
                                    <a href="<?php echo e(URL::to('/admin/testimonials/edit-'.$testimonial->id )); ?>"
                                        class="btn btn-outline-info btn-sm mx-1"> <i
                                            class="fa-regular fa-pen-to-square"></i></a>
                                    <a href="javascript:void(0)"
                                        <?php if(env('Environment') == 'sendbox'): ?> onclick="myFunction()" <?php else: ?> onclick="statusupdate('<?php echo e(URL::to('admin/testimonials/delete-'.$testimonial->id)); ?>')" <?php endif; ?>
                                        class="btn btn-outline-danger btn-sm">
                                        <i class="fa-regular fa-trash"></i></a>
                                </td>
                            </tr>
                    
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Pc\Desktop\project\resources\views/admin/testimonial/index.blade.php ENDPATH**/ ?>