<header class="page-topbar">
    <div class="navbar-header">
        <button class="navbar-toggler d-lg-none d-md-block px-4" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarcollapse" aria-expanded="false" aria-controls="sidebarcollapse">
            <i class="fa-regular fa-bars fs-4"></i>
        </button>
        <div class="px-3 d-flex align-items-center">
        <!-- dekstop-tablet-mobile-language-dropdown-button-start-->
            <?php if(App\Models\SystemAddons::where('unique_identifier', 'language')->first() != null &&
            App\Models\SystemAddons::where('unique_identifier', 'language')->first()->activated == 1): ?>
                <div class="position-relative mx-1">
                    <div class="dropdown">
                        <a class="btn btn-sm border-primary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="<?php echo e(helper::image_path(session()->get('flag'))); ?>" alt="" class="img-fluid mx-1" width="25px">
                            <span class="<?php echo e(Session::get('theme') == 'dark' ? 'text-white' : ''); ?>"><?php echo e(session()->get('language')); ?> </span>
                        </a>
                        <ul class="dropdown-menu drop-menu <?php echo e(session()->get('direction') == 2 ? 'drop-menu-rtl' : 'drop-menu'); ?>">
                            <?php $__currentLoopData = helper::listoflanguage(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $languagelist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li>
                                    <a class="dropdown-item d-flex text-start"
                                        href="<?php echo e(URL::to('/lang/change?lang=' . $languagelist->code)); ?>">
                                        <img src="<?php echo e(helper::image_path($languagelist->image)); ?>"
                                            alt="" class="img-fluid mx-1" width="25px">
                                        <?php echo e($languagelist->name); ?>

                                    </a>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
            <!-- dekstop-tablet-mobile-language-dropdown-button-end-->
            <div class="dropwdown d-inline-block">
                <?php if(session('vendor_login')): ?>
                    <a href="<?php echo e(URL::to('admin/admin_back')); ?>"
                        class="btn btn-primary btn-sm"><?php echo e(trans('labels.back_to_admin')); ?></a>
                <?php endif; ?>
                <?php if(Auth::user()->type == 2 && Auth::user()->is_available == 1): ?>
                    <a href="<?php echo e(URL::to('/' . Auth::user()->slug)); ?>" target="_blank"
                        class="btn btn-sm btn-outline-secondary"> <i class="fa-regular fa-square-up-right"></i>
                        <?php echo e(trans('labels.view_shop')); ?> </a>
                <?php endif; ?>
                <button class="btn header-item" data-bs-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo e(helper::image_path(Auth::user()->image)); ?>">
                    <span class="d-none d-xxl-inline-block d-xl-inline-block ms-1"><?php echo e(Auth::user()->name); ?></span>
                    <i class="fa-regular fa-angle-down d-none d-xxl-inline-block d-xl-inline-block"></i>
                </button>
                <div class="dropdown-menu box-shadow">
                    <a href="<?php echo e(URL::to('admin/settings')); ?>#editprofile"
                        class="dropdown-item d-flex align-items-center">
                        <i class="fa-light fa-address-card fs-5 mx-2"></i><?php echo e(trans('labels.edit_profile')); ?>

                    </a>
                    <a href="<?php echo e(URL::to('admin/settings')); ?>#changepasssword" class="dropdown-item d-flex align-items-center">
                       
                        <i class="fa-light fa-lock-keyhole fs-5 mx-2"></i><?php echo e(trans('labels.change_password')); ?>


                    </a>
                    <a href="javascript:void(0)" onclick="statusupdate('<?php echo e(URL::to('admin/logout')); ?>')"
                        class="dropdown-item d-flex align-items-center">
                        <i class="fa-light fa-right-from-bracket fs-5 mx-2"></i><?php echo e(trans('labels.logout')); ?>

                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
<?php /**PATH C:\Users\Pc\Desktop\project\resources\views/admin/layout/header.blade.php ENDPATH**/ ?>