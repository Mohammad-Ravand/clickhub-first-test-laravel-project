<?php
$i = 1;
?>
<?php $__currentLoopData = $userdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<div class="col" data-aos="fade-up" data-aos-delay="<?php echo e($i++); ?>00" data-aos-duration="1000">
    <a href="<?php echo e(URL::to($user->slug . '/')); ?>" target="_blank">
        <div class="card mx-1 rounded-0 view-all-hover">
            <img src="<?php echo e(helper::image_path($user->cover_image)); ?>"
                class="card-img-top rounded-0 object-fit-cover img-fluid object-fit-cover"
                height="185" alt="...">
            <div class="card-body">
                <h5 class="card-title hotel-title"><?php echo e($user->website_title); ?></h5>
                <p class="hotel-subtitle text-muted">
                    <?php echo e($user->description); ?>

                </p>
            </div>
        </div>
    </a>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?><?php /**PATH C:\Users\Pc\Desktop\project\resources\views/landing/storelist.blade.php ENDPATH**/ ?>