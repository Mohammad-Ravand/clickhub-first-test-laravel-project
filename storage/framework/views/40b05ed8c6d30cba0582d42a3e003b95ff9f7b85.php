<div class="header sticky-top">
    <div class="container">
        <div class="Navbar py-3">
            <div class="logo">
                <a href="<?php echo e(URL::to('/')); ?>">
                    <img src="<?php echo e(helper::image_path(helper::appdata('')->logo)); ?>" height="50" alt="">
                </a>
            </div>
            <div class="d-flex align-items-center ">
                <?php if(App\Models\SystemAddons::where('unique_identifier', 'language')->first() != null &&
                        App\Models\SystemAddons::where('unique_identifier', 'language')->first()->activated == 1): ?>
                    <div class="d-none language-button-icon mx-2">
                        <a href="#" class="d-none">
                            <div class="dropdown show language-dropdown border rounded-2 mx-2">
                                     <a class=" dropdown-toggle mx-1 border-0 rounded-1 language-drop py-1 " href="#"
                                        role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="fa-solid fa-globe fs-4 pt-1"></i>
                                    </a>
                                <div class="dropdown-menu dropdown-menu-right mt-2" aria-labelledby="dropdownMenuLink">
                                        <?php $__currentLoopData = Helper::listoflanguage(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $languagelist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li>
                                                <a class="dropdown-item text-dark d-flex text-start"
                                                    href="<?php echo e(URL::to('/lang/change?lang=' . $languagelist->code)); ?>">
                                                    <img src="<?php echo e(helper::image_path($languagelist->image)); ?>" alt=""
                                                        class="img-fluid mx-1" width="25px"> <?php echo e($languagelist->name); ?>

                                                </a>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="language-button d-none">
                        <div class="d-flex align-items-center mx-2 lng-n-button">
                            <a href="#">
                                <div class="dropdown border py-1 rounded-2 mx-3">
                                    <a class=" dropdown-toggle mx-1 border-0 rounded-1 language-drop" href="#"
                                        role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo e(helper::image_path(session()->get('flag'))); ?>" alt=""
                                            class="img-fluid" width="25px">
                                        <?php echo e(session()->get('language')); ?>

                                    </a>

                                    <ul class="dropdown-menu drop-menu <?php echo e(session()->get('direction') == 2 ? 'drop-menu-rtl' : 'drop-menu'); ?>">
                                        <?php $__currentLoopData = helper::listoflanguage(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $languagelist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li>
                                                <a class="dropdown-item text-dark d-flex text-start"
                                                    href="<?php echo e(URL::to('/lang/change?lang=' . $languagelist->code)); ?>">
                                                    <img src="<?php echo e(helper::image_path($languagelist->image)); ?>"
                                                        alt="" class="img-fluid mx-1" width="25px">
                                                    <?php echo e($languagelist->name); ?>

                                                </a>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="togl-btn">
                    <i class="fa-solid fa-bars"></i>
                </div>
            </div>

            <nav class=" <?php echo e(session()->get('direction') == 2 ? 'menu-2' : 'menu'); ?>">
                <!--deletebtn-start-->
                <div class="<?php echo e(session()->get('direction') == 2 ? 'deletebtn-button-header-rtl' : 'deletebtn-button-header'); ?>">
                    <i class="fa-solid fa-xmark"></i>
                </div>
                <!--deletebtn-End-->
                <div class="menu-list-1152px-none mx-5">
                    <ul class="navbar-nav flex-row">
                        <li class="nav-item dropdown px-3">
                            <a class="nav-link text-white" href="<?php echo e(URL::to('/')); ?>" role="button">
                                <?php echo e(trans('landing.home')); ?>

                            </a>
                        </li>
                        <li class="nav-item dropdown px-3">
                            <a class="nav-link  text-white" href="<?php echo e(URL::to('/#features')); ?>" role="button">
                                <?php echo e(trans('landing.features')); ?>

                            </a>
                        </li>
                        <li class="nav-item dropdown px-3">
                            <a class="nav-link text-white" href="<?php echo e(URL::to('/#our-stores')); ?>" role="button">
                                <?php echo e(trans('landing.our_store_menu')); ?>

                            </a>
                        </li>
                        <li class="nav-item dropdown px-3">
                            <a class="nav-link text-white" href="<?php echo e(URL::to('/#pricing-plans')); ?>" role="button">
                                <?php echo e(trans('landing.pricing_plan')); ?>

                            </a>
                        </li>
                        <?php if(App\Models\SystemAddons::where('unique_identifier', 'blog')->first() != null &&
                                App\Models\SystemAddons::where('unique_identifier', 'blog')->first()->activated == 1): ?>
                            <li class="nav-item dropdown px-3">
                                <a class="nav-link text-white" href="<?php echo e(URL::to('/#blogs')); ?>" role="button">
                                    <?php echo e(trans('landing.blogs')); ?>

                                </a>
                            </li>
                        <?php endif; ?>
                        <li class="nav-item dropdown px-3">
                            <a class="nav-link text-white" href="<?php echo e(URL::to('/#contact-us')); ?>" role="button">
                                <?php echo e(trans('landing.contact_us')); ?>

                            </a>
                        </li>
                    </ul>
                </div>
                
                <div class="header-btn d-flex align-items-center">
                <?php if(App\Models\SystemAddons::where('unique_identifier', 'language')->first() != null &&
                    App\Models\SystemAddons::where('unique_identifier', 'language')->first()->activated == 1): ?>
                    <a href="#">
                        <div class="dropdown border py-1 rounded-2 mx-3">
                            <a class=" dropdown-toggle mx-1 border-0 rounded-1 language-drop" href="#"
                                role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="<?php echo e(helper::image_path(session()->get('flag'))); ?>" alt=""
                                    class="img-fluid" width="25px">
                                <?php echo e(session()->get('language')); ?>

                            </a>

                            <ul class="dropdown-menu drop-menu <?php echo e(session()->get('direction') == 2 ? 'drop-menu-rtl' : 'drop-menu'); ?>">
                                <?php $__currentLoopData = helper::listoflanguage(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $languagelist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <a class="dropdown-item text-dark d-flex text-start"
                                            href="<?php echo e(URL::to('/lang/change?lang=' . $languagelist->code)); ?>">
                                            <img src="<?php echo e(helper::image_path($languagelist->image)); ?>"
                                                alt="" class="img-fluid mx-1" width="25px">
                                            <?php echo e($languagelist->name); ?>

                                        </a>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </a>
                    <?php endif; ?>
                    <a href="<?php echo e(URL::to('/admin')); ?>" target="_blank"
                        class="header-btn-login text-white px-3 py-2 border-0 rounded-2 py-2 px-2"> <?php echo e(trans('landing.get_started')); ?></a>
                </div>
                
            </nav>
        </div>
    </div>
</div>
<?php /**PATH C:\Users\Pc\Desktop\project\resources\views/landing/layout/header.blade.php ENDPATH**/ ?>