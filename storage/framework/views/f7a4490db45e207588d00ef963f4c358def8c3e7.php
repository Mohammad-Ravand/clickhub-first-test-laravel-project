<?php $__env->startSection('content'); ?>
    <div class="d-flex align-items-center mb-3">

        <h5 class="text-uppercase"><?php echo e(trans('labels.payment_settings')); ?></h5>

    </div>

    <div class="row">

        <div class="col-12">

            <div class="card border-0">

                <div class="card-body">

                    <form action="<?php echo e(URL::to('admin/payment/update')); ?>" method="POST" class="payments"
                        enctype="multipart/form-data">

                        <?php echo csrf_field(); ?>

                        <div class="accordion accordion-flush" id="accordionExample">

                            <?php
                                
                                $i = 1;
                                
                            ?>

                            <?php $__currentLoopData = $getpayment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $pmdata): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php
                                    
                                    $transaction_type = strtolower($pmdata->payment_name);
                                    
                                    $image_tag_name = $transaction_type . '_image';
                                    
                                ?>

                                <input type="hidden" name="transaction_type[]" value="<?php echo e($pmdata->id); ?>">

                                <div
                                    class="accordion-item card rounded border mb-3 <?php echo e($transaction_type == 'cod' && Auth::user()->type == 1 ? 'd-none' : ''); ?> <?php echo e($transaction_type == 'banktransfer' && Auth::user()->type == 2 ? 'd-none' : ''); ?>">

                                    <h2 class="accordion-header" id="heading<?php echo e($transaction_type); ?>">

                                        <button
                                            class="<?php echo e(session()->get('direction') == 2 ? 'accordion-button-rtl' : 'accordion-button'); ?> rounded collapsed"
                                            type="button" data-bs-toggle="collapse"
                                            data-bs-target="#targetto-<?php echo e($i); ?>-<?php echo e($transaction_type); ?>"
                                            aria-expanded="false"
                                            aria-controls="targetto-<?php echo e($i); ?>-<?php echo e($transaction_type); ?>">

                                            <b><?php echo e(trans('labels.' . $transaction_type)); ?></b>
                                            <?php if(
                                                $transaction_type == 'mercadopago' ||
                                                    $transaction_type == 'myfatoorah' ||
                                                    $transaction_type == 'paypal' ||
                                                    $transaction_type == 'toyyibpay'): ?>
                                                <?php if(env('Environment') == 'sendbox'): ?>
                                                    <span class="badge badge bg-danger ms-2"><?php echo e(trans('labels.addon')); ?></span>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </button>

                                    </h2>

                                    <div id="targetto-<?php echo e($i); ?>-<?php echo e($transaction_type); ?>"
                                        class="accordion-collapse collapse"
                                        aria-labelledby="heading<?php echo e($transaction_type); ?>"
                                        data-bs-parent="#accordionExample">

                                        <div class="accordion-body">

                                            <div class="row">

                                                <?php if($transaction_type == 'banktransfer'): ?>
                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <label class="form-label">

                                                                <?php echo e(trans('labels.bank_name')); ?> <span class="text-danger">
                                                                    *</span> </label>

                                                            <input type="text" class="form-control" name="bank_name"
                                                                placeholder="<?php echo e(trans('labels.bank_name')); ?>"
                                                                value="<?php echo e($pmdata->bank_name); ?>"
                                                                <?php echo e(Auth::user()->type == 1 ? 'required' : ''); ?>>

                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <label class="form-label">

                                                                <?php echo e(trans('labels.account_holder_name')); ?> <span
                                                                    class="text-danger"> *</span> </label>

                                                            <input type="text" class="form-control"
                                                                name="account_holder_name"
                                                                placeholder="<?php echo e(trans('labels.account_holder_name')); ?>"
                                                                value="<?php echo e($pmdata->account_holder_name); ?>"
                                                                <?php echo e(Auth::user()->type == 1 ? 'required' : ''); ?>>

                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <label class="form-label">

                                                                <?php echo e(trans('labels.account_number')); ?> <span
                                                                    class="text-danger"> *</span></label>

                                                            <input type="text" class="form-control" name="account_number"
                                                                placeholder="<?php echo e(trans('labels.account_number')); ?>"
                                                                value="<?php echo e($pmdata->account_number); ?>"
                                                                <?php echo e(Auth::user()->type == 1 ? 'required' : ''); ?>>

                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <label class="form-label">

                                                                <?php echo e(trans('labels.bank_ifsc_code')); ?> <span
                                                                    class="text-danger"> *</span></label>

                                                            <input type="text" class="form-control" name="bank_ifsc_code"
                                                                placeholder="<?php echo e(trans('labels.bank_ifsc_code')); ?>"
                                                                value="<?php echo e($pmdata->bank_ifsc_code); ?>"
                                                                <?php echo e(Auth::user()->type == 1 ? 'required' : ''); ?>>

                                                        </div>

                                                    </div>
                                                <?php endif; ?>

                                                <?php if(in_array($transaction_type, [
                                                        'razorpay',
                                                        'stripe',
                                                        'flutterwave',
                                                        'paystack',
                                                        'mercadopago',
                                                        'paypal',
                                                        'myfatoorah',
                                                        'toyyibpay',
                                                    ])): ?>
                                                    <div class="col-md-6">

                                                        <p class="form-label"><?php echo e(trans('labels.environment')); ?></p>

                                                        <div class="form-check form-check-inline">

                                                            <input class="form-check-input" type="radio"
                                                                name="environment[<?php echo e($transaction_type); ?>]"
                                                                id="<?php echo e($transaction_type); ?>_<?php echo e($key); ?>_environment"
                                                                value="1"
                                                                <?php echo e($pmdata->environment == 1 ? 'checked' : ''); ?>>

                                                            <label class="form-check-label"
                                                                for="<?php echo e($transaction_type); ?>_<?php echo e($key); ?>_environment">

                                                                <?php echo e(trans('labels.sandbox')); ?> </label>

                                                        </div>

                                                        <div class="form-check form-check-inline">

                                                            <input class="form-check-input" type="radio"
                                                                name="environment[<?php echo e($transaction_type); ?>]"
                                                                id="<?php echo e($transaction_type); ?>_<?php echo e($i); ?>_environment"
                                                                value="2"
                                                                <?php echo e($pmdata->environment == 2 ? 'checked' : ''); ?>>

                                                            <label class="form-check-label"
                                                                for="<?php echo e($transaction_type); ?>_<?php echo e($i); ?>_environment">

                                                                <?php echo e(trans('labels.production')); ?> </label>

                                                        </div>

                                                    </div>

                                                    <div class="col-md-6 d-flex justify-content-end align-items-center">

                                                        <input id="checkbox-switch-<?php echo e($transaction_type); ?>" type="checkbox"
                                                            class="checkbox-switch"
                                                            name="is_available[<?php echo e($transaction_type); ?>]" value="1"
                                                            <?php echo e($pmdata->is_available == 1 ? 'checked' : ''); ?>>

                                                        <label for="checkbox-switch-<?php echo e($transaction_type); ?>"
                                                            class="switch">

                                                            <span
                                                                class="<?php echo e(session()->get('direction') == 2 ? 'switch__circle-rtl' : 'switch__circle'); ?>"><span
                                                                    class="switch__circle-inner"></span></span>

                                                            <span
                                                                class="switch__left <?php echo e(session()->get('direction') == 2 ? 'pe-2' : 'ps-2'); ?>"><?php echo e(trans('labels.off')); ?></span>

                                                            <span
                                                                class="switch__right <?php echo e(session()->get('direction') == 2 ? 'ps-2' : 'pe-2'); ?>"><?php echo e(trans('labels.on')); ?></span>

                                                        </label>

                                                    </div>

                                                    <div
                                                        class="col-md-6 <?php echo e($transaction_type == 'mercadopago' ? 'd-none' : ''); ?>  <?php echo e($transaction_type == 'myfatoorah' ? 'd-none' : ''); ?>">

                                                        <div class="form-group">

                                                            <label for="<?php echo e($transaction_type); ?>_publickey"
                                                                class="form-label">

                                                                <?php echo e(trans('labels.public_key')); ?> <span
                                                                    class="text-danger"> *</span></label>

                                                            <input type="text" id="<?php echo e($transaction_type); ?>_publickey"
                                                                class="form-control"
                                                                name="public_key[<?php echo e($transaction_type); ?>]"
                                                                placeholder="<?php echo e(trans('labels.public_key')); ?>"
                                                                value="<?php echo e($pmdata->public_key); ?>">

                                                        </div>

                                                    </div>

                                                    <div
                                                        class=" <?php echo e($transaction_type == 'mercadopago' ? 'col-md-12' : 'col-md-6'); ?> <?php echo e($transaction_type == 'myfatoorah' ? 'col-md-12' : 'col-md-6'); ?>">

                                                        <div class="form-group">

                                                            <label for="<?php echo e($transaction_type); ?>_secretkey"
                                                                class="form-label">

                                                                <?php echo e(trans('labels.secret_key')); ?> <span
                                                                    class="text-danger"> *</span></label>

                                                            <input type="text" required
                                                                id="<?php echo e($transaction_type); ?>_secretkey"
                                                                class="form-control"
                                                                name="secret_key[<?php echo e($transaction_type); ?>]"
                                                                placeholder="<?php echo e(trans('labels.secret_key')); ?>"
                                                                value="<?php echo e($pmdata->secret_key); ?>">

                                                        </div>

                                                    </div>

                                                    <?php if($transaction_type == 'flutterwave'): ?>
                                                        <div class="col-md-12">

                                                            <div class="form-group">

                                                                <label for="encryption_key"
                                                                    class="form-label"><?php echo e(trans('labels.encryption_key')); ?>

                                                                    <span class="text-danger"> *</span>

                                                                </label>

                                                                <input type="text" required id="encryptionkey"
                                                                    class="form-control" name="encryption_key"
                                                                    placeholder="<?php echo e(trans('labels.encryption_key')); ?>"
                                                                    value="<?php echo e($pmdata->encryption_key); ?>">

                                                            </div>

                                                        </div>
                                                    <?php endif; ?>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <label for="image" class="form-label">

                                                                <?php echo e(trans('labels.image')); ?> </label>

                                                            <input type="file" class="form-control"
                                                                name="<?php echo e($image_tag_name); ?>">

                                                            <img src="<?php echo e(helper::image_path($pmdata->image)); ?>"
                                                                alt="" class="img-fluid rounded hw-50 mt-1">

                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <label for="<?php echo e($transaction_type); ?>currency"
                                                                class="form-label"> <?php echo e(trans('labels.currency')); ?> <span
                                                                    class="text-danger"> *</span>

                                                            </label>

                                                            <input type="text" required
                                                                id="<?php echo e($transaction_type); ?>currency" class="form-control"
                                                                name="currency[<?php echo e($transaction_type); ?>]"
                                                                placeholder="<?php echo e(trans('labels.currency')); ?>"
                                                                value="<?php echo e($pmdata->currency); ?>">

                                                        </div>

                                                    </div>
                                                <?php else: ?>
                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <label for="image" class="form-label">

                                                                <?php echo e(trans('labels.image')); ?> </label>

                                                            <input type="file" class="form-control"
                                                                name="<?php echo e($image_tag_name); ?>">

                                                            <img src="<?php echo e(helper::image_path($pmdata->image)); ?>"
                                                                alt="" class="img-fluid rounded hw-50 mt-1">

                                                        </div>

                                                    </div>

                                                    <div class="col-md-6 d-flex justify-content-end align-items-center">

                                                        <input id="checkbox-switch-<?php echo e($transaction_type); ?>"
                                                            type="checkbox" class="checkbox-switch"
                                                            name="is_available[<?php echo e($transaction_type); ?>]" value="1"
                                                            <?php echo e($pmdata->is_available == 1 ? 'checked' : ''); ?>>

                                                        <label for="checkbox-switch-<?php echo e($transaction_type); ?>"
                                                            class="switch">

                                                            <span
                                                                class="<?php echo e(session()->get('direction') == 2 ? 'switch__circle-rtl' : 'switch__circle'); ?>"><span
                                                                    class="switch__circle-inner"></span></span>

                                                            <span
                                                                class="switch__left <?php echo e(session()->get('direction') == 2 ? 'pe-2' : 'ps-2'); ?>"><?php echo e(trans('labels.off')); ?></span>

                                                            <span
                                                                class="switch__right <?php echo e(session()->get('direction') == 2 ? 'ps-2' : 'pe-2'); ?>"><?php echo e(trans('labels.on')); ?></span>

                                                        </label>

                                                    </div>
                                                <?php endif; ?>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>

                        <div class="form-group text-end">

                            <button class="btn btn-secondary"
                                <?php if(env('Environment') == 'sendbox'): ?> type="button" onclick="myFunction()" <?php else: ?> type="submit" <?php endif; ?>><?php echo e(trans('labels.save')); ?></button>

                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(url(env('ASSETPATHURL') . 'admin-assets/js/payment.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Pc\Desktop\project\resources\views/admin/payment/payment.blade.php ENDPATH**/ ?>