<?php $__env->startSection('content'); ?>
    <h5 class="text-uppercase"><?php echo e(trans('labels.settings')); ?></h5>
    <div class="row settings mt-3">
        <div class="col-xl-3 mb-3">
            <div class="card card-sticky-top border-0">
                <ul class="list-group list-options">
                    <a href="#basicinfo" data-tab="basicinfo"
                        class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center active"
                        aria-current="true"><?php echo e(trans('labels.basic_info')); ?> <i class="fa-regular fa-angle-right"></i></a>
                    <?php if(Auth::user()->type == 2): ?>
                        <a href="#themesettings" data-tab="themesettings"
                            class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                            aria-current="true"><?php echo e(trans('labels.theme_settings')); ?> <i
                                class="fa-regular fa-angle-right"></i></a>
                    <?php endif; ?>
                    <a href="#editprofile" data-tab="editprofile"
                        class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                        aria-current="true"><?php echo e(trans('labels.edit_profile')); ?> <i class="fa-regular fa-angle-right"></i></a>
                    <a href="#changepasssword" data-tab="changepasssword"
                        class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                        aria-current="true"><?php echo e(trans('labels.change_password')); ?> <i
                            class="fa-regular fa-angle-right"></i></a>
                    <a href="#seo" data-tab="seo"
                        class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                        aria-current="true"><?php echo e(trans('labels.seo')); ?> <i class="fa-regular fa-angle-right"></i></a>
                    <?php if(Auth::user()->type == 2): ?>
                        <?php if(App\Models\SystemAddons::where('unique_identifier', 'whatsapp_message')->first() != null &&
                                App\Models\SystemAddons::where('unique_identifier', 'whatsapp_message')->first()->activated == 1): ?>
                            <a href="#whatsappmessagesettings" data-tab="whatsappmessagesettings"
                                class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                                aria-current="true"><?php echo e(trans('labels.whatsapp_message')); ?> <?php if(env('Environment') == 'sendbox'): ?>
                                    <span class="badge badge bg-danger me-5"><?php echo e(trans('labels.addon')); ?></span>
                                <?php endif; ?>
                                <i class="fa-regular fa-angle-right"></i></a>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if(Auth::user()->type == 1): ?>
                        <?php if(App\Models\SystemAddons::where('unique_identifier', 'custom_domain')->first() != null &&
                                App\Models\SystemAddons::where('unique_identifier', 'custom_domain')->first()->activated == 1): ?>
                            <a href="#custom_domain" data-tab="custom_domain"
                                class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                                aria-current="true"><?php echo e(trans('labels.custom_domain')); ?><?php if(env('Environment') == 'sendbox'): ?>
                                    <span class="badge badge bg-danger me-5"><?php echo e(trans('labels.addon')); ?></span>
                                <?php endif; ?> <i class="fa-regular fa-angle-right"></i></a>
                        <?php endif; ?>
                        <?php if(App\Models\SystemAddons::where('unique_identifier', 'google_analytics')->first() != null &&
                                App\Models\SystemAddons::where('unique_identifier', 'google_analytics')->first()->activated == 1): ?>
                            <a href="#google_analytics" data-tab="google_analytics"
                                class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                                aria-current="true"><?php echo e(trans('labels.google_analytics')); ?><?php if(env('Environment') == 'sendbox'): ?>
                                    <span class="badge badge bg-danger me-5"><?php echo e(trans('labels.addon')); ?></span>
                                <?php endif; ?> <i class="fa-regular fa-angle-right"></i></a>
                        <?php endif; ?>
                        <a href="#landing_page" data-tab="landing_page"
                            class="list-group-item basicinfo p-3 list-item-secondary d-flex justify-content-between align-items-center"
                            aria-current="true"><?php echo e(trans('labels.landing_page')); ?>

                            <i class="fa-regular fa-angle-right"></i>
                        </a>
                    <?php endif; ?>
                   
                </ul>
            </div>
        </div>
        <div class="col-xl-9">
            <div id="settingmenuContent">
                <div id="basicinfo">
                    <div class="row mb-5">
                        <div class="col-12">
                            <div class="card border-0 box-shadow">
                                <div class="card-body">
                                    <div class="d-flex align-items-center mb-3">
                                        <h5 class="text-uppercase"><?php echo e(trans('labels.basic_info')); ?></h5>
                                    </div>
                                    <form action="<?php echo e(URL::to('admin/settings/update')); ?>" method="POST"
                                        enctype="multipart/form-data">
                                        <?php echo csrf_field(); ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="form-label"><?php echo e(trans('labels.currency')); ?><span
                                                                    class="text-danger"> * </span></label>
                                                            <input type="text" class="form-control" name="currency"
                                                                value="<?php echo e(@$settingdata->currency); ?>"
                                                                placeholder="<?php echo e(trans('labels.currency')); ?>" required>
                                                            <?php $__errorArgs = ['currency'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                                <small class="text-danger"><?php echo e($message); ?></small>
                                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <p class="form-label">
                                                                <?php echo e(trans('labels.currency_position')); ?>

                                                            </p>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input form-check-input-secondary"
                                                                    type="radio" name="currency_position" id="radio"
                                                                    value="1"
                                                                    <?php echo e(@$settingdata->currency_position == 'left' ? 'checked' : ''); ?> />
                                                                <label for="radio"
                                                                    class="form-check-label"><?php echo e(trans('labels.left')); ?></label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input form-check-input-secondary"
                                                                    type="radio" name="currency_position" id="radio1"
                                                                    value="2"
                                                                    <?php echo e(@$settingdata->currency_position == 'right' ? 'checked' : ''); ?> />
                                                                <label for="radio1"
                                                                    class="form-check-label"><?php echo e(trans('labels.right')); ?></label>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="<?php echo e(env('Environment') == 'sendbox' ? 'col-md-2' : 'col-md-3'); ?>">
                                                            <label class="form-label"
                                                                for=""><?php echo e(trans('labels.maintenance_mode')); ?>

                                                            </label>
                                                            <input id="maintenance_mode-switch" type="checkbox"
                                                                class="checkbox-switch" name="maintenance_mode"
                                                                value="1"
                                                                <?php echo e($settingdata->maintenance_mode == 1 ? 'checked' : ''); ?>>
                                                            <label for="maintenance_mode-switch" class="switch">
                                                                <span
                                                                    class="<?php echo e(session()->get('direction') == 2 ? 'switch__circle-rtl' : 'switch__circle'); ?>"><span
                                                                        class="switch__circle-inner"></span></span>
                                                                <span
                                                                    class="switch__left <?php echo e(session()->get('direction') == 2 ? 'pe-2' : 'ps-2'); ?>"><?php echo e(trans('labels.off')); ?></span>
                                                                <span
                                                                    class="switch__right <?php echo e(session()->get('direction') == 2 ? 'ps-2' : 'pe-2'); ?>"><?php echo e(trans('labels.on')); ?></span>
                                                            </label>
                                                        </div>
                                                        <?php if(Auth::user()->type == 2): ?>
                                                            <?php if(App\Models\SystemAddons::where('unique_identifier', 'customer_login')->first() != null &&
                                                                    App\Models\SystemAddons::where('unique_identifier', 'customer_login')->first()->activated == 1): ?>
                                                                <div
                                                                    class="<?php echo e(env('Environment') == 'sendbox' ? 'col-md-4' : 'col-md-3'); ?>">
                                                                    <label class="form-label"
                                                                        for=""><?php echo e(trans('labels.checkout_login_required')); ?>

                                                                    </label>
                                                                    <?php if(env('Environment') == 'sendbox'): ?>
                                                                        <span
                                                                            class="badge badge bg-danger ms-2 mb-0"><?php echo e(trans('labels.addon')); ?></span>
                                                                    <?php endif; ?>
                                                                    <input id="checkout_login_required-switch"
                                                                        type="checkbox" class="checkbox-switch"
                                                                        name="checkout_login_required" value="1"
                                                                        <?php echo e($settingdata->checkout_login_required == 1 ? 'checked' : ''); ?>>
                                                                    <label for="checkout_login_required-switch"
                                                                        class="switch">
                                                                        <span
                                                                            class="<?php echo e(session()->get('direction') == 2 ? 'switch__circle-rtl' : 'switch__circle'); ?>"><span
                                                                                class="switch__circle-inner"></span></span>
                                                                        <span
                                                                            class="switch__left <?php echo e(session()->get('direction') == 2 ? 'pe-2' : 'ps-2'); ?>"><?php echo e(trans('labels.off')); ?></span>
                                                                        <span
                                                                            class="switch__right <?php echo e(session()->get('direction') == 2 ? 'ps-2' : 'pe-2'); ?>"><?php echo e(trans('labels.on')); ?></span>
                                                                    </label>
                                                                </div>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-label"><?php echo e(trans('labels.time_zone')); ?></label>
                                                    <select class="form-select" name="timezone">
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Pacific/Midway' ? 'selected' : ''); ?>

                                                            value="Pacific/Midway">(GMT-11:00) Midway Island, Samoa
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Adak' ? 'selected' : ''); ?>

                                                            value="America/Adak">(GMT-10:00) Hawaii-Aleutian
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Etc/GMT+10' ? 'selected' : ''); ?>

                                                            value="Etc/GMT+10">(GMT-10:00) Hawaii</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Pacific/Marquesas' ? 'selected' : ''); ?>

                                                            value="Pacific/Marquesas">(GMT-09:30) Marquesas Islands
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Pacific/Gambier' ? 'selected' : ''); ?>

                                                            value="Pacific/Gambier">(GMT-09:00) Gambier Islands
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Anchorage' ? 'selected' : ''); ?>

                                                            value="America/Anchorage">(GMT-09:00) Alaska</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Ensenada' ? 'selected' : ''); ?>

                                                            value="America/Ensenada">(GMT-08:00) Tijuana, Baja
                                                            California </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Etc/GMT+8' ? 'selected' : ''); ?>

                                                            value="Etc/GMT+8">(GMT-08:00) Pitcairn Islands</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Los_Angeles' ? 'selected' : ''); ?>

                                                            value="America/Los_Angeles">(GMT-08:00) Pacific Time
                                                            (US
                                                            &amp; Canada) </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Denver' ? 'selected' : ''); ?>

                                                            value="America/Denver">(GMT-07:00) Mountain Time (US
                                                            &amp;
                                                            Canada) </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Chihuahua' ? 'selected' : ''); ?>

                                                            value="America/Chihuahua">(GMT-07:00) Chihuahua, La
                                                            Paz,
                                                            Mazatlan </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Dawson_Creek' ? 'selected' : ''); ?>

                                                            value="America/Dawson_Creek">(GMT-07:00) Arizona
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Belize' ? 'selected' : ''); ?>

                                                            value="America/Belize">(GMT-06:00) Saskatchewan,
                                                            Central
                                                            America </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Cancun' ? 'selected' : ''); ?>

                                                            value="America/Cancun">(GMT-06:00) Guadalajara, Mexico
                                                            City,
                                                            Monterrey </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Chile/EasterIsland' ? 'selected' : ''); ?>

                                                            value="Chile/EasterIsland">(GMT-06:00) Easter Island
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Chicago' ? 'selected' : ''); ?>

                                                            value="America/Chicago">(GMT-06:00) Central Time (US
                                                            &amp;
                                                            Canada) </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/New_York' ? 'selected' : ''); ?>

                                                            value="America/New_York">(GMT-05:00) Eastern Time (US
                                                            &amp;
                                                            Canada) </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Havana' ? 'selected' : ''); ?>

                                                            value="America/Havana">(GMT-05:00) Cuba</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Bogota' ? 'selected' : ''); ?>

                                                            value="America/Bogota">(GMT-05:00) Bogota, Lima, Quito,
                                                            Rio
                                                            Branco </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Caracas' ? 'selected' : ''); ?>

                                                            value="America/Caracas">(GMT-04:30) Caracas</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Santiago' ? 'selected' : ''); ?>

                                                            value="America/Santiago">(GMT-04:00) Santiago</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/La_Paz' ? 'selected' : ''); ?>

                                                            value="America/La_Paz">(GMT-04:00) La Paz</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Atlantic/Stanley' ? 'selected' : ''); ?>

                                                            value="Atlantic/Stanley">(GMT-04:00) Faukland Islands
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Campo_Grande' ? 'selected' : ''); ?>

                                                            value="America/Campo_Grande">(GMT-04:00) Brazil
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Goose_Bay' ? 'selected' : ''); ?>

                                                            value="America/Goose_Bay">(GMT-04:00) Atlantic Time
                                                            (Goose
                                                            Bay) </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Glace_Bay' ? 'selected' : ''); ?>

                                                            value="America/Glace_Bay">(GMT-04:00) Atlantic Time
                                                            (Canada) </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/St_Johns' ? 'selected' : ''); ?>

                                                            value="America/St_Johns">(GMT-03:30) Newfoundland
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Araguaina' ? 'selected' : ''); ?>

                                                            value="America/Araguaina">(GMT-03:00) UTC-3</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Montevideo' ? 'selected' : ''); ?>

                                                            value="America/Montevideo">(GMT-03:00) Montevideo
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Miquelon' ? 'selected' : ''); ?>

                                                            value="America/Miquelon">(GMT-03:00) Miquelon, St.
                                                            Pierre
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Godthab' ? 'selected' : ''); ?>

                                                            value="America/Godthab">(GMT-03:00) Greenland</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Argentina' ? 'selected' : ''); ?>

                                                            value="America/Argentina/Buenos_Aires">(GMT-03:00)
                                                            Buenos
                                                            Aires </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Sao_Paulo' ? 'selected' : ''); ?>

                                                            value="America/Sao_Paulo">(GMT-03:00) Brasilia</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'America/Noronha' ? 'selected' : ''); ?>

                                                            value="America/Noronha">(GMT-02:00) Mid-Atlantic
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Atlantic/Cape_Verde' ? 'selected' : ''); ?>

                                                            value="Atlantic/Cape_Verde">(GMT-01:00) Cape Verde Is.
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Atlantic/Azores' ? 'selected' : ''); ?>

                                                            value="Atlantic/Azores">(GMT-01:00) Azores</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Europe/Belfast' ? 'selected' : ''); ?>

                                                            value="Europe/Belfast">(GMT) Greenwich Mean Time :
                                                            Belfast
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Europe/Dublin' ? 'selected' : ''); ?>

                                                            value="Europe/Dublin">(GMT) Greenwich Mean Time :
                                                            Dublin
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Europe/Lisbon' ? 'selected' : ''); ?>

                                                            value="Europe/Lisbon">(GMT) Greenwich Mean Time :
                                                            Lisbon
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Europe/London' ? 'selected' : ''); ?>

                                                            value="Europe/London">(GMT) Greenwich Mean Time :
                                                            London
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Africa/Abidjan' ? 'selected' : ''); ?>

                                                            value="Africa/Abidjan">(GMT) Monrovia, Reykjavik
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Europe/Amsterdam' ? 'selected' : ''); ?>

                                                            value="Europe/Amsterdam">(GMT+01:00) Amsterdam, Berlin,
                                                            Bern, Rome, Stockholm, Vienna</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Europe/Belgrade' ? 'selected' : ''); ?>

                                                            value="Europe/Belgrade">(GMT+01:00) Belgrade,
                                                            Bratislava,
                                                            Budapest, Ljubljana, Prague</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Europe/Brussels' ? 'selected' : ''); ?>

                                                            value="Europe/Brussels">(GMT+01:00) Brussels,
                                                            Copenhagen,
                                                            Madrid, Paris </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Africa/Algiers' ? 'selected' : ''); ?>

                                                            value="Africa/Algiers">(GMT+01:00) West Central Africa
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Africa/Windhoek' ? 'selected' : ''); ?>

                                                            value="Africa/Windhoek">(GMT+01:00) Windhoek</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Beirut' ? 'selected' : ''); ?>

                                                            value="Asia/Beirut">(GMT+02:00) Beirut</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Africa/Cairo' ? 'selected' : ''); ?>

                                                            value="Africa/Cairo">(GMT+02:00) Cairo</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Gaza' ? 'selected' : ''); ?>

                                                            value="Asia/Gaza">(GMT+02:00) Gaza</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Africa/Blantyre' ? 'selected' : ''); ?>

                                                            value="Africa/Blantyre">(GMT+02:00) Harare, Pretoria
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Jerusalem' ? 'selected' : ''); ?>

                                                            value="Asia/Jerusalem">(GMT+02:00) Jerusalem</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Europe/Minsk' ? 'selected' : ''); ?>

                                                            value="Europe/Minsk">(GMT+02:00) Minsk</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Damascus' ? 'selected' : ''); ?>

                                                            value="Asia/Damascus">(GMT+02:00) Syria</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Europe/Moscow' ? 'selected' : ''); ?>

                                                            value="Europe/Moscow">(GMT+03:00) Moscow, St.
                                                            Petersburg,
                                                            Volgograd </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Africa/Addis_Ababa' ? 'selected' : ''); ?>

                                                            value="Africa/Addis_Ababa">(GMT+03:00) Nairobi</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Tehran' ? 'selected' : ''); ?>

                                                            value="Asia/Tehran">(GMT+03:30) Tehran</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Dubai' ? 'selected' : ''); ?>

                                                            value="Asia/Dubai">(GMT+04:00) Abu Dhabi, Muscat
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Yerevan' ? 'selected' : ''); ?>

                                                            value="Asia/Yerevan">(GMT+04:00) Yerevan</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Kabul' ? 'selected' : ''); ?>

                                                            value="Asia/Kabul">(GMT+04:30) Kabul</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Yekaterinburg' ? 'selected' : ''); ?>

                                                            value="Asia/Yekaterinburg">(GMT+05:00) Ekaterinburg
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Tashkent' ? 'selected' : ''); ?>

                                                            value="Asia/Tashkent"> (GMT+05:00) Tashkent</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Kolkata' ? 'selected' : ''); ?>

                                                            value="Asia/Kolkata"> (GMT+05:30) Chennai, Kolkata,
                                                            Mumbai,
                                                            New Delhi</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Katmandu' ? 'selected' : ''); ?>

                                                            value="Asia/Katmandu">(GMT+05:45) Kathmandu</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Dhaka' ? 'selected' : ''); ?>

                                                            value="Asia/Dhaka">(GMT+06:00) Astana, Dhaka</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Novosibirsk' ? 'selected' : ''); ?>

                                                            value="Asia/Novosibirsk">(GMT+06:00) Novosibirsk
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Rangoon' ? 'selected' : ''); ?>

                                                            value="Asia/Rangoon">(GMT+06:30) Yangon (Rangoon)
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Bangkok' ? 'selected' : ''); ?>

                                                            value="Asia/Bangkok">(GMT+07:00) Bangkok, Hanoi,
                                                            Jakarta
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Kuala_Lumpur' ? 'selected' : ''); ?>

                                                            value="Asia/Kuala_Lumpur">(GMT+08:00) Kuala Lumpur
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Krasnoyarsk' ? 'selected' : ''); ?>

                                                            value="Asia/Krasnoyarsk">(GMT+07:00) Krasnoyarsk
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Hong_Kong' ? 'selected' : ''); ?>

                                                            value="Asia/Hong_Kong">(GMT+08:00) Beijing, Chongqing,
                                                            Hong
                                                            Kong, Urumqi</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Irkutsk' ? 'selected' : ''); ?>

                                                            value="Asia/Irkutsk">(GMT+08:00) Irkutsk, Ulaan Bataar
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Australia/Perth' ? 'selected' : ''); ?>

                                                            value="Australia/Perth">(GMT+08:00) Perth</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Australia/Eucla' ? 'selected' : ''); ?>

                                                            value="Australia/Eucla">(GMT+08:45) Eucla</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Tokyo' ? 'selected' : ''); ?>

                                                            value="Asia/Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Seoul' ? 'selected' : ''); ?>

                                                            value="Asia/Seoul">(GMT+09:00) Seoul</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Yakutsk' ? 'selected' : ''); ?>

                                                            value="Asia/Yakutsk">(GMT+09:00) Yakutsk</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Australia/Adelaide' ? 'selected' : ''); ?>

                                                            value="Australia/Adelaide">(GMT+09:30) Adelaide
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Australia/Darwin' ? 'selected' : ''); ?>

                                                            value="Australia/Darwin">(GMT+09:30) Darwin</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Australia/Brisbane' ? 'selected' : ''); ?>

                                                            value="Australia/Brisbane">(GMT+10:00) Brisbane
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Australia/Hobart' ? 'selected' : ''); ?>

                                                            value="Australia/Hobart">(GMT+10:00) Hobart</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Vladivostok' ? 'selected' : ''); ?>

                                                            value="Asia/Vladivostok">(GMT+10:00) Vladivostok
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Australia/Lord_Howe' ? 'selected' : ''); ?>

                                                            value="Australia/Lord_Howe">(GMT+10:30) Lord Howe
                                                            Island
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Etc/GMT-11' ? 'selected' : ''); ?>

                                                            value="Etc/GMT-11">(GMT+11:00) Solomon Is., New
                                                            Caledonia
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Magadan' ? 'selected' : ''); ?>

                                                            value="Asia/Magadan">(GMT+11:00) Magadan</option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Pacific/Norfolk' ? 'selected' : ''); ?>

                                                            value="Pacific/Norfolk">(GMT+11:30) Norfolk Island
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Asia/Anadyr' ? 'selected' : ''); ?>

                                                            value="Asia/Anadyr">(GMT+12:00) Anadyr, Kamchatka
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Pacific/Auckland' ? 'selected' : ''); ?>

                                                            value="Pacific/Auckland">(GMT+12:00) Auckland,
                                                            Wellington
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Etc/GMT-12' ? 'selected' : ''); ?>

                                                            value="Etc/GMT-12">(GMT+12:00) Fiji, Kamchatka,
                                                            Marshall
                                                            Is. </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Pacific/Chatham' ? 'selected' : ''); ?>

                                                            value="Pacific/Chatham">(GMT+12:45) Chatham Islands
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Pacific/Tongatapu' ? 'selected' : ''); ?>

                                                            value="Pacific/Tongatapu">(GMT+13:00) Nuku'alofa
                                                        </option>
                                                        <option
                                                            <?php echo e(@$settingdata->timezone == 'Pacific/Kiritimati' ? 'selected' : ''); ?>

                                                            value="Pacific/Kiritimati">(GMT+14:00) Kiritimati
                                                        </option>
                                                    </select>
                                                    <?php $__errorArgs = ['timezone'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                        <small class="text-danger"><?php echo e($message); ?></small>
                                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                </div>
                                                <?php if(Auth::user()->type == 1): ?>
                                                    <?php if(App\Models\SystemAddons::where('unique_identifier', 'vendor_app')->first() != null &&
                                                            App\Models\SystemAddons::where('unique_identifier', 'vendor_app')->first()->activated == 1): ?>
                                                        <div class="form-group">
                                                            <label
                                                                class="form-label"><?php echo e(trans('labels.firebase_server_key')); ?></label>
                                                            <?php if(env('Environment') == 'sendbox'): ?>
                                                                <span
                                                                    class="badge badge bg-danger ms-2 mb-0"><?php echo e(trans('labels.addon')); ?></span>
                                                            <?php endif; ?>
                                                            <input type="text" class="form-control"
                                                                name="firebase_server_key"
                                                                value="<?php echo e(@$settingdata->firebase); ?>"
                                                                placeholder="<?php echo e(trans('labels.firebase_server_key')); ?>"
                                                                required>
                                                            <?php $__errorArgs = ['firebase_server_key'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                                <small class="text-danger"><?php echo e($message); ?></small> <br>
                                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                                <div class="form-group">
                                                    <label class="form-label"><?php echo e(trans('labels.website_title')); ?><span
                                                            class="text-danger"> * </span></label>
                                                    <input type="text" class="form-control" name="website_title"
                                                        value="<?php echo e(@$settingdata->website_title); ?>"
                                                        placeholder="<?php echo e(trans('labels.website_title')); ?>">
                                                    <?php $__errorArgs = ['website_title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                        <small class="text-danger"><?php echo e($message); ?></small>
                                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label"><?php echo e(trans('labels.copyright')); ?><span
                                                            class="text-danger"> * </span></label>
                                                    <input type="text" class="form-control" name="copyright"
                                                        value="<?php echo e(@$settingdata->copyright); ?>"
                                                        placeholder="<?php echo e(trans('labels.copyright')); ?>">
                                                    <?php $__errorArgs = ['copyright'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                        <small class="text-danger"><?php echo e($message); ?></small>
                                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                </div>
                                            </div>
                                            <?php if(Auth::user()->type == 2): ?>
                                                <?php if(App\Models\SystemAddons::where('unique_identifier', 'unique_slug')->first() != null &&
                                                        App\Models\SystemAddons::where('unique_identifier', 'unique_slug')->first()->activated == 1): ?>
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.personlized_link')); ?><span
                                                                class="text-danger"> * </span></label>
                                                        <?php if(env('Environment') == 'sendbox'): ?>
                                                            <span
                                                                class="badge badge bg-danger ms-2 mb-0"><?php echo e(trans('labels.addon')); ?></span>
                                                        <?php endif; ?>
                                                        <div class="input-group">
                                                            <span class="input-group-text"><?php echo e(URL::to('/')); ?></span>
                                                            <input type="text" class="form-control" id="slug"
                                                                name="slug" value="<?php echo e(Auth::user()->slug); ?>"
                                                                required>
                                                        </div>
                                                        <?php $__errorArgs = ['slug'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <?php if(Auth::user()->type == 2): ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.contact_email')); ?><span
                                                                class="text-danger"> * </span></label>
                                                        <input type="email" class="form-control" name="email"
                                                            value="<?php echo e(@$settingdata->email); ?>"
                                                            placeholder="<?php echo e(trans('labels.contact_email')); ?>" required>
                                                        <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.contact_mobile')); ?><span
                                                                class="text-danger"> * </span></label>
                                                        <input type="number" class="form-control" name="contact_mobile"
                                                            value="<?php echo e(@$settingdata->contact); ?>"
                                                            placeholder="<?php echo e(trans('labels.contact_mobile')); ?>" required>
                                                        <?php $__errorArgs = ['contact_mobile'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label class="form-label"><?php echo e(trans('labels.address')); ?><span
                                                                class="text-danger"> * </span></label>
                                                        <textarea class="form-control" name="address" rows="3" placeholder="<?php echo e(trans('labels.address')); ?>"><?php echo e(@$settingdata->address); ?></textarea>
                                                        <?php $__errorArgs = ['address'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.delivery_type')); ?><span
                                                                class="text-danger"> * </span></label>
                                                        <select class="form-select" name="delivery_type">
                                                            <option value="" selected disabled>
                                                                <?php echo e(trans('labels.select')); ?>

                                                            </option>
                                                            <option value="delivery"
                                                                <?php echo e(@$settingdata->delivery_type == 'delivery' ? 'selected' : ''); ?>>
                                                                <?php echo e(trans('labels.delivery')); ?>

                                                            </option>
                                                            <option value="pickup"
                                                                <?php echo e(@$settingdata->delivery_type == 'pickup' ? 'selected' : ''); ?>>
                                                                <?php echo e(trans('labels.pickup')); ?>

                                                            </option>
                                                            <option value="both"
                                                                <?php echo e(@$settingdata->delivery_type == 'both' ? 'selected' : ''); ?>>
                                                                <?php echo e(trans('labels.both')); ?>

                                                            </option>
                                                        </select>
                                                        <?php $__errorArgs = ['delivery_type'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.facebook_link')); ?></label>
                                                        <input type="text" class="form-control" name="facebook_link"
                                                            value="<?php echo e(@$settingdata->facebook_link); ?>"
                                                            placeholder="<?php echo e(trans('labels.facebook_link')); ?>">
                                                        <?php $__errorArgs = ['facebook_link'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.twitter_link')); ?></label>
                                                        <input type="text" class="form-control" name="twitter_link"
                                                            value="<?php echo e(@$settingdata->twitter_link); ?>"
                                                            placeholder="<?php echo e(trans('labels.twitter_link')); ?>">
                                                        <?php $__errorArgs = ['twitter_link'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.instagram_link')); ?></label>
                                                        <input type="text" class="form-control" name="instagram_link"
                                                            value="<?php echo e(@$settingdata->instagram_link); ?>"
                                                            placeholder="<?php echo e(trans('labels.instagram_link')); ?>">
                                                        <?php $__errorArgs = ['instagram_link'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.linkedin_link')); ?></label>
                                                        <input type="text" class="form-control" name="linkedin_link"
                                                            value="<?php echo e(@$settingdata->linkedin_link); ?>"
                                                            placeholder="<?php echo e(trans('labels.linkedin_link')); ?>">
                                                        <?php $__errorArgs = ['linkedin_link'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-label"><?php echo e(trans('labels.description')); ?><span
                                                                class="text-danger"> * </span></label>
                                                        <textarea class="form-control" name="description" rows="3" placeholder="<?php echo e(trans('labels.description')); ?>"><?php echo e(@$settingdata->description); ?></textarea>
                                                        <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row justify-content-between">
                                                            <label class="col-auto col-form-label"
                                                                for=""><?php echo e(trans('labels.footer_features')); ?>

                                                                <span class="" data-bs-toggle="tooltip"
                                                                    data-bs-placement="top"
                                                                    title="Ex. <i class='fa-solid fa-truck-fast'></i> Visit https://fontawesome.com/ for more info">
                                                                    <i class="fa-solid fa-circle-info"></i> </span></label>
                                                            <?php if(count($getfooterfeatures) > 0): ?>
                                                                <span class="col-auto"><button
                                                                        class="btn btn-sm btn-outline-info" type="button"
                                                                        onclick="add_features('<?php echo e(trans('labels.icon')); ?>','<?php echo e(trans('labels.title')); ?>','<?php echo e(trans('labels.description')); ?>')">
                                                                        <?php echo e(trans('labels.add_new')); ?>

                                                                        <?php echo e(trans('labels.footer_features')); ?> <i
                                                                            class="fa-sharp fa-solid fa-plus"></i></button></span>
                                                            <?php endif; ?>
                                                        </div>
                                                        <?php $__empty_1 = true; $__currentLoopData = $getfooterfeatures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $features): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                            <div class="row">
                                                                <input type="hidden" name="edit_icon_key[]"
                                                                    value="<?php echo e($features->id); ?>">
                                                                <div class="col-md-3 form-group">
                                                                    <div class="input-group">
                                                                        <input type="text"
                                                                            class="form-control feature_required  <?php echo e(session()->get('direction') == 2 ? 'input-group-rtl' : ''); ?>"
                                                                            onkeyup="show_feature_icon(this)"
                                                                            name="edi_feature_icon[<?php echo e($features->id); ?>]"
                                                                            placeholder="<?php echo e(trans('labels.icon')); ?>"
                                                                            value="<?php echo e($features->icon); ?>" required>
                                                                        <p
                                                                            class="input-group-text <?php echo e(session()->get('direction') == 2 ? 'input-group-icon-rtl' : ''); ?>">
                                                                            <?php echo $features->icon; ?>

                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 form-group">
                                                                    <input type="text" class="form-control"
                                                                        name="edi_feature_title[<?php echo e($features->id); ?>]"
                                                                        placeholder="<?php echo e(trans('labels.title')); ?>"
                                                                        value="<?php echo e($features->title); ?>" required>
                                                                </div>
                                                                <div class="col-md-5 form-group">
                                                                    <input type="text" class="form-control"
                                                                        name="edi_feature_description[<?php echo e($features->id); ?>]"
                                                                        placeholder="<?php echo e(trans('labels.description')); ?>"
                                                                        value="<?php echo e($features->description); ?>" required>
                                                                </div>
                                                                <div class="col-md-1 form-group">
                                                                    <button class="btn btn-danger" type="button"
                                                                        onclick="statusupdate('<?php echo e(URL::to('admin/settings/delete-feature-' . $features->id)); ?>')">
                                                                        <i class="fa fa-trash"></i> </button>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                            <div class="row">
                                                                <div class="col-md-3 form-group">
                                                                    <div class="input-group">
                                                                        <input type="text"
                                                                            class="form-control feature_required"
                                                                            onkeyup="show_feature_icon(this)"
                                                                            name="feature_icon[]"
                                                                            placeholder="<?php echo e(trans('labels.icon')); ?>">
                                                                        <p class="input-group-text"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 form-group">
                                                                    <input type="text"
                                                                        class="form-control feature_required"
                                                                        name="feature_title[]"
                                                                        placeholder="<?php echo e(trans('labels.title')); ?>"
                                                                        required>
                                                                </div>
                                                                <div class="col-md-5 form-group">
                                                                    <input type="text"
                                                                        class="form-control feature_required"
                                                                        name="feature_description[]"
                                                                        placeholder="<?php echo e(trans('labels.description')); ?>"
                                                                        required>
                                                                </div>
                                                                <div class="col-md-1 form-group">
                                                                    <button class="btn btn-info" type="button"
                                                                        onclick="add_features('<?php echo e(trans('labels.icon')); ?>','<?php echo e(trans('labels.title')); ?>','<?php echo e(trans('labels.description')); ?>')">
                                                                        <i class="fa-sharp fa-solid fa-plus"></i> </button>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <span class="extra_footer_features"></span>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(Auth::user()->type == 2): ?>
                                                <div class="form-group col-sm-6">
                                                    <label class="form-label"><?php echo e(trans('labels.banner')); ?><small>(1920
                                                            x 400)</small></label>
                                                    <input type="file" class="form-control" name="banner">
                                                    <?php $__errorArgs = ['banner'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                        <small class="text-danger"><?php echo e($message); ?></small> <br>
                                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    <img class="img-fluid rounded hw-70 mt-1 object-fit-cover"
                                                        src="<?php echo e(helper::image_path(@$settingdata->banner)); ?>"
                                                        alt="">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label
                                                        class="form-label"><?php echo e(trans('labels.landing_page_cover_image')); ?>

                                                        (650 x 300)
                                                    </label>
                                                    <input type="file" class="form-control"
                                                        name="landin_page_cover_image">
                                                    <?php $__errorArgs = ['landin_page_cover_image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                        <small class="text-danger"><?php echo e($message); ?></small> <br>
                                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    <img class="img-fluid rounded hw-70 mt-1 object-fit-cover"
                                                        src="<?php echo e(helper::image_path($settingdata->cover_image)); ?>"
                                                        alt="">
                                                </div>
                                                <?php if(App\Models\SystemAddons::where('unique_identifier', 'notification')->first() != null &&
                                                        App\Models\SystemAddons::where('unique_identifier', 'notification')->first()->activated == 1): ?>
                                                    <div class="form-group col-md-6">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.notification_sound')); ?></label>
                                                        <?php if(env('Environment') == 'sendbox'): ?>
                                                            <span
                                                                class="badge badge bg-danger ms-2 mb-0"><?php echo e(trans('labels.addon')); ?></span>
                                                        <?php endif; ?>
                                                        <input type="file" class="form-control"
                                                            name="notification_sound">
                                                        <?php $__errorArgs = ['notification_sound'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small><br>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                        <?php if(!empty($settingdata->notification_sound) && $settingdata->notification_sound != null): ?>
                                                            <audio controls class="mt-1">
                                                                <source
                                                                    src="<?php echo e(url(env('ASSETPATHURL') . 'admin-assets/notification/' . $settingdata->notification_sound)); ?>"
                                                                    type="audio/mpeg">
                                                            </audio>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <div class="form-group text-end">
                                                <button class="btn btn-secondary"
                                                    <?php if(env('Environment') == 'sendbox'): ?> type="button" onclick="myFunction()" <?php else: ?> type="submit" name="updatebasicinfo" value="1" <?php endif; ?>><?php echo e(trans('labels.save')); ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(Auth::user()->type == 2): ?>
                    <div id="themesettings">
                        <div class="row mb-5">
                            <div class="col-12">
                                <div class="card border-0 box-shadow">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center mb-3">
                                            <h5 class="text-uppercase"><?php echo e(trans('labels.theme_settings')); ?></h5>
                                        </div>
                                        <form method="POST" action="<?php echo e(URL::to('admin/settings/updatetheme')); ?>"
                                            enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="form-label"><?php echo e(trans('labels.logo')); ?>

                                                                    <small>(250 x
                                                                        250)</small></label>
                                                                <input type="file" class="form-control"
                                                                    name="logo">
                                                                <?php $__errorArgs = ['logo'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                                    <small class="text-danger"><?php echo e($message); ?></small>
                                                                    <br>
                                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                                <img class="img-fluid rounded hw-70 mt-1 object-fit-contain"
                                                                    src="<?php echo e(helper::image_path(@$settingdata->logo)); ?>"
                                                                    alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="form-label"><?php echo e(trans('labels.favicon')); ?>

                                                                    (16 x
                                                                    16)</label>
                                                                <input type="file" class="form-control"
                                                                    name="favicon">
                                                                <?php $__errorArgs = ['favicon'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                                    <small class="text-danger"><?php echo e($message); ?></small>
                                                                    <br>
                                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                                <img class="img-fluid rounded hw-70 mt-1 object-fit-contain"
                                                                    src="<?php echo e(helper::image_path(@$settingdata->favicon)); ?>"
                                                                    alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label
                                                                    class="form-label"><?php echo e(trans('labels.primary_color')); ?>

                                                                    <span class="text-danger"> * </span> </label>
                                                                <input type="color"
                                                                    class="form-control form-control-color w-100 border-0"
                                                                    name="primary_color"
                                                                    value="<?php echo e(@$settingdata->primary_color); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label
                                                                    class="form-label"><?php echo e(trans('labels.secondary_color')); ?>

                                                                    <span class="text-danger"> * </span> </label>
                                                                <input type="color"
                                                                    class="form-control form-control-color w-100 border-0"
                                                                    name="secondary_color"
                                                                    value="<?php echo e(@$settingdata->secondary_color); ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                    if (Auth::user()->allow_without_subscription == 1) {
                                                        $themes = explode(',', '1,2,3,4,5');
                                                    } else {
                                                        $themes = explode(',', @$settingdata->template);
                                                    }
                                                ?>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-label"><?php echo e(trans('labels.theme')); ?>

                                                            <span class="text-danger"> * </span> </label>
                                                        <?php if(env('Environment') == 'sendbox'): ?>
                                                            <span
                                                                class="badge badge bg-danger ms-2"><?php echo e(trans('labels.addon')); ?></span>
                                                        <?php endif; ?>
                                                        <ul class="theme-selection">
                                                            <?php $__currentLoopData = $themes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <li>
                                                                    <input type="radio" name="template"
                                                                        id="template<?php echo e($item); ?>"
                                                                        value="<?php echo e($item); ?>"
                                                                        <?php echo e(@$settingdata->template == $item ? 'checked' : ''); ?>>
                                                                    <label for="template<?php echo e($item); ?>">
                                                                        <img
                                                                            src="<?php echo e(helper::image_path('theme-' . $item . '.png')); ?>">
                                                                    </label>
                                                                </li>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="form-group text-end">
                                                    <button class="btn btn-secondary"
                                                        <?php if(env('Environment') == 'sendbox'): ?> type="button" onclick="myFunction()" <?php else: ?> type="submit" <?php endif; ?>><?php echo e(trans('labels.save')); ?></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div id="editprofile">
                    <div class="row mb-5">
                        <div class="col-12">
                            <div class="card border-0 box-shadow">
                                <div class="card-body">
                                    <div class="d-flex align-items-center mb-3">
                                        <h5 class="text-uppercase"><?php echo e(trans('labels.edit_profile')); ?></h5>
                                    </div>
                                    <form method="POST"
                                        action="<?php echo e(URL::to('admin/settings/update-profile-' . Auth::user()->slug)); ?>"
                                        enctype="multipart/form-data">
                                        <?php echo csrf_field(); ?>
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label class="form-label"><?php echo e(trans('labels.name')); ?><span
                                                        class="text-danger"> * </span></label>
                                                <input type="text" class="form-control" name="name"
                                                    value="<?php echo e(Auth::user()->name); ?>"
                                                    placeholder="<?php echo e(trans('labels.name')); ?>" required>
                                                <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <span class="text-danger"><?php echo e($message); ?></span>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="form-label"><?php echo e(trans('labels.email')); ?><span
                                                        class="text-danger"> * </span></label>
                                                <input type="email" class="form-control" name="email"
                                                    value="<?php echo e(Auth::user()->email); ?>"
                                                    placeholder="<?php echo e(trans('labels.email')); ?>" required>
                                                <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <span class="text-danger"><?php echo e($message); ?></span>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="form-label"
                                                    for="mobile"><?php echo e(trans('labels.mobile')); ?><span
                                                        class="text-danger"> * </span></label>
                                                <input type="number" class="form-control" name="mobile" id="mobile"
                                                    value="<?php echo e(Auth::user()->mobile); ?>"
                                                    placeholder="<?php echo e(trans('labels.mobile')); ?>" required>
                                                <?php $__errorArgs = ['mobile'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <span class="text-danger"><?php echo e($message); ?></span>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="form-label"><?php echo e(trans('labels.image')); ?><small>(250
                                                        x 250)</small></label>
                                                <input type="file" class="form-control" name="profile">
                                                <?php $__errorArgs = ['profile'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <span class="text-danger"><?php echo e($message); ?></span> <br>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                <img class="img-fluid rounded hw-70 mt-1"
                                                    src="<?php echo e(helper::image_Path(Auth::user()->image)); ?>" alt="">
                                            </div>
                                            <?php if(Auth::user()->type == 2): ?>
                                                <div class="form-group col-md-6">
                                                    <label for="country"
                                                        class="form-label"><?php echo e(trans('labels.country')); ?><span
                                                            class="text-danger"> * </span></label>
                                                    <select name="country" class="form-select" id="country" required>
                                                        <option value=""><?php echo e(trans('labels.select')); ?></option>
                                                        <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($country->id); ?>"
                                                                <?php echo e($country->id == Auth::user()->country_id ? 'selected' : ''); ?>>
                                                                <?php echo e($country->name); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="city"
                                                        class="form-label"><?php echo e(trans('labels.city')); ?><span
                                                            class="text-danger">
                                                            * </span></label>
                                                    <select name="city" class="form-select" id="city" required>
                                                        <option value=""><?php echo e(trans('labels.select')); ?></option>
                                                    </select>
                                                </div>
                                            <?php endif; ?>
                                            <div class="form-group text-end">
                                                <button class="btn btn-secondary"
                                                    <?php if(env('Environment') == 'sendbox'): ?> type="button" onclick="myFunction()" <?php else: ?> type="submit" name="updateprofile" value="1" <?php endif; ?>><?php echo e(trans('labels.save')); ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="changepasssword">
                    <div class="row mb-5">
                        <div class="col-12">
                            <div class="card border-0 box-shadow">
                                <div class="card-body">
                                    <div class="d-flex align-items-center mb-3">
                                        <h5 class="text-uppercase"><?php echo e(trans('labels.change_password')); ?></h5>
                                    </div>
                                    <form action="<?php echo e(URL::to('admin/settings/change-password')); ?>" method="POST">
                                        <?php echo csrf_field(); ?>
                                        <div class="row">
                                            <div class="form-group col-sm-12">
                                                <label class="form-label"><?php echo e(trans('labels.current_password')); ?><span
                                                        class="text-danger"> * </span></label>
                                                <input type="password" class="form-control" name="current_password"
                                                    value="<?php echo e(old('current_password')); ?>"
                                                    placeholder="<?php echo e(trans('labels.current_password')); ?>" required>
                                                <?php $__errorArgs = ['current_password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <span class="text-danger"><?php echo e($message); ?></span>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="form-label"><?php echo e(trans('labels.new_password')); ?><span
                                                        class="text-danger"> * </span></label>
                                                <input type="password" class="form-control" name="new_password"
                                                    value="<?php echo e(old('new_password')); ?>"
                                                    placeholder="<?php echo e(trans('labels.new_password')); ?>" required>
                                                <?php $__errorArgs = ['new_password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <span class="text-danger"><?php echo e($message); ?></span>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="form-label"><?php echo e(trans('labels.confirm_password')); ?><span
                                                        class="text-danger"> * </span></label>
                                                <input type="password" class="form-control" name="confirm_password"
                                                    value="<?php echo e(old('confirm_password')); ?>"
                                                    placeholder="<?php echo e(trans('labels.confirm_password')); ?>" required>
                                                <?php $__errorArgs = ['confirm_password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <span class="text-danger"><?php echo e($message); ?></span>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="form-group text-end">
                                                <button class="btn btn-secondary"
                                                    <?php if(env('Environment') == 'sendbox'): ?> type="button" onclick="myFunction()" <?php else: ?> type="submit" <?php endif; ?>><?php echo e(trans('labels.save')); ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="seo">
                    <div class="row mb-5">
                        <div class="col-12">
                            <div class="card border-0 box-shadow">
                                <div class="card-body">
                                    <div class="d-flex align-items-center mb-3">
                                        <h5 class="text-uppercase"><?php echo e(trans('labels.seo')); ?></h5>
                                    </div>
                                    <form action="<?php echo e(URL::to('admin/settings/updateseo')); ?>" method="POST"
                                        enctype="multipart/form-data">
                                        <?php echo csrf_field(); ?>
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="form-label"><?php echo e(trans('labels.meta_title')); ?><span
                                                        class="text-danger"> * </span></label>
                                                <input type="text" class="form-control" name="meta_title"
                                                    value="<?php echo e(@$settingdata->meta_title); ?>"
                                                    placeholder="<?php echo e(trans('labels.meta_title')); ?>" required>
                                                <?php $__errorArgs = ['meta_title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <span class="text-danger"><?php echo e($message); ?></span>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label"><?php echo e(trans('labels.meta_description')); ?><span
                                                        class="text-danger"> * </span></label>
                                                <textarea class="form-control" name="meta_description" rows="3"
                                                    placeholder="<?php echo e(trans('labels.meta_description')); ?>" required><?php echo e(@$settingdata->meta_description); ?></textarea>
                                                <?php $__errorArgs = ['meta_description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <span class="text-danger"><?php echo e($message); ?></span>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label"><?php echo e(trans('labels.og_image')); ?> (1200 x
                                                    650) <span class="text-danger"> * </span></label>
                                                <input type="file" class="form-control" name="og_image">
                                                <?php $__errorArgs = ['og_image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <span class="text-danger"><?php echo e($message); ?></span> <br>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                <img class="img-fluid rounded hw-70 mt-1 object-fit-cover"
                                                    src="<?php echo e(helper::image_Path(@$settingdata->og_image)); ?>"
                                                    alt="">
                                            </div>
                                            <div class="form-group text-end">
                                                <button class="btn btn-secondary"
                                                    <?php if(env('Environment') == 'sendbox'): ?> type="button" onclick="myFunction()" <?php else: ?> type="submit" <?php endif; ?>><?php echo e(trans('labels.save')); ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(Auth::user()->type == 2): ?>
                    <?php if(App\Models\SystemAddons::where('unique_identifier', 'whatsapp_message')->first() != null &&
                            App\Models\SystemAddons::where('unique_identifier', 'whatsapp_message')->first()->activated == 1): ?>
                        <div id="whatsappmessagesettings">
                            <div class="row mb-5">
                                <div class="col-12">
                                    <div class="card border-0 box-shadow">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center mb-3">
                                                <h5 class="text-uppercase">
                                                    <?php echo e(trans('labels.whatsapp_message_settings')); ?>

                                                </h5>
                                            </div>
                                            <form method="POST"
                                                action="<?php echo e(URL::to('admin/settings/whatsapp_update')); ?>"
                                                enctype="multipart/form-data">
                                                <?php echo csrf_field(); ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label
                                                                class="form-label fw-bold"><?php echo e(trans('labels.order_variable')); ?>

                                                            </label>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <ul class="list-group list-group-flush">
                                                                        <li class="list-group-item px-0">Order No :
                                                                            <code>{order_no}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Payment type :
                                                                            <code>{payment_type}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Subtotal :
                                                                            <code>{sub_total}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Total Tax :
                                                                            <code>{total_tax}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Delivery
                                                                            charge : <code>{delivery_charge}</code></li>
                                                                        <li class="list-group-item px-0">Discount
                                                                            amount : <code>{discount_amount}</code></li>
                                                                        <li class="list-group-item px-0">Grand total :
                                                                            <code>{grand_total}</code>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <ul class="list-group list-group-flush">
                                                                        <li class="list-group-item px-0">Customer name
                                                                            : <code>{customer_name}</code></li>
                                                                        <li class="list-group-item px-0">Customer
                                                                            mobile : <code>{customer_mobile}</code></li>
                                                                        <li class="list-group-item px-0">Address :
                                                                            <code>{address}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Building :
                                                                            <code>{building}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Landmark :
                                                                            <code>{landmark}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Postal code :
                                                                            <code>{postal_code}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Delivery type
                                                                            : <code>{delivery_type}</code></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <ul class="list-group list-group-flush">
                                                                        <li class="list-group-item px-0">Notes :
                                                                            <code>{notes}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Item Variable
                                                                            : <code>{item_variable}</code></li>
                                                                        <li class="list-group-item px-0">Time :
                                                                            <code>{time}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Date :
                                                                            <code>{date}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Store name :
                                                                            <code>{store_name}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Store URL :
                                                                            <code>{store_url}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Track order
                                                                            URL : <code>{track_order_url}</code></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label
                                                                class="form-label fw-bold"><?php echo e(trans('labels.item_variable')); ?>

                                                            </label>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <ul class="list-group list-group-flush">
                                                                        <li class="list-group-item px-0">Item name :
                                                                            <code>{item_name}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">QTY :
                                                                            <code>{qty}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Variants :
                                                                            <code>{variantsdata}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">Item price :
                                                                            <code>{item_price}</code>
                                                                        </li>
                                                                        <li class="list-group-item px-0">
                                                                            <input type="text" name="item_message"
                                                                                class="form-control"
                                                                                placeholder="<?php echo e(trans('labels.item_message')); ?>"
                                                                                value="<?php echo e(@$settingdata->item_message); ?>">
                                                                            <?php $__errorArgs = ['item_message'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                                                <span class="text-danger"
                                                                                    id="timezone_error"><?php echo e($message); ?></span>
                                                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label
                                                                class="form-label fw-bold"><?php echo e(trans('labels.whatsapp_message')); ?>

                                                                <span class="text-danger"> * </span> </label>
                                                            <textarea class="form-control" required="required" name="whatsapp_message" cols="50" rows="10"><?php echo e(@$settingdata->whatsapp_message); ?></textarea>
                                                            <?php $__errorArgs = ['whatsapp_message'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                                <span class="text-danger"><?php echo e($message); ?></span>
                                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-label"><?php echo e(trans('labels.contact')); ?><span
                                                                    class="text-danger"> * </span></label>
                                                            <input type="text" class="form-control numbers_only"
                                                                name="contact" value="<?php echo e(@$settingdata->contact); ?>"
                                                                placeholder="<?php echo e(trans('labels.contact')); ?>" required>
                                                            <?php $__errorArgs = ['contact'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                                <small class="text-danger"><?php echo e($message); ?></small>
                                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group text-end">
                                                        <button class="btn btn-secondary"
                                                            <?php if(env('Environment') == 'sendbox'): ?> type="button" onclick="myFunction()" <?php else: ?> type="submit" <?php endif; ?>><?php echo e(trans('labels.save')); ?></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if(Auth::user()->type == 1): ?>
                    <?php if(App\Models\SystemAddons::where('unique_identifier', 'custom_domain')->first() != null &&
                            App\Models\SystemAddons::where('unique_identifier', 'custom_domain')->first()->activated): ?>
                        <div id="custom_domain">
                            <div class="row mb-5">
                                <div class="col-12">
                                    <div class="card border-0 box-shadow">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center mb-3">
                                                <h5 class="text-uppercase"><?php echo e(trans('labels.custom_domain')); ?></h5>
                                            </div>
                                            <form method="POST" action="<?php echo e(URL::to('admin/settings/updatecustomedomain')); ?>"
                                                enctype="multipart/form-data">
                                                <?php echo csrf_field(); ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label
                                                                class="form-label"><?php echo e(trans('labels.cname_section_title')); ?>

                                                                <span class="text-danger"> * </span> </label>
                                                            <input type="text" class="form-control" name="cname_title"
                                                                required value="<?php echo e(@$settingdata->cname_title); ?>">
                                                            <?php $__errorArgs = ['cname_title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                                <span class="text-danger"><?php echo e($message); ?></span>
                                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label
                                                                class="form-label"><?php echo e(trans('labels.cname_section_text')); ?>

                                                                <span class="text-danger"> * </span> </label>
                                                            <textarea class="form-control" rows="3" id="cname_text" required name="cname_text"><?php echo e(@$settingdata->cname_text); ?></textarea>
                                                            <?php $__errorArgs = ['cname_text'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                                <span class="text-danger"><?php echo e($message); ?></span>
                                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group text-end">
                                                        <button class="btn btn-secondary"
                                                            <?php if(env('Environment') == 'sendbox'): ?> type="button" onclick="myFunction()" <?php else: ?> type="submit"  <?php endif; ?>><?php echo e(trans('labels.save')); ?></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if(App\Models\SystemAddons::where('unique_identifier', 'google_analytics')->first() != null &&
                            App\Models\SystemAddons::where('unique_identifier', 'google_analytics')->first()->activated): ?>
                        <div id="google_analytics">
                            <div class="row mb-5">
                                <div class="col-12">
                                    <div class="card border-0 box-shadow">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center mb-3">
                                                <h5 class="text-uppercase"><?php echo e(trans('labels.google_analytics')); ?></h5>
                                            </div>
                                            <form method="POST" action="<?php echo e(URL::to('admin/settings/updateanalytics')); ?>"
                                                enctype="multipart/form-data">
                                                <?php echo csrf_field(); ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="form-label"><?php echo e(trans('labels.tracking_id')); ?>

                                                                <span class="text-danger"> * </span> </label>
                                                            <input type="text" class="form-control" name="tracking_id"
                                                                required value="<?php echo e(@$settingdata->tracking_id); ?>">
                                                            <?php $__errorArgs = ['tracking_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                                <span class="text-danger"><?php echo e($message); ?></span>
                                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="form-label"><?php echo e(trans('labels.view_id')); ?> <span
                                                                    class="text-danger"> * </span> </label>
                                                            <input type="text" class="form-control" name="view_id"
                                                                required value="<?php echo e(@$settingdata->view_id); ?>">
                                                            <?php $__errorArgs = ['view_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                                <span class="text-danger"><?php echo e($message); ?></span>
                                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group text-end">
                                                        <button class="btn btn-secondary"
                                                            <?php if(env('Environment') == 'sendbox'): ?> type="button" onclick="myFunction()" <?php else: ?> type="submit" <?php endif; ?>><?php echo e(trans('labels.save')); ?></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div id="landing_page">
                        <div class="row mb-5">
                            <div class="col-12">
                                <div class="card border-0 box-shadow">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center mb-3">
                                            <h5 class="text-uppercase"><?php echo e(trans('labels.landing_page')); ?></h5>
                                        </div>
                                        <form action="<?php echo e(URL::to('/admin/landingsettings')); ?>" method="POST"
                                            enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label class="form-label"><?php echo e(trans('labels.logo')); ?> <small>(250 x
                                                            250)</small></label>
                                                    <input type="file" class="form-control" name="logo">
                                                    <?php $__errorArgs = ['logo'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                        <small class="text-danger"><?php echo e($message); ?></small> <br>
                                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    <img class="img-fluid rounded hw-70 mt-1 object-fit-contain"
                                                        src="<?php echo e(helper::image_path(@$settingdata->logo)); ?>"
                                                        alt="">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label class="form-label"><?php echo e(trans('labels.favicon')); ?> (16 x
                                                        16)</label>
                                                    <input type="file" class="form-control" name="favicon">
                                                    <?php $__errorArgs = ['favicon'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                        <small class="text-danger"><?php echo e($message); ?></small> <br>
                                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    <img class="img-fluid rounded hw-70 mt-1 object-fit-contain"
                                                        src="<?php echo e(helper::image_path(@$settingdata->favicon)); ?>"
                                                        alt="">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label class="form-label"><?php echo e(trans('labels.primary_color')); ?></label>
                                                    <input type="color"
                                                        class="form-control form-control-color w-100 border-0"
                                                        name="landing_primary_color"
                                                        value="<?php echo e($settingdata->primary_color); ?>">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label
                                                        class="form-label"><?php echo e(trans('labels.secondary_color')); ?></label>
                                                    <input type="color"
                                                        class="form-control form-control-color w-100 border-0"
                                                        name="landing_secondary_color"
                                                        value="<?php echo e($settingdata->secondary_color); ?>">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="form-label"><?php echo e(trans('labels.contact_email')); ?><span
                                                            class="text-danger"> * </span></label>
                                                    <input type="email" class="form-control" name="landing_email"
                                                        value="<?php echo e(@$settingdata->email); ?>"
                                                        placeholder="<?php echo e(trans('labels.contact_email')); ?>" required>
                                                    <?php $__errorArgs = ['landing_email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                        <small class="text-danger"><?php echo e($message); ?></small>
                                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="form-label"><?php echo e(trans('labels.contact_mobile')); ?><span
                                                            class="text-danger"> * </span></label>
                                                    <input type="number" class="form-control" name="landing_mobile"
                                                        value="<?php echo e(@$settingdata->contact); ?>"
                                                        placeholder="<?php echo e(trans('labels.contact_mobile')); ?>" required>
                                                    <?php $__errorArgs = ['contact_mobile'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                        <small class="text-danger"><?php echo e($message); ?></small>
                                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label"><?php echo e(trans('labels.address')); ?><span
                                                            class="text-danger"> * </span></label>
                                                    <textarea class="form-control" name="landing_address" rows="3" placeholder="<?php echo e(trans('labels.address')); ?>"><?php echo e(@$settingdata->address); ?></textarea>
                                                    <?php $__errorArgs = ['address'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                        <small class="text-danger"><?php echo e($message); ?></small>
                                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.facebook_link')); ?></label>
                                                        <input type="text" class="form-control"
                                                            name="landing_facebook_link"
                                                            value="<?php echo e(@$settingdata->facebook_link); ?>"
                                                            placeholder="<?php echo e(trans('labels.facebook_link')); ?>">
                                                        <?php $__errorArgs = ['facebook_link'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.twitter_link')); ?></label>
                                                        <input type="text" class="form-control"
                                                            name="landing_twitter_link"
                                                            value="<?php echo e(@$settingdata->twitter_link); ?>"
                                                            placeholder="<?php echo e(trans('labels.twitter_link')); ?>">
                                                        <?php $__errorArgs = ['twitter_link'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.instagram_link')); ?></label>
                                                        <input type="text" class="form-control"
                                                            name="landing_instagram_link"
                                                            value="<?php echo e(@$settingdata->instagram_link); ?>"
                                                            placeholder="<?php echo e(trans('labels.instagram_link')); ?>">
                                                        <?php $__errorArgs = ['instagram_link'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label
                                                            class="form-label"><?php echo e(trans('labels.linkedin_link')); ?></label>
                                                        <input type="text" class="form-control"
                                                            name="landing_linkedin_link"
                                                            value="<?php echo e(@$settingdata->linkedin_link); ?>"
                                                            placeholder="<?php echo e(trans('labels.linkedin_link')); ?>">
                                                        <?php $__errorArgs = ['linkedin_link'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                            <small class="text-danger"><?php echo e($message); ?></small>
                                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-end">
                                                <button
                                                    <?php if(env('Environment') == 'sendbox'): ?> type="button" onclick="myFunction()" <?php else: ?> type="submit" <?php endif; ?>
                                                    class="btn btn-secondary"><?php echo e(trans('labels.save')); ?></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script>
        var cityurl = "<?php echo e(URL::to('admin/getcity')); ?>";
        var select = "<?php echo e(trans('labels.select')); ?>";
        var cityid = "<?php echo e(Auth::user()->city_id != null ? Auth::user()->city_id : '0'); ?>";
    </script>
    <script src="<?php echo e(url('storage/app/public/admin-assets/js/user.js')); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.12.1/ckeditor.js"></script>
    <script src="<?php echo e(url(env('ASSETPATHURL') . 'admin-assets/js/settings.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Pc\Desktop\project\resources\views/admin/otherpages/settings.blade.php ENDPATH**/ ?>