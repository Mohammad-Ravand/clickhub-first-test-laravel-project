<footer>
    <div class="footer-bg-color overflow-hidden">
        <div class="container footer-container">
            <div class="footer-contain row row-cols-md-4">
                <div class="col-md-4 col-lg-4 mt-4 me-auto">
                    <div data-aos="zoom-in">
                        <a href="<?php echo e(URL::to('/')); ?>">
                            <img src="<?php echo e(helper::image_path(helper::appdata('')->logo)); ?>" height="50" alt="">
                        </a>
                        <p class="footer-contain mt-4 col-lg-10">
                            <?php echo e(trans('landing.footer_description')); ?>

                        </p>
                    </div>
                </div>
                <div class="col-md-8 col-lg-8">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-xl-4 footer-contain">
                            <div data-aos="zoom-in">
                                <p class="footer-title mb-2 mt-4"><?php echo e(trans('landing.pages')); ?></p>
                                <p class="py-1"><a href="<?php echo e(URL::to('/aboutus')); ?>"><?php echo e(trans('landing.about_us')); ?></a></p>
                                <p class="py-1"><a href="<?php echo e(URL::to('/privacypolicy')); ?>"><?php echo e(trans('landing.privacy_policy')); ?></a></p>
                                <p class="py-1"><a href="<?php echo e(URL::to('/termscondition')); ?>"><?php echo e(trans('landing.terms_conditions')); ?></a></p>
                                <p class="py-1"><a href="<?php echo e(URL::to('/contact')); ?>"><?php echo e(trans('landing.contact_us')); ?></a></p>

                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-xl-4 footer-contain">
                            <div data-aos="zoom-in">
                                <p class="footer-title mb-2 mt-4"><?php echo e(trans('landing.other')); ?></p>
                                <?php if(App\Models\SystemAddons::where('unique_identifier', 'blog')->first() != null &&
                                        App\Models\SystemAddons::where('unique_identifier', 'blog')->first()->activated == 1): ?>
                                    <p class="py-1"><a href="<?php echo e(URL::to('/blogs')); ?>"><?php echo e(trans('landing.blogs')); ?></a></p>
                                <?php endif; ?>
                                <p><a href="<?php echo e(URL::to('/faqs')); ?>"><?php echo e(trans('landing.faqs')); ?></a></p>
                                <p><a href="<?php echo e(URL::to('/stores')); ?>"><?php echo e(trans('landing.our_stors')); ?></a></p>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-5 col-xl-4 footer-contain">
                            <div data-aos="zoom-in">
                                <p class="footer-title mb-2 mt-4"><?php echo e(trans('landing.help')); ?></p>
                                <p class="py-1"><a href="mailto:<?php echo e(helper::appdata('')->email); ?>"><?php echo e(helper::appdata('')->email); ?></a></p>
                                <p class="py-1"><a href="tel:<?php echo e(helper::appdata('')->contact); ?>"><?php echo e(helper::appdata('')->contact); ?></a></p>
                                <div class="d-md-none d-lg-none d-xl-none d-xll-none">
                                    <div
                                        class="icon-flex col-md-2 pt-2 d-flex align-items-center justify-content-center">
                                        <?php if(helper::appdata('')->facebook_link != null): ?>
                                            <p class="footer-btn">
                                                <button class="border-0 rounded-circle  shadow-lg">
                                                    <a href="<?php echo e(helper::appdata('')->facebook_link); ?>"
                                                        class="icon-name"><i
                                                            class="fa-brands fa-facebook-f fs-6 text-dark"></i></a>
                                                </button>
                                            </p>
                                        <?php endif; ?>
                                        <?php if(helper::appdata('')->instagram_link != null): ?>
                                            <p class="footer-btn">
                                                <button class="border-0 shadow-lg">
                                                    <a href="<?php echo e(helper::appdata('')->instagram_link); ?>"
                                                        class="icon-name"><i
                                                            class="fa-brands fa-instagram text-dark"></i></a>
                                                </button>
                                            </p>
                                        <?php endif; ?>
                                        <?php if(helper::appdata('')->twitter_link != null): ?>
                                            <p class="footer-btn">
                                                <button class="border-0 shadow-lg">
                                                    <a href="<?php echo e(helper::appdata('')->twitter_link); ?>"
                                                        class="icon-name"><i
                                                            class="fa-brands fa-twitter text-dark"></i></a>
                                                </button>
                                            </p>
                                        <?php endif; ?>
                                        <?php if(helper::appdata('')->linkedin_link != null): ?>
                                            <p class="footer-btn">
                                                <button class="border-0 shadow-lg">
                                                    <a href="<?php echo e(helper::appdata('')->linkedin_link); ?>"
                                                        class="icon-name"><i
                                                            class="fa-brands fa-linkedin-in text-dark"></i></a>
                                                </button>
                                            </p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="text-white mt-5">
            <div class="d-flex justify-content-between align-items-center pb-2">
                <h5 class="copy-right-text m-0"><?php echo e(helper::appdata('')->copyright); ?></h5>
                <div class="icon-flex col-md-2 pt-2 d-flex align-items-center justify-content-end footer-icon-bottom">
                    <?php if(helper::appdata('')->facebook_link != null): ?>
                        <p class="footer-btn">
                            <button class="border-0 rounded-circle  shadow-lg">
                                <a href="<?php echo e(helper::appdata('')->facebook_link); ?>" target="_blank" class="icon-name"><i
                                        class="fa-brands fa-facebook-f fs-6 text-dark"></i></a>
                            </button>
                        </p>
                    <?php endif; ?>
                    <?php if(helper::appdata('')->instagram_link != null): ?>
                        <p class="footer-btn">
                            <button class="border-0 shadow-lg">
                                <a href="<?php echo e(helper::appdata('')->instagram_link); ?>" target="_blank" class="icon-name"><i
                                        class="fa-brands fa-instagram text-dark"></i></a>
                            </button>
                        </p>
                    <?php endif; ?>
                    <?php if(helper::appdata('')->twitter_link != null): ?>
                        <p class="footer-btn">
                            <button class="border-0 shadow-lg">
                                <a href="<?php echo e(helper::appdata('')->twitter_link); ?>" target="_blank" class="icon-name"><i
                                        class="fa-brands fa-twitter text-dark"></i></a>
                            </button>
                        </p>
                    <?php endif; ?>
                    <?php if(helper::appdata('')->linkedin_link != null): ?>
                        <p class="footer-btn">
                            <button class="border-0 shadow-lg">
                                <a href="<?php echo e(helper::appdata('')->linkedin_link); ?>" target="_blank" class="icon-name"><i
                                        class="fa-brands fa-linkedin-in text-dark"></i></a>

                            </button>
                        </p>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</footer>
<?php /**PATH C:\Users\Pc\Desktop\project\resources\views/landing/layout/footer.blade.php ENDPATH**/ ?>